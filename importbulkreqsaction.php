<?php 
include_once("top.php");
$requested_date =date($dbase->dateformat);
$requested_time = strtotime($requested_date);
$is_valid_target_date =true;
$cust_appoval_date =true;
if($_FILES && $_POST){
	$reqres = $dbase->getfieldsorder($_POST['batchid']);

	$batchid = $_POST['batchid'];
	$projid = $dbase->executeQuery("SELECT `project_id` FROM `env_batch` WHERE `id`=".$batchid." AND `closed`=0","single")[0];
	if($_SESSION['partlinq_user']['USERTYPE'] == 0){
		$created_by = $_POST['client_admin_id'];
	}else{
		$created_by = $_SESSION['partlinq_user']['ID'];
	}
	if($_SESSION['partlinq_user']['USERTYPE'] == 0 || $_SESSION['partlinq_user']['USERTYPE'] == 3){
		$user_id = $_POST['sFld'.$reqarr['rqu']];
	}else{
		$user_id =$_SESSION['partlinq_user']['ID'];
	}
	$is_cust_date = true;
	if(isset($_FILES['request_file']) && count($reqres) >0 && $projid >0 && $batchid>0){	
		$excelheaderformat = $dbase->executeQuery("SELECT `req_field_label` FROM `env_projectheaders` WHERE `batchid`=".$batchid." AND `hidden_type`=0 AND `req_form_order`>0 AND is_auto_type <>".REQUESTOR." AND itypeid<>11 ORDER BY `req_form_order` ASC","multiple");
		$excelheaderformat1 = array_column($excelheaderformat,"req_field_label");//print_r($excelheaderformat);
		$newarr = array("request_type");
		$excelheaderformat = array_merge($excelheaderformat1,$newarr);
		 $customer_id =0;
		 $model_id =0;
		 $supplier_id =0;
		 $filepath = $docroot."upload/bulkrequests/";
		
			$dbase->chmodr($filepath,0777);
            if(!is_dir($filepath.$projid."/")){ mkdir($filepath.$projid."/",0777);
			}else{
				$dbase->chmodr($filepath.$projid."/",0777);  
			}
	
		  $errors= array();
		  $file_name = $_FILES['request_file']['name'];
		  $file_size =$_FILES['request_file']['size'];
		  $file_tmp =$_FILES['request_file']['tmp_name'];
		  $file_type=$_FILES['request_file']['type'];
		  $file_ext=strtolower(end(explode('.',$_FILES['request_file']['name'])));
			$reqfieldindex = $dbase->getreqfieldindex($batchid);
			$requestorid =$_POST['sFld'.$reqfieldindex];
			 $requestorquery = " sFld".$reqfieldindex."=".$requestorid.",";
			 $extensions= array("xls","xlsx");
		  	 $newfilename = $filepath.$projid."/".$file_name;
	
		  if(in_array($file_ext,$extensions)=== false){
			 $errors[]="extension not allowed, please choose a xls or xlsx file.";
			  	echo '{"status":"failure","message":"extension not allowed, please choose a xls or xlsx file","notinserted":""}';
				exit;
		  }
		 
		  if(empty($errors)==true){		 
			 move_uploaded_file($file_tmp,$newfilename);
			try {
				
				$inputFileType = PHPExcel_IOFactory::identify($newfilename);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($newfilename);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($newfilename, PATHINFO_BASENAME). '": '.$e->getMessage());
			}
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objPHPExcel->getActiveSheet()->setShowGridlines(true);
			$timestamps=time();
			$highestRow = $objWorksheet->getHighestRow();
			$highestColumn = $objWorksheet->getHighestColumn(); 
			$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); 
			$i=0;
			$j=0;
			$fileheader = array();
		
		
				for ($col = 0; $col < $highestColumnIndex; ++$col) {
				
					if(trim($objWorksheet->getCellByColumnAndRow($col, 1)->getValue()) !=""){
						$requesterdata = strtolower(str_replace(" ","_",trim($excelheaderformat[$col])));
						  $reqheaderarrformat[]  = $requesterdata;
						 $exceldata =strtolower(str_replace(' ', '_',trim($objWorksheet->getCellByColumnAndRow($col, 1)->getValue())));
						if($requesterdata == $exceldata){
							$fileheader[] = $exceldata;
						}
						
					}
				}
			 $lastcol = count($reqres)-1;
			 $reqtypecol = count($reqheaderarrformat)-1;
		
		 if(count($reqheaderarrformat) !== count($fileheader)){    
			echo '{"status":"failure","message":"Import file header mismatching. Please change it and upload again","notinserted":""}';
				exit();				
		   }
			  $insertstatus = false;
			  $targetdate_errmsg1 =CUSTERRMSG;
			  $targetdate_errmsg2 = "Do you want to import the requests ?";
			  $approval_duedate_errmsg ="Customer Approval Due Date is mandatory";
			  $m=0;
			  $not_inserted ="";
			  $insertquery =[];
			  $items=array();
			  $n=0;
			  	  $not_inserted ="";
			  ////
			  
					$index1=0;
			  		$index =0;
			 		$validationerrarr='"data": [';
			 		$notinsertedarr='"data": [';
				    $validationerrarr1="";
				    $notinsertedarr1="";
			  for($row = 2; $row <= $highestRow; ++$row) {
					if($objWorksheet->getCellByColumnAndRow(0, $row)->getValue() !=""){
				    $reqval="";

					$searchfields="";
					$buildquery ="";
					$m=0;
					$validationerrarr1 ='[';
					$notinsertedarr1 ='[';
     			 
					for($k=0;$k<count($reqres);$k++){
						$compliance_time = "";
						$col="";
						if($reqres[$k]['req_field_label'] != "" && $objWorksheet->getCellByColumnAndRow($m, $row)->getValue()!=''){
							 $col =$dbase->escape(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow($m, $row)->getValue())));
							array_push($newarr,trim($objWorksheet->getCellByColumnAndRow($m, $row)->getValue()));
							if($k>=2){
							$validationerrarr1 .='"'.trim($objWorksheet->getCellByColumnAndRow($m, $row)->getValue()).'",';
							$notinsertedarr1 .='"'.trim($objWorksheet->getCellByColumnAndRow($m, $row)->getValue()).'",';
							}
						}
						
					 if($k == $lastcol){	
							$request_type = $objWorksheet->getCellByColumnAndRow($reqtypecol, $row)->getValue();
							$buildquery .="request_type=".$request_type.",";
							 $validationerrarr1 .='"'.$request_type.'"],';
							 $notinsertedarr1 .='"'.$request_type.'"],';
						 
						}
						
					if($reqres[$k]['is_auto_type'] == REQUESTOR){
							$buildquery .=$requestorquery;
					}else if($reqres[$k]['itypeid'] == 11){
							$buildquery .="part_type=".$_POST['part_type'].",";
					}else{
					
						
						 if($reqres[$k]['is_auto_type'] == CUSTOMER && $col !=""){
								$custcondn ="";
							
								$custarr = explode("(",$col);
								if(count($custarr) == 2){
									$custarr[1] = explode(")",$custarr[1])[0];
									$custcondn = " AND (cd.cust_code='".trim($custarr[1])."' OR cd.cust_name='".trim($custarr[0])."' OR cd.cust_code='".trim($custarr[0])."' OR cd.cust_name='".trim($custarr[1])."')";
								}else if(count($custrr) == 1){
									$custcondn = " AND (cd.cust_code='".$col."' OR cd.cust_name='".$col."'";
								}
							 	$cntres =$dbase->executeQuery(" SELECT count(cd.id)as cnt, cd.id as customer_id 
								FROM `env_customer_details` cd 
								WHERE  cd.`isactive`=1 ".$custcondn." ","single");
							 	$req_alloc_res =$dbase->executeQuery(" SELECT count(cd.id)as cnt, cd.id 
								FROM `env_requestor_alloc` cd 
								WHERE  cd.user_id=".$user_id." AND cd.customer_id=".$cntres['customer_id'] ,"single");
							 if($req_alloc_res['cnt'] == 0){
							 $dbase->query("INSERT INTO  `env_requestor_alloc` (`user_id`,`customer_id`,`created_by`,`created_date`) VALUES('".$user_id."','".$cntres['customer_id']."','".$created_by."','".date("Y-m-d H:i:s")."')");
							 }
							 	$req_alloc_res =$dbase->executeQuery(" SELECT count(cd.id)as cnt, cd.id 
								FROM `env_requestor_alloc` cd 
								WHERE   cd.user_id=".$user_id." AND cd.customer_id=".$cntres['customer_id'] ,"single");
							  $req_alloc_res =$dbase->executeQuery("SELECT COUNT(ra.id) as cnt ,ra.customer_id 
							  FROM `env_requestor_alloc` ra WHERE  ra.`user_id`=".$requestorid." 
							  AND ra.`customer_id`='".$cntres['customer_id']."'","single");
							 
								if($cntres['cnt'] == 1 && $req_alloc_res['cnt'] >0){
									$customer_id =  $cntres['customer_id'];
									if($customer_id >0){
										$buildquery .="sFld".$reqres[$k]['oid']."='".$customer_id."',";
										$searchfields .= " sFld".$reqres[$k]['oid']."='".$customer_id."' AND ";


									}
								}
							
						}else if($reqres[$k]['is_auto_type'] == SUPPLIER_CODE && $col !=""){
							
								$supplierres = $dbase->executeQuery("SELECT COUNT(id)as cnt,id FROM `env_supplier` 
								WHERE `code` ='".$col."'","single");
								if($supplierres['cnt'] == 1){
									$supplier_id = $supplierres['id'];
								}else if($supplierres['cnt'] == 0){
									
									$dbase->executeNonQuery("INSERT INTO  `env_supplier` SET `code`='".$col."' ,`isactive`=1,`created_by`=".$_SESSION['partlinq_user']['ID'].",`created_date`='".date("Y-m-d")."'");
									$supplier_id =$dbase->lastId();
								}
							if($supplier_id >0){
							    $buildquery .="sFld".$reqres[$k]['oid']."='".$supplier_id."',";
								$searchfields .= " sFld".$reqres[$k]['oid']."='".$supplier_id."' AND ";
							}
						
						}else if($reqres[$k]['is_auto_type'] == MODEL_NUMBER && $col !=""){
							
						$modres = $dbase->executeQuery("SELECT COUNT(id)as cnt,id FROM `env_model_details` 
						WHERE `modelnum` ='".$col."'","single");
						
						if($modres['cnt'] == 1){
							$model_id = $modres['id'];
						}else if($modres['cnt'] == 0){
							$dbase->executeNonQuery("INSERT INTO  `env_model_details` SET `modelnum`='".$col."' ,`isactive`=1,`created_by`=".$_SESSION['partlinq_user']['ID'].",`created_date`='".date("Y-m-d")."'");
							$model_id =$dbase->lastId();
						}
						if($model_id >0){
							$buildquery .="sFld".$reqres[$k]['oid']."='".$model_id."',";
							$searchfields .= " sFld".$reqres[$k]['oid']."='".$model_id."' AND ";
						}
						
						}else if($reqres[$k]['hidden_type'] == REQUESTED_DATE){
							  $buildquery .="sFld".$reqres[$k]['oid']."='".$requested_date."',";
					
						}else if($reqres[$k]['itypeid'] == CUST_PARTNUM_TYPE){
							$buildquery .="sFld".$reqres[$k]['oid']."='".$col."',";
							$searchfields .= " sFld".$reqres[$k]['oid']."='".$col."' AND ";
						
						}else if($reqres[$k]['hidden_type'] == STATUS){
							$buildquery .="sFld".$reqres[$k]['oid']."='".$reqcreatorarr[0]."',";
						
						}else if($reqres[$k]['hidden_type'] == PASA_STATUS){
							$buildquery .="sFld".$reqres[$k]['oid']."='".$reqcreatorarr[1]."',";
						
						}else if($reqres[$k]['itypeid'] == CUST_DUE_DATE_TYPE){
							
							if($col !="" && $col != "43990" && $col !="1970-01-01"){
								$cust_due_date = date($dbase->dateformat,strtotime(trim($col)));
									 $buildquery .="sFld".$reqres[$k]['oid']."='".date($dbase->dateformat,strtotime(trim($col)))."',";
							}else{
								 $buildquery .="sFld".$reqres[$k]['oid']."='',";
								$cust_due_date ="";
							}
						}else if($reqres[$k]['hidden_type'] == SQA_TARGET_DATE && $cust_due_date !=""){
							$buildquery .="sFld".$reqres[$k]['oid']."='". $dbase->getpreviousdate($cust_due_date,SQA_TARGET_DATE_WEEK,"week")."',";
						
						}else if($reqres[$k]['hidden_type'] == COMPLIANCE_DATE && $cust_due_date !=""){
							 $compliance_time = strtotime($dbase->getpreviousdate($cust_due_date,COMPLIANCE_DATE_WEEK,"month"));
							$buildquery .="sFld".$reqres[$k]['oid']."='".$dbase->getpreviousdate($cust_due_date,COMPLIANCE_DATE_WEEK,"month")."',";
						
						}else if($reqres[$k]['itypeid'] == WEIGHT){
							$buildquery .="sFld".$reqres[$k]['oid']."='".$col."',";
							  if($col >0){
								$reqval =  $col;
								  
							  }else{
								  $reqval =0;
							  }
						}
						
						$m++;
					}
						
						
					}//for
							if($compliance_time < $requested_time && $compliance_time !=""){
								//$validationerrarr1 = $validationerrarr1,0,-1);
							
							}else{
								$validationerrarr1 ='';
							}
				
						$items[$n] = $newarr;
						$n++;
						$buildquery = substr($buildquery,0,-1);
						if($_SESSION['partlinq_user']['USERTYPE']  == 3 && $cust_due_date == ""){
							$is_cust_date =false;
						}
						
						$selquery = "SELECT COUNT(id) as cnt FROM env_urlgrab WHERE ".$searchfields." iPrjID='".$projid."' AND is_delete='0' AND `iBatch`='".$_POST['batchid']."' AND request_type= 1";
			
				$result = $dbase->executeQuery($selquery,"single");
					if($requestorid >0 && $customer_id >0 && $supplier_id >0 && $model_id >0 && $request_type > 0 && 
					   ($reqval !="" ||  $reqval==0 ) &&  $searchfields !="" && $buildquery !=""){
							if($compliance_time < $requested_time){
									$validationerrarr.= $validationerrarr1;
									$is_valid_target_date =false;
								
							}else{
								$validationerrarr1 ="";
							}
					
						if(($result['cnt'] == 0 && $request_type == 1) || $request_type == 2){
							$insertquery[] =" INSERT INTO env_urlgrab SET `iPrjID`=".$_POST['batchid'].",`iBatch`=".$_POST['batchid'].",`userid`=0,`created_by`='".$created_by."',`sFld0`='".$sFld0."',".$buildquery;
						}
			 	
					}else{
					//$notinsertedarr .= $notinsertedarr1;
					}
						if($result['cnt'] >=0 && $request_type != 2){
							$notinsertedarr .= $notinsertedarr1;
							$not_inserted =1;
						}
							////
					}
			  }
			  ////
		
			 // echo $notinsertedarr;exit;
			 // print_r($insertquery);
			  if(($is_valid_target_date && $cust_appoval_date) || (!$is_valid_target_date && $_POST['is_submit'] == 1 && $cust_appoval_date))
				  for($s=0;$s<count($insertquery);$s++){
					  {
						
					  if(!$dbase->executeNonQuery($insertquery[$s])){
						   $notinsertedarr .= $notinsertedarr1;
							$not_inserted .= $row.",";
						  
						
						}else{
						  $notinsertedarr1 ="";
							$insertstatus = true;
						}
				  }
			  }else{
				  $validationerrarr = substr($validationerrarr,0,-1);
				if(!$is_valid_target_date && !$cust_appoval_date){
					$msg = $targetdate_errmsg1.",".$approval_duedate_errmsg.".<br/>".$targetdate_errmsg2;
					echo '{"status":"failure","errtype":"2","message":"'.$msg.'","notinserted":"",'.$validationerrarr.']}';	
				}else if(!$is_valid_target_date && $cust_appoval_date){
					$msg = $targetdate_errmsg1.".<br/>".$targetdate_errmsg2;
					echo '{"status":"failure","errtype":"2","message":"'.$msg.'","notinserted":"",'.$validationerrarr.']}';
				}else if($is_valid_target_date && !$cust_appoval_date){
					echo '{"status":"failure","errtype":"3","message":"'.$msg.'","notinserted":"",'.substr($validationerrarr,0,-1).']}';
				}
					   exit;
			  }
			 // if(count($insertquery) == 0){
				 // $notinsertedarr .= $notinsertedarr1;
			 // }
			// print_r($insertquery);
			// echo $notinsertedarr."----";exit;
			  if($_POST['is_submit'] == 1){
				// echo $notinsertedarr."---";
			  	 if($not_inserted !=""){
					$notinsertedarr = substr($notinsertedarr,0,-1)."]";
						echo '{"status":"failure","message":"Duplicate records are not allowed for new request <br/>.Please check not inserted records :","notinserted":"1",'.$notinsertedarr.'}';
					  
					}else{
						echo '{"status":"success","message":"Request file imported successfully ","notinserted":"","errtype":"","data":""}';
					}
			  }
				
		
	   }
	}else{
		echo '{"status":"failure","message":"No file is added for import. Please upload a file""notinserted":"","errtype":"","data":""}';
		exit();	   
	   } 
}else{
	echo'{"status":"failure","message":"Please fill mandatory fields","errtype":"","data":""}';
}	?>


