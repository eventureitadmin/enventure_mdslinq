<?php
	include_once("header.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{?>
<link href="css/jqgrid.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquerymigrate.js"></script>
<script type="text/javascript" src="js/jqgrid/locale_en.js"></script>
<script type="text/javascript" src="js/jqgrid/jqgrid.js"></script>

    <body>    
<div class="container-fluid" >
<br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group" >
										<?php if($_SESSION['partlinq_user']['USERTYPE'] == 1 || $_SESSION['partlinq_user']['USERTYPE'] == 2 || $_SESSION['partlinq_user']['USERTYPE'] == 3 || $_SESSION['partlinq_user']['USERTYPE'] == 6 ){?>
										<input type="button" class="btn btn-primary btn-xs " name="save<?php echo $tbindex;?>" id="save<?php echo $tbindex;?>" value="Save"/>
										<?php if($_SESSION['partlinq_user']['USERTYPE'] == 3 || $_SESSION['partlinq_user']['USERTYPE'] == 2){?>
										<input type="button" class="btn btn-danger btn-xs " name="cancel<?php echo $tbindex;?>" id="cancel<?php echo $tbindex;?>" value="Cancel"/>
										<?php }?>
										<?php }?>
										</div>	
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<table id="list"></table>
										<div id="pager"></div>										
									</div>
								</div>									
							</div>
						</div>
					</div>
				</div>
	<!-- /.row -->
</div>
            <!-- /#page-wrapper -->
		<script type="text/javascript">
		$().ready(function() {
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var target = $(e.target).attr("href");
		  if(target == "#newlist"){			
            'use strict';
            var initDateEdit = function (elem) {
                    setTimeout(function () {
                        $(elem).datepicker({
                            dateFormat: 'dd-M-yy',
                            autoSize: true,
                            //showOn: 'button', // it dosn't work in searching dialog
                            changeYear: true,
                            changeMonth: true,
                            showButtonPanel: true,
                            showWeek: true
                        });
                    }, 100);
                },
                initDateSearch = function (elem) {
                    setTimeout(function () {
                        $(elem).datepicker({
                            dateFormat: 'dd-M-yy',
                            autoSize: true,
                            changeYear: true,
                            changeMonth: true,
                            showWeek: true,
                            showButtonPanel: true
                        });
                    }, 100);
                },
                numberTemplate = {formatter: 'number', align: 'right', sorttype: 'number', editable: true,
                    searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge', 'nu', 'nn', 'in', 'ni'] }
				},
				setprodcolor = function(cellvalue, options, rowObject){
					var str1 = "~0";
					var str2 = "~1";
					if(cellvalue.indexOf(str1) != -1 || cellvalue.indexOf(str2) != -1){
						var cellvalueArr = cellvalue.split("~");
						if(cellvalueArr[1]=='0'){
							return '<span style="font-weight:bold;color: red; display: block; width: 100%; height: 100%;">' + cellvalueArr[0] + '</span>';
						}
						else{
							return cellvalueArr[0];
						}						
					}
					else{
						return cellvalue;
					}
				},
				setqccolor = function(cellvalue, options, rowObject){
					var str1 = "~0";
					var str2 = "~1";
					if(cellvalue.indexOf(str1) != -1 || cellvalue.indexOf(str2) != -1){
						var cellvalueArr = cellvalue.split("~");
						if(cellvalueArr[1]=='0'){
							return '<span style="font-weight:bold;color: red; display: block; width: 100%; height: 100%;">' + cellvalueArr[0] + '</span>';
						}
						else{
							return cellvalueArr[0];
						}						
					}
					else{
						return cellvalue;
					}
				},
				statusTemplate = {
        			formatter: "checkbox", align: "center", width: 20,
        			stype: "select", searchoptions: {searchhidden: true, sopt: ["eq"], value: "'':Select;1:Completed;0:Not Completed" }
    			},				
                grid = $("#list");

            grid.jqGrid({
				url:'requestdata.php?batch='+batch+'&pid='+pid,
                datatype: "json",
				mtype: "POST",
                colNames: [
							'ID',
							<?php 
								$headerlist = $dbase->getprojectheaders($_GET["batch"]);
								if(count($headerlist)>0){
									for($i=1;$i<count($headerlist);$i++){
										echo "'".$headerlist[$i]['headername']."',";
									}
								}								
							?>
						  ],
                colModel: [
				{name:'ID',index:'ID',align:'left',hidden:true,editable:true,width:100},							
				<?php
								if(count($headerlist)>0){
									for($i=1;$i<count($headerlist);$i++){
										if(($headerlist[$i]['orderid']-1)==0){
											echo "{name:'sFld".($headerlist[$i]['orderid']-1)."',index:'sFld".($headerlist[$i]['orderid']-1)."',align:'left',editable:true,width:100,searchoptions: { sopt: ['eq','in']}},";	
										}
										else{										
											echo "{name:'sFld".($headerlist[$i]['orderid']-1)."',index:'sFld".($headerlist[$i]['orderid']-1)."',align:'left',editable:true,width:150,searchoptions: { sopt: ['eq', 'ne', 'bw', 'bn', 'ew', 'en', 'cn', 'nc']}},";
										}
									}
								}	
							?>				
                ],
                rowNum: 1000,
              	//rowList: ['1000', '2000', '3000', '4000'],
				loadonce: false,
                pager: '#pager',
                gridview: true,
                rownumbers: true,
                sortname: 'id',
                viewrecords: true,
				showlimit : true,
				maxlimit : 1000,
                sortorder: 'asc',
                caption: 'Request List',
				multiselect: true,
				autoencode: true,
                height: '400px',
				autowidth:true, 
				shrinkToFit:false,
				loadComplete: function() {
					var ids = grid.jqGrid('getDataIDs');
					for(var i=0; i<=ids.length; i++) {
						var rowId = ids[i];
						var rowData = grid.jqGrid('getRowData',rowId);						
					}
				},					
				jsonReader : { repeatitems: false } ,
            }).jqGrid('navGrid', '#pager', {add: false, edit: false, del: false}, {}, {}, {},
                {
                    multipleSearch: true,
					multipleGroup:true,
					showQuery: false,
					closeOnEscape:true,
                    overlay: 0,
					beforeShowSearch: function() {
						$('#searchmodfbox_list').css('width','650');
						return true;
					},					
                    onSearch: function () {
                        var i, l, rules, rule, $grid = $('#list'),
                            postData = $grid.jqGrid('getGridParam', 'postData'),
                            filters = $.parseJSON(postData.filters);
                    }});
			grid.navButtonAdd('#pager', {
					caption: "Export to Excel",
					buttonicon: "ui-icon-disk",
					onClickButton: function () {
						ExportToExcel(grid.jqGrid('getRowData'),"assignparts",grid.jqGrid('getGridParam','colNames'));
					},
					position: "last"
				});	
		  }
		
		});				
		});
		</script>
<?php } ?>