<?php
    include_once("header.php");

	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{ ?>
	<style>
		.yesbtn{
				margin-left: 70px !important;
			}
		.nobtn{
				margin-right: 70px !important;
			}
	</style>

			<div class="container" style="margin-top:40px">
				<div class="row"><div class="alert alert-danger col-md-4 col-md-offset-4" role="alert" id="errormsgid" style="display:none">				</div></div>
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
						<div class="login-panel panel panel-default">
							<div class="panel-heading">
								 <img style="display: block; margin-left: auto; margin-right: auto;max-width: 120px;" src="images/mdslinq.png"/>
								<p style="font-size:24px;font-weight:900;color:#024B9A;text-align:center;letter-spacing:5px;">IMDS Manager</p>
								<h4 align="center">Verification Code</h4>
							</div>
							<?php if($msg!=''){?>
							<div class="alert alert-danger">
								<?php echo $msg; $msg=''; ?> 
							</div>
							<?php } ?>							
							<div class="panel-body">
								<form role="form" id="verifyfrm_login" action="" method="post">
									<fieldset>
										<input type="hidden" name="type" id="type" value="2">
										<div class="form-group">
											<input class="form-control required" placeholder="Verification Code" id="verify_code" name="verify_code" type="text" value="<?php echo $_SESSION['partlinq_user']['VERIFYCODE']?>" readonly/>
											<p class="error" style="color:red;display:none">This field is required</p>
										</div>

										<!-- Change this to a button or input when using this as a form -->
										 <div class="form-group">
										<button type="button" id="verify_btn" class="btn btn-primary btn-lg btn-block">Continue</button>
										<button type="button" id="cancel_btn" class="btn btn-danger btn-lg btn-block" 
												onclick="window.location.href='cancelrequest.php'">Cancel</button>
									
											 </div>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
				</div>


			</div>
	
	<script>
			$(document).ready(function(){

			var isvaliddata = false;
			$("#verify_btn").click(function(){

							$.ajax({
							type:"POST",
							url:"checkemailaddress.php",
							data:$("#verifyfrm_login").serialize(),
							dataType:"json",
							success: function(response){
								var obj = response;
								if(obj.status=='success'){
										//$.notify(obj.message,"success");
									if(obj.message == ""){
										$(".error").css("display","none");
										window.location.href="resetpassword.php";
									}else{
									$(".error").html(obj.message );
										$(".error").css("display","block");
									}

								}
								else{
								$.notify(obj.message,"error");
								}
							}
						});

		});		});	
	</script>

<?php }  ?>
