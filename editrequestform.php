<?php

include_once("top.php");
$due_date="";
if(isset($_POST)){
	$reqres = $dbase->getfieldsorder($_POST['batch']);
	$result =$dbase->getrequestdetails($_POST['batch'],$_POST['proj'],$_POST['editid']);
	$custfieldindex = $dbase->getcustfieldindex($_POST['batch']);
	$selcustid ="";
	$request_date = $result["sFld".($reqarr['rd'])];
	if(strtotime($_POST['custduedate']) < strtotime(date($dbase->dateformat)) && $_POST['is_readonly'] ==1){
		$readonly ="readonly";
		 $disabled ="disabled";
	}else{
		$readonly ="";
		$disabled ="";
	}
	
}	
?>
	<script type="text/javascript" src="js/pricecustomfunctions.js"></script>
	<script type="text/javascript" src="js/requestform.js"></script>
	<style>
		.ui-autocomplete { height: 110px; width:100px;overflow-y: scroll; overflow-x: hidden;}
		.ui-autocomplete {
			z-index: 9999 !important;
		}
	</style>
<div class="container-fluid" >
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <form role="form" id="editfrm_imdsrequest" name="editfrm_imdsrequest" action="" method="post">
										
										<!---------------->
										
										<div class="col-lg-4" id="editrequest-container1">
											<input type="hidden" name="batch" id="batch" value="<?php echo $_POST['batch']; ?>">
											<input type="hidden" name="editid" id="editid" value="<?php echo $_POST['editid']; ?>">
											<input type="hidden" name="isreadonly" id="isreadonly" value="<?php echo $readonly; ?>">
											
								<?php if(count($reqres) >0){
										foreach($reqres as $key=>$val){
											
												if($val['is_auto_type'] ==4){
														$username = $result["sFld".($val['oid'])];
														$userid = $result["sFldid".($val['oid'])];
														
														if($_SESSION['partlinq_user']['USERTYPE']=='3' || $_SESSION['partlinq_user']['USERTYPE']=='0'){
															if($readonly ==""){
																 $readonly ="readonly";
															}
														}else{
														 	if($readonly == ""){
																$readonly ="";
															}
														}
												}else if($disabled == ""){
													$readonly ="";
												}
										 	if($key == 4){?>
														</div>
														<div class="col-lg-1"></div>
														<div class="col-lg-4" id="editrequest-container2">
											<?php }?>	
											<?php if($val['is_auto'] == 1){?>	
												<div class="form-group">
											
										
											<?php if($val['is_auto_type'] == 4){
												
												?>
														<label><?php echo $val['req_field_label'];?> <span style="color:red;">*</span><label for="sFld<?php echo $val['oid']?>name" style="display:none;padding-left:10px;" class="error">This field is required.</label></label>
													<input type="text" name="sFld<?php echo $val['oid']?>name" id="sFld<?php echo $val['oid']?>name" class="form-control required mandatory" value="<?php echo $username;?>"  <?php echo $readonly;?>/>
												 <input type="hidden" name="sFld<?php echo $val['oid']?>" id="sFld<?php echo $val['oid']?>" class="form-control " value="<?php echo $userid;?>" onchange="showcustomerdata('<?php echo $_POST['proj']?>','<?php echo $_GET['batchid']?>','<?php echo $custfieldindex?>','<?php echo $val['oid']?>',0)" <?php echo $readonly;?>/>
														
										<?php }else if($val['is_auto_type'] == 1){?>
														<label><?php echo $val['req_field_label'];?> <span style="color:red;">*</span><label for="sFld<?php echo $val['oid']?>" style="display:none;padding-left:10px;" class="error">This field is required.</label></label>
										
									
											<?php   $selcustid =$result["sFldid".($val['oid'])];
												
													?>
												<select id="sFld<?php echo $val['oid']?>" name="sFld<?php echo $val['oid']?>" class="required mandatory" style="width:250px"  <?php echo $disabled;?>></select>
												
												<?php }else{?>
												<label><?php echo $val['req_field_label'];?> <span style="color:red;">*</span><label for="sFld<?php echo $val['oid']?>name" style="display:none;padding-left:10px;" class="error">This field is required.</label></label>
												     <input type="text" name="sFld<?php echo $val['oid']?>name" id="sFld<?php echo $val['oid']?>name" class="form-control required mandatory" value="<?php echo $result["sFld".($val['oid'])]; ?>"  <?php echo $readonly;?>/>
												 <input type="hidden" name="sFld<?php echo $val['oid']?>" id="sFld<?php echo $val['oid']?>" class="form-control " value="<?php echo $result["sFldid".($val['oid'])]; ?>"  />
   
												<?php }?>
												 
                                            </div>
											<?php }elseif($val['itypeid'] == 3){ ?>
												<div>
															<label><?php echo $val['req_field_label'];?> 
										<span style="color:red;">*</span><label for="sFld<?php echo $val['oid']?>" style="display:none;padding-left:10px;" class="error">This field is required.</label></label></div>
                                              			 <div class="form-group"><input type="text" name="sFld<?php echo $val['oid']?>" id="sFld<?php echo $val['oid']?>" value="<?php echo $result["sFld".($val['oid'])]; ?>" class="form-control required mandatory"  autocomplete="off" <?php echo $readonly;?>/></div>
										
										<?php } elseif($val['fieldtype'] == 6){ ?>
										
													 <div>
                                                <label><?php echo $val['req_field_label'];?> <span style="color:red;">*</span><label for="sFld<?php echo $val['oid']?>" style="display:none;padding-left:10px;" class="error">This field is required.</label></label>
                                            </div>	
                                            <div class="form-group input-group">
                                                <input type="text" name="sFld<?php echo $val['oid']?>" id="sFld<?php echo $val['oid']?>"  value="<?php echo $result["sFld".($val['oid'])]; ?>" class="form-control required mandatory" autocomplete="off" <?php echo $readonly;?>/>
												<span class="input-group-addon">grams</span>
                                            </div>
									
										<?php }elseif($val['fieldtype'] == 5){ 
															$due_date = $result["sFld".($val['oid'])];
															?>
													
								<div><label><?php echo $val['req_field_label'];?> <span style="color:red;">*</span><label for="sFld<?php echo $val['oid']?>" style="display:none;padding-left:10px;" class="error">This field is required.</label></label></div>
                                            <div class="form-group input-group date " data-provide="datepicker">
                                                <input type="text" name="sFld<?php echo $val['oid']?>" id="sFld<?php echo $val['oid']?>" value="<?php echo $result["sFld".($val['oid'])]; ?>" class="form-control required mandatory"  autocomplete="off"  onchange="changecustduedate(this.value,'<?php echo $reqarr['sqadt'];?>','<?php echo $reqarr['cmptdate'];?>');"/>
												<span class="input-group-btn">
													<button class="btn btn-default" type="button">
														<i class="fa fa-calendar"></i>
													</button>
												</span>												
                                            </div>
		
											<div class="custduedateerr" style="color:red;display:none"><?php echo CUSTERRMSG;?><br/></div>

											<?php }else if($val['itypeid'] == 11){?>
										<div class="form-group">
													<label><?php echo $val['req_field_label'];?> 
														<span style="color:red;">*</span>
													<label for="part_type" style="display:none;padding-left:10px;" class="error">This field is required.</label>
													</label><br/>
												<select id="part_type" name="part_type" class=" mandatory required" style="width:250px">
													<option value="">-Select-</option>
													<?php $parttypedetails = $dbase->getparttypedetails();
															  if(count($parttypedetails) >0){
																  for($i=0;$i<count($parttypedetails);$i++){
																	
																	  if($result["part_type"] == $parttypedetails[$i]['id']){
																		  $select = 'selected="selected"';
																	  }else{
																		  $select = '';
																	  }
																echo '<option value="'.$parttypedetails[$i]['id'].'" '.$select.'>'.$parttypedetails[$i]['name'].'</option>';
																  }
															  }
													?>
												</select>
												</div>
											<div class="form-group">
													<label>Request Type 
														<span style="color:red;">*</span>
													<label for="request_type" style="display:none;padding-left:10px;" class="error">This field is required.</label>
													</label><br/>
												<select id="request_type" name="request_type" class="required" style="width:250px">
													<option value="">-Select-</option>
													<?php
										
											$reqtypedetails = $dbase->getreqtypedetails();
															  if(count($reqtypedetails) >0){
																  for($i=0;$i<count($reqtypedetails);$i++){
										if($result['request_type'] == $reqtypedetails[$i]['id']){
											$select = 'selected="selected"';
										  }else{
											  $select = '';
										  }
				echo '<option value="'.$reqtypedetails[$i]['id'].'" '.$select.'>'.$reqtypedetails[$i]['req_name'].'</option>';
																  }
															  }
													?>
												</select>
												</div>
											<?php }elseif($val['fieldtype'] == 3){ ?>
													<div>
												<label><?php echo $val['req_field_label'];?> <span style="color:red;">*</span><label for="sFld<?php echo $result[($val['oid'])]; ?>" style="display:none;padding-left:10px;" class="error">This field is required.</label></label></div>
                                               <div class="form-control"> <input type="text" name="sFld<?php echo $val['oid']?>" id="sFld<?php echo $val['oid']?>" class="form-control required mandatory" value="<?php echo $result["sFld".($val['oid'])]; ?>" readonly <?php echo $readonly;?>/>
                                            </div>
										<?php } elseif($val['hidden_type'] == COMPLIANCE_DATE){?>
											<input type="hidden" name="sFld<?php echo $val['oid']?>" id="sFld<?php echo $val['oid']?>"  value="<?php echo $dbase->getpreviousdate($due_date,COMPLIANCE_DATE_WEEK,"month");?>">
<?php } elseif($val['hidden_type'] == SQA_TARGET_DATE){?>
											<input type="hidden" name="sFld<?php echo $val['oid']?>" id="sFld<?php echo $val['oid']?>"  value="<?php echo $dbase->getpreviousdate($due_date,SQA_TARGET_DATE_WEEK,"week");?>">


													<?php }?>
													<?php }}?>
                                            	
										
										
                                            <button type="button" id="submitbtn" class="btn btn-primary">Save</button>
										</div>
										<!----------------------->
								     </form>
                                    <div class="col-lg-1"></div>
									<div class="col-lg-3" id="addreqform-container1" style="display:none">
										<form role="form" id="frm_imdsmodel" name="frm_imdsmodel" 
										action="" method="post">
										<input type="hidden" name="type" id="type" value="1"/>
										<div class="form-group">
											<label></label><br/>
											<h4>Add Model</h4>
										</div>
									   	<div class="form-group">
                                                <label>Model Number <span style="color:red;">*</span><label for="modelnum" style="display:none;padding-left:10px;" class="error">This field is required.</label></label>
								<input type="text" id="modelnum" name="modelnum" class="form-control required"/>
                                          </div>
											<div class="form-group">
												<label></label><br/>
											<button type="submit" id="savemodel" name="savemodel" 
												class="btn btn-primary" onclick="formsubmit('frm_imdsmodel','addreqform-container1','sFld<?php echo $reqarr['mn']?>')">save</button>
                                            </div>
										</form>
									</div>
									
									<div class="col-lg-3" id="addreqform-container3" style="display:none">
										<form role="form" id="frm_imdssupplier" name="frm_imdssupplier" 
										action="" method="post">
										<input type="hidden" name="type" id="type" value="3">
										<div class="form-group">
											<label></label><br/>
											<h4>Add Supplier </h4>
										</div>
									   	<div class="form-group">
                                                <label>Supplier Code <span style="color:red;">*</span><label for="code" style="display:none;padding-left:10px;" class="error">This field is required.</label></label>
								<input type="text" id="code" name="code" class="form-control required ">
                                            </div>
											<div class="form-group">
												<label></label><br/>
											<button type="submit" id="savesupplier" name="savesupplier" 
												class="btn btn-primary" onclick="formsubmit('frm_imdssupplier','addreqform-container3','sFld<?php echo $reqarr['sc']?>')">save</button>
                                            </div>
										</form>
									</div>
								
                                  </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>		
</div>

<script>
	function validatereq(){
		var isvalidformdata = true;
			$(".mandatory").each(function(index) {
				 var currentElement = $(this);
			
					if(currentElement.val() == ""){
						isvalidformdata =false;
						$(".error").eq(index).show();
				
					}else{
						if(!currentElement.hasClass("alphanumericspace")){
							$(".error").eq(index).hide();
						}else{
							if(/^[a-z\d\._\s]+$/i.test(currentElement.val())){
								$(".error").eq(index).hide();
							}else{
								isvalidformdata =false;
								$(".error").eq(index).html("Please enter only alphabets and numbers ");
								$(".error").eq(index).show();
							}
						}
					}
			
				});
		return isvalidformdata;
	}
	$(document).ready(function(){
		
		jQuery.validator.addMethod("alpha", function(value, element) {
						  return this.optional( element ) || /^[a-zA-Z ]+$/.test( value );
						}, 'Please enter only alphabets.');
		jQuery.validator.addMethod("alphanumeric", function(value, element) {
		  return this.optional( element ) || /^[a-zA-Z0-9]+$/i.test( value );
		}, 'Please enter only alphabets and numbers.');	
		jQuery.validator.addMethod("alphanumericspace", function(value, element) {
		  return this.optional( element ) || /^[a-z.A-Z0-9\s]+$/i.test( value );
		}, 'Please enter only alphabets and numbers.');	
		$("#submitbtn").click(function(){
			var isvalidform1 = validatereq();
				
			if(isvalidform1){
			 			if($("#sFld<?php echo $custfieldindex;?>").val()>0){
						if(gettimeforfield($("#sFld<?php echo $reqarr['cmptdate'];?>").val(),"duedate") < gettimeforfield("<?php echo $request_date;?>","duedate")){
								 requestconfirmmsg($('#editfrm_imdsrequest'),datevalidationmsg,batch,bootboxobj,cfmbtnobj,$("#editid").val());
								 }else{
								 	saverequest($('#editfrm_imdsrequest'),$("#editid").val());
								 }
							
						 }else{
							 $.notify("Please fill mandatory fields","error");
						 }
			}else{
			 $.notify("Please fill mandatory fields","error");
			}


		});
		$('#editfrm_imdsrequest .input-group.date').datepicker({
				   format: 'dd-M-yy',
					todayHighlight: true,
					autoclose: true,
					startDate: '0d',
					clearBtn:true
		});
		if(usertype == 3 || usertype == 2  || usertype == 0){
			showcustomerdata("<?php echo $_POST['proj'];?>","<?php echo $_POST['batch'];?>","<?php echo $reqarr['cn'];?>","<?php echo $selcustid; ?>",$("#sFld<?php echo $reqarr['rqu'];?>").val());
		}
		<?php if($_POST['is_readonly'] == 0){?>
					var is_add =0;
					<?php if($_POST['usertype'] == 2 && $disabled == ""){?>
						
						 getautocompletedata("sFld<?php echo $reqarr['rqu']?>","requester","addreqform-container4",4,1,"<?php echo $reqarr['cn'];?>","<?php echo $_POST['proj'];?>","<?php echo $_POST['batch'];?>",1,"<?php echo $result['sFld'.$custfieldindex];?>");
					<?php }?>
		
						 getautocompletedata("sFld<?php echo $reqarr['mn']?>","modelnum","addreqform-container1",1,1,"<?php echo $reqarr['cn'];?>","<?php echo $_POST['proj'];?>","<?php echo $_POST['batch'];?>",0);
						
						 getautocompletedata("sFld<?php echo $reqarr['sc']?>","code","addreqform-container3",3,1,"<?php echo $reqarr['cn'];?>","<?php echo $_POST['proj'];?>","<?php echo $_POST['batch'];?>",0);
		
			<?php }?>
				 
	});
	</script>
