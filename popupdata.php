
		<!-- Modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			  <div class="modal-header" style="background-color:#286090;color:#fff;padding:6px;">
				<button type="button" class="close" style="color:white; opacity:1;font-size:25px;" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="filterheader"></h4>
			  </div>
			  <div class="modal-body"  id="filterbody" style="height:auto">
				<p>Press <b>space bar</b> to check checkbox from search dropdown</p>
				  <select id="filtersel" name="filtersel" class="filtersel">
					  <option value="">Select</option>
					  <option value="text">Contains</option>
					  <option value="blank">Blank</option>
					  <option value="notblank">Not Blank</option>
					<!---  <option value="multipleenv">Multiple Env ID</option>--->
				 </select>
					<input type="hidden" name="selected_col" id="selected_col" value=""/>
					<input type="text" name="search_text" id="search_text" value=""/><br/><br/>
				  <div id="selectbox">
					  <select id="columndata" name="smasterdata[]" class="selmasterdata" multiple="multiple"> 
					  </select>
				</div>
			</div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-primary" name="search_btn" id="search_btn">Search</button>
			  </div>
		</div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
	
	    <!-----short cut menu popup------------>
			  <!--------Filter pop up------->
		<!-- Modal -->
<div class="modal fade " id="menuModal" class="menuModal" tabindex="-1" role="dialog" aria-labelledby="menuModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
	<div class="modal-content">
			  <div class="modal-header" style="background-color:#286090;color:#fff;padding:6px;">
				<button type="button" class="close" style="color:white; opacity:1;font-size:25px;"   data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="menuModalLabel">Shortcut keys list</h4>
			  </div>
	 		 <div class="modal-body"  id="menubody">
				<table class="table table-bordered" id="menutable" >
					  <thead style="background-color:#ecf0f1;" >
						   <tr>
							  <th scope="col">Keys</th>
							  <th scope="col">Description</th>
							</tr>
					  </thead>
					  <tbody class="table">
						   <tr>
							   <?php $tabindex=1; ?>
							  <td tabindex="<?php echo $tabindex++; ?>">F1</td>
							  <td tabindex="<?php echo $tabindex++; ?>">To click save button press F1</td>
						   </tr>
						   <tr>
							  <td tabindex="<?php echo $tabindex++; ?>">ALT+ E</td>
							  <td tabindex="<?php echo $tabindex++; ?>">Shortcut edit request page</td>
						  </tr>
						  <tr>
							  <td tabindex="<?php echo $tabindex++; ?>">CTRL+ S</td>
							  <td tabindex="<?php echo $tabindex++; ?>">Shortcut keys list popup</td>
						  </tr>
						  <tr>
							  <td tabindex="<?php echo $tabindex++; ?>">F2</td>
							  <td tabindex="<?php echo $tabindex++; ?>">To edit cell press F2</td>
						  </tr>
						   <tr>
							  <td tabindex="<?php echo $tabindex++; ?>">CTRL+G</td>
							  <td tabindex="<?php echo $tabindex++; ?>">To focus on grid view press CTRL+G </td>
						  </tr>
						  
						   <tr>
							  <td tabindex="<?php echo $tabindex++; ?>">ALT+ P </td>
							  <td tabindex="<?php echo $tabindex++; ?>">Show status popup</td>
						  </tr>
						
						   <tr>
							  <td tabindex="<?php echo $tabindex++; ?>">F3</td>
							  <td tabindex="<?php echo $tabindex++; ?>">Column sort by ascending</td>
						  </tr>
						   <tr>
							  <td tabindex="<?php echo $tabindex++; ?>">F4</td>
							  <td tabindex="<?php echo $tabindex++; ?>">Column sort by descending</td>
						  </tr>
						  
					  </tbody>
				 </table>
			</div><!---body-->
		  
	</div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

