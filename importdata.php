<?php
	include_once("header.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{		
	?>
    <body>
		<?php include("menu.php"); ?>
		<link href="../css/jquery_confirm.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="../js/jquery_validate.js"></script>
		<script type="text/javascript" src="../js/jquery_confirm.js"></script>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="">Import Data</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
								<?php include_once("message.php"); ?>
								<?php if($_GET['msg']==1){ echo show_success_msg('Data file imported successfully'); } ?>
								<?php if($_GET['msg']==2){ echo show_error_msg('File upload error'); } ?>
								<?php if($_GET['msg']==3){ echo show_error_msg('No file is added for import. Please upload a file'); } ?>
								<?php if($_GET['msg']==4){ echo show_error_msg('Please select Project.'); } ?>
								<?php if($_GET['msg']==5){ echo show_error_msg('Please select Batch.'); } ?>
								<?php if($_GET['msg']==6){ echo show_error_msg('Data file Updated successfully.'); } ?>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <form role="form" id="frm_dataimport" name="frm_dataimport" action="" method="post" enctype="multipart/form-data">
										<div class="form-group" >
                                                <label>Select Project</label>
                                                <select id="project_id" name="project_id" class="form-control required" onchange="window.location='importdata.php?pid='+this.value" >
                                                    <option value="">-Select-</option>
													<?php 
													$project_query = "SELECT ID,projname FROM env_project WHERE isclosed='0'";
													$project_result = $dbase->executeQuery($project_query,"multiple");
													for($i=0;$i<count($project_result);$i++){
															$selected1 = "";
															if($_GET['pid']==$project_result[$i]['ID']){
																$selected1 = "selected";
															}
															else{
																$selected1 = "";
															}
															echo '<option value="'.$project_result[$i]['ID'].'" '.$selected1 .'>'.$project_result[$i]['projname'].'</option>';
													}
													?>
                                                </select>
                                            </div>											
                                           <div class="form-group" >
                                                <label>Select Batch</label>
                                                <select id="batch_id" name="batch_id" class="form-control required">
                                                    <option value="">-Select-</option>
													<?php
														$batch_query = "SELECT b.`id`, (SELECT p.projname FROM env_project p WHERE p.ID=b.`project_id`) as projname,CONCAT('Batch ','',b.`batchno`) AS batchno FROM `env_batch` b WHERE b.`closed`='0' AND b.project_id='".$_GET['pid']."'";
														$batch_result = $dbase->executeQuery($batch_query,"multiple");
														for($i=0;$i<count($batch_result);$i++){
															if($batch_result[$i]['id']==$batchid){
																$select = 'selected="selected"';
															}
															else{
																$select = '';
															}
															echo '<option value="'.$batch_result[$i]['id'].'" '.$select.'>'.$batch_result[$i]['projname'].' - '.$batch_result[$i]['batchno'].'</option>';
														}
													?>
                                                </select>
                                            </div>
											   <div class="form-group" >
                                                <label>Select User</label>
                                                <select id="user_id" name="user_id" class="form-control required">
                                                    <option value="">-Select-</option>
													<?php
														$user_query = "SELECT id,fullname FROM `env_user` WHERE `usertype`=1 AND isactive=1";
														$user_result = $dbase->executeQuery($user_query,"multiple");
														for($i=0;$i<count($user_result);$i++){
															
															echo '<option value="'.$user_result[$i]['id'].'" >'.$user_result[$i]['fullname'].'</option>';
														}
													?>
                                                </select>
                                            </div>
                                         <div class="form-group">
                                                <label>Data Type</label>
                                                <label class="radio-inline" style="margin-left:10px;">
                                                    <input type="radio" name="datatype" value="1" checked >Fresh Data
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="datatype" value="2"> Existing Data
                                                </label>
                                            </div>											
                                             <div class="form-group">
                                                <label>Upload Data File</label>
                                                <input type="file" id="data_file" name="data_file" class="required">
                                            </div>
                                            <button type="button" class="btn btn-primary" id="btn_dataimport">Submit</button>
                                        </form>
                                    </div>
                                </div>
							
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>	
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->
		<script type="text/javascript">
		$().ready(function() {
			$("#frm_dataimport").validate();
			$("#data_file").change(function () {
				var fileExtension = ["xls", "xlsx"];
				if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
				$.alert({
					title: 'Alert',
					content: 'Only formats are allowed : '+fileExtension.join(', '),
					//icon: 'fa fa-rocket',
					animation: 'scale',
					closeAnimation: 'scale',
					buttons: {
						okay: {
							text: 'Ok',
							btnClass: 'btn-blue'
						}
					}					
				});							
					$("#data_file").val("");
				}
			});	
		$('#btn_dataimport').click(function(){
			$('#frm_dataimport').attr('action', 'importdataaction.php').submit();
		});				
		});
		</script>
	<?php
		 include_once("footer.php");
	}	
?>
