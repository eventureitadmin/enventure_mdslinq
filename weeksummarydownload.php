<?php
include_once("top.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
else{
		if($_GET){
			$project_id = $_GET['pid'];
			$batch_id = $_GET['bid'];
			$reportdate = date('Y-m-d',$_GET['date']);
			if($project_id > 0 && $batch_id > 0 && $reportdate != ''){
			$header = array("Status","Count");
			try {
				$headingStyleArray = array(
					'font'  => array(
						'bold'  => true,
						'color' => array('rgb' => '000000'),
						'size'  => 10
					),
				   'borders' => array(
					  'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				  )
				);		
					$valueStyleArray = array(
					'font'  => array(
						'color' => array('rgb' => '000000'),
						'size'  => 10
					),
				   'borders' => array(
					  'allborders' => array(
					  'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				  ),
				);
					$createExcel = new PHPExcel();
					$createExcel->setActiveSheetIndex(0);
					$r = 1;
					$q=0;
					$createExcel->getActiveSheet()->mergeCells('A'.$r.':B'.$r);
					$createExcel->getActiveSheet()->getStyle('A'.$r)->applyFromArray($headingStyleArray);
					$createExcel->getActiveSheet()->setCellValueByColumnAndRow($q, $r,"Summary Report for ".date('d-M-Y',strtotime($reportdate)));
					$r++;
					$createExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
					$createExcel->getActiveSheet()->getStyle('A'.$r)->applyFromArray($headingStyleArray);
					$createExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
					$createExcel->getActiveSheet()->getStyle('B'.$r)->applyFromArray($headingStyleArray);		
					foreach($header as $key =>$value){
						$createExcel->getActiveSheet()->setCellValueByColumnAndRow($q, $r, ucfirst(str_replace('_', ' ',trim($value))));
						$q++;
					}
					$r++;	
			} catch (Exception $e) {
					  die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME). '": ' . $e->getMessage());
			}			
			$statusfld = $dbase->getNameNew("env_batch","status_headerid","id='".$batch_id."'");
			if($statusfld !='' && $statusfld > 0){
				$sfld = ($statusfld - 1);
			}
			if($sfld!='' && $sfld > 0){
				$sfldcol = "`sFld".$sfld."`";
				$statusquery1 = "SELECT m . * , IFNULL((SELECT COUNT( t1.ID ) AS cnt FROM `env_report` t1 WHERE t1.`iPrjID` = '".$project_id."' AND t1.`iBatch` = '".$batch_id."' AND t1.".$sfldcol." = m.env_status AND reportdate='".$reportdate."' GROUP BY t1.".$sfldcol." ),0) AS cnt FROM ( SELECT s.`statusval` AS env_status FROM `env_status` s WHERE s.`isactive` = '1' AND s.`customer_id` = '".$_SESSION['partlinq_user']['CUSTOMERID']."') m";
			$statusresult1 = $dbase->executeQuery($statusquery1,"multiple");
					if(count($statusresult1) > 0){
						$tot = 0;
						for($i=0;$i<count($statusresult1);$i++){
							$tot += $statusresult1[$i]['cnt'];
							$createExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $r, $statusresult1[$i]['env_status']);
							$createExcel->getActiveSheet()->getStyle("A".$r)->applyFromArray($valueStyleArray);						
							$createExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $r, $statusresult1[$i]['cnt']);
							$createExcel->getActiveSheet()->getStyle("B".$r)->applyFromArray($valueStyleArray);					
							$r++; 							
						}
							$createExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $r, "Total");
							$createExcel->getActiveSheet()->getStyle("A".$r)->applyFromArray($headingStyleArray);						
							$createExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $r, $tot);
							$createExcel->getActiveSheet()->getStyle("B".$r)->applyFromArray($headingStyleArray);							
					}				
			}
			$query7 = "SELECT p.projname, CONCAT( 'Batch_', b.`batchno` ) AS batchname FROM `env_batch` b, env_project p WHERE p.id = b.project_id AND b.id = '".$batch_id."' AND b.project_id = '".$project_id."'";
			$result7 = $dbase->executeQuery($query7,"single");
			$filename=$result7['projname']."_".$result7['batchname']."_summary_".date('dmY').".xlsx";			
			$createExcel->getActiveSheet()->setTitle('Sheet1');
			$objWriter = PHPExcel_IOFactory::createWriter($createExcel, 'Excel2007');
			//$objWriter->save($filepath.$filename);
			ob_end_clean();
			// We'll be outputting an excel file
			header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			// It will be called file.xls
			header('Content-Disposition: attachment; filename="'.$filename.'"');		
			$objWriter->save('php://output');
			exit();				
			}
		}	

}
?>