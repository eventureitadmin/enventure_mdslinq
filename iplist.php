<?php
	include_once("header.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{

?>
    <body>
		<?php include("menu.php"); ?>
<link href="css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<link href="css/dataTables/dataTables.responsive.css" rel="stylesheet">			
            <div id="page-wrapper" style="padding:40px 5px 0 5px">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="">IP List</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
				<?php if(isset($_GET['msg'])){ ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
								<?php if($_GET['msg']==1){ echo show_success_msg('IP blocked successfully'); } ?>
							    <?php if($_GET['msg']==2){ echo show_error_msg('Please select Project.'); } ?>
								 <?php if($_GET['msg']==3){ echo show_error_msg('Data save error.'); } ?>

                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>	
				<?php } ?>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover" id="weeklytable">
                                        <thead>
                                            <tr>
												<th>IP</th>
												<th>Browser</th>
												<th>Date & Time</th>
												<th>Referer Page</th>
                                                <th>Landing page</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
		 							$cond = "WHERE user_logdate='".date('Y-m-d')."'";
		 	 						$ipqry = "SELECT `user_ip`, `user_browser`, DATE_FORMAT(CONCAT(`user_logdate`,' ',`user_logtime`),'%d-%b-%Y %r') AS logdate, `user_refferer`, `user_page` FROM `env_login_iplog` ".$cond." ORDER BY id DESC";
		 							$ipres = $dbase->executeQuery($ipqry,"multiple");
										for($i=0;$i<count($ipres);$i++){
											if (0 == $i % 2) {
												$class = 'class="even"';
											}
											else{
												$class = 'class="odd"';
											}

											echo '<tr '.$class.'>                                              
											    <td>'.$ipres[$i]['user_ip'].'</td>
                                                <td>'.$ipres[$i]['user_browser'].'</td>
												<td>'.$ipres[$i]['logdate'].'</td>
												<td>'.$ipres[$i]['user_refferer'].'</td>
												<td>'.$ipres[$i]['user_page'].'</td>
                                            </tr>';
										}
										?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>				
                </div>				
            </div>
            <!-- /#page-wrapper -->
<script src="js/dataTables/jquery.dataTables.min.js"></script>
<script src="js/dataTables/dataTables.bootstrap.min.js"></script>		
		<script type="text/javascript">
		$(document).ready(function() {
                $('#weeklytable').DataTable({
                        responsive: true,
					   'aoColumnDefs': [{
							'bSortable': false,
							'aTargets': ['nosort']
						}]						
                });			
		});
		</script>
	<?php
		 include_once("footer.php");
	}	
?>
