<?php
	include_once("top.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{
		if($_FILES && $_POST){
			$batchid = $_POST['batch'];
			$pid = $_POST['pid'];
			
		if(isset($_FILES['unrpt_file'])){	//echo "ss";	
		  $filepath = $docroot."/upload/unreportedparts/";
		  $errors= array();
		  $file_name = $_FILES['unrpt_file']['name'];
		  $file_size =$_FILES['unrpt_file']['size'];
		  $file_tmp =$_FILES['unrpt_file']['tmp_name'];
		  $file_type=$_FILES['unrpt_file']['type'];
		  $file_ext=strtolower(end(explode('.',$_FILES['unrpt_file']['name'])));
		
		  $extensions= array("xls","xlsx");
		  
		  if(in_array($file_ext,$extensions)=== false){
			 $errors[]="extension not allowed, please choose a xls or xlsx file.";
			  	echo '{"status":"success","msg":"Extension not allowed, please choose a xls or xlsx file."}';
			  	exit;
		  }
		  
		  if(empty($errors)==true){		  
			 move_uploaded_file($file_tmp,$filepath.$file_name);
			try {
				$inputFileName = $filepath.$file_name;
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME). '": '.$e->getMessage());
			}
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objPHPExcel->getActiveSheet()->setShowGridlines(true);
			$timestamps=time();
			$highestRow = $objWorksheet->getHighestRow();
			$highestColumn = $objWorksheet->getHighestColumn(); 
			$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); 
			$i=0;
			$j=0;
			$fileheader = array();
			for ($col = 0; $col < $highestColumnIndex; ++$col) {
				if(trim($objWorksheet->getCellByColumnAndRow($col, 1)->getValue()) !=""){
				$fileheader[] = strtolower(str_replace(' ', '_',trim($objWorksheet->getCellByColumnAndRow($col, 1)->getValue())));
				}
			}
			//echo"<prre>";print_r($unrpt_header);
			//echo"<prre>";print_r($fileheader);
			if($unrpt_header !== $fileheader){
			echo '{"status":"success","msg":"Import file header mismatching. Please change it and upload again."}';
				exit();				
		  }	
			  $error_msg ="";
			
			for ($row = 2; $row <= $highestRow; $row++) {
			
				if($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()!=''){
					$data = array();
				
					$data['sFld0']=$dbase->escape(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue())));
				$data['sFld1']=$dbase->escape(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue())));
			 	$data['sFld2']=$dbase->escape(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue())));
				$data['sFld3']=$dbase->escape(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue())));
				$data['sFld4']=$dbase->escape(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue())));
				$data['sFld5']=$dbase->escape(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue())));
				$data['sFld6']=$dbase->escape(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue())));
				$data['sFld7']=$dbase->escape(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue())));
				$data['sFld8']=$dbase->escape(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue())));
				$i++;
				
					$status = $dbase->saveunreportedparts1($pid,$batchid,$data);	
					if(!$status){
						$error_msg .= $data['part_num'].",";
						
					}        
				}
			}	
			  if($error_msg == ""){
			 	 echo '{"status":"success","msg":"File has been imported successfully"}';
			  }else{
				 $error_msg = substr($error_msg,0,-1);
				   echo '{"status":"success","msg":"Not Inserted"'.$error_msg.'}';
			
			  }
			exit();
		  }
		  else{
			  	echo '{"status":"success","msg":"File upload error"}';
				exit();			 
		  }
	   }
	   else{
		   	echo '{"status":"success","msg":"No file is added for import. Please upload a file"}';
		exit();	   
	   }   
//}		
	?>

	<?php
	
	}
		
	}
?>
