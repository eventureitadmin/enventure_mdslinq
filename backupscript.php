<?php 
define('DB_USERNAME','root');

define('DB_PASSWORD','AdminEnv@874');

define('DB_NAME','mdslinq');


set_time_limit(0);

ini_set('max_execution_time', 0);

ini_set('memory_limit','-1');

$sourcefilepath = "/home/mdslinq/public_html/";

$destinationfilepath = "/home/mdslinq/public_html/backup/";

$backupdir = date("D");

$backupdir = strtolower($backupdir);

if (!is_dir($destinationfilepath.$backupdir)) mkdir($destinationfilepath.$backupdir, 0777,TRUE) ;

if (!is_writable($destinationfilepath.$backupdir)) chmod($destinationfilepath.$backupdir, 0777);

$handle=opendir($destinationfilepath.$backupdir);

while (($file = readdir($handle))!==false) {

  @unlink($destinationfilepath.$backupdir.'/'.$file);

}
closedir($handle); 

$filesbakupname = "files_backup_".date('dmY').".zip";

$databasebakupname = "database_backup_".date('dmY').".gz";

$combinefilename = date('dmY')."_mdslinq_files_db_backup.zip";

$exclude_folders = array('backup');

$exclude_files = array('');


$zip = new ZipArchive();

$zip->open($destinationfilepath.$backupdir.'/'.$filesbakupname, ZipArchive::CREATE);

addFolderToZip($sourcefilepath,$zip,'',$exclude_folders,$exclude_files);

$zip->close(); 

//backupfiles($sourcefilepath,$destinationfilepath,$backupdir,$filesbakupname);

backupdatabase($destinationfilepath,$backupdir,$databasebakupname);

combinefiles($destinationfilepath,$backupdir,$combinefilename);


//function backupfiles($sourcefilepath,$destinationfilepath,$backupdir,$filesbakupname){
//    system("zip -r ".$destinationfilepath.$backupdir."/".$filesbakupname." ".$sourcefilepath." -x '*/backup/*'");
//}


function addFolderToZip($dir, $zipArchive, $zipdir = '',$exclude_folders,$exclude_files){
    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {

            //Add the directory
            if(!empty($zipdir)) $zipArchive->addEmptyDir($zipdir);
          
            // Loop through all the files
            while (($file = readdir($dh)) !== false) {
          
                //If it's a folder, run the function again!
                if(!is_file($dir . $file)){
                    // Skip parent and root directories
                    if( ($file !== ".") && ($file !== "..")){
		      if((!in_array(basename($file),$exclude_folders))){
                        addFolderToZip($dir . $file . "/", $zipArchive, $zipdir . $file . "/",$exclude_folders,$exclude_files);
                        }
                    }
                  
                }else{
                    // Add the files
                    if(!in_array(basename($file),$exclude_files)){
		      $zipArchive->addFile($dir . $file, $zipdir . $file);
                    }
                  
                }
            }
        }
    }
}

function backupdatabase($destinationfilepath,$backupdir,$databasebakupname){

    $backupFile = $destinationfilepath.$backupdir.'/'.$databasebakupname;
  
    $command = "/usr/bin/mysqldump -u ".DB_USERNAME." -p".DB_PASSWORD." ".DB_NAME." | gzip > $backupFile";

    system($command);  
}

function combinefiles($destinationfilepath,$backupdir,$combinefilename){

$path=$destinationfilepath.$backupdir.'/';

  $zip = new ZipArchive();
  
  $zip->open($path.$combinefilename, ZipArchive::CREATE);
  
  if (false !== ($dir = opendir($path))) {
  
	  while (false !== ($file = readdir($dir))){
	  
	      if ($file != '.' && $file != '..'){
	      
			$zip->addFile($path.$file,$file);
			
	      }
	  }
      }
      else{
	  die('Can\'t read dir');
      }
  $zip->close();
  
  if (false !== ($dir = opendir($path))) {
  
	  while (false !== ($file = readdir($dir))){
	  
	      if ($file != '.' && $file != '..' && $file != $combinefilename){
	      
			@unlink($path.$file);
	      }
	  }
      }
      else{
	  die('Can\'t read dir');
      }  
}
?>