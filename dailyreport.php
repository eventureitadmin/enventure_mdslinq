<?php
	include_once("header.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{
		$userres = $dbase->executeQuery("SELECT `allow_gen_rpt` FROM `env_user` WHERE `id`= ".$_SESSION['partlinq_user']['ID'],"single");
		$projres = $dbase->executeQuery("SELECT `project_id`,(SELECT `projname` FROM `env_project` WHERE `ID`=project_id AND `isclosed`=0 AND `isactive`=1)as projname FROM `env_batch` WHERE `id`= ".$_SESSION['partlinq_user']['ID'],"single");
	
?>
    <body>
		<?php include("menu.php"); ?>
<link href="css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<link href="css/dataTables/dataTables.responsive.css" rel="stylesheet">	
            <div id="page-wrapper" style="padding:40px 5px 0 5px">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="">Daily Report</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <form role="form" id="frm_dailyreport" name="frm_dailyreport" action="" method="post" enctype="multipart/form-data" >
											<input type="hidden" id="batch_id" name="batch_id" value="<?php echo $_SESSION['partlinq_user']['BATCHID'];?>">
												<div class="form-group">
													<label>Project Name: </label>
													<?php echo $projres['projname'];?>
												</div>
                                         
																			
											<div class="form-group"><label>From Date<span style="color:red;"> * </span>
									<label for="from_date" style="display:none;padding-left:10px;" class="error">This field is required.</label></label></div>
                                            <div class="form-group input-group date " data-provide="datepicker">
                                                <input type="text" name="from_date" id="from_date" value="" class="form-control required"  autocomplete="off" />
												<span class="input-group-btn">
													<button class="btn btn-default" type="button">
														<i class="fa fa-calendar"></i>
													</button>
												</span>												
                                            </div>
											
											<div class="form-group"><label>To Date<span style="color:red;"> * </span>
									<label for="to_date" style="display:none;padding-left:10px;" class="error">This field is required.</label></label></div>
                                            <div class="form-group input-group date " data-provide="datepicker">
                                                <input type="text" name="to_date" id="to_date" value="" class="form-control required"  autocomplete="off" />
												<span class="input-group-btn">
													<button class="btn btn-default" type="button">
														<i class="fa fa-calendar"></i>
													</button>
												</span>												
                                            </div>
												<div class="form-group" id="selcustomerid" >
                                                <label>Select Status <span style="color:red;"> * </span>							
													<label for="status_id" style="display:none;padding-left:10px;" class="error">This field is required.</label></label><br/>
													 <a href="#" class="chosen-toggle select" style="color:black">Select All</a>
                                                 	<a href="#" class="chosen-toggle deselect" style="margin-left:120px;color:black">Unselect All</a>
                                                <select id="status_id" name="status_id[]" data-placeholder="Select Status..." multiple class="form-control required chosen-select" style="width:290px;">
                                                
													<?php 
													$status_array =$dbase->executeQuery("SELECT `id`,`statusval` FROM `env_status` WHERE `isactive`=1 AND `customer_id`=".$_SESSION['partlinq_user']['CUSTOMERID']." ORDER BY id DESC","multiple");
													 
													foreach($status_array as $key=>$value){
														if($key > 0){
															
															echo '<option value="'.$value['id'].'">'.$value['statusval'].'</option>';															
														}
													}
													?>
                                                </select>
                                            </div>
											<?php if($_SESSION['partlinq_user']['USERTYPE']=='0' || ($_SESSION['partlinq_user']['USERTYPE']=='1' && $userres['allow_daily_rpt']=='1')){ ?>
                                            <button type="button" class="btn btn-primary" id="btn_report">Generate Report</button>
											<?php } ?>
                                        </form>
                                    </div>
                                </div>
							
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>	
                <!-- /.row -->			
            </div>
            <!-- /#page-wrapper -->
<script src="js/dataTables/jquery.dataTables.min.js"></script>
<script src="js/dataTables/dataTables.bootstrap.min.js"></script>		
		<script type="text/javascript">
		$().ready(function() {
			$('#frm_dailyreport .input-group.date').datepicker({
				   format: 'dd-M-yy',
					todayHighlight: true,
					autoclose: true,
					maxDate: 0,
					endDate: '0d',
					clearBtn:true
			});
			
			$(".chosen-select").chosen({disable_search_threshold: 10,selectAll : true});
			$('.chosen-toggle').each(function(index) {
					$(this).on('click', function(){
						$(this).parent().find('option').prop('selected', $(this).hasClass('select')).parent().trigger('chosen:updated');
					});
			});
			$('#btn_report').click(function(){
			
				$("#frm_dailyreport").validate();
				$('#frm_dailyreport').attr('action', 'dailyreportdownload.php').submit();
			});
                			
		});
		</script>
		 <?php  
			if(ENABLE_SECURITY && $_SESSION['partlinq_user']['USERTYPE'] > 0){
				include("autologoutscript.php");
			}
		 ?>
	<?php
		 include_once("footer.php");
	}	
?>
