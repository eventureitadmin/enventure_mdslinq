
<?php   $signature = 'Thank you! <br/><br/><table style="width: 562.5pt; transform: scale(0.84); transform-origin: left top 0px;" min-scale="0.84" cellspacing="0" cellpadding="0" border="0">
<tbody>
	<tr>
	<td style="width:262.5pt;padding:0;">
	<table style="width:100%;" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
		<td style="padding:0;" valign="top">
		<p style="font-size:11pt;font-family:Calibri,sans-serif;margin:0;"><b>
			<span style="color:#0054A6;font-size:11.5pt;font-family:Verdana,sans-serif;">Team ComplianceXL</span>
			</b><span style="color:black;font-size:13.5pt;font-family:Verdana,sans-serif;"></span></p>
		</td>
		</tr>
		<tr>
		<td style="padding:7.5pt 0 0 0;">
		<p style="font-size:11pt;font-family:Calibri,sans-serif;margin:0;"><b><span style="color:#0054A6;font-size:10pt;font-family:Verdana,sans-serif;">Tel:</span></b><span style="color:black;font-size:10pt;font-family:Verdana,sans-serif;">&nbsp;</span><span style="color:black;font-size:10pt;font-family:Verdana,sans-serif;">+1
		872 529 6162</span><span style="color:black;font-size:9pt;font-family:Verdana,sans-serif;"></span></p>
		</td>
		</tr>
		<tr>
		<td style="padding:0 0 7.5pt 0;">
		<p style="font-size:11pt;font-family:Calibri,sans-serif;margin:0;"><b><span style="color:#0054A6;font-size:10pt;font-family:Verdana,sans-serif;">Visit us at:</span></b><span style="color:black;font-size:10pt;font-family:Verdana,sans-serif;">&nbsp;</span><span style="color:black;font-size:10pt;font-family:Verdana,sans-serif;"><a href="https://www.compliancexl.com/?utm_source=email&amp;utm_medium=signature" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable">www.compliancexl.com</a></span><span style="color:black;font-size:9pt;font-family:Verdana,sans-serif;"></span></p>
		</td>
		</tr>
		</tbody>
		</table>
	</td>
	<td style="width:27%;padding:0;">
		<table style="width:100%;" cellspacing="0" cellpadding="0" border="0">
		<tbody><tr>
		<td style="padding:3.75pt 11.25pt 3.75pt 3.75pt;border-style:none solid none none;border-right-width:1pt;border-right-color:#0054A6;">
		<p style="font-size:11pt;font-family:Calibri,sans-serif;text-align:right;margin:0;" align="right">
		<a href="http://www.compliancexl.com/?utm_source=email&amp;utm_medium=signature" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable"><span style="font-family:Verdana,sans-serif;text-decoration:none;"><img  src="https://compliancexl.com/compliancexllogo.png"  style="width: 135pt; height: 36pt;"  border="0"></span></a>
			<span style="color:black;font-family:Verdana,sans-serif;"><br>

		</span><span style="color:black;font-size:7.5pt;font-family:Verdana,sans-serif;">An Enventure brand</span><span style="color:black;font-size:12pt;font-family:Verdana,sans-serif;"></span></p>
		</td>
	</tr>
	</tbody>
	</table>
	</td>
	<td style="width:10%;padding:0;">
	<table style="width:100%;" cellspacing="0" cellpadding="0" border="0">
	<tbody><tr>
	<td style="padding:3.75pt;">
	<p style="font-size:11pt;font-family:Calibri,sans-serif;text-align:center;margin:0;" align="center">
	<a href="https://www.linkedin.com/showcase/compliance-xl/?utm_source=email&amp;utm_medium=signature" target="_blank" >
		<span style="font-family:Verdana,sans-serif;text-decoration:none;">
			<img  src="https://compliancexl.com/li.png"  style="width: 11.99pt; height: 11.99pt;"  border="0"></span></a><span style="color:black;font-family:Verdana,sans-serif;">&nbsp;&nbsp;&nbsp;</span><a href="https://twitter.com/ComplianceXL?utm_source=email&amp;utm_medium=signature" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable"><span style="font-family:Verdana,sans-serif;text-decoration:none;"><img  src="https://compliancexl.com/tw.png" style="width: 11.99pt; height: 11.99pt;"  border="0"></span></a><span style="color:black;font-size:12pt;font-family:Verdana,sans-serif;"></span></p>
	</td>
	</tr>
	</tbody></table>
	</td>
</tr>
<tr>
		<td colspan="3" style="padding:0;">
		<table style="width:100%;" cellspacing="0" cellpadding="0" border="0">
		<tbody><tr style="height:18.75pt;">
		<td style="background-color:#009A4E;width:199.5pt;height:18.75pt;padding:0;">
		<p style="font-size:11pt;font-family:Calibri,sans-serif;text-align:center;margin:0;" align="center">
		<span style="color:white;font-size:8.5pt;font-family:Verdana,sans-serif;">MATERIAL COMPLIANCE</span><span style="color:white;font-size:10pt;font-family:Verdana,sans-serif;"></span></p>
		</td>
		<td style="width:3pt;height:18.75pt;padding:0;"></td>
		<td style="background-color:#0054A6;width:199.5pt;height:18.75pt;padding:0;">
		<p style="font-size:11pt;font-family:Calibri,sans-serif;text-align:center;margin:0;" align="center">
		<span style="color:white;font-size:8.5pt;font-family:Verdana,sans-serif;">SUPPLY CHAIN SUSTAINABILITY</span><span style="color:white;font-size:10pt;font-family:Verdana,sans-serif;"></span></p>
		</td>
		<td style="width:3pt;height:18.75pt;padding:0;"></td>
		<td style="background-color:#404041;width:199.5pt;height:18.75pt;padding:0;">
		<p style="font-size:11pt;font-family:Calibri,sans-serif;text-align:center;margin:0;" align="center">
		<span style="color:white;font-size:8.5pt;font-family:Verdana,sans-serif;">ETHICAL SOURCING</span><span style="color:white;font-size:10pt;font-family:Verdana,sans-serif;"></span></p>
		</td>
		</tr>
		</tbody></table>
		</td>
		</tr>
		<tr>
		<td colspan="3" style="padding:7.5pt 0 0 0;">
		<table style="width:100%;" cellspacing="1" cellpadding="0" border="0">
		<tbody><tr style="height:18.75pt;">
		<td style="width:562.5pt;height:18.75pt;padding:0;">
		<p style="font-size:11pt;font-family:Calibri,sans-serif;margin:0;"><span style="color:gray;font-size:7pt;font-family:Verdana,sans-serif;">Internet e-mail Confidentiality Disclaimer: This message and any attachments is intended solely for the addressee(s) and
		contains confidential, privileged and non-disclosable information. Please do not copy, distribute or forward this e-mail to anyone, unless authorized. If you are not the intended recipient of this message and/or have received this message in error, please notify
		the sender immediately by return e-mail and destroy/delete all copies of the e-mail.</span><span style="color:gray;font-size:12pt;font-family:Verdana,sans-serif;"></span></p>
		</td>
		</tr>
		</tbody></table>
		</td>
</tr>
</tbody></table>';
?>