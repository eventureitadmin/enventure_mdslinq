<?php
$reqres = $dbase->getfieldsorder($_GET['batch']);
define("REQUESTED_DATE", 1);
define("STATUS", 2);
define("PASA_STATUS",3);
define("SQA_TARGET_DATE", 4);
define("COMPLIANCE_DATE", 5);
define("CUST_DUE_DATE", 6);
$requested_date =date($dbase->dateformat);

$custfieldindex = $dbase->getcustfieldindex($_GET['batch']);
?>
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<style>
		.ui-autocomplete { height: 120px; overflow-y: scroll; overflow-x: hidden;}
	</style>
<div class="container-fluid" >
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <form role="form" id="frm_imdsrequest" name="frm_imdsrequest" action="" method="post">
									<div class="col-lg-3" id="request-container">
									<input type="hidden" name="batch" id="batch" value="<?php echo $_GET['batch']; ?>">
									<?php if(count($reqres) >0){
										$k=0;
										foreach($reqres as $key=>$val){?>
										<?php 	if($key ==4){?>
											</div>
											<div class="col-lg-1"></div>
										<div class="col-lg-3" id="request-container">
											<?php }?>
										<?php if($val['is_auto'] == 1){?>
										
												<?php if($val['is_auto_type'] == 4){
												if($_SESSION['partlinq_user']['USERTYPE']=='3'){
													$username = $_SESSION['partlinq_user']['USERNAME'];
													$userid = $_SESSION['partlinq_user']['ID'];
									               $readonly ="readonly";
												}else{
													$username = "";
													$userid = "";
													$readonly ="";
												}
												?>
									<div class="form-group">
									<label><?php echo $val['req_field_label'];?> 
										<span style="color:red;">*</span>
									<label for="sFld<?php echo $val['oid']?>" style="display:none;padding-left:10px;" class="error">This field is required.</label>
								</label>
                                                <input type="text" name="sFld<?php echo $val['oid']?>name" id="newsFld<?php echo $val['oid']?>name" class="form-control required" value="<?php echo $username;?>" onchange=""  <?php echo $readonly;?> />
												 <input type="hidden" name="sFld<?php echo $val['oid']?>" id="newsFld<?php echo $val['oid']?>" class="form-control " value="<?php echo $userid;?>"  />
												</div>
										<?php }else if($val['is_auto_type'] == 1 || $val['is_auto_type'] == 5){?>
										<div class="form-group">
													<label><?php echo $val['req_field_label'];?> 
														<span style="color:red;">*</span>
													<label for="sFld<?php echo $val['oid']?>" style="display:none;padding-left:10px;" class="error">This field is required.</label>
													</label><br/>
												<select id="sFld<?php echo $val['oid']?>" name="sFld<?php echo $val['oid']?>" class="required" style="width:290px"></select>
												</div>
										<?php }else if($val['is_auto_type'] == 2 || $val['is_auto_type'] == 3){?>
										<div class="form-group">
													<label><?php echo $val['req_field_label'];?> 
														<span style="color:red;">*</span>
														<label for="sFld<?php echo $val['oid']?>" style="display:none;padding-left:10px;" class="error">This field is required.</label>
													</label>
                                                <input type="text" name="sFld<?php echo $val['oid']?>name" id="newsFld<?php echo $val['oid']?>name" class="form-control required" value="" />
										<input type="hidden" name="sFld<?php echo $val['oid']?>" id="newsFld<?php echo $val['oid']?>" class="form-control " value=""  />
												</div>
										<?php }?>
										<?php }elseif($val['fieldtype'] == 4){ ?>
											<?php if($val['hidden_type'] == REQUESTED_DATE){?>
													<input type="hidden" name="sFld<?php echo $val['oid']?>" id="sFld<?php echo $val['oid']?>"  value="<?php echo $requested_date;?>">
											<?php } elseif($val['hidden_type'] == STATUS){?>
																	<input type="hidden" name="sFld<?php echo $val['oid']?>" id="sFld<?php echo $val['oid']?>"  value="<?php echo $reqcreatorarr[0];?>">
											<?php } elseif($val['hidden_type'] == PASA_STATUS){?>
											<input type="hidden" name="sFld<?php echo $val['oid']?>" id="sFld<?php echo $val['oid']?>"  value="<?php echo $reqcreatorarr[1];?>">
											<?php } elseif($val['hidden_type'] == COMPLIANCE_DATE){?>
											<input type="hidden" name="sFld<?php echo $val['oid']?>" id="sFld<?php echo $val['oid']?>"  value="<?php echo $dbase->getpreviousdate($requested_date,COMPLIANCE_DATE_WEEK,"month");?>">
<?php } elseif($val['hidden_type'] == SQA_TARGET_DATE){?>
											<input type="hidden" name="sFld<?php echo $val['oid']?>" id="sFld<?php echo $val['oid']?>"  value="<?php echo $dbase->getpreviousdate($requested_date,SQA_TARGET_DATE_WEEK,"week");?>">
										<?php }?>
										<?php }elseif($val['itypeid'] == 3){?>
								<div class="form-group">
									<label><?php echo $val['req_field_label'];?> 
										<span style="color:red;">*</span><label for="sFld<?php echo $val['oid']?>" style="display:none;padding-left:10px;" class="error">This field is required.</label></label></div>
                                                <div class="form-group"><input type="text" name="sFld<?php echo $val['oid']?>" id="sFld<?php echo $val['oid']?>" value="" class="form-control required"  autocomplete="off"/></div>
													


									<?php }elseif($val['fieldtype'] == 5){?>
								<div><label><?php echo $val['req_field_label'];?> <span style="color:red;">*</span><label for="sFld<?php echo $val['oid']?>" style="display:none;padding-left:10px;" class="error">This field is required.</label></label></div>
                                            <div class="form-group input-group date " data-provide="datepicker">
                                                <input type="text" name="sFld<?php echo $val['oid']?>" id="sFld<?php echo $val['oid']?>" value="" class="form-control required"  autocomplete="off" onchange="changecustduedate(this.value,'<?php echo $reqarr['sqadt'];?>','<?php echo $reqarr['cmptdate'];?>');"/>
												<span class="input-group-btn">
													<button class="btn btn-default" type="button">
														<i class="fa fa-calendar"></i>
													</button>
												</span>												
                                            </div>
											<div class="custduedateerr" style="color:red;display:none"><?php echo CUSTERRMSG;?><br/></div>
											<?php }else if($val['itypeid'] == 11){?>
										<div class="form-group">
													<label><?php echo $val['req_field_label'];?> 
														<span style="color:red;">*</span>
													<label for="part_type" style="display:none;padding-left:10px;" class="error">This field is required.</label>
													</label><br/>
												<select id="part_type" name="part_type" class="required" style="width:290px">
													<option value="">-Select-</option>
													<?php $parttypedetails = $dbase->getparttypedetails();
															  if(count($parttypedetails) >0){
																  for($i=0;$i<count($parttypedetails);$i++){
																	  echo "<option value='".$parttypedetails[$i]['id']."'>".$parttypedetails[$i]['name']."</option>";
																  }
															  }
													?>
												</select>
												</div>
										<div class="form-group">
													<label>Request Type 
														<span style="color:red;">*</span>
													<label for="request_type" style="display:none;padding-left:10px;" class="error">This field is required.</label>
													</label><br/>
												<select id="request_type" name="request_type" class="required" style="width:290px" onchange="resetreqdata(1,1)">
													<option value="">-Select-</option>
													<?php $reqtypedetails = $dbase->getreqtypedetails();
															  if(count($reqtypedetails) >0){
																  for($i=0;$i<count($reqtypedetails);$i++){
																	  echo "<option value='".$reqtypedetails[$i]['id']."'>".$reqtypedetails[$i]['req_name']."</option>";
																  }
															  }
													?>
												</select>
												</div>


										
									<?php }	 elseif($val['fieldtype'] == 6){ ?>
											 <div>
                                                <label><?php echo $val['req_field_label'];?> <span style="color:red;">*</span><label for="sFld<?php echo $val['oid']?>" style="display:none;padding-left:10px;" class="error">This field is required.</label></label>
                                            </div>	
                                            <div class="form-group input-group">
                                                <input type="text" name="sFld<?php echo $val['oid']?>" id="sFld<?php echo $val['oid']?>"  value="" class="form-control required" autocomplete="off"/>
												<span class="input-group-addon">grams</span>
                                            </div>	
										<?php }?>
										<?php }?>
										
												<?php }?>
											
										
										
                                            <button type="submit" class="btn btn-primary">Submit</button>
										</div>
								     </form>
                                    <div class="col-lg-1"></div>
									<div class="col-lg-3" id="newaddreqform-container1" style="display:none">
										<form role="form" id="frm_imdsmodel" name="frm_imdsmodel" 
										action="" method="post">
										<input type="hidden" name="type" id="type" value="1"/>
										<div class="form-group">
											<label></label><br/>
											<h4>Add Model</h4>
										</div>
									   	<div class="form-group">
                                                <label>Model Number <span style="color:red;">*</span><label for="modelnum" style="display:none;padding-left:10px;" class="error">This field is required.</label></label>
								<input type="text" id="modelnum" name="modelnum" value="" class="form-control required"/>
                                          </div>
											<div class="form-group">
												<label></label><br/>
											<button type="submit" id="savemodel" name="savemodel" 
												class="btn btn-primary" onclick="formsubmit('frm_imdsmodel','newaddreqform-container1','newsFld<?php echo $reqarr['mn']?>')">save</button>
                                            </div>
										</form>
									</div>
									<div class="col-lg-3" id="newaddreqform-container2" style="display:none">
										<form role="form" id="frm_imdscustomer" name="frm_imdscustomer" 
										action="" method="post">
										<input type="hidden" name="type" id="type" value="2"/>
										<div class="form-group">
											<label></label><br/>
											<h4>Add Customer </h4>
										</div>
										<div class="form-group">
                                             <label>Customer ID <span style="color:red;">*</span>
												 <label for="cust_code" style="display:none;padding-left:10px;" 														class="error">This field is required.</label>
											</label>
								<input type="text" id="cust_code" name="cust_code"  value="" class="form-control required"/>
                                            </div>
									   	<div class="form-group">
                                                <label>Customer Name <span style="color:red;">*</span>
													<label for="cust_name" style="display:none;padding-left:10px;" class="error">Please enter only alphabets and numbers</label></label>
								<input type="text" id="cust_name" name="cust_name" value=""  class="form-control required alphanumericspace"/>
                                            </div>
									
										<div class="form-group">
												<label></label><br/>
											<button type="submit" id="savecustomer" name="savecustomer" 
												class="btn btn-primary" onclick="formsubmit('frm_imdscustomer','newaddreqform-container2','newsFld<?php echo $reqarr['cn']?>')">save</button>
                                            </div>
										</form>
									</div>
									<div class="col-lg-3" id="newaddreqform-container3" style="display:none">
										<form role="form" id="frm_imdssupplier" name="frm_imdssupplier" 
										action="" method="post">
										<input type="hidden" name="type" id="type" value="3">
										<div class="form-group">
											<label></label><br/>
											<h4>Add Supplier </h4>
										</div>
									   	<div class="form-group">
                                                <label>Supplier Code <span style="color:red;">*</span><label for="code" style="display:none;padding-left:10px;" class="error">This field is required.</label></label>
								<input type="text" id="code" value="" name="code" class="form-control required ">
                                            </div>
											<div class="form-group">
												<label></label><br/>
											<button type="submit" id="savesupplier" name="savesupplier" 
												class="btn btn-primary" onclick="formsubmit('frm_imdssupplier','newaddreqform-container3','newsFld<?php echo $reqarr['sc']?>')">save</button>
                                            </div>
										</form>
									</div>
								
                                  </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>		
</div>
<script>
	

	$(document).ready(function(){
		
		if(usertype == 3 ){
			showcustomerdata(pid,"<?php echo $_GET['batch'];?>","<?php echo $reqarr['cn'];?>",0,"<?php echo $_SESSION['partlinq_user']['ID']?>");
		}
		if(usertype == 2){
		
				getautocompletedata("newsFld<?php echo $reqarr['rqu']?>","requester","newaddreqform-container4",4,0,"<?php echo $reqarr['cn']?>","<?php echo $reqarr['rqu']?>",pid,"<?php echo $_GET['batch']?>","<?php echo $_GET['batch']?>",1);
		}
		
		getautocompletedata("newsFld<?php echo $reqarr['mn']?>","modelnum","newaddreqform-container1",1,0,"<?php echo $reqarr['cn']?>",pid,"<?php echo $_GET['batch']?>",0);
		 getautocompletedata("newsFld<?php echo $reqarr['sc']?>","code","newaddreqform-container3",3,0,"<?php echo $reqarr['cn']?>",pid,"<?php echo $_GET['batch']?>",0);
	});
	</script>
