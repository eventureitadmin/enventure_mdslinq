<?php include_once("top.php");?>
<script type="text/javascript" src="js/Chart.min.js"></script>
<body>
	<script>
	function getdata(labelarr,colorarr,dataarr,statusidsarr){
		var data = {
			id:statusidsarr,
			labels:labelarr,
			datasets: [
			{
			  data: dataarr,
			  size:'90%',
			  backgroundColor: colorarr,
			  hoverBackgroundColor:colorarr
			}]
			};
		return data;
	}
		function getoptions(total){
			var summaryoptions=	 {
		
				elements: {
				  center: {
					text: 'Total Parts /'+total,
					color: 'black', // Default is #000000
					fontStyle: 'Verdana', // Default is Arial
					fontWeight:'bold',
					sidePadding: 20, // Default is 20 (as a percentage)
					minFontSize: 12, // Default is 20 (in px), set to false and text will not wrap.
					lineHeight: 10 // Default is 25 (in px), used for when text wraps
				  }
				},
  				cutoutPercentage: 55, //Here for innerRadius. It's already exists
				outerRadius: 200,//Here for outerRadius
				responsive: true,
				legend: {
				  display: true,
				  position:'right',
					labels: {
					usePointStyle: true
		   },
		 onClick: (evt, item) => {

			//event code goes here
		  }
		}
	  };
			return summaryoptions;
		}
		function drawchart(Chart){
			 Chart.pluginService.register({
			  beforeDraw: function(chart) {
				if (chart.config.options.elements.center) {
				  // Get ctx from string
				  var ctx = chart.chart.ctx;

				  // Get options from the center object in options
				  var centerConfig = chart.config.options.elements.center;
				  var fontStyle = centerConfig.fontStyle || 'Arial';
				  var txt = centerConfig.text;
				  var color = centerConfig.color || '#000';
				  var maxFontSize = centerConfig.maxFontSize || 75;
				  var sidePadding = centerConfig.sidePadding || 20;
				  var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
				  // Start with a base font of 30px
				  ctx.font = "50px " + fontStyle;

				  // Get the width of the string and also the width of the element minus 10 to give it 5px side padding
				  var stringWidth = ctx.measureText(txt).width;
				  var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

				  // Find out how much the font can grow in width.
				  var widthRatio = elementWidth / stringWidth;
				  var newFontSize = Math.floor(30 * widthRatio);
				  var elementHeight = (chart.innerRadius * 2);

				  // Pick a new font size so it will not be larger than the height of label.
				  var fontSizeToUse = Math.min(newFontSize, elementHeight, maxFontSize);
				  var minFontSize = centerConfig.minFontSize;
				  var lineHeight = centerConfig.lineHeight || 25;
				  var wrapText = false;

				  if (minFontSize === undefined) {
					minFontSize = 20;
				  }

				  if (minFontSize && fontSizeToUse < minFontSize) {
					fontSizeToUse = minFontSize;
					wrapText = true;
				  }

				  // Set font settings to draw it correctly.
				  ctx.textAlign = 'center';
				  ctx.textBaseline = 'middle';
				  var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
				  var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
				  ctx.font = fontSizeToUse + "px " + fontStyle;
				  ctx.fillStyle = color;
					var helpers = Chart.helpers;
					var fontSize = helpers.getValueOrDefault(centerConfig.minFontSize, 1);
					var text = centerConfig.text.split('/');
					ctx.fillText(text[0], centerX, centerY - fontSize );
					ctx.fillText(text[1], centerX, centerY + fontSize );
               
					   }
			  
			  }
			});
		}
		function chartclick(chartid,promisedDeliveryChart,batch,$is_week_rpt=0){
			document.getElementById(chartid).onclick = function(evt)
			{ 
				var activePoints = promisedDeliveryChart.getElementsAtEvent(evt);
				if(activePoints.length > 0)
				{
				  var clickedElementindex = activePoints[0]["_index"];
				  //get specific label by index 
				  var label = promisedDeliveryChart.data.labels[clickedElementindex];
					if($is_week_rpt == 0){
					var chartData = activePoints[0]['_chart'].config.data.id[clickedElementindex];
					var statusarr = chartData.split("_");
					window.location.href="home.php?batch="+batch+"&statusid="+statusarr[0]+"&sorder="+statusarr[1]+"&scid="+statusarr[2];
					}
				  //get value by index      
				  var value = promisedDeliveryChart.data.datasets[0].data[clickedElementindex];
				}
			}
		}


	</script>
	<?php 
	function getchartobj($id,$reportname){
		return '<div style="float:left;width:600px;display:none" class="summarychart">
		<p style="text-align:left;display:none" class="rptheaders">'.$reportname.'</p>
		<canvas   id="'.$id.'"  height="120px" class="canvascls"></canvas></div>';
	}
	?>
	<?php
if(isset($_POST)){
$statusheadres = $dbase->executeQuery("SELECT `status_headerid`,project_id FROM `env_batch` WHERE `closed` =0 
			AND id='".$_POST['batch']."'","single");
	        $projectid = $statusheadres['project_id'];
			$statushead1 = explode(',',$statusheadres['status_headerid']);
			if(count($statushead1) >0){
				for($p=0;$p<count($statushead1);$p++){
					if($statushead1[$p] >0){
					 $statusfield =($statushead1[$p])-1;
					$selqry1 =$dbase->getstatusgraphdetails($projectid,$_POST['batch'],"",$statusquery,$_POST['cid'],$statusfield);
						if($selqry1 !=""){
							$statusfinalres = $dbase->executeQuery($selqry1,"multiple");
						}else{
							$statusfinalres ="";
						}
						$colors1 = array();
						$seriesarray = array();
						$labelarray = array();
						$datapointsarray = array();
						$total =0;
						if($statusfinalres != ""){
							$reportname =" Current week status: ".date("d-M-Y");
							for($h=0;$h<count($statusfinalres);$h++){
								$colors1[$h] = $statusfinalres[$h]['colorcode'];
								$datapointsarray[$h] = $statusfinalres[$h]['envstatusid']."_".$statusheadres[$p]."_0";
								$labelarray[$h] = $statusfinalres[$h]['env_status']."-".$statusfinalres[$h]['cnt'];
								$seriesarray[$h] = $statusfinalres[$h]['cnt'];
								$total +=$statusfinalres[$h]['cnt'];
							}
								
							$chartid= "currentweekchart".$p;
						echo	getchartobj($chartid,$reportname);
												
									?>
	<script>
	var chartid = '<?php echo $chartid;?>';
	var batch = '<?php echo $_POST['batch'];?>';
	var promisedDeliveryChart = new Chart(document.getElementById(chartid), {
			  type: 'doughnut',
			  data: getdata([<?php echo "'" . implode("','", $labelarray) . "'";?> ],[<?php echo "'" . implode("','", $colors1) . "'";?> ],[<?php echo "'" . implode("','", $seriesarray) . "'";?> ],[<?php echo "'" . implode("','", $datapointsarray) . "'";?> ]),
			  options:getoptions("<?php echo $total;?>")	
	});
	chartclick(chartid,promisedDeliveryChart,'<?php echo $_POST['batch'];?>');	
	drawchart(Chart);
	</script>
									
			<?php $isweekreportdataexists =0;
						//Weekreport
						if($_POST['is_weekly_report'] == 1){
		 							  $weekreportquery = "SELECT DISTINCT `reportdate` FROM `env_report` WHERE `iPrjID`='".$projectid."' AND `iBatch`='".$_POST['batch']."' ORDER BY `reportdate` DESC LIMIT 0,1";
		 							$weekreportresult = $dbase->executeQuery($weekreportquery,"multiple");
							$cnt = count($weekreportresult);
							//$cnt=0;
		 							if($cnt > 0){
										$isweekreportdataexists =1;
										for($j=0;$j<count($weekreportresult);$j++){	
										
					$rptqry =$dbase->getweekstatusdetails($projectid,$_POST['batch'],$statusquery,$_POST['cid'],$statusfield,$weekreportresult[$j]['reportdate']);
										
											if($rptqry !=""){
												$weekstatusfinalres = $dbase->executeQuery($rptqry,"multiple");//echo $selqry;
											
											}else{
												$weekstatusfinalres ="";
											}
												$wcolors = array();
												$wseriesarray = array();
												$wlabelarray = array();
												$wdatapointsarray = array();
												$total =0;
												if($weekstatusfinalres != ""){
													$reportname =" Last week report on ".date("d-M-Y",strtotime($weekreportresult[0]['reportdate']));
													for($x=0;$x<count($weekstatusfinalres);$x++){
														$wcolors[$x] = $weekstatusfinalres[$x]['colorcode'];
														$wdatapointsarray[$x] = $weekstatusfinalres[$x]['envstatusid'];
														$total += $weekstatusfinalres[$x]['cnt'];
														$wseriesarray[$x] = $weekstatusfinalres[$x]['cnt'];
														$wlabelarray[$x] = $weekstatusfinalres[$x]['env_status']."-".$weekstatusfinalres[$x]['cnt'];
													}
							$chartid = "weekchart".$j;
							echo	getchartobj($chartid,$reportname);
								?>
								<script>
							var chartid = '<?php echo $chartid;?>';
							var batch = '<?php echo $_POST['batch'];?>';
							var promisedDeliveryChart = new Chart(document.getElementById(chartid), {
									  type: 'doughnut',
									  data: getdata([<?php echo "'" . implode("','", $wlabelarray) . "'";?> ],[<?php echo "'" . implode("','", $wcolors) . "'";?> ],[<?php echo "'" . implode("','", $wseriesarray) . "'";?> ],[<?php echo "'" . implode("','", $wdatapointsarray) . "'";?> ]),
									 options:getoptions("<?php echo $total;?>")	
							});
							chartclick(chartid,promisedDeliveryChart,'<?php echo $_POST['batch'];?>',1);	
							drawchart(Chart);
							</script>
											<?php	}
										}
									}else{
											$j =0;
											$chartid = "weekchart".$j;
											echo	getchartobj($chartid,"Report is not generated");
											if($statusfinalres != ""){
											for($h=0;$h<count($statusfinalres);$h++){
												$colors1[$h] = $statusfinalres[$h]['colorcode'];
												$datapointsarray[$h] = $statusfinalres[$h]['envstatusid']."_".$statusheadres[$p]."_0";
												$labelarray[$h] = $statusfinalres[$h]['env_status']."-0";
												$wdatapointsarray[$h] = $statusfinalres[$x]['envstatusid'];
												$wseriesarray[$h] = 0;
											}

										?>
										<script>
											var chartid = '<?php echo $chartid;?>';
											var batch = '<?php echo $_POST['batch'];?>';
											var promisedDeliveryChart = new Chart(document.getElementById(chartid), {
													  type: 'doughnut',
													  data: getdata([<?php echo "'" . implode("','", $labelarray) . "'";?> ],[<?php echo "'" . implode("','", $colors1) . "'";?> ],[<?php echo "'" . implode("','", $wseriesarray) . "'";?> ],[<?php echo "'" . implode("','", $wdatapointsarray) . "'";?> ]),
													  options:getoptions(0)		
											});
											chartclick(chartid,promisedDeliveryChart,'<?php echo $_POST['batch'];?>',1);	
											drawchart(Chart);
											</script>
									<?php	}
									}
						}?>

					<?php
							///
								//customer report
						$custdetails = $dbase->getcustomerdetails($projectid,$_POST['batch']);
					
						if(count($custdetails) >0){
							for($y=0;$y<count($custdetails);$y++){
								$selectedcid= $custdetails[$y]['cid'];
								////
								$chartalign ="left";
								$width=680;
								
								$custrptname = $custdetails[$y]['cname'];

									$selqry2 =$dbase->getstatusgraphdetails($projectid,$_POST['batch'],$custdetails[$y]['cid'],$statusquery,$_POST['cid'],$statusfield);
								 if($selqry2 !=""){
									$statusfinalres2 = $dbase->executeQuery($selqry2,"multiple");;
								}else{
									$statusfinalres2 ="";
								}
							
							    $colors2 = array();
								$seriesarray2 = array();
								$labelarray2 = array();
								$datapointsarray2 = array();
								$total =0;
								if($statusfinalres2 != ""){//print_r($statusfinalres);
									$total =0;
									for($h=0;$h<count($statusfinalres2);$h++){
										$colors2[$h] = $statusfinalres2[$h]['colorcode'];
										$datapointsarray2[$h] = $statusfinalres2[$h]['envstatusid']."_".$statusheadres[$p].'_'.$selectedcid;
										$labelarray2[$h] = $statusfinalres2[$h]['env_status']."-".$statusfinalres2[$h]['cnt'];
										$total += $statusfinalres2[$h]['cnt'];
										$seriesarray2[$h] = $statusfinalres2[$h]['cnt'];
									}
								$chartid="customerchart".$y;
								echo	getchartobj($chartid,$custrptname);
									?>
							<script>
							var chartid = '<?php echo $chartid;?>';
							var promisedDeliveryChart = new Chart(document.getElementById(chartid), {
								  type: 'doughnut',
								  data: getdata([<?php echo "'" . implode("','", $labelarray2) . "'";?> ],[<?php echo "'" . implode("','", $colors2) . "'";?> ],
												[<?php echo "'" . implode("','", $seriesarray2) . "'";?> ],[<?php echo "'" . implode("','", $datapointsarray2) . "'";?> ]),
								options:getoptions("<?php echo $total;?>")	
							});
							chartclick(chartid,promisedDeliveryChart,'<?php echo $_POST['batch'];?>');	
							drawchart(Chart);
//$(".rptheaders").css("display","block");
            
							</script>
						<?php	}
								////
							}
						}//cust
							?>
						
						<?php }
	
					}
				}
			}
}
?>
	<script>

	</script>
	
</body>
</html>

