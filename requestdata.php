<?php
include_once("top.php");
if($_SESSION['partlinq_user']['ID']==''){
	header("Location:login.php");
}
else{
	$page = $_REQUEST['page'];
	$limit = $_REQUEST['rows'];
	$sidx = $_REQUEST['sidx'];
	$sord = $_REQUEST['sord'];
	$pid = $_GET['pid'];
	if(!$sidx) $sidx =1;
	
$filters = str_replace('\"','"' ,$_POST['filters']);
$search = $_POST['_search'];
$cond = "";
$where = "";

if(($search==true) &&($filters != "")) {
    $filters = json_decode($filters);
    $whereArray = array();
    $rules = $filters->rules;
    $groupOperation = $filters->groupOp;
    foreach($rules as $rule) {
        $fieldName = $rule->field;
        $fieldData = $rule->data;
        switch ($rule->op) {
       case "eq":
            $fieldOperation = " = '".$fieldData."'";
            break;
       case "ne":
            $fieldOperation = " != '".$fieldData."'";
            break;
       case "lt":
            $fieldOperation = " < '".$fieldData."'";
            break;
       case "gt":
            $fieldOperation = " > '".$fieldData."'";
            break;
       case "le":
            $fieldOperation = " <= '".$fieldData."'";
            break;
       case "ge":
            $fieldOperation = " >= '".$fieldData."'";
            break;
       case "nu":
            $fieldOperation = " = ''";
            break;
       case "nn":
            $fieldOperation = " != ''";
            break;
       case "in":
			$temp = explode(",",$fieldData);
			$fieldData = "'" . implode ( "', '", $temp ) . "'";				
            $fieldOperation = " IN (".$fieldData.")";
            break;
       case "ni":
            $fieldOperation = " NOT IN '".$fieldData."'";
            break;
       case "bw":
            $fieldOperation = " LIKE '".$fieldData."%'";
            break;
       case "bn":
            $fieldOperation = " NOT LIKE '".$fieldData."%'";
            break;
       case "ew":
            $fieldOperation = " LIKE '%".$fieldData."'";
            break;
       case "en":
            $fieldOperation = " NOT LIKE '%".$fieldData."'";
            break;
       case "cn":
            $fieldOperation = " LIKE '%".$fieldData."%'";
            break;
       case "nc":
            $fieldOperation = " NOT LIKE '%".$fieldData."%'";
            break;
        default:
            $fieldOperation = "";
            break;
            }
        if($fieldOperation != "") $whereArray[] =  $fieldName.$fieldOperation;
    }
    if (count($whereArray)>1) {
        $where .= " AND ".join(" ".$groupOperation." ", $whereArray);
    }
	elseif(count($whereArray)==1){
		$where .=" AND ".$fieldName.$fieldOperation;
	}
	else {
        $where = "";
    }
	 $cond = $where;
}
	
	$orderby = 'ORDER BY '.$sidx.' '.$sord;
	
	$data['pid'] = $pid;
	$data['cond'] = $cond;
	$data['tlcond'] = $tlcond;
	$data['orderby'] = $orderby;
	
	$query1= "SELECT `ID` , (

SELECT u.username
FROM env_user u
WHERE u.id = sFld1
) AS sFld1, (

SELECT CONCAT( `cust_name` , ' ( ', `cust_code` , ' )' )
FROM `env_customer_details`
WHERE id = sFld2
AND `isactive` =1
) AS sFld2, (

SELECT `code`
FROM `env_supplier`
WHERE id = sFld3
AND `isactive` =1
) AS sFld3, `sFld4` , (

SELECT `modelnum`
FROM `env_model_details`
WHERE id = sFld5
AND `isactive` =1
) AS sFld5, `sFld6` , `sFld7` , `sFld8` , `sFld9` , `sFld10` , `sFld11` , `sFld12` , `sFld13` , `sFld14` , `sFld15` , `sFld16` , `sFld17` , `sFld18` , `sFld19` , `sFld20` , `sFld21` , `sFld22` , `sFld23` , `sFld24`
FROM `env_urlgrab`
WHERE `iPrjID` = '1'
AND `iBatch` = '1'";
	$result1 = $dbase->executeQuery($query1,"multiple");
	$count = count($result1);
	if( $count > 0 ) { 
		$total_pages = ceil($count/$limit);
	} else {
		$total_pages = 0; 
	} 
	if ($page > $total_pages){
		$page=$total_pages;
	}
	$start = $limit*$page - $limit; // do not put $limit*($page - 1) 
	if($start=='-'.$limit){
		$start = 0;
	}
	$data['start'] = $start;
	$data['limit'] = $limit;
	
		$query2= "SELECT `ID` , (

SELECT u.username
FROM env_user u
WHERE u.id = sFld1
) AS sFld1, (

SELECT CONCAT( `cust_name` , ' ( ', `cust_code` , ' )' )
FROM `env_customer_details`
WHERE id = sFld2
AND `isactive` =1
) AS sFld2, (

SELECT `code`
FROM `env_supplier`
WHERE id = sFld3
AND `isactive` =1
) AS sFld3, `sFld4` , (

SELECT `modelnum`
FROM `env_model_details`
WHERE id = sFld5
AND `isactive` =1
) AS sFld5, `sFld6` , `sFld7` , `sFld8` , `sFld9` , `sFld10` , `sFld11` , `sFld12` , `sFld13` , `sFld14` , `sFld15` , `sFld16` , `sFld17` , `sFld18` , `sFld19` , `sFld20` , `sFld21` , `sFld22` , `sFld23` , `sFld24`
FROM `env_urlgrab`
WHERE `iPrjID` = '1'
AND `iBatch` = '1'";
		$result2 = $dbase->executeQuery($query2,"multiple");
		$response = new stdClass();
		$response->page = $page;
		$response->total = $total_pages;
		$response->records = $count;
		$response->rows = $result2;
	echo json_encode($response);
	exit;
}
?>
