<?php
    include_once("header.php");

	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{ 



?>

<style>
.marginBottom-0 {margin-bottom:0;}
.logopadding{padding: 5px 10px;}
.dropdown-submenu{position:relative;}
.dropdown-submenu>.dropdown-menu{top:0;left:100%;margin-top:-6px;margin-left:-1px;-webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;}
.dropdown-submenu>a:after{display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;}
.dropdown-submenu:hover>a:after{border-left-color:#555;}
.dropdown-submenu.pull-left{float:none;}.dropdown-submenu.pull-left>.dropdown-menu{left:-100%;margin-left:10px;-webkit-border-radius:6px 0 6px 6px;-moz-border-radius:6px 0 6px 6px;border-radius:6px 0 6px 6px;}
/*https://www.bootply.com/nZaxpxfiXz*/
</style>
<?php  $password =$dbase->encrypt_decrypt('decrypt',$dbase->executeQuery("SELECT `userpassword` FROM `env_user` WHERE  id=".$_SESSION['partlinq_user']['ID'],"single")[0]);
?>
<script>

	function isvalidpaassword(newpassword,pass_pattern){
			var isvalidpass = false;
			//var splChars = "*|\:<>[]{}`\;()@&$#%!";
			var splChars = "!`@#$%^&*()+=-[]\\\';,./{}|\":<>?~_";
	
			if(!/[A-Z]/.test(newpassword)) {
				isvalidpass = false;
			} else if (!/[a-z]/.test(newpassword)) {
				isvalidpass = false;
			} else if (!/[0-9]/.test(newpassword)) {
				isvalidpass = false;
			} else if (!/[\d=!\-@._*A-Za-z]/.test(newpassword)){
				isvalidpass = false;
			}else if(newpassword.length < 8){
				isvalidpass = false;
			}else{
				for (var i = 0; i < newpassword.length; i++) {
					if (splChars.indexOf(newpassword.charAt(i)) != -1){
						isvalidpass =true;
					}
				}
	
			}
		return isvalidpass;
	}
</script>
<nav class="navbar navbar-inverse navbar-static-top marginBottom-0" role="navigation" style="position: fixed;top:0;width: 100%;background-color:#E5E5E5;border-color=white">
	<div class="navbar-header">
		<a class="navbar-brand logopadding" href="javascript:void(0);"><img style="max-width: 120px;" src="images/mdslinq.png"/></a>
	</div>
	
	 <ul class="nav navbar-right navbar-top-links">
		 <li class="dropdown" >
				<a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:black;background-color:#E5E5E5">
					<i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['partlinq_user']['FULLNAME']; ?> <b class="caret"></b>
				</a>
				<ul class="dropdown-menu dropdown-user">
                        <li><a href="#" data-toggle='modal' title='Change Password' onclick='showchangepwd()' id='changepwd'><i class="fa fa-user fa-fw" style="color:black;background-color:#E5E5E5"></i> Change Password</a>
                        </li>					
					<li class="divider"></li>
					<li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
			</ul>
		</li>
	</ul>
	</nav>

        <div class="container">
			<div class="row"><div class="alert alert-danger col-md-4 col-md-offset-4" role="alert" id="errormsgid" style="display:none">
  
</div></div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
							<h4 align="left">Reset Password</h4>
                        </div>
						<?php if($msg!=''){?>
						<div class="alert alert-danger">
							<?php echo $msg; $msg=''; ?> 
						</div>
						<?php } ?>							
                        <div class="panel-body">
                            <form role="form" id="resetfrm_login" action="" method="post">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Old Password" id="oldpassword" name="oldpassword" type="hidden"  value="<?php echo $password;?>"  />
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control required pwcheck" placeholder="New Password" id="newpassword" name="newpassword" type="password"  value=""/>
										<p class="error" style="color:red;display:none">This field is required</p>
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
									<button type="button" id="reset_btn" class="btn btn-primary btn-lg btn-block">Reset</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script>var pass_pattern = <?php echo PASSWORDREG; ?> 
var batchid ='<?php echo $_SESSION['partlinq_user']['BATCHID'];?>';
</script>
<style>
	.navbar-inverse .navbar-nav > .open > a, .navbar-inverse .navbar-nav > .open > a:focus, .navbar-inverse .navbar-nav > .open > a:hover {
	background-color:white
}
	.navbar-inverse{
			border-color:#E5E5E5
	}
</style>
<script>
(function($){
function validatepwd(pass_pattern){
	var isvaliddata = true;
	$("#errormsgid").css("display","none");
	$(".error").css("display","none");
	$(".pwcheck").each(function(index){
		var currentobj = $(this);
		if(currentobj.val() == ""){
			$(".error").eq(index).css("display","block");
			isvaliddata = false;
		}else{
			$(".error").eq(index).hide();
			var passvalid = isvalidpaassword(currentobj.val(),pass_pattern);
			 if(!passvalid){
				$("#errormsgid").html("<strong>Error</strong> Password contains at least one number, one lowercase character and one special character and one uppercase letter with min 8 characters");
				$("#errormsgid").css("display","block");
				
				isvaliddata = false;
			}else{
				isvaliddata =true;
			}
		}
	});

	return isvaliddata;
}
	$(document).ready(function(){
		var isvaliddata = false;
		$("#reset_btn").click(function(){
		isvaliddata = validatepwd(pass_pattern);
	
		if(isvaliddata){

				$.ajax({
						type:"POST",
						url:"resetpasswordupdate.php",
						data:$("#resetfrm_login").serialize(),
						success: function(response){
							obj = JSON.parse(response);
							if(obj.msg=='success'){
									$.notify(obj.message,"success");
									window.location.href="home.php?batch="+batchid;

							}
							else{
							$.notify(obj.message,"error");
							}
						}
					});
		}
		});
	
	});
})(jQuery);	
</script>
<?php 
	if(ENABLE_SECURITY){
		include("autologoutscript.php");
	}
?>
<?php  }?>
