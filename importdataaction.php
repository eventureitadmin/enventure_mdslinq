<?php
include_once("top.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
else{
	if($_FILES){
		if($_POST){
			$project_id = $_POST['project_id'];
			$batch_id = $_POST['batch_id'];
			$datatype = $_POST['datatype'];
			$user_id = $_POST['user_id'];
		}	
		if($project_id == ''){
			header("Location: importdata.php?msg=4");
			exit();		
		}	
			if($batch_id== ''){
			header("Location: importdata.php?msg=5");
			exit();		
		}
		if(isset($_FILES['data_file'])){		
		  $filepath = $docroot."/upload/data/";
		  $errors= array();
		  $file_name = $_FILES['data_file']['name'];
		  $file_size =$_FILES['data_file']['size'];
		  $file_tmp =$_FILES['data_file']['tmp_name'];
		  $file_type=$_FILES['data_file']['type'];
		  $file_ext=strtolower(end(explode('.',$_FILES['data_file']['name'])));
		  
		  $extensions= array("xls","xlsx");
		  
		  if(in_array($file_ext,$extensions)=== false){
			 $errors[]="extension not allowed, please choose a xls or xlsx file.";
		  }
		  if(empty($errors)==true){		  
				move_uploaded_file($file_tmp,$filepath.$file_name);
				try {
					$inputFileName = $filepath.$file_name;
					$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$objPHPExcel = $objReader->load($inputFileName);
				} catch (Exception $e) {
					die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME). '": '.$e->getMessage());
				}
				$objWorksheet = $objPHPExcel->getActiveSheet();
				$objPHPExcel->getActiveSheet()->setShowGridlines(true);
				$timestamps=time();
				$highestRow = $objWorksheet->getHighestRow();
				$highestColumn = $objWorksheet->getHighestColumn(); 
				$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
			  	if($datatype=='1'){
					$i=1;
					$j=0;
					for ($row = 2; $row <= $highestRow; ++$row) {
						$rowdata1='';
						$rowdataary1=array();
						$canAdd=1;
						if(strlen(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()))!=0){
							for ($col =0; $col <= 149; ++$col){
								$rowdata1 .=trim("'".addslashes(trim($objWorksheet->getCellByColumnAndRow($col, $row)->getValue(),"`")))."'|,";		
							}
							$rowdata1=rtrim($rowdata1,'|,');
							$rowdataary1=explode('|,',$rowdata1);

						   $qryflds='';
							for($a=0;$a<sizeof($rowdataary1);$a++){
								$qryflds .="sFld".$a.",";
							}
							$qryflds=rtrim($qryflds,',');
							$rowdata1=str_replace('|,',',',trim($rowdata1));

							if($canAdd==1){						
								$sql101="INSERT INTO env_urlgrab (iPrjID,iBatch,userid,$qryflds) VALUES ('".$project_id."','".$batch_id."','".$user_id."',$rowdata1)";
								if($dbase->executeNonQuery($sql101)){
								}
								else{
									echo "Line Number ' ".$i." '. Couldnot Import Data.";exit;
								}							
							}
							else{
								echo "Line Number ' ".$i." '. Couldnot Import Data.";exit;
							}						
						}
						$i++;
					}
					header("Location: importdata.php?msg=1");
					exit();	
		 	 }
			 if($datatype=='2'){
					$i=1;
					$j=0;
					for ($row = 2; $row <= $highestRow; ++$row) {
						$rowdata1='';
						$rowdataary1=array();
						$canAdd=1;
						if(strlen(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()))!=0){
							$envid = trim(addslashes(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue())));
							for ($col =1; $col <= 149; ++$col){
								$rowdata1 .=trim("'".addslashes(trim($objWorksheet->getCellByColumnAndRow($col, $row)->getValue(),"`")))."'|,";		
							}
							$rowdata1=rtrim($rowdata1,'|,');
							$rowdataary1=explode('|,',$rowdata1);

						   $qryflds='';
							$w=1;
							for($a=0;$a<sizeof($rowdataary1);$a++){
								$qryflds .="sFld".$w."=".$rowdataary1[$a].",";
								$w++;
							}
							$qryflds=rtrim($qryflds,',');
							$rowdata1=str_replace('|,',',',trim($rowdata1));

							if($canAdd==1){
								$sql101="UPDATE env_urlgrab SET ".$qryflds." WHERE iPrjID = '".$project_id."' AND iBatch='".$batch_id."' AND sFld0='".$envid."'";
								//echo "<br/>";
								if($dbase->executeNonQuery($sql101)){
								}
								else{
									echo "Line Number ' ".$i." '. Couldnot update Data.";exit;
								}	
							}
							else{
								echo "Line Number ' ".$i." '. Couldnot update Data.";exit;
							}						
						}
						$i++;
					}
					header("Location: importdata.php?msg=6");
					exit();					 
			 }
		  }
		  else{
				header("Location: importdata.php?msg=2");
				exit();			 
		  }			
	   }
	   else{
		header("Location: importdata.php?msg=3");
		exit();	   
	   }  		
	}
}
?>
