<?php
$data['browser'] = $logtrack->getUserBrowser();
$data['userip'] = $logtrack->getIP();
$data['usercurrenturl'] = $logtrack->getCurrentURL();
$data['userrefererurl'] = $logtrack->getRefererURL();
$blockipmsg = false;
$ipblockres=$logtrack->getloginconfig();
if($ipblockres['blacklistips']!=''){
	$blockipary = explode(",",$ipblockres['blacklistips']);
	if(in_array($data['userip'],$blockipary)){
		$blockipmsg = true;
	}
}
if($ipblockres['whitelistips']!=''){
	$allowipary = explode(",",$ipblockres['whitelistips']);
	if(in_array($data['userip'],$allowipary)){
		$blockipmsg = false;
	}
}
$insdata['user_ip'] = $data['userip'];
$insdata['user_browser'] = $data['browser'];
$insdata['user_logdate'] = date('Y-m-d');
$insdata['user_logtime'] = date('H:i:s');
$insdata['user_refferer'] = $data['userrefererurl'];
$insdata['user_page'] = $data['usercurrenturl'];
if($logtrack->checkiplog($data['userip'])){
	$logtrack->insertiplog($insdata);
}
if($_SESSION['partlinq_user']['ID'] != '' && $_SESSION['partlinq_user']['USERTYPE'] > 0){
	if(!$logtrack->isloginrecordnotfound($_SESSION['partlinq_user']['ID'])){
		header("Location:autologout.php");
		exit();
	}	
}
if($_SESSION['partlinq_user']['AUDIT_ID'] > 0){
if($logtrack->isuserlogoutbyadmin($_SESSION['partlinq_user']['AUDIT_ID'])){
	header("Location:autologout.php");
	exit();
}
else{
	$logtrack->updatecurrenttime($_SESSION['partlinq_user']['AUDIT_ID']);
}
}
?>