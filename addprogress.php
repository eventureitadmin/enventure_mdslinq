<?php
	include_once("header.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{
	
		$userres = $dbase->executeQuery("SELECT `allow_addprogress` FROM `env_user` WHERE `id`= ".$_SESSION['partlinq_user']['ID'],"single");
		/*if($_POST){
			$project_id = $_POST['project_id'];
			if($project_id == ''){
				header("Location: addprogress.php?msg=4");
				exit();		
			}else{
				$dbase->executeNonQuery("UPDATE env_project SET progress_val='".$_POST['progress_val']."'
				WHERE ID=".$project_id);
			}	
		}	*/
		if(isset($_GET['pid'])){
			$projectres = $dbase->executeQuery("SELECT ID,progress_val FROM env_project WHERE isclosed='0' AND isactive=1 AND ID =".$_GET['pid'],"single");
			$progressval = $projectres['progress_val'];
		}else{
			$progressval = "";
		}
?>
    <body>
		<?php include("menu.php"); ?>
		<link href="../css/jquery_confirm.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="../js/jquery_validate.js"></script>
		<script type="text/javascript" src="../js/jquery_confirm.js"></script>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="">Add Progress</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
								<?php include_once("message.php"); ?>
								<?php if($_GET['msg']==1){ echo show_success_msg('Data has been saved successfully'); } ?>
							    <?php if($_GET['msg']==2){ echo show_error_msg('Please select Project.'); } ?>
								 <?php if($_GET['msg']==3){ echo show_error_msg('Data save error.'); } ?>
                               
                                <div class="row">
                                    <div class="col-lg-4">
                                       <form role="form" id="frm_addprogress" name="frm_addprogress" action="" method="post" 											>
										<div class="form-group" >
                                                <label>Select Project</label>
                                                <select id="project_id" name="project_id" class="form-control required" onchange="window.location='addprogress.php?pid='+this.value" >
                                                    <option value="">-Select-</option>
													<?php 
													if($_SESSION['partlinq_user']['USERTYPE']=='1'){
														$projqry1 = " AND ID IN(SELECT DISTINCT(`iPrjID`) as projids 
														FROM `env_urlgrab`
														WHERE `userid` = ".$_SESSION['partlinq_user']['ID'].")";
													}
													else{
														$projqry1 = "";
													}
													$project_query = "SELECT ID,projname FROM env_project WHERE isclosed='0'".$projqry1;
													$project_result = $dbase->executeQuery($project_query,"multiple");
													for($i=0;$i<count($project_result);$i++){
															$selected1 = "";
															if($_GET['pid']==$project_result[$i]['ID']){
																$selected1 = "selected";
															}
															else{
																$selected1 = "";
															}
															echo '<option value="'.$project_result[$i]['ID'].'" '.$selected1 .'>'.$project_result[$i]['projname'].'</option>';
													}
													?>
                                                </select>
                                            </div>
										   <label>Progress Value</label>
											<div class="form-group">
                                             <input name="progress_val" id="progress_val" value="<?php echo $progressval;?>">
                                          </div>
                                         	<?php if($_SESSION['partlinq_user']['USERTYPE']=='0' || ($_SESSION['partlinq_user']['USERTYPE']=='1' && $userres['allow_addprogress']=='1')){ ?>
                                            <button type="button" class="btn btn-primary" id="btn_save">Save</button>
											<?php } ?>
                                        </form>
                                    </div>
                                </div>
							
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>	
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover" id="weeklytable">
                                        <thead>
                                            <tr>
                                                <th>Project Name</th>
												<th>Progress Value</th>
                                                <th class="nosort">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
		if($_SESSION['partlinq_user']['USERTYPE'] == 1){
			$subqry = " AND ID IN(SELECT DISTINCT `iPrjID` FROM `env_urlgrab` WHERE `userid`= ".$_SESSION['partlinq_user']['ID'].")";
		}else{
			$subqry = "";
		}
		 	 $projectqry = "SELECT `ID`,`projname`,`progress_val` FROM `env_project` WHERE isclosed='0' AND `isactive`=1".$subqry;
		 							$projres = $dbase->executeQuery($projectqry,"multiple");		
										for($i=0;$i<count($projres);$i++){
											if (0 == $i % 2) {
												$class = 'class="even"';
											}
											else{
												$class = 'class="odd"';
											}
											echo '<tr '.$class.'>
                                                <td>'.$projres[$i]['projname'].'</td>
											    <td>'.$projres[$i]['progress_val'].'</td>
                                                <td ><a href=addprogress.php?pid='.$projres[$i]['ID'].' >Edit</a></td>
                                            </tr>';
										}
										?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>				
                </div>				
            </div>
            <!-- /#page-wrapper -->
		<script type="text/javascript">
		$(document).ready(function() {
		$("#frm_addprogress").validate();	
		$('#btn_save').click(function(){
			if($("#frm_addprogress").valid()){
			$.ajax({
			   url:"addprogressajax.php",
			   type: "POST",
			   data:  $("#frm_addprogress").serializeArray(),
			   dataType:"JSON",
			   success: function(data)
				{	
					if(data.msg == "success"){
						window.location.href ="addprogress.php?msg=1";
	 				
					}else{
					
						window.location.href ="addprogress.php?msg=2";
					}
				 	
				}
			});
			}
		});
               /* $('#weeklytable').DataTable({
                        responsive: true,
					   'aoColumnDefs': [{
							'bSortable': false,
							'aTargets': ['nosort']
						}]						
                });	*/			
		});
		</script>			
	</body>

	<?php
		 include_once("footer.php");
	}	
?>
