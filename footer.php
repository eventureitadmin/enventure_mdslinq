<style>
.footer_cls {
  position: fixed;
  bottom: 0;
  width: 100%;
  height: 40px;
  line-height: 40px;
  background-color: #E5E5E5;
}
</style>
<footer class="footer_cls">
      <div class="footer_container">
		  <div class="row">
			  <div class="col-lg-12 text-center">
			  <div class="col-lg-4"><strong>MDSlinq</strong> &#169; IMDS Manager | ComplianceXL.</div>
			  <div class="col-lg-4"><strong>Email:</strong> <a href="mailto:support@mdslinq.com?subject=Partlinq IMDS Support" style="color:black;">support@mdslinq.com</a></div>
			  <div class="col-lg-4 text-muted">Copyright &#169; <?php echo date('Y'); ?> <strong>Enventure</strong>. All Rights Reserved</div>
		  </div>
		  </div>
      </div>
    </footer>
</body>
</html>
