	/**
	 * Scroll bar for shortcut keys menu popup
	 */
	$('#menuModal').on('shown.bs.modal', function () {
		 hot4.deselectCell();
    	$('#menuModal table#menutable tbody td').first().focus();
    });	
	var currCell = $('#menuModal table#menutable td').first();
	$('#menuModal table#menutable').keydown(function (e) {
		
		var c = "";
		if (e.which == 39) {
			// Right Arrow
			c = currCell.next();
		} else if (e.which == 37) {
			// Left Arrow
			c = currCell.prev();
		} else if (e.which == 38) {
			// Up Arrow
			c = currCell.closest('tr').prev().find('td:eq(' + currCell.index() + ')');
		} else if (e.which == 40) {
			// Down Arrow
			c = currCell.closest('tr').next().find('td:eq(' + currCell.index() + ')');
		} else if ((e.which == 9 && !e.shiftKey)) {
			// Tab
			e.preventDefault();
			c = currCell.next();
		} else if ( (e.which == 9 && e.shiftKey)) {
			// Shift + Tab
			e.preventDefault();
			c = currCell.prev();
		}
		if (c.length > 0) {
			currCell = c;
			currCell.focus();
		}
	});
	/**
	 * Scroll bar for shortcut keys menu popup
	 */
$('#popupdialog').on('shown.bs.modal', function () {
		 hot4.deselectCell();
    	$('#popupdialog table#envstatustable tbody td').first().focus();
    });	
	var currCell1 = $('#popupdialog table#envstatustable td').first();
	$('#popupdialog table#envstatustable').keydown(function (e) {
	
		var c = "";
		if (e.which == 39) {
			// Right Arrow
			c = currCell1.next();
		} else if (e.which == 37) {
			// Left Arrow
			c = currCell1.prev();
		} else if (e.which == 38) {
			// Up Arrow
			c = currCell1.closest('tr').prev().find('td:eq(' + currCell1.index() + ')');
		} else if (e.which == 40) {
			// Down Arrow
			c = currCell1.closest('tr').next().find('td:eq(' + currCell1.index() + ')');
		} else if ((e.which == 9 && !e.shiftKey)) {
			// Tab
			e.preventDefault();
			c = currCell1.next();
		} else if ( (e.which == 9 && e.shiftKey)) {
			// Shift + Tab
			e.preventDefault();
			c = currCell1.prev();
		}
		if (c.length > 0) {
			currCell1 = c;
			currCell1.focus();
		}
	});
 	/***
	 * Shortcut keys 
	 */
	function init4() {
		
	
		/**
		 * Clear filter
		 */
			shortcut.add("ctrl+f5", function() {
			   hot4.deselectCell();
			   $("#clear4").focus();
			   $("#clea4r").click();
		       });
		/**
		 * Save data
		 */
			shortcut.add("f1", function() {
			   hot4.deselectCell();
			  $("#save4").focus();
			  $("#save4").click();
			});
		
		/**
		 * Focus on grid
		 */
			shortcut.add("ctrl+g", function() {
			   hot4.selectCell(0,selgridcol);
				 
			});
		/**
		 * Show short cut menu 
		 */
			shortcut.add("ctrl+s", function() {
				
				 	  showshortcutmenu();
			   
			});
		/**
		 * Mouse right click
		 */
			shortcut.add("shift+F10", function() {
				var  selobj = hot4.getSelected(); 
				
				if(selobj != "" && selobj != null && selobj != undefined)
				{
					 var selhotrow = hot4.getSelected()[0][0];
					 var selhotcol = hot4.getSelected()[0][1];
					hot4.selectCell(selhotrow,selhotcol);
				}
			   
			});
		
		
			/**
		     * Column sort by ascending
		     */
			 shortcut.add("f3",function(){
				 var selectedcell = hot4.getSelected();
				if(typeof selectedcell === "object"){
					hot4.deselectCell();
					 hot4.updateSettings({
						
						  columnSorting: {
								column:selectedcell[0][1],
								sortOrder: 'asc'
							 }
						});
		 			 hot4.selectColumns(selectedcell[0][1]);
				 }
			 });
		 /**
		  * Column sort by descending
		  */
			 shortcut.add("f4",function(){
				 var selectedcell = hot4.getSelected();
				 
				 if(typeof selectedcell === "object"){
					 hot4.deselectCell();
					 hot4.updateSettings({
						  columnSorting: {
								column:selectedcell[0][1], 
								sortOrder: 'desc'
							 }
						});
					 hot4.selectColumns(selectedcell[0][1]);
		 
				 }
			 });
			shortcut.add("escape",function(){
				 if($('#myModal').is(':visible')){
					var selcolindex = $("#selected_col").val();
					selcolindex = parseInt(selcolindex);
					hot4.selectColumns(selcolindex);
					 
				}
			});
			shortcut.add("alt+h", function() {
				 $("#himg").click();
		   	 });
			shortcut.add("alt+f", function() {
				$("#fimg").click();
		 	 });
		}
	  $(window).keydown(function (key) {
		if (key.keyCode == 38) {
			arrow("prev");
		}
		if (key.keyCode == 40) {
			arrow("next");
		}
	});
	$(document).ready(function(){
		$(".chosen-select").chosen({search_contains: true,width:"150px"});
		init4();
		$("#export4").click(function(){
			 if(hot4.countRows() >0){
			$("#req_export_form4").submit();
			 }

		});
		$("#part_type4").change(function(){
			$("#selparttype4").val($(this).val());
			loadallreqGridData();
		});
		
		if(usertype == 0){
			$("#save4").hide();
		}
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var target = $(e.target).attr("href"); 
		   if(target == "#allreqlist"){
			   loadallreqGridData();
		  }
		
		});
	    $(".nav-tabs a").click(function(e){
			$(this).tab('show');
	    });
		   	
	/**
	 * Focus on filter popup
	 */
	$('#myModal').on('shown.bs.modal', function () {
    	$("#filtersel").focus();
		
    });
	
	
	/**
	 * Filter popup and on click enter search data based on filtered value
	 */
    $(document).keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if($('#myModal').is(':visible') && keycode == '13'){
				$('.sol-selection-container').hide();
			 	$("#search_btn").click();
	   	}
	});	
	
          
	 
		//Clear btn click
		$("#clear4").click(function(){
			$("#selparttype4").val("");
			$("#selreqtype4").val("");
			$("#part_type4").val("").trigger('chosen:updated');
			$("#request_type4").val("").trigger('chosen:updated');
			if(statusid >0){
				window.location.href="home.php?batch="+batch;
			}else{
				$('.nav-tabs a[href="#allreqlist"]').tab('show');
				
				loadallreqGridData();
			}
			
		});

	});


	function  loadallreqGridData() {
		var result = null;
		 $.blockUI({ 
			message: 'Please wait...'
		}); 
        var totalrows = 0; 
        if(hot4 !="" && hot4 != null && hot4 != undefined){
		    totalrows = hot4.getData().length;
         }
		var loadurl = "";
		var saveurl = "";
		loadurl = "load_allreqlist.php";
		var gridht  = $(window).height()-($("#headerpanel").height()+$(".panel-heading").height()+180);
		if(loadurl != "" && loadurl != null && pid >0 ){
			
		$.ajax({
					url: loadurl,
					dataType: 'json',
					type: 'POST',
					data:{pid:pid,batch:batch,cid:cid,statusid:statusid,sorder:sorder,scid:scid,pt:$("#part_type4").val(),rt:$("#request_type4").val()},
					success: function (res) {
						$.unblockUI();
						hot4.selectCell(0,selgridcol); 
						result = res;
						var stdata = JSON.stringify(res.data);
						var data = JSON.parse(stdata);
						hot4.loadData(res.data); 
						var resultdata =[];
						var len = hot4.countRows();
						tableData = hot4.getData();
						$("#totalcnt4").val(len);
						$("#filtercnt4").html("Total Count :<b>"+len+"</b>");
						
							 hot4.updateSettings({
							  height: gridht
							});
						if(len >0){
								if(statusid > 0 && statusid != "" && sorder != '' && sorder != null){
									hot4.selectColumns((Number(sorder)+Number(1)));
								}else{
										hot4.selectCell(0,selgridcol); 
								}
								
							   showsavmsg("green","Data loaded successfully.");
						}else{
							showsavmsg("green"," Data is not available.");
						}

					}
				  });
			}
		}
			   
