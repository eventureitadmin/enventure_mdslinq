var tooltip, 
	hidetooltiptimer;
var bootboxobj ={
		'font-size': '15px',
		'text-align': 'center',
		'margin-top': function (){
			var w = $( window ).height();
			var b = $(".modal-dialog").height();
			// should not be (w-h)/2
			var h = (w-b)/3;
			return h+"px";
		}
	};
var cfmbtnobj =  {
					confirm: {
						label: 'Yes',
						className: 'btn-success pull-left yesbtn'
					},
					cancel: {
						label: 'No',
						className: 'btn-danger nobtn'
					}
				};
var cfmbtnobj1 =  {
					confirm: {
						label: 'Yes',
						className: 'btn-success pull-left yesbtn1'
					},
					cancel: {
						label: 'No',
						className: 'btn-danger nobtn1'
					}
				};

		function gettimeforfield(value,type){
			var d1arr = value.split("-");
			var month = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
			var monnum = month.indexOf(d1arr[1]);
			var yr = "20"+d1arr[2];
			var mn = monnum;
			var da = d1arr[0];
			var newD1 ="";
			if(type =="duedate"){
				 newD1 = new Date(yr,mn,da).getTime();
			}else if(type =="compliancetgtdate"){
			var cmpldate = new Date(yr, mn, da);
				cmpldate.setDate(cmpldate.getDate()-60);
			newD1 = getday(cmpldate.getDate())+"-"+month[cmpldate.getMonth()]+"-"+cmpldate.getFullYear().toString().substr(-2);
			}else if(type =="sqadate"){
	
				var numWeeks = 2;
				var now = new Date(yr, mn, da);
				now.setDate(now.getDate()- numWeeks * 7);

				var newD1 = getday(now.getDate()) + '-' +month[(now.getMonth())] + '-' + now.getFullYear().toString().substr(-2);
			}
			return newD1;
		}
function showloadimg(){
		$.blockUI({ 
			message: '<img src="images/loading.gif" />',
			css: { 
            border: 'none', 
            padding: '10px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff',
			fontSize:'10px'
        } });
	}
	function showAlertMsg(msg){
		 bootbox.alert({
				 size:"small",
				message: msg,
				backdrop: true,
				 button:{label: 'OK',
							className: 'pull-left yesbtn'},
			 
		 }).find('.modal-content').css({
    			'font-size': '15px',
    			'text-align': 'center',
				'margin-top': function (){
					var w = $( window ).height();
					var b = $(".modal-dialog").height();
					// should not be (w-h)/2
					var h = (w-b)/3;
					return h+"px";
				}
			});
	}
function showterrormsg(msg){
		 bootbox.alert({
				 size:"large",
				message: msg,
				backdrop: true,
				 button:{label: 'OK',
							className: 'pull-left yesbtn'},
			 
		 }).find('.modal-content').css({
    			'font-size': '15px',
    			'text-align': 'center',
				'margin-top': function (){
					var w = $( window ).height();
					var b = $(".modal-dialog").height();
					// should not be (w-h)/2
					var h = (w-b)/3;
					return h+"px";
				}
			});
	}
	function selectElementText(el){
		var range = document.createRange() // create new range object
		range.selectNodeContents(el) // set range to encompass desired element text
		var selection = window.getSelection() // get Selection object from currently user selected text
		selection.removeAllRanges() // unselect any user selected text (if any)
		selection.addRange(range) // add range to Selection object to select it
	}
	function copySelectionText(){
		var copysuccess // var to check whether execCommand successfully executed
		try{
			copysuccess = document.execCommand("copy") // run command to copy selected text to clipboard
		} catch(e){
			copysuccess = false
		}
		return copysuccess
	}
	function createtooltip(msg){ // call this function ONCE at the end of page to create tool tip object
		tooltip = document.createElement('div')
		tooltip.style.cssText = 
			'position:absolute; background:black; color:white; padding:4px;z-index:10000;'
			+ 'border-radius:2px; font-size:12px;box-shadow:3px 3px 3px rgba(0,0,0,.4);'
			+ 'opacity:0;transition:opacity 0.3s'
		tooltip.innerHTML = msg;
		document.body.appendChild(tooltip)
	}
	function getSelectionText(){
		var selectedText = ""
		if (window.getSelection){ // all modern browsers and IE9+
			selectedText = window.getSelection().toString()
		}
		return selectedText
	}
	function showtooltip(e){
		var evt = e || event
		clearTimeout(hidetooltiptimer)
		tooltip.style.left = evt.pageX - 10 + 'px'
		tooltip.style.top = evt.pageY + 15 + 'px'
		tooltip.style.opacity = 1
		hidetooltiptimer = setTimeout(function(){
			tooltip.style.opacity = 0
		}, 500)
	}
	function getEamDetails(startRow,startCol,pid,catid,partid,usertype,is_form=false,val,field){
		if(is_form){
			usertype = usertype.trim();
			var eamurl = 'geteamdetails.php?rows=&cols=&proj='+pid+'&catid='+catid+"&partid="+partid+"&usertype="+usertype+"&field="+field;
			
		$('#unspscbody').load(eamurl,function(){
				$(".helpBtn").css("display","none");
				$("#eam_data").val(val);
				checkeam(pid,startCol,startRow,"",partid,2,field);
				$('#unspscdialog').modal({show:true});
		});  
		
		
		}else{
			var eamurl = 'geteamdetails.php?rows='+startRow+'&cols='+startCol+'&proj='+pid+'&catid='+catid+"&partid="+partid+"&usertype="+usertype+"&field=";
		$('#unspscbody').load(eamurl,function(){
				$(".helpBtn").css("display","none");
				$("#eam_data").val(val);
				checkeam(pid,startCol,startRow,"",partid,2,"");
				$('#unspscdialog').modal({show:true});

		});  
		
		}
		$('#unspscdialog').on('shown.bs.modal', function () {
				$("#eam_data").focus();
				//$("#saveeambtn").hide();
			
		});
		
	}
	function saveheaders(url){
	   $.ajax({
			url:url,
			dataType: 'html',
			type: 'POST',
			success: function (data) {
				 window.location.reload();
			}  
		 });
	}
	
	function getBrandDetails(startRow,startCol,pid,catid,partid,usertype,is_form=false,val,field){
		if(is_form){
			usertype = usertype.trim();
			var brandurl = 'getbranddetails.php?rows=&cols=&proj='+pid+'&catid='+catid+"&partid="+partid+"&usertype="+usertype+"&field="+field;
			
		$('#unspscbody').load(brandurl,function(){
				$(".helpBtn").css("display","none");
				$("#brand_data").val(val);
				checkbrand(pid,startCol,startRow,"",partid,2,field);
				$('#unspscdialog').modal({show:true});
		});  
	}else{
			var brandurl = 'getbranddetails.php?rows='+startRow+'&cols='+startCol+'&proj='+pid+'&catid='+catid+"&partid="+partid+"&usertype="+usertype+"&field=";
		$('#unspscbody').load(brandurl,function(){
				$(".helpBtn").css("display","none");
				$("#brand_data").val(val);
				checkbrand(pid,startCol,startRow,"",partid,2,"");
				$('#unspscdialog').modal({show:true});
		}); 
     	}
		$('#unspscdialog').on('shown.bs.modal', function () {
				$("#brand_data").focus();
				$("#saveeambtn").hide();
			
		});
	}
function getUnspscDetails(startRow,startCol,pid,catid,partid,usertype,is_form=false,val,field){
		if(is_form){
			usertype = usertype.trim();
		
			var nounmodindex = $("#validationtype17").val();
					var nounmodval = $("#sFld"+nounmodindex).val();
					if(nounmodval == "" || nounmodval == null || nounmodval.length == 0){
						var nonmodname = $("#sFld"+nounmodindex).attr("labelname");
						showAlertMsg("<b>"+nonmodname+"</b>  is empty.So please select "+nonmodname);
					}else{
						
$("#unspscbody").load("getunspscdetails.php", { "proj":pid,"rows":startRow,"cols":startCol,"cols":startCol,"catid":catid,"vals":val,"field":field},function(){	
	
								checkunspsc(pid,catid,startRow,startCol,val,field);
										if($("#unspsctitle").length > 0 ){
											var unspsctitle  = $("#unspsctitle").val();
										}else{
											var unspsctitle  =  "SELECT UNSPSC";
										}
										$("#unspacheader").text(unspsctitle);

									$(".helpBtn").css("display","none");

									$('#unspscdialog').modal({show:true});

							});  
					}
	
		
		}else{
			var unspscurl = 'getunspscdetails.php?rows='+startRow+'&cols='+startCol+'&proj='+pid+'&catid='+catid+"&partid="+partid+"&usertype="+usertype+"&field=";
		$('#unspscbody').load(unspscurl,function(){
				$(".helpBtn").css("display","none");
				$("#search_data").val(val);
				checkunspsc(pid,catid,startRow,startCol,val,"");
				$('#unspscdialog').modal({show:true});

		});  
		
		}
			$('#unspscdialog').on('shown.bs.modal', function () {
				$("#search_data").focus();
			});
	}

	jQuery(function($){
		 $('[data-toggle="tooltip"]').tooltip(); 
		 $('#fimg').on('click', function(){
			 $(".tooltip").tooltip('hide');
			 var src = ($('#fbtn img').attr('src') === '../images/up.jpg') ? '../images/down.jpg' : '../images/up.jpg';
			 $('#fbtn img').attr('src', src);	
			 if(src=='../images/up.jpg'){
				  $('#fbtn').append($("#fpanel"));
				  $("#fpanel").css('display','block');
				 
			 }else{
				$("#fpanel").css('display','none');
				 
			 }
			 $(this).toggleClass('active');
		});
		 $('#himg').on('click', function(){
			  $(".tooltip").tooltip('hide');
			 var src = ($('#hbtn img').attr('src') === '../images/up.jpg') ? '../images/down.jpg' : '../images/up.jpg';
			 $('#hbtn img').attr('src', src);	
			 if(src=='../images/up.jpg'){

				  $('#hbtn').append($("#hpanel"));
				  $("#selformemory_chosen").css("width","280px");
				  $("#iCommodity_chosen").css("width","280px");
				  $("#iCommoditySet_chosen").css("width","280px");
				  $("#selforfreezcolumn_chosen").css("width","280px");
				  if($("#assign_to_chosen").length > 0){
				   $("#assign_to_chosen").css("width","280px");
				  }
				
				  $("#hpanel").css('display','block');
				

			 }else{
				 $("#hpanel").css('display','none');
				  
				
			 }
			 $(this).toggleClass('active');
		});
	 $('.smenu').on('click', function(){
		  $(".tooltip").tooltip('hide');
				 showshortcutmenu();
		 });

	});
