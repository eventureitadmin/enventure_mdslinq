	/**
	 * Load Data
	 */
	function  loadGridData3() {
		
		var result = null;
		 $.blockUI({ 
			message: 'Please wait...'
		}); 
        var totalrows = 0; 
        if(hot3 !="" && hot3 != null && hot3 != undefined){
		    totalrows = hot3.getData().length;
         }
		var loadurl = "";
		var saveurl = "";
		loadurl = "load_unreportedparts.php?batch="+batch;
		saveurl = "save_unrptparts.php?pid="+pid+"&batch="+batch;
		var gridht  = $(window).height()-($("#headerpanel").height()+$(".panel-heading").height()+gridheight);
		if(loadurl != "" && loadurl != null && pid >0 ){
			
			$.ajax({
					url: loadurl,
					dataType: 'json',
					type: 'GET',
					success: function (res) {
						result = res;
						var stdata = JSON.stringify(res.data);
						var data = JSON.parse(stdata);
						hot3.loadData(res.data); 
                    	$.unblockUI();
						var resultdata =[];
						var len = hot3.countRows();
						tableData = hot3.getData();
						$("#totalcnt3").val(res.data.length);
                    	hot3.selectCell(0,2); 
						$("#filtercnt3").html("Total Count :<b>"+len+"</b>");

									
							 hot3.updateSettings({
							  height: gridht
							});
						
						//showsavmsg("green","Data is not available");
						if(saveurl != "" && saveurl != null && res.data.length > 0 && (usertype == 0 || usertype == 1)){
								$.ajax({
								url: saveurl,
								data: {"data": JSON.stringify(tableData)},     
								type: 'POST',
								dataType:'json',
								success: function (res) { 
								
								
									if(res !="" && res != null && res != undefined){
									  var msg = res.msg;
										if(msg !="" && msg != null && msg != undefined){
											if(msg == "success"){
												if(len > 0){
												 showsavmsg("green","Data loaded successfully.");
												}else{
												 showsavmsg("green"," Data is not available");
												}

											}else{
												 showsavmsg("red","Unable to load data.Please try again.");

											}
										}
									}

								 },
								error: function (xhr) {
									showsavmsg("red","Unable to load data.Please try again.");

								}
							});
						}else{
			
								hot3.selectCell(0,2); 
							   $("#filtercnt3").html("Total Count :<b>"+len+"</b>");
							if(hot3.countRows() >0){
								showsavmsg("green","Data loaded successfully.");
							}else{
								showsavmsg("green","Data is not available");
							}
						}

					}, 
					 error: function (xhr) {
						showsavmsg("red","Unable to load data.Please try again.");

					 }  
				  });
			}
		}
 	/***
	 * Shortcut keys 
	 */
	function init3() {
		
	
		/**
		 * Clear filter
		 */
			shortcut.add("ctrl+f5", function() {
			   hot3.deselectCell();
			   $("#clear3").focus();
			   $("#clear3").click();
		       });
		/**
		 * Save data
		 */
			shortcut.add("f1", function() {
			   hot3.deselectCell();
			  $("#save3").focus();
			  $("#save3").click();
			});
		
		/**
		 * Focus on grid
		 */
			shortcut.add("ctrl+g", function() {
			   hot3.selectCell(0,1);
				 
			});
		/**
		 * Show short cut menu 
		 */
			shortcut.add("ctrl+s", function() {
				
				 	  showshortcutmenu();
			   
			});
		/**
		 * Mouse right click
		 */
			shortcut.add("shift+F10", function() {
				var  selobj = hot3.getSelected(); 
				
				if(selobj != "" && selobj != null && selobj != undefined)
				{
					 var selhotrow = hot3.getSelected()[0][0];
					 var selhotcol = hot3.getSelected()[0][1];
					hot3.selectCell(selhotrow,selhotcol);
				}
			   
			});
		
		
			/**
		     * Column sort by ascending
		     */
			 shortcut.add("f3",function(){
				 var selectedcell = hot3.getSelected();
				if(typeof selectedcell === "object"){
					hot3.deselectCell();
					 hot3.updateSettings({
						
						  columnSorting: {
								column:selectedcell[0][1],
								sortOrder: 'asc'
							 }
						});
		 			 hot3.selectColumns(selectedcell[0][1]);
				 }
			 });
		 /**
		  * Column sort by descending
		  */
			 shortcut.add("f4",function(){
				 var selectedcell = hot3.getSelected();
				 
				 if(typeof selectedcell === "object"){
					 hot3.deselectCell();
					 hot3.updateSettings({
						  columnSorting: {
								column:selectedcell[0][1], 
								sortOrder: 'desc'
							 }
						});
					 hot3.selectColumns(selectedcell[0][1]);
		 
				 }
			 });
			shortcut.add("escape",function(){
				 if($('#myModal').is(':visible')){
					var selcolindex = $("#selected_col").val();
					selcolindex = parseInt(selcolindex);
					hot3.selectColumns(selcolindex);
					 
				}
			});
			shortcut.add("alt+h", function() {
				 $("#himg").click();
		   	 });
			shortcut.add("alt+f", function() {
				$("#fimg").click();
		 	 });
		}
		/**
		 * Popup with up and down arrow option
		 */
	  function arrow(dir) {
		  var w = $('.modal-body');
			var activeTableRow = $('.table tbody tr.active')[dir](".table tbody tr");
			if (activeTableRow.length) {
				$('.table tbody tr.active').removeClass("active");
				activeTableRow.prop("class", "active");
			} 
		  
		}

	/**
	 * Remove non ascii
	 */
	function removenonascii(str) {

	  if ((str===null) || (str==='')){
		   return false;
	  }
	 else{
	   str = str.toString();
	 }

	  return str.replace(/[^\x20-\x7E]/g, '');
	}

	 function arrow(dir) {
		var activeTableRow = $('.table tbody tr.active')[dir](".table tbody tr");
		if (activeTableRow.length) {
			$('.table tbody tr.active').removeClass("active");
			activeTableRow.addClass('active');
		} 
	 }
	  $(window).keydown(function (key) {
		if (key.keyCode == 38) {
			arrow("prev");
		}
		if (key.keyCode == 40) {
			arrow("next");
		}
	});
	$(document).ready(function(){
		$("#export3").click(function(){
			if(hot3.countRows() >0){
			$("#unrpt_export_form").submit();
			}

		});
		$("#import").click(function(){
			if($("#unrpt_file").val() ==""){
				$.notify("Please select a file","error");
			}else{
				 $.blockUI({ 
						message: 'Please wait...'
					}); 
	
				var formData = new FormData($("#form_header")[0]);
				  formData.append('unrpt_file', $('#unrpt_file')[0].files[0]);
				  formData.append('batch', $('#batch').val());
				  formData.append('pid', $('#pid').val());
				
				$.ajax({
				   url:"importunreportedparts.php",
				   type: "POST",
				   enctype: 'multipart/form-data',
				   data:  formData,
				   contentType: false,
				   cache:false,async:false,
				   processData:false,
				   beforeSend : function()
				   {
				   },
				   success: function(data)
					{
						var obj = JSON.parse(data);
						if(obj.status ="success"){
							//$.notify("",);
							//showsavmsg("green",obj.msg);
							$.notify(obj.msg,"success");
							$("#unrpt_file").val('');
						loadGridData3();
						}else{
							//showsavmsg("red",obj.msg);
							$.notify(obj.msg,"error");
							 $.unblockUI();
					    
						}
					}
				});
			
			}
			$("#frm_header").validate();
		});
	
		$("#unrpt_file").change(function () {
			var fileExtension = ["xls", "xlsx"];
			if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
				$.notify("Only formats are allowed : "+fileExtension.join(', '),"error");

				$("#unrpt_file").val("");
			}
		});
		init3();
		
		
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var target = $(e.target).attr("href"); 
		  if(target == "#unrptlist"){
			 loadGridData3();	
		  }
		
		});
	    $(".nav-tabs a").click(function(e){
			$(this).tab('show');
	    });
		   	
	/**
	 * Focus on filter popup
	 */
	$('#myModal').on('shown.bs.modal', function () {
    	$(".filtersel").focus();
		
    });
	
	
	/**
	 * Filter popup and on click enter search data based on filtered value
	 */
    $(document).keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if($('#myModal').is(':visible') && keycode == '13'){
				$('.sol-selection-container').hide();
			 	$("#search_btn").click();
	   	}
	});	
	
	
	
	/**
	 * Save data
	 */
		$("#save3").click(function(){
			
			var resultdata = hot3.getData();
			var cnt = hot3.countRows();
			if(cnt > 0){
				var ischecked = $('input[type="checkbox"]').is(':checked'); 
				 $.blockUI({ 
						message: 'Please wait...'
					}); 
					
						var saveurl = "save_unrptparts.php?pid="+pid+"&batch="+batch;
				
						if(saveurl != "" && saveurl != null ){
							$.ajax({
								url: saveurl,
								data: {"data": JSON.stringify(resultdata)},     
								type: 'POST',
								dataType:'json',
								success: function (res) {
									var completedpartcnt = 0;
									var cnt = hot3.countRows(); 
									var rowscnt = hot3.countRows();
										var totalcnt = $("#totalcnt2").val();
										var gridfilteredcnt = $("#gridfilteredcnt").val();
										if(gridfilteredcnt > 0){
											var totalpartscnt = totalcnt-completedpartcnt;
											$("#filtercnt3").html("Filter Count : <b>"+rowscnt+"</b>  /  "+totalpartscnt);

										}else{

											$("#filtercnt3").html("Total Count :<b>"+rowscnt+"</b>");
										}
									
									if(res !="" && res != null && res != undefined){
									  var msg = res.msg;
										if(msg !="" && msg != null && msg != undefined){
											if(msg == "success"){
												showsavmsg("green","Data saved successfully.");

											}else{
												showsavmsg("red","Unable to save data.Please try again.");

											}
										}
									}

								},
								error: function () {
									showsavmsg("red","Unable to save data.Please try again.");
								}
							});
						}
				
			}else{
				showsavmsg("red","Data is not available");
			}
		});
	
          
	 
		//Clear btn click
		$("#clear3").click(function(){
			$('.nav-tabs a[href="#unrptlist"]').tab('show');
			loadGridData3();
		
		});
		
	$("#delete3").click(function(){
		var ischecked = $('input[type="checkbox"]').is(':checked');
		if(ischecked){
					bootbox.confirm({ 
					size: "small",
					message: deleteconfirmmsg,
					centerVertical:1,
					buttons: {
						cancel: {
							label: 'No',
							className: 'btn-danger nobtn'
						},
						confirm: {
							label: 'Yes',
							className: 'btn-success pull-left yesbtn' 
						},

					},
					callback: function(result){ 
						if(result){
								var resultdata = hot3.getData();
							var cnt = hot3.countRows();
							if(cnt > 0){
							var ischecked = $('input[type="checkbox"]').is(':checked'); 
							 $.blockUI({ 
									message: 'Please wait...'
								}); 

									var saveurl = "save_unrptparts.php?pid="+pid+"&batch="+batch;
									$.ajax({
											url: saveurl,
											data: {"data": JSON.stringify(resultdata)},     
											type: 'POST',
											dataType:'json',
											success: function (res) {
												var completedpartcnt = 0;

												var myData =[];
												var cnt = hot3.countRows(); 
												if(ischecked){
													for (var i = 0, ilen = cnt; i < ilen; i++) {
														 var chk = resultdata[i][1];
														 if(chk == "false" || !chk)
														 {
															resultdata[i][1] = false;
															myData.push(i);
														 }else{ 
															  resultdata[i][1] = false;
														 }
													}

													var filtered = resultdata.filter(function (d, ix) {
																return myData.indexOf(ix) >= 0;
															});
															hot3.updateSettings({
																	data:filtered
															});
														 completedpartcnt = filtered.length;
												}

													var rowscnt = hot3.countRows();
													var totalcnt = $("#totalcnt2").val();
													var gridfilteredcnt = $("#gridfilteredcnt").val();
													if(gridfilteredcnt > 0){
														var totalpartscnt = totalcnt-completedpartcnt;
														$("#filtercnt3").html("Filter Count : <b>"+rowscnt+"</b>  /  "+totalpartscnt);

													}else{

														$("#filtercnt3").html("Total Count :<b>"+rowscnt+"</b>");
													}

												if(res !="" && res != null && res != undefined){
												  var msg = res.msg;
													if(msg !="" && msg != null && msg != undefined){
														if(msg == "success"){
															showsavmsg("green","Deleted successfully.");

														}else{
															showsavmsg("red","Unable to delete part(s).Please try again.");

														}
													}
												}

											},
											error: function () {
												showsavmsg("red","Unable to delete part(s).Please try again.");
											}
									
										});}


						}else{
							var cnt1 = hot3.countRows();
							var resultdata = hot3.getData();
							for (var i = 0, ilen = cnt1; i < ilen; i++) {
							resultdata[i][1] = false;
								 hot3.updateSettings({
											data:resultdata
									});
							}
						}
					}
					}).find('.modal-content').css({
    			'font-size': '15px',
    			'text-align': 'center',
				'margin-top': function (){
					var w = $( window ).height();
					var b = $(".modal-dialog").height();
					// should not be (w-h)/2
					var h = (w-b)/2;
					return h+"px";
				}
			});
			}else{
						$.notify("Please click checkbox to delete part(s)","error");
						}
			});
	});
