function showcustomerdata(pid,batchid,fieldindex,selid=0,selval=""){
	
	$.ajax({
		url: "getcustdetailsbyrequestor.php",
		type:"POST",
		dataType: "html",
		data: {pid:pid,batchid:batchid,requestorid:selval,selid:selid},
		success: function( data ) {
			$("#sFld"+fieldindex).html("");
			$("#sFld"+fieldindex).html(data);
		}
	});
	
}
			function formsubmit(formname,divname,field){
				$("#"+formname).validate({
				 submitHandler: function () {
						$.ajax({
							type:"POST",
							url:"savedata.php",
							data:$("#"+formname).serialize(),
							success: function(response){
							
								if(response !=""){
									var obj = JSON.parse(response);
									if(obj.status=='success'){
										$.notify(obj.message,"success");
										$("#"+field+"name").val(obj.label);
										$("#"+field).val(obj.value);
										//$("#"+formname).trigger("reset");
										$("#"+divname).css("display","none");
									}
									else{
										showAlertMsg(obj.message);
										//bootbox.alert({size:"small",message:obj.message});
									}
								}else{
									showAlertMsg("Something went wrong. Please try again!");
									//bootbox.alert({size:"small",message:"Something went wrong. Please try again!"});
								}
							}
						});
				  }
				});	
			}
			function hideallform(newfield=""){
								 
				$("#"+newfield+"addreqform-container1").css("display","none");
				$("#"+newfield+"addreqform-container1").find("form").trigger("reset");
				$("#"+newfield+"addreqform-container2").css("display","none");
				$("#"+newfield+"addreqform-container2").find("form").trigger("reset");
				$("#"+newfield+"addreqform-container3").css("display","none");
				$("#"+newfield+"addreqform-container3").find("form").trigger("reset");
											 
			}
			function getautocompletedata(field,newfield,divname,type,is_edit=0,cindex=0,pid,batch,is_req=0,selid=0){
			
				 $("#"+field+"name").autocomplete({
				 source: function( request, response ) {
					 if(is_edit == 1){
					 hideallform();
					 }else{
						 if(type == 4 && $("#request_type").val() == 2){
							 $("#request_type").val("");
							 $("#part_type").val("");
							 resetreqdata(1,1);
						 }
					  hideallform("new");
					 }
					   // Fetch data
					   $.ajax({
						url: "getautocompletedata.php",
						type: 'post',
						dataType: "html",
						data: {
						 search: request.term,type:type
						},
						 minLength: 2,
						success: function( data ) {

									var parsed = JSON.parse(data);
									var newArray = new Array(parsed.length);
									var i = 0;

									parsed.forEach(function (entry) {
										var newObject = {
											label: entry.label,
											value:entry.value,
										};
										newArray[i] = newObject;
										i++;
									});
							if(parsed.length == 0)
							{ 
								$("#"+field+"name").val("");
								$("#"+field).val("");
								$("#"+newfield).val($("#"+field).val());
								if($("#"+newfield).hasClass("error")){
								  $("#"+newfield).removeClass("error");
								}
							
									$("#"+divname).css("display","block");
									$("#"+divname).find("#"+newfield).focus();
								
							}
							else{
								$("#"+divname).val('');
								if(is_edit == 1){
									 hideallform();
									 }else{
									  hideallform("new");
									 }
							}
						response(newArray);
						}
					   });
					  },
					 change: function (e, ui) {
						if (!( ui.item)){ e.target.label = "";
						 $("#"+field+"name").val("");
						 $("#"+field).val("")
										}
					 },
					  select: function (event, ui) {
					   // Set selection

					  $("#"+field+"name").val(ui.item.label); // save selected id to input
					  $("#"+field).val(ui.item.value); // save selected id to input
						  if(is_edit !=1 && $("#request_type").val() == 2){
							 
							  if(type == 1){
								setpreviousdata(ui.item.value, "");
							  }else if(type == 3){
								  setpreviousdata(ui.item.value, $("#newsFld5").val());
							 }
								  
						  }
						  if(is_req ==1){
							  if(is_edit ==1){
						 		 showcustomerdata(pid,batch,cindex,selid,ui.item.value);
							  }else{
								  showcustomerdata(pid,batch,cindex,0,ui.item.value);
							  }
						  }
					   return false;
					  }
					 }).data("ui-autocomplete")._renderItem = function (ul, item) {

									return $("<li></li>")
								.data("ui-autocomplete-item", item)
								.append("<a><table width='100%'><tr><td>" + item.label + "</td></tr></table></a>")
								.appendTo(ul);
								};

			}
