	/**
	 * Scroll bar for shortcut keys menu popup
	 */
	$('#menuModal').on('shown.bs.modal', function () {
		 hot1.deselectCell();
    	$('#menuModal table#menutable tbody td').first().focus();
    });	
	var currCell = $('#menuModal table#menutable td').first();
	$('#menuModal table#menutable').keydown(function (e) {
		console.log(currCell);
		var c = "";
		if (e.which == 39) {
			// Right Arrow
			c = currCell.next();
		} else if (e.which == 37) {
			// Left Arrow
			c = currCell.prev();
		} else if (e.which == 38) {
			// Up Arrow
			c = currCell.closest('tr').prev().find('td:eq(' + currCell.index() + ')');
		} else if (e.which == 40) {
			// Down Arrow
			c = currCell.closest('tr').next().find('td:eq(' + currCell.index() + ')');
		} else if ((e.which == 9 && !e.shiftKey)) {
			// Tab
			e.preventDefault();
			c = currCell.next();
		} else if ( (e.which == 9 && e.shiftKey)) {
			// Shift + Tab
			e.preventDefault();
			c = currCell.prev();
		}
		if (c.length > 0) {
			currCell = c;
			currCell.focus();
		}
	});
	/**
	 * Scroll bar for shortcut keys menu popup
	 */
$('#popupdialog').on('shown.bs.modal', function () {
		 hot1.deselectCell();
    	$('#popupdialog table#envstatustable tbody td').first().focus();
    });	
	var currCell1 = $('#popupdialog table#envstatustable td').first();
	$('#popupdialog table#envstatustable').keydown(function (e) {
	
		var c = "";
		if (e.which == 39) {
			// Right Arrow
			c = currCell1.next();
		} else if (e.which == 37) {
			// Left Arrow
			c = currCell1.prev();
		} else if (e.which == 38) {
			// Up Arrow
			c = currCell1.closest('tr').prev().find('td:eq(' + currCell1.index() + ')');
		} else if (e.which == 40) {
			// Down Arrow
			c = currCell1.closest('tr').next().find('td:eq(' + currCell1.index() + ')');
		} else if ((e.which == 9 && !e.shiftKey)) {
			// Tab
			e.preventDefault();
			c = currCell1.next();
		} else if ( (e.which == 9 && e.shiftKey)) {
			// Shift + Tab
			e.preventDefault();
			c = currCell1.prev();
		}
		if (c.length > 0) {
			currCell1 = c;
			currCell1.focus();
		}
	});
 	/***
	 * Shortcut keys 
	 */
	function init2() {
		
	
		/**
		 * Clear filter
		 */
			shortcut.add("ctrl+f5", function() {
			   hot1.deselectCell();
			   $("#clear").focus();
			   $("#clear").click();
		       });
		/**
		 * Save data
		 */
			shortcut.add("f1", function() {
			   hot1.deselectCell();
			  $("#save2").focus();
			  $("#save2").click();
			});
		
		/**
		 * Focus on grid
		 */
			shortcut.add("ctrl+g", function() {
			   hot1.selectCell(0,selgridcol);
				 
			});
		/**
		 * Show short cut menu 
		 */
			shortcut.add("ctrl+s", function() {
				
				 	  showshortcutmenu();
			   
			});
		/**
		 * Mouse right click
		 */
			shortcut.add("shift+F10", function() {
				var  selobj = hot1.getSelected(); 
				
				if(selobj != "" && selobj != null && selobj != undefined)
				{
					 var selhotrow = hot1.getSelected()[0][0];
					 var selhotcol = hot1.getSelected()[0][1];
					hot1.selectCell(selhotrow,selhotcol);
				}
			   
			});
		
		
			/**
		     * Column sort by ascending
		     */
			 shortcut.add("f3",function(){
				 var selectedcell = hot1.getSelected();
				if(typeof selectedcell === "object"){
					hot1.deselectCell();
					 hot1.updateSettings({
						
						  columnSorting: {
								column:selectedcell[0][1],
								sortOrder: 'asc'
							 }
						});
		 			 hot1.selectColumns(selectedcell[0][1]);
				 }
			 });
		 /**
		  * Column sort by descending
		  */
			 shortcut.add("f4",function(){
				 var selectedcell = hot1.getSelected();
				 
				 if(typeof selectedcell === "object"){
					 hot1.deselectCell();
					 hot1.updateSettings({
						  columnSorting: {
								column:selectedcell[0][1], 
								sortOrder: 'desc'
							 }
						});
					 hot1.selectColumns(selectedcell[0][1]);
		 
				 }
			 });
			shortcut.add("escape",function(){
				 if($('#myModal').is(':visible')){
					var selcolindex = $("#selected_col").val();
					selcolindex = parseInt(selcolindex);
					hot1.selectColumns(selcolindex);
					 
				}
			});
			shortcut.add("alt+h", function() {
				 $("#himg").click();
		   	 });
			shortcut.add("alt+f", function() {
				$("#fimg").click();
		 	 });
		}
		/**
		 * Popup with up and down arrow option
		 */
	  function arrow(dir) {
		  var w = $('.modal-body');
			var activeTableRow = $('.table tbody tr.active')[dir](".table tbody tr");
			if (activeTableRow.length) {
				$('.table tbody tr.active').removeClass("active");
				activeTableRow.prop("class", "active");
			} 
		  
		}

	  $(window).keydown(function (key) {
		if (key.keyCode == 38) {
			arrow("prev");
		}
		if (key.keyCode == 40) {
			arrow("next");
		}
	});
	$(document).ready(function(){
		$(".chosen-select").chosen({search_contains: true,width:"200px"});
		
		init2();
		$("#export2").click(function(){
			 if(hot1.countRows() >0){
			$("#req_export_form2").submit();
			 }

		});
		$("#part_type2").change(function(){
			$("#selparttype2").val($(this).val());
			loadGridData();
		});
		if(usertype == 0){
			$("#save2").hide();
		}
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var target = $(e.target).attr("href"); 
		   if(target == "#comppartslist"){
			   loadGridData();
		  }
		
		});
	    $(".nav-tabs a").click(function(e){
			$(this).tab('show');
	    });
		   	
	/**
	 * Focus on filter popup
	 */
	$('#myModal').on('shown.bs.modal', function () {
    	$("#filtersel").focus();
		
    });
	
	
	/**
	 * Filter popup and on click enter search data based on filtered value
	 */
    $(document).keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if($('#myModal').is(':visible') && keycode == '13'){
				$('.sol-selection-container').hide();
			 	$("#search_btn").click();
	   	}
	});	
	

	/**
	 * Save data
	 */
		$("#save2").click(function(){
			var ischecked = $('input[type="checkbox"]').is(':checked');
			var resultdata = hot1.getData();
			var cnt = hot1.countRows();
			if(cnt > 0){
				 $.blockUI({ 
						message: 'Please wait...'
					}); 
					
						var saveurl = "save_completedparts.php?pid="+pid+"&batch="+batch;
						if(saveurl != "" && saveurl != null ){
							$.ajax({
								url: saveurl,
								data: {"data": JSON.stringify(resultdata)},     
								type: 'POST',
								dataType:'json',
								success: function (res) {
									if(usertype == 1){
										var completedpartcnt = 0;
										var myData =[];
										var cnt = hot1.countRows(); 
										//if(ischecked){
											for (var i = 0, ilen = cnt; i < ilen; i++) {
												 var chk = resultdata[i][1];
													var status =$.trim(resultdata[i][statusfieldindex]);
												 if(completedstatusarr.indexOf(status) >-1 )
												 {
													 myData.push(i);
													 resultdata[i][1] = false;
												 }
											}
										//}
											var filtered = resultdata.filter(function (d, ix) {
														return myData.indexOf(ix) >= 0;
													});
													hot1.updateSettings({
															data:filtered
													});
												 completedpartcnt = filtered.length;
									}
										var rowscnt = hot1.countRows();
										var totalcnt = $("#totalcnt2").val();
										var gridfilteredcnt = $("#gridfilteredcnt").val();
										if(gridfilteredcnt > 0){
											var totalpartscnt = totalcnt-completedpartcnt;
											$("#filtercnt2").html("Filter Count : <b>"+rowscnt+"</b>  /  "+totalpartscnt);

										}else{

											$("#filtercnt2").html("Total Count :<b>"+rowscnt+"</b>");
										}
									
									if(res !="" && res != null && res != undefined){
									  var msg = res.msg;
										if(msg !="" && msg != null && msg != undefined){
											if(msg == "success"){
												showsavmsg("green","Data saved successfully.");

											}else{
												showsavmsg("red","Unable to save data.Please try again.");

											}
										}
									}

								},
								error: function () {
									showsavmsg("red","Unable to save data.Please try again.");
								}
							});
						}
				
			}else{
				showsavmsg("green","Data is not available");
			}
		});
	
          
	 
		//Clear btn click
		$("#clear2").click(function(){ 
				$("#selparttype2").val("");
				$("#selreqtype2").val("");
				$("#part_type2").val("").trigger('chosen:updated');
				$("#request_type2").val("").trigger('chosen:updated');
			if(statusid >0){
				window.location.href="home.php?batch="+batch;
			}else{
				$('.nav-tabs a[href="#comppartslist"]').tab('show');
				loadGridData();
			}
			
		});

	});


	function  loadGridData() {
	
		var result = null;
		 $.blockUI({ 
			message: 'Please wait...'
		}); 
        var totalrows = 0; 
        if(hot1 !="" && hot1 != null && hot1 != undefined){
		    totalrows = hot1.getData().length;
         }
		var loadurl = "";
		var saveurl = "";
		loadurl = "load_completedparts.php";
		saveurl = "save_completedparts.php?pid="+pid+"&batch="+batch+"&cid="+cid+"&statusid="+statusid+"&sorder="+sorder+"&scid="+selcid;
		var gridht  = $(window).height()-($("#headerpanel").height()+$(".panel-heading").height()+180);
		if(loadurl != "" && loadurl != null && pid >0){
			
			$.ajax({
					url: loadurl,
					dataType: 'json',
					type: 'POST',
				data:{"pid":pid,"batch":batch,"statusid":statusid,"sorder":sorder,"scid":selcid,"pt":$("#part_type2").val(),"rt":$("#request_type2").val()},
					
					success: function (res) {
						result = res;
						var stdata = JSON.stringify(res.data);
						var data = JSON.parse(stdata);
						hot1.loadData(res.data); 
                    	$.unblockUI();
						var resultdata =[];
						var len = hot1.countRows();
						tableData = hot1.getData();
						$("#totalcnt2").val(res.data.length);
										
							 hot1.updateSettings({
							  height: gridht
							});
	
						if(statusid > 0 && statusid != ""){
							if(sorder != '' && sorder != null){
								 hot1.selectColumns((Number(sorder)+Number(1)));
							}
							$("#filtercnt2").html("Filter Count: <b>"+len+"</b> / "+result.totalcnt);
						}else{
							hot1.selectCell(0,selgridcol); 
							$("#filtercnt2").html("Total Count :<b>"+len+"</b>");
						}
						if(saveurl != "" && saveurl != null && res.data.length > 0 && usertype > 0){
								$.ajax({
								url: saveurl,
								data: {"data": JSON.stringify(tableData)},     
								type: 'POST',
								dataType:'json',
								success: function (res) { 
								
									if(res !="" && res != null && res != undefined){
									  var msg = res.msg;
										if(msg !="" && msg != null && msg != undefined){
											if(msg == "success"){
												if(len > 0){
												 showsavmsg("green","Data loaded successfully.");
												}else{
												 showsavmsg("green"," Data is not available");
												}

											}else{
												 showsavmsg("red","Unable to load data.Please try again.");

											}
										}
									}

								 },
								error: function (xhr) {
									showsavmsg("red","Unable to load data.Please try again.");

								}
							});
						}else{
							if(usertype == 0){
								if(statusid > 0 && statusid != ""){
										if(sorder != '' && sorder != null){
											 hot1.selectColumns((Number(sorder)+Number(1)));
										}
										$("#filtercnt2").html("Filter Count: <b>"+len+"</b> / "+result.totalcnt);
									}else{
										hot1.selectCell(0,selgridcol); 
										$("#filtercnt2").html("Total Count :<b>"+len+"</b>");
									}
								showsavmsg("green","Data loaded successfully.");
							}else{
								showsavmsg("green","Data is not available");
							}
						}

					}, 
					 error: function (xhr) {
						showsavmsg("red","Unable to load data.Please try again.");

					 }  
				  });
			}
		}
