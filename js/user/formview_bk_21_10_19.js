
			 var panelheads= []; 
			 
			 /**
			  * Hide or view panel
			  */
			 function hideviewpanel(panelid,infoid){
				
				 if($("#"+infoid).hasClass("in")){
				 	 $(panelid).val("false");
				  }else{
				 	 $("#"+panelid).val("true");
				  }
			 }
			 /**
			  * show panel
			  */
			 function showpanel(sectionid,infoid){
			 	 $("#"+sectionid).attr("aria-expanded","true");
				 $("#"+infoid).attr("aria-expanded","true");
				 $("#"+infoid).addClass("in");
				 
			 }
			
			 function updatepanelchange(){
				 var panelsarr = [];
			  	 var panelindexs = "";
				 var item ="";
				 $(".headerpanel").each(function( i ) {
					 var panelid = $(this).attr("hpanel");
					  item += ".lipanel"+panelid;
					  panelsarr.push(".lipanel"+panelid);
					
				 });
				 var panelidsdata = panelsarr.join(',');
				 $("#panelids").val(panelidsdata);
				
			 }
			 /**
			  * panel ids order
			  */
			$.fn.orderChildren =  function(order) {
				this.each(function() {
					var el = $(this);
					for(var i = order.length - 1; i >= 0; i--) {
						el.prepend(el.children(order[i]));
					}
				});
				return this;
			}
			 /**
			  * Spell check
			  */
			function spellcheck(checkword,callback){
				$.ajax({
					url: 'formviewspellcheck.php',
					dataType: 'json',
					type: 'post',
					data: {"checkword": checkword},
					success: function( data){
						
						return callback(data);
					}
				});	
			}
			 /**
			  * Check Attribute Value
			  */
			 function checkattributevalue(textvalue,datavalues){
				var datavaluesarr = datavalues.split("~");
				var envheaderid = datavaluesarr[0];
				var commodityid = datavaluesarr[1];
				$.post("checkattributevalue.php",{pagetype:"prod",textvalue:textvalue,envheaderid: envheaderid,commodityid:commodityid},function(data){	
						if(data=='AVAIL'){
							$("#word"+envheaderid).addClass("tdColor");
						}
						else{
							$("#word"+envheaderid).removeClass("tdColor");
						}
				});		
			}
			 
			 /**
			  * Get Fill Rate
			  */
			 function getFillRate(){
					var total_mandatory=0;
					var total_mandatory_filled=0;
					$(".attribute").each(function() {
						if($(this).hasClass("mandatory")){
							if($(this).val()!=''){
								total_mandatory_filled++;
							}
							total_mandatory++;
						}
					});
					var fillratevalue = Math.round(((total_mandatory_filled/total_mandatory)*100));
					
					var fillratecolor = 'red';
					if (!isNaN(fillratevalue)) {
						if (fillratevalue >= maxfillrateqc){
							fillratecolor = 'green';
						}
					}
					if(isNaN(fillratevalue)){
						fillratevalue = "0";
					}
					$("#fillrate").html(fillratevalue+"%").css('background-color', fillratecolor);
				}
				 /**
				  * Get Attribute Value
				  */
				 function getattributevalue(){
					var attr_desp = '';
					$(".attribute").each(function(index) {
						if($(this).val() !=''){
							attr_desp += $(this).val()+", ";
							 
								/*var uomval = $(".uomclass")[index].value;
								if(uomval !=''){
									attr_desp += uomval+", ";
								}*/
							
						}
						/*console.log($(".uomclass"));
						if($(".uomclass")[index] != "undefined"){
							var uomval = $(".uomclass")[index].value;
							attr_desp += uomval+", ";
						}*/
					});	
					 
					if(attr_desp != ''){
						$("#attr_desp").html('');
						$("#attr_desp").html(attr_desp.slice(0,-2));
					}
				}
				  /**
				  * Set panel height
				  */
				 function setpanelheight(infoid){
					  $("#"+infoid).on('shown.bs.collapse', function () {
						 $("#"+infoid).css({        
							'overflow': 'auto',
							'height': '200px'
						});
					});
				 }
				 /**
				  * Show  fillrate alert message 
				  */
				 function showalert(fillrate,btnname){
					$("#alertfillrate").html(fillrate);
					$("#alertbtnname").html(btnname);
					 if(btnname != 'save'){
						$("#overlay").show();
					 }
				}
				 
				 /**
				  * Form view submission
				  */
				 function submitform(whichbtn){
					$("#overlay").hide();
					$("#frm_grid1").submit(); 
				 }
				
				 /**
				  * Get attribute List
				  */
				function getattributelist(selector,descriptor,atrributeword){
					if (!$("#"+selector).data('uiAutocomplete')) {
					var availableAttributes;

					$.post("attributelist.php",{pagetype:"prod",descriptor:descriptor,atrributeword:atrributeword},function(data){	
						var dataarr = data.split("||");
						availableAttributes="[";
						for(var i=0; i<dataarr.length;i++){
							if(i==(dataarr.length-1)){
								availableAttributes += "'"+dataarr[i]+"'";
							}
							else{
								availableAttributes += "'"+dataarr[i]+"',";
							}
						}
						availableAttributes += "]";
						availableAttributes=eval(availableAttributes);
						
					});
					}
				}
				 /**
				  * Get panel hide or show activity
				  */
				 function getpanelsactivity(){
					 hideviewpanel("ihpanel","ihinfo");
					 hideviewpanel("attrpanel","attrinfo");
					 hideviewpanel("ohpanel","ohinfo");
				 }
				 /**
				  * showshortcutmenu
				  */
					function showmenulist(){
						$("#formmenuModal").modal('show');
					}
					function formviewuploadfiles(ids)
					{
					  window.open(fomviewuploadurl+'?pid='+pid+'&ids='+ids,'UploadFiles','toolbar=0,menubar=0,scrollbars=1,width=480,fullscreen=0');
					}
				 /***
				  * Shortcut keys 
				  */
				function shortcutkeys() {
					shortcut.add("f1", function() {
						$("#save").focus();
						$("#save").click();
						});
					shortcut.add("alt+r", function() {
						$("#reject").focus();
						$("#reject").click();
						});
					shortcut.add("alt+c", function() {
						$("#close").focus();
						$("#close").click();
						});
				     shortcut.add("alt+s", function() {
						$("#submit").focus();
						$("#submit").click();
					});
					shortcut.add("ctrl+s", function() {
						$("#shortcutkeylist").focus();
						showmenulist();
					});
				
					shortcut.add("alt+left", function() {
						
						$("#previous").focus();
						$("#previous").click();
					});
					shortcut.add("alt+right", function() {
						$("#next").focus();
						$("#next").click();
					});
					shortcut.add("alt+y", function() {
						$("#yesbtn").focus();
						$("#yesbtn").click();
					});
					shortcut.add("alt+z", function() {
						$("#nobtn").focus();
						$("#nobtn").click();
					});
					shortcut.add("alt+f", function() {
						$(".selheaders").trigger('chosen:activate');
						$(".selheaders").trigger('chosen:open');
					});
					shortcut.add("alt+a", function() {
						if(usertype == 2 || usertype == 3){
						var validationtype15 = $("#validationtype15");
						var  validationtype15 =  $("#validationtype15").val();
					        $("#sFld"+validationtype15).focus();
						showEAM(validationtype15,pid,catid,partid,usertype,15);
						}
					});
					shortcut.add("alt+n", function() {
						if(usertype == 2){
								var validationtype17 = $("#validationtype17");
								if(validationtype17.length > 0){
									validationtype17 = validationtype17.val();
									  $("#sFld"+validationtype17).focus();
									showEAM($("#validationtype17").val(),pid,catid,partid,usertype,17);
								}
						}
					});
					shortcut.add("alt+p", function() {
						if(usertype == 2){
						 $("#prodcommpopup").modal("show");
						}
					
					});
					shortcut.add("alt+u", function() {
						var validationtype13 = $("#validationtype13");
						if(validationtype13.length > 0){
					  		 $("#validationtype13").focus();
						showEAM($("#validationtype13").val(),pid,catid,partid,usertype,13);
							}
					});
					shortcut.add("alt+b", function() {
						if(usertype == 2 || usertype == 3){
							var validationtype18 = $("#validationtype18");
							if(validationtype18.length > 0){
								$("#validationtype18").focus();
								showEAM($("#validationtype18").val(),pid,catid,partid,usertype,18);
							}
						}
					});
					
				}
				
			 window.onload = function () { 
			$(".uomclass").change(function(value) {
				var cuurenval = $(this).val();
				cuurenval = cuurenval.split("-(");
				$(this).val(cuurenval[0]);
			});
			 /**
			  * Spell check
			  */
			$(".spellcheck").each(function() {
				var changedvalue = $(this).val();
				var textboxid = $(this).attr("id");
				var textboxid = textboxid.substr(4);
				if(changedvalue != ''){
					spellcheck(changedvalue, function(spellcheckstatus) {

						$("#spellchecklabel_"+textboxid).html(spellcheckstatus);
					});		
				}
			});
				$(".attributeclass").each(function() {
					var textvalue = $(this).val();
					var datavalues = $(this).attr("data-type");
					checkattributevalue(textvalue,datavalues);
				});
				getFillRate();
				getattributevalue();
			}
			function fileupload(ids,fname)
			{
				$("#sFld"+ids).val(fname);
				$("#lbl_"+ids).html(fname);
			}
			/**
			 * change non ascii to ascii word
			 */
			function containsAllAscii(str) {
				return  /^[\000-\177]*$/.test(str) ;
			} 			 

		/**
		 * Remove non ascii
		 */
		function removenonascii(str) {
			if ((str===null) || (str==='')){
				   return false;
			 }
			 else{
			   str = str.toString();
			 }

			  return str.replace(/[^\x20-\x7E]/g, '');
		}
		
		
		function validate(pattern,obj,msg=""){
			if(obj.val() != "" && obj.val() != null){
			var isvalid = checkvaliddata(obj.val(),pattern,obj,msg);
				if(isvalid)
				{
					obj.removeClass("errormsg");
				}else{
					 obj.addClass("errormsg");
					 var currval = obj.val();labelname = obj.attr('labelname');
					if(currval.length > 0 && currval != "" && currval != null ){
					 $.toaster({ priority : 'danger',title:'Error',message :labelname+"-"+msg,'timeout': 6000, 'fade': 'slow'});
					}
				}
			}
			
		}

function notvalid(obj,msg){
	if(obj !="" && obj != null && obj != undefined){
		obj.css("border","1px solid #FF0000");
		obj.css("border-radius","4px");
		obj.css("padding","5px");
		obj.prop("title",msg);
	}
	
}
function valid(obj){
	if(obj !="" && obj != null && obj != undefined){
		obj.css("border","1px solid #DCDCDC");
		obj.css("border-radius","4px");
		obj.css("padding","5px");
		obj.prop("title","");
	
	}
	
}
function checkvaliddata(val,expression,obj,msg){
		
	if(val !="" && val != undefined && val != null){
		val = val.trim();
		var isvaliddata = expression.test(val);
		if(!isvaliddata){
			notvalid(obj,msg);
			return false;
		}else{
			valid(obj);
			return true;
		}
	}
	
}
	function checkerrorsnew(validationtypes){
		 var isvalid ="";
		 for( var j= 0;j< validationtypes.length;j++){
			var optionvalue = validationtypes[j].optionvalue;
			var optionlabel = validationtypes[j].optionlabel;
			var regexp = validationtypes[j].reg_exp;
			 var regexpobj = new RegExp(regexp);
			$(".validation"+optionvalue).each(function(index,obj) {
				var cval = $(this).val();
					cval = cval.trim();
				if(cval != "" && cval != null && cval != undefined){
					validate(regexpobj, $(this)," Allow only "+optionlabel);
				}

			});
		 }


	}
	 function validateoh(regexp,optionlabel,obj){
		 var regexpobj = new RegExp(regexp);
			if(obj.val() != "" && obj.val() != null && obj.val() !=  undefined)
			{
				validate(regexpobj,obj," Allow only "+optionlabel);
			}else{
				 obj.css("border","1px solid #ccc");
			}

	 }

	function selectElementText(el){
		var range = document.createRange() // create new range object
		range.selectNodeContents(el) // set range to encompass desired element text
		var selection = window.getSelection() // get Selection object from currently user selected text
		selection.removeAllRanges() // unselect any user selected text (if any)
		selection.addRange(range) // add range to Selection object to select it
	}
	function copySelectionText(){
		var copysuccess // var to check whether execCommand successfully executed
		try{
			copysuccess = document.execCommand("copy") // run command to copy selected text to clipboard
		} catch(e){
			copysuccess = false
		}
		return copysuccess
	}
	var tooltip, // global variables oh my! Refactor when deploying!
	hidetooltiptimer

	function createtooltip(msg){ // call this function ONCE at the end of page to create tool tip object
		tooltip = document.createElement('div')
		tooltip.style.cssText = 
			'position:absolute; background:black; color:white; padding:4px;z-index:10000;'
			+ 'border-radius:2px; font-size:12px;box-shadow:3px 3px 3px rgba(0,0,0,.4);'
			+ 'opacity:0;transition:opacity 0.3s'
		tooltip.innerHTML = msg;
		document.body.appendChild(tooltip)
	}
	function getSelectionText(){
		var selectedText = ""
		if (window.getSelection){ // all modern browsers and IE9+
			selectedText = window.getSelection().toString()
		}
		return selectedText
	}
	function showtooltip(e){
		var evt = e || event
		clearTimeout(hidetooltiptimer)
		tooltip.style.left = evt.pageX - 10 + 'px'
		tooltip.style.top = evt.pageY + 15 + 'px'
		tooltip.style.opacity = 1
		hidetooltiptimer = setTimeout(function(){
			tooltip.style.opacity = 0
		}, 500)
	}
		
	function showfocus(){
		 var selval = $("#selheaders").val();
		setTimeout(function(){  $("#sFld"+selval).focus(); }, 0);
	}
	function showEAM(typenum,pid,catid,partid,usertype,vadtype){
		
		if(vadtype == 15){
			$("#unspacheader").text("SELECT  Manufacturer");
			var eamval = $("#sFld"+typenum).val();
			getEamDetails("","",pid,catid,partid,usertype,true,eamval,typenum);
		}
		if(vadtype == 17){
			$("#unspacheader").text("SELECT  Manufacturer");
			var nounval = $("#sFld"+typenum).val();
			var partid	= $("#partid").val();
			getnoundetails(pid,"","",nounval,catid,typenum,usertype,partid);
		}
		if(vadtype == 18){
			$("#unspacheader").text("SELECT  Brand");
			var eamval = $("#sFld"+typenum).val();
			getBrandDetails("","",pid,catid,partid,usertype,true,eamval,typenum);
		}
		if(vadtype == 13){
			
			var eamval = $("#sFld"+typenum).val();
			getUnspscDetails("","",pid,catid,partid,usertype,true,eamval,typenum);
		}
				
	}
		function fillme(vals) {
			if(vals != "" && vals != null){
				$("#sFld"+prodcommindex).val(vals);
				$("#prodcommpopup").modal("hide");
				$("#sFld"+prodcommindex).focus();
			}
		}
