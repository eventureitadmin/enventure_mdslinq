	/**
	 * Show filter popup
	 */
	function openModal(){
     
		$("#filtersel").val("");
		$('.selmasterdata').css("display:none");
		$('.selmasterdata').find('option').remove().end();
		$("#search_text_div").hide();
		$("#filtersel").focus();
		var filtersel = $("#filtersel").val();
		var selection = hot.getSelected();
		if(filtersel !="" && filtersel != null && filtersel != undefined && filtersel == "text" && filtersel == "multipleenv"){
			 $("#search_text").show();
		}else{
		  $("#search_text").hide();
		}
		if(selection !="" && selection != null && selection != undefined){
		  var selcol = selection[0][1];hot.selectCell(0,selcol);
			
				var colname = $('th.ht__highlight').find("a.colHeader").html();
				$("#filterheader").html('');
				$("#filterheader").html("Filter by value : <b>"+colname+"</b>");
				$("#selected_col").val(selcol);
				 showsearchbox();
				 hot.deselectCell();
			
		}
    }
	function getMultipleENVRows(search_text,selcol,search){
		var rows = [];
		if(search_text.length > 0){
			var searchtxt = search_text.split(",");
			for(var k=0;k<searchtxt.length;k++){
				var queryResult1 = search.querycolumn(searchtxt[k],4);
				if(typeof queryResult1 == "object" && queryResult1.length >0){
				 rows.push(queryResult1[0].row);
				}
			}
		}
		return rows;
	}
	/**
	 * if select type as contains then hide searchbox on filter popup
	 */
	function hidesearchbox(){
		
		 showsearchbox();
		 $('.selmasterdata').find('option').remove().end();
		 $(".selmasterdata").remove().end();

	}
	function setfilterheight(){
		var solheight = $(".sol-selection").height();
		if(solheight > 0){
			var popupheight = solheight+150;
		   $("#filterbody").css("height",popupheight);
		}else{
		  $("#filterbody").css("height",'150');
		 }
	}
	/**
	 * Show searchbox on filter popup 
	 */
	function showsearchbox(){
		var selchkdata = "";
		var selcol =$("#selected_col").val();
		var uniquecolumnData = getUniqueCellData(selcol);
		 $('.selmasterdata').find('option').remove().end();
		 $(".selmasterdata").remove().end();
		var isselectexists = $(".selmasterdata").length;
		  if(isselectexists <= 0){
				$("#selectbox").append('<select id="my-select" name="smasterdata[]" class="selmasterdata" multiple="multiple"></select>');
				
		  }
		  for(var k=0;k<uniquecolumnData.length;k++){
			  selchkdata +="<option value='"+uniquecolumnData[k]+"'>"+uniquecolumnData[k]+"</option>";
		  }

			$('.selmasterdata').append(selchkdata);

			  $('.selmasterdata').searchableOptionList({
					showSelectAll: true,
				 	maxHeight: '250px',
				  	events: {
						onOpen: function(){
							setfilterheight();
						},
						onClose: function(){
							setfilterheight();
						}
					}
				});
		$("#myModal").modal("show");
	}

 
	/**
	 * Get Unique CellData
	 */
	function getUniqueCellData(column){
		var dataAtCol = hot.getSourceDataAtCol(column);
		return dataAtCol.filter((value, index, self) => self.indexOf(value) === index && value != "");
	}
	/**
	 * Get RowsFromSelectedData
	 */
	function getRowsFromSelectedData(selcol) {
		var rows = []; var selectedDataArr = [];
		var dataarr = hot.getSourceDataAtCol(selcol);
		 $('.sol-checkbox:checked').each(function (index, value) { 
			
			 selectedDataArr.push($(this).val());
		 });
		for(var i = 0, l = dataarr.length; i < l; i++) {
			if(selectedDataArr.indexOf(dataarr[i]) > -1){
				rows.push(i);

			}	
		 }
		
		return rows;
	  }
	/**
	 * Get RowsFromObjects
	 */
	function getRowsFromObjects(queryResult) {
		var rows = [];

		for (var i = 0, l = queryResult.length; i < l; i++) {
			 rows.push(queryResult[i].row);
		}

		return rows;
	  }
	/**
	 * Get EmptyDataRows
	 */
	  function getEmptyDataRows(col) {
		  var  rows = [];

	   var hotdata = hot.getData();
	   for(var i=0;i<hotdata.length;i++){

			   if((hotdata[i][col] == "" || hotdata[i][col] == null) && typeof hotdata[i][col] != undefined ){
				 rows.push(i);
			   }

	   }
		return rows;
	  }
	/**
	 * Get Not Empty Data Rows
	 */
	 function getNotEmptyDataRows(col) {
		  var  rows = [];
		  var hotdata = hot.getData();
	   for(var i=0;i<hotdata.length;i++){
			  if(hotdata[i][col] != "" && hotdata[i][col] != null && typeof hotdata[i][col] != undefined ){
				 rows.push(i);
			   }

	   }

		return rows;
	  }
	/**
	 * uploadfiles
	 */
	function uploadfiles(curRow,curCol)
	{
	
		var iHeader=$("#iHeader").val();
		var iValue=$("#iValue").val();
			window.open('upload.php?pid='+pid+'&curRow='+curRow+'&curCol='+curCol,
						'Upload     Files','toolbar=0,menubar=0,scrollbars=1,width=480,fullscreen=0');

	}  
	/**
	 * fileupload
	 */
	function fileupload(curRow,curCol,fname)
	{
	   hot.setDataAtCell(curRow,curCol,fname);

	}
	/**
	 * checkallofmine
	 */
	function checkallofmine(chk)
	{  
		var cnt = hot.countRows();
		var myData = hot.getData();
		for (var i = 0, ilen = cnt; i < ilen; i++) {
			myData[i][2] =chk;
		}
		hot.updateSettings({
				data:myData
	    });
	   $("input[class=checker]").prop("checked",chk);		
		
	}
	/**
	 * selunselall
	 */
   function selunselall(is_checked){
	 $("input[name=chkforhide]").each(
		function() 
		{
		   if($(this).is(":disabled")) {
			 this.checked =  true;
			}else{
			 this.checked = is_checked;
			}

		});   
	     $("input[class=shchkall]").prop("checked",is_checked);
	  
	}
	/**
	 * hideshow
	 */
	function hideshow(iType,iprid)
	{
		var sHeaderary=new Array();
		var freezcolactive = $("#freezcolactive").is(":checked");
		var s = 0;
		$("input[name=chkforhide]").each(
		function() 
		{
			if(this.checked==true)
			{

			sHeaderary[s]=this.value;
			s++;
			}

		});  
		var saveheaderurl = "save_headers.php?pid="+pid+"&sHeaderary="+sHeaderary+"&iType="+usertype+"&userid="+userid+"&freezcolactive="+freezcolactive;
		if(freezcolactive && sHeaderary.length > 5){
			bootbox.confirm({
					message: freezcolmsg,
					buttons: {
						confirm: {
							label: 'Yes',
							className: 'btn-success'
						},
						cancel: {
							label: 'No',
							className: 'btn-danger'
						}
					},
					callback: function (result) {
						if(result){
						   saveheaders(saveheaderurl);
						 }
					}
			});
		}else{
			 saveheaders(saveheaderurl)
		}
		/* $.ajax({
			url:"save_headers.php?pid="+pid+"&sHeaderary="+sHeaderary+"&iType="+usertype+"&userid="+userid,
			dataType: 'html',
			type: 'POST',
			success: function (data) {
				 window.location.reload();
			}  
		 });*/
	}

	/**
	 * Get colsheaderss
	 */
	   function getcolsheaders(iType,iprid)
	   {
		    $("#colheadersmodal").modal("show");
	   }
	/**
	 * Spellcheck
	 */
		function spellcheck(checkword,callback){
			$.ajax({
				url: 'spellcheck.php',
				dataType: 'json',
				type: 'post',
				data: {"checkword": checkword},
				success: function( data){

					return callback(data);
				}
			});	
		}
	/**
	 * firstRowRenderer
	 */
		function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
		  Handsontable.renderers.TextRenderer.apply(this, arguments);
		  td.style.fontWeight = 'bold';
		  td.style.color = 'red';
		}
	/**
	 * showshortcutmenu
	 */
		function showshortcutmenu(){
			
			$("#menuModal").modal('show');
	
		}
	function checkmandatoryval(resultdata,cnt) {
		var isvalidnounmod =true;
		for (var i = 0, ilen = cnt; i < ilen; i++) {
			var chk = resultdata[i][2];
			 if(chk && chk !=="false")  
			 {
				 if(resultdata[i][prodcommindex] == ""){
					 var errmsg = 'Please provide ';
					for(var key in mandatoryfields){
						  if(resultdata[i][key] == ""){
							 var mandataname = mandatoryfields[key];
							 isvalidnounmod = false;
							 errmsg += mandataname+",";

						  }

					 }
					 if(!isvalidnounmod){
						   errmsg = errmsg.slice(0, -1);
						$.toaster({ priority : 'danger',message :errmsg});
							return false;
						}
				 }
			 }
		}
		return true;
	}
	/**
	 * fileuploadunspsc
	 */
		 function fileuploadunspsc(curRow,curCol,fname,fname1)
        { 
			if(hot.getSettings().columns[curCol].type == "validation13"){
				   hot.setDataAtCell(curRow,curCol,fname1);
			
				 var newcol = curCol-1;
				  if(hot.getSettings().columns[newcol].type == "validation14"){
				   hot.setDataAtCell(curRow,newcol,fname);
				 }
			 }
			
			 if(hot.getSettings().columns[curCol].type == "validation14"){
				   hot.setDataAtCell(curRow,curCol,fname);
			
			 	 var newcol = curCol+1;
				 if(hot.getSettings().columns[newcol].type == "validation13"){
				   hot.setDataAtCell(curRow,newcol,fname1);
				 }
			 } 
			hot.updateSettings({
				 data:hot.getData()
			});	
			
        }
	/**
	 * fileuploadnm
	 */
        function fileuploadnm(curRow,curCol,fname,fname1)
        {
       		$(".iCommoditySet").val(fname).trigger("chosen:updated");
	    }
    	/**
		 * change non ascii to ascii word
		 */
		function containsAllAscii(str) {
			return  /^[\000-\177]*$/.test(str) ;
		}
	
	/**
	 * Load Data
	 */
	function  loadGridData() {
		var result = null;
		 $.blockUI({ 
				message: 'Please wait...'
			}); 
        var totalrows = 0; 
        if(hot !="" && hot != null && hot != undefined){
		    totalrows = hot.getData().length;
         }
		var loadurl = "load_qarework.php?pid="+pid+"&isk="+isk+"&catid="+catid+"&usertype="+usertype+"&batch="+batch+"&reworkbatch="+reworkbatch;
		var saveurl = "save_qarework.php?pid="+pid+"&isk="+isk+"&catid="+catid+"&usertype="+usertype+"&batch="+batch+"&reworkbatch="+reworkbatch;
			$.ajax({
					url: loadurl,
					dataType: 'json',
					type: 'GET',
					success: function (res) {
						result = res;
						var stdata = JSON.stringify(res.data);
						var data = JSON.parse(stdata);
						hot.loadData(res.data); 
						var resultdata =[];
					
						var len = hot.countRows();
						tableData = hot.getData();
						$("#totalcnt").val(res.data.length);
						$("#filtercnt").html("Total Count :<b>"+len+"</b>");
						
						if(saveurl != "" && saveurl != null && res.data.length > 0){
								$.ajax({
								url: saveurl,
								data: {"data": JSON.stringify(tableData)},     
								type: 'POST',
								dataType:'json',
								success: function (res) { 
									hot.selectCell(0,3); 
									
									if(res !="" && res != null && res != undefined){
									  var msg = res.msg;
										if(msg !="" && msg != null && msg != undefined){
											if(msg == "success"){
												if(len > 0){
												 showsavmsg("green","Data loaded successfully.");
												}else{
												 showsavmsg("green"," Data is not available");
												}

											}else{
												 showsavmsg("red","Unable to load data.Please try again.");

											}
										}
									}

								 },
								error: function (xhr) {
									showsavmsg("red","Unable to load data.Please try again.");

								}
							});
						}else{
							showsavmsg("red","Data is not available");
						}

					}, 
					 error: function (xhr) {
						showsavmsg("red","Unable to load data.Please try again.");

					 }  
				  });
		
	}
 	/***
	 * Shortcut keys 
	 */
	function init() {
		
		/**
		 * Select column
		 */
			shortcut.add("alt+shift+m", function() {
				
				  	  $(".selformemory").focus();
				 	  hot.deselectCell();
					  $(".selformemory").trigger('chosen:open');
				 
		 });
			
		/**
		 * Select Noun/Modifier
		 */
			shortcut.add("alt+shift+n", function() {
				 
					 	 hot.deselectCell();
						$(".iCommodity").focus();
				 		$(".iCommodity").trigger('chosen:open');
				 
				
			});
		/**
		 * Select schema
		 */
			shortcut.add("alt+shift+t", function() {
				 
					  hot.deselectCell();
					 $(".iCommoditySet").focus();
					 $(".iCommoditySet").trigger('chosen:open');
				
			});
		/**
		 * Save schema
		 */
			shortcut.add("alt+t", function() {
				   hot.deselectCell();
					$(".iCommoditySet").blur();
				 	$("#saveschema").focus();
					$("#saveschema").click();
			    
			});
		/**
		 * Clear filter
		 */
			shortcut.add("ctrl+f5", function() {
			
						$("#clear").focus();
						$("#clear").click();
				 
				
			});
		/**
		 * Save data
		 */
			shortcut.add("f1", function() {
			        hot.deselectCell();
					  $("#save").focus();
					  $("#save").click();
				
				
			});
		/**
		 * Hide and view columns popup hide/show
		 */
			shortcut.add("f6", function() {
				
					  hot.deselectCell();
					  $("#getcolsheaders").focus();
					  $("#getcolsheaders").click();
					
			});
		/**
		 * Focus on grid
		 */
			shortcut.add("ctrl+g", function() {
			   hot.selectCell(0,3);
				 
			});
		/**
		 * Show short cut menu 
		 */
			shortcut.add("ctrl+s", function() {
				
				 	  showshortcutmenu();
			   
			});
		/**
		 * Mouse right click
		 */
			shortcut.add("shift+F10", function() {
				var  selobj = hot.getSelected(); 
				
				if(selobj != "" && selobj != null && selobj != undefined)
				{
					 var selhotrow = hot.getSelected()[0][0];
					 var selhotcol = hot.getSelected()[0][1];
					hot.selectCell(selhotrow,selhotcol);
				}
			   
			});
		   /**
	     	* Check particular checkbox
	    	*/
			shortcut.add("ctrl+space", function() {
				var  selobj = hot.getSelected(); 
				if(selobj != "" && selobj != null && selobj != undefined)
				{
					 var selhotrow = hot.getSelected()[0][0];
					 var ischk = hot.getDataAtCell(selhotrow,2);
						if(ischk === "false"){ 
							var isspacepressed = "true";
						}else{
							var isspacepressed = "false";
						}
					    hot.setDataAtCell(selhotrow,2,isspacepressed);
						
				    
				}
			});
		 /**
		  * Check / uncheck all checkboxes
		  */
			shortcut.add("f8", function() {
				
		    var ischeck = $('.checker').is(':checked');var chk = "";
			   if(ischeck !="" && ischeck != null && ischeck != undefined){
				   if(ischeck){
					    chk = false;
				   }else{
					    chk = true;
				   }
				  
			   }else{
			    	chk = true;
			   }
				checkallofmine(chk);
				
			});
		
			/**
		     * Column sort by ascending
		     */
			 shortcut.add("f3",function(){
				 
				 var selectedcell = hot.getSelected();
				if(typeof selectedcell === "object"){
					hot.deselectCell();
					 hot.updateSettings({
						
						  columnSorting: {
								column:selectedcell[0][1],
								sortOrder: 'asc'
							 }
						});
		 			 hot.selectColumns(selectedcell[0][1]);
				 }
			 });
		 /**
		  * Column sort by descending
		  */
			 shortcut.add("f4",function(){
				 var selectedcell = hot.getSelected();
				 
				 if(typeof selectedcell === "object"){
					 hot.deselectCell();
					 hot.updateSettings({
						  columnSorting: {
								column:selectedcell[0][1], 
								sortOrder: 'desc'
							 }
						});
					 hot.selectColumns(selectedcell[0][1]);
		 
				 }
			 });
			shortcut.add("escape",function(){
				 if($('#myModal').is(':visible')){
					var selcolindex = $("#selected_col").val();
					selcolindex = parseInt(selcolindex);
					hot.selectColumns(selcolindex);
					 
				}
			});
				shortcut.add("shift+h", function() {
					 $("#himg").click();
				});
				shortcut.add("shift+f", function() {
					$("#fimg").click();
				 });
		}
		/**
		 * Popup with up and down arrow option
		 */
	  function arrow(dir) {
		  var w = $('.modal-body');
			var activeTableRow = $('.table tbody tr.active')[dir](".table tbody tr");
			if (activeTableRow.length) {
				$('.table tbody tr.active').removeClass("active");
				activeTableRow.prop("class", "active");
			} 
		  
		}

	/**
	 * Remove non ascii
	 */
	function removenonascii(str) {

	  if ((str===null) || (str==='')){
		   return false;
	  }
	 else{
	   str = str.toString();
	 }

	  return str.replace(/[^\x20-\x7E]/g, '');
	}
	/**
	 * Load grid data on submit or reject part
	 */
	function loadgriddataagain(currpartdata,arry,partdata){
		var griddata = hot.getData();
		var dataarr = hot.getSourceDataAtCol(0);
		var index = dataarr.indexOf(currpartdata);
		var len = hot.countRows();
		
			if (index > -1) {
			  griddata.splice(index, 1);
				hot.updateSettings({
				 data:griddata
				});
			}
			len = hot.countRows();
		  $("#filtercnt").html("Total Count :<b>"+len+"</b>");
			window.open('about:blank', 'formviewpopup','toolbar=0,menubar=0,scrollbars=1,fullscreen=1');
					 $("#q1").val(currpartdata);
					 $("#q").val(partdata);
					 $("#arry").val(arry);
					 $("#pid").val(pid);
					 $("#isk").val(isk);
					 $("#catid").val(catid);
					 $("#formview").submit();
				
		    
		
	}
	
	function loaddataagain(partid,arry,partdata){
		var iHeader=$("#iHeader").val();
		var result = null;var loadurl= "";
		 $.blockUI({ 
				message: 'Please wait...'
		}); 
		
		var loadurl = "load_qarework.php?pid="+pid+"&isk="+isk+"&catid="+catid+"&usertype="+usertype+"&batch="+batch+"&reworkbatch="+reworkbatch;
		
		if(loadurl != "" && loadurl != null && pid > 0 && isk > 0 && usertype >0 )
		{
			$.ajax({
					url: loadurl,
					dataType: 'json',
					type: 'GET',
					data: {"iHeader": iHeader },
					success: function (res) {
						//var resultdata = hot.getData();
						var currpartdata  = "";
						if(res.data != "" && res.data != null && res.data != undefined){
							var len = res.data[0].length ;
							if(len > 0){
								currpartdata = res.data[0][0];

								hot.updateSettings({
									data:res.data
								});

									if(len > 0){
									 showsavmsg("green","Data loaded successfully.");
									}else{
									 showsavmsg("red"," Data is not available");
									}
							
								window.open('about:blank', 'formviewpopup','toolbar=0,menubar=0,scrollbars=1,fullscreen=1');
												 $("#q1").val(currpartdata);
												 $("#q").val(partdata);
												 $("#arry").val(arry);
												 $("#pid").val(pid);
												 $("#isk").val(isk);
												 $("#catid").val(catid);
												 $("#formview").submit();
							}
						}
					 }, error: function (xhr) {
						showsavmsg("red","Load data error.Please try again....");

					 } 
			});
		}
		
	}
	
function setgridheight(){
	var headerpaneldiv = $("#headerpaneldiv")[0].offsetHeight;
	var footerpaneldiv = $("#footerpaneldiv")[0].offsetHeight;
	var gridheight     = $(window).height()-headerpaneldiv-footerpaneldiv;
	hot.updateSettings({
		height:gridheight
	});
}
function hideshowhead(){
	var diff = $('#headerpaneldiv').offset().top - $('#footerpaneldiv').offset().top;
	var isbtnactive = $("#btnpanel").is(":hidden");
	var headerpaneldiv = $("#headerpaneldiv").height();
	if(isbtnactive){
		diff = diff-$("#btnpanel").height();
	}
	$("#btnpanel").toggle();

}
function hideshowftr(){
	$("#footerpanel").toggle();

}
function saveparts(saveurl,resultdata,ischecked){
	if(saveurl != "" && saveurl != null ){
		$.ajax({
								url: saveurl,
								data: {"data": JSON.stringify(resultdata)},     
								type: 'POST',
								dataType:'json',
								success: function (res) {
									var completedpartcnt = 0;

									var myData =[];
									var cnt = hot.countRows(); 

									if(ischecked){
										for (var i = 0, ilen = cnt; i < ilen; i++) {
											 var chk = resultdata[i][2];
											 if(chk ==="false")
											 {
												myData.push(i);
											 }
										}

											var filtered = resultdata.filter(function (d, ix) {
													return myData.indexOf(ix) >= 0;
												});
											hot.updateSettings({
														data:filtered
												});
											 completedpartcnt = filtered.length;

										var rowscnt = hot.countRows();
										var totalcnt = $("#totalcnt").val();
										var gridfilteredcnt = $("#gridfilteredcnt").val();
										if(gridfilteredcnt > 0){
											var totalpartscnt = totalcnt-completedpartcnt;
											$("#filtercnt").html("Filter Count : <b>"+rowscnt+"</b>  /  "+totalpartscnt);

										}else{

											$("#filtercnt").html("Total Count :<b>"+rowscnt+"</b>");
										}
										showsavmsg("green","Data saved successfully.");

									}else{

										if(res !="" && res != null && res != undefined){
										  var msg = res.msg;
											if(msg !="" && msg != null && msg != undefined){
												if(msg == "success"){
													showsavmsg("green","Data saved successfully.");

												}else{
													showsavmsg("red","Unable to save data.Please try again.");

												}
											}
										}


									}


								},
								error: function () {


									 showsavmsg("red","Unable to save data.Please try again.");


								}
							});
	}
}
function checkattrmandatory(resultdata,cnt) {
	var isvalidnounmod =true;
	for (var i = 0, ilen = cnt; i < ilen; i++) {
		var chk = resultdata[i][2];
		 if(chk && chk !=="false")  
		 {
				for(var key in mandatoryattrfields){
					var oid = mandatoryattrfields[key]['oid'];
					if(resultdata[i][oid] == ""){
						  return false;
					}
				}
		 }
	}
	return true;
}
$(document).ready(function(){
	init();
	if(issdldactive == 1){
			var show_short_dec = document.getElementById('show_short_dec');
			if(typeof show_short_dec === "object"){
				createtooltip("Copied!") // create tooltip by calling it ONCE per page. See "Note" below
				show_short_dec.addEventListener('mouseup', function(e){
				   var selected = getSelectionText() // call getSelectionText() to see what was selected
					if (selected.length > 0){ // if selected text length is greater than 0
						var copysuccess = copySelectionText() // copy user selected text to clipboard
						showtooltip(e)
					}
				}, false);
			}
		}
	$("#logoutid").click(function(){
			$("#hpanel").hide();
		});
	   /**
	    * Hide and view columns popup
		*/
		

		$("input[type='checkbox'].chkforhide").change(function(){
			var a = $("input[type='checkbox'].chkforhide");
			if(a.length === a.filter(":checked").length){
				$("input[type='checkbox'].shchkall").prop('checked',true);
			}else{
				$("input[type='checkbox'].shchkall").removeAttr('checked');
			}
		});
	/**
	 * Focus on filter popup
	 */
	$('#myModal').on('shown.bs.modal', function () {
    	$("#filtersel").focus();
		
    });
	/**
	 * Scroll bar for shortcut keys menu popup
	 */
	$('#menuModal').on('shown.bs.modal', function () {
		 hot.deselectCell();
    	$('#menuModal table#menutable tbody td').first().focus();
    });	
	var currCell = $('#menuModal table#menutable td').first();
	$('#menuModal table#menutable').keydown(function (e) {
		var c = "";
		if (e.which == 39) {
			// Right Arrow
			c = currCell.next();
		} else if (e.which == 37) {
			// Left Arrow
			c = currCell.prev();
		} else if (e.which == 38) {
			// Up Arrow
			c = currCell.closest('tr').prev().find('td:eq(' + currCell.index() + ')');
		} else if (e.which == 40) {
			// Down Arrow
			c = currCell.closest('tr').next().find('td:eq(' + currCell.index() + ')');
		} else if ((e.which == 9 && !e.shiftKey)) {
			// Tab
			e.preventDefault();
			c = currCell.next();
		} else if ( (e.which == 9 && e.shiftKey)) {
			// Shift + Tab
			e.preventDefault();
			c = currCell.prev();
		}
		if (c.length > 0) {
			currCell = c;
			currCell.focus();
		}
	});
	/**
	 * Scroll bar for hide and view columns popup
	 */
	$('#colheadersmodal').on('shown.bs.modal', function () {
		 hot.deselectCell();
		$('#colheadersmodal table#iheaderstable tbody td').first().focus();
		
	});
	var currheaderCell = $('#colheadersmodal table#iheaderstable td').first();
	$('#colheadersmodal table#iheaderstable').keydown(function (e) {
		var hc = ""; 
		if (e.which == 39) {
			// Right Arrow
			hc = currheaderCell.next();
		} else if (e.which == 37) {
			// Left Arrow
			hc = currheaderCell.prev();
		} else if (e.which == 38) {
			// Up Arrow
			hc = currheaderCell.closest('tr').prev().find('td:eq(' + currheaderCell.index() + ')');
		} else if (e.which == 40) {
			// Down Arrow
			hc = currheaderCell.closest('tr').next().find('td:eq(' + currheaderCell.index() + ')');
		} else if ( (e.which == 9 && !e.shiftKey)) {
			// Tab
			e.preventDefault();
			hc= currheaderCell.next();
		} else if ( (e.which == 9 && e.shiftKey)) {
			// Shift + Tab
			e.preventDefault();
			hc = currheaderCell.prev();
		}
		if (hc.length > 0) {
			currheaderCell = hc;
			currheaderCell.focus();
		}
		//check /uncheck checkbox
		 if(e.which == 67){
			 var checkboxobj =  currheaderCell[0].firstElementChild;
				var ischeck = checkboxobj.checked;
				  if(ischeck){
					  checkboxobj.checked = false;
				  }else{
					  checkboxobj.checked = true;
				  }

		 }
		if(e.which == 121){
			e.preventDefault();
			$("#shideshow").click();
		}
		if(e.which == 120){
			var isshchkall = $('.shchkall').is(':checked');var chk = "";
		   if(isshchkall !="" && isshchkall != null && isshchkall != undefined){
			   if(isshchkall){
					chk = false;
			   }else{
					chk = true;
			   }

		   }else{
				chk = true;
		   }
			selunselall(chk);
		}
	});
	
	 $(".prodcomment").focus();
	/**
	 * Filter popup and on click enter search data based on filtered value
	 */
    $(document).keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if($('#myModal').is(':visible') && keycode == '13'){
				$('.sol-selection-container').hide();
			 	$("#search_btn").click();
	   	}
	});	
	
	/**
	 *Search Data by filter type
	 */
	$("#search_btn").click(function(){
		  $("#myModal").modal('hide');
		  var selcol =  $("#selected_col").val();
			
		  var  tData = hot.getData();
		  var search = hot.getPlugin('search');

		  var filtersel =  $("#filtersel").val();
		  var totalcnt = $("#totalcnt").val();
		  var handsontabledata = ""; 
		  var rows = "";
		  var seldata  = $('#my.selmasterdataselect');
		  var filterpartids = "";
		
			if(filtersel != "" && filtersel != null && filtersel != undefined)
			{
				var search_text = $("#search_text").val().trim();
				if(filtersel === "text"){
					var queryResult = search.querycolumn(search_text,selcol);
						rows  = getRowsFromObjects(queryResult);
				 } else if(filtersel === "blank"){
				 	 rows  =  getEmptyDataRows(selcol);
				 }
				else if(filtersel === "notblank"){
					rows  =  getNotEmptyDataRows(selcol);
				}else if(filtersel === "multipleenv"){
					rows  =  getMultipleENVRows(search_text,selcol,search);
				}
				var filtered = "";
				if((filtersel === "blank" || filtersel === "notblank") || ((filtersel === "text" || filtersel === "multipleenv" ) && search_text !="" && search_text != null 
				   && search_text != undefined))
				{
					 filtered = tData.filter(function (d, ix) {
								return rows.indexOf(ix) >= 0;
							});
					
						hot.updateSettings({
								 data:filtered
							});	
					
				}
				
			
			}
		
		   if(seldata !="" && seldata != null && seldata != undefined && filtersel !== "text"){
				rows = getRowsFromSelectedData(selcol); 
				if(rows.length >0){
					var searchedData = tData.filter(function (d, ix) {
						
						return rows.indexOf(ix) >= 0;
					});
					
					hot.updateSettings({
						 data:searchedData
					});
				
				}
		  }
			var len = hot.countRows();
			if(len == 0){
				showsavmsg("green","No Data Found.Please clear filter....");
				
			}else{
				if(selcol !="" && selcol != undefined && selcol != null){
					hot.selectCell(0,parseInt(selcol));
					
				}
			}
			    hot.selectColumns(parseInt(selcol));
				$("#gridfilteredcnt").val(len);
				$("#filtercnt").html("Filter Count : <b>"+len+"</b>  /  "+totalcnt);
				$("#selected_col").val("");
				$("#search_text").val("");
				
		});
	/**
	 * Select column based on selected value from select column dropdown
	 */
	 $('#selformemory').on('change', function() { 
		 var selcolIndex = 3+parseInt(this.value);
		
		 if(selcolIndex != "" && selcolIndex != null && selcolIndex != undefined){
		 	 hot.selectColumns(selcolIndex);
			 setTimeout(function(){ hot.selectCell(0,selcolIndex); }, 0);
		 }
		 
	 });
	/**
	 * Filter type change
	 */
		 $('#filtersel').on('change', function() { 
			 
				  if(this.value == 'text' || this.value == 'multipleenv' || this.value == 'blank' || this.value == 'notblank')
				  {
					  if(this.value == 'blank' || this.value == 'notblank'){
						   $("#search_text").hide();
					  }else{
						   $("#search_text").show();
					  }
					  
					   $("#search_text_div").hide();
					   hidesearchbox();
					
				  }
				  else
				  {
					$("#search_text_div").hide();
					$("#search_text").hide();
					 showsearchbox();
				  }
			 
		 });

	/**
	 * Save data
	 */
	/*	$("#save").click(function(){
			 hot.deselectCell();
			var resultdata = hot.getData();
			var cnt = hot.countRows();
			if(cnt > 0){
				var ischecked = $('input[type="checkbox"]').is(':checked'); 
				var isvalidnounmod = true;
				if(ischecked){
					isvalidnounmod = checkmandatoryval(resultdata,cnt);
				}else{
					isvalidnounmod = true;
				}
				if(!isvalidnounmod){
				return false;
				}
				if(isvalidnounmod){
					 $.blockUI({ 
						message: 'Please wait...'
					}); 

						var saveurl = "save_qarework.php?pid="+pid+"&isk="+isk+"&catid="+catid+"&usertype="+usertype+"&batch="+batch+"&reworkbatch="+reworkbatch;
						if(saveurl != "" && saveurl != null){
							$.ajax({
								url: saveurl,
								data: {"data": JSON.stringify(resultdata)},     
								type: 'POST',
								dataType:'json',
								success: function (res) {
									var completedpartcnt = 0;

									var myData =[];
									var cnt = hot.countRows(); 

									if(ischecked){
										for (var i = 0, ilen = cnt; i < ilen; i++) {
											 var chk = resultdata[i][2];
											 if(chk ==="false")
											 {
												myData.push(i);
											 }
										}

											var filtered = resultdata.filter(function (d, ix) {
													return myData.indexOf(ix) >= 0;
												});
											hot.updateSettings({
														data:filtered
												});
											 completedpartcnt = filtered.length;

										var rowscnt = hot.countRows();
										var totalcnt = $("#totalcnt").val();
										var gridfilteredcnt = $("#gridfilteredcnt").val();
										if(gridfilteredcnt > 0){
											var totalpartscnt = totalcnt-completedpartcnt;
											$("#filtercnt").html("Filter Count : <b>"+rowscnt+"</b>  /  "+totalpartscnt);

										}else{

											$("#filtercnt").html("Total Count :<b>"+rowscnt+"</b>");
										}
										showsavmsg("green","Data saved successfully.");

									}else{

										if(res !="" && res != null && res != undefined){
										  var msg = res.msg;
											if(msg !="" && msg != null && msg != undefined){
												if(msg == "success"){
													showsavmsg("green","Data saved successfully.");

												}else{
													showsavmsg("red","Unable to save data.Please try again.");

												}
											}
										}


									}


								},
								error: function () {


									 showsavmsg("red","Unable to save data.Please try again.");


								}
							});
						}
				}
			}else{
				showsavmsg("red","Data is not available");
			}
		});*/
	$("#save").click(function(){
			 hot.deselectCell();
			var resultdata = hot.getData();
			var cnt = hot.countRows();
			//chk attr mandatory
			var isvalidattr = true;
			if(cnt > 0){
				var ischecked = $('input[type="checkbox"]').is(':checked'); 
				var isvalidnounmod = true;
				if(ischecked){
					isvalidnounmod = checkmandatoryval(resultdata,cnt);
					//check attr validate
					isvalidattr = checkattrmandatory(resultdata,cnt);
				}else{
					isvalidnounmod = true;
				}
				if(!isvalidnounmod){
				return false;
				}
				var saveurl = "save_qarework.php?pid="+pid+"&isk="+isk+"&catid="+catid+"&usertype="+usertype+"&batch="+batch+"&reworkbatch="+reworkbatch;
				if(isvalidnounmod){
					if(!isvalidattr){
						  bootbox.confirm({
							message: "Attribute values are empty.Are you sure to submit part(s)?",
							buttons: confirmbtnobj,
							callback: function (result) {
								if(result){
									saveparts(saveurl,resultdata,ischecked);
								}
							}
						});
					}else{
						saveparts(saveurl,resultdata,ischecked);
					}
					
				}
			}else{
				showsavmsg("red","Data is not available");
			}
		});
	
           /**
		    *Save schema
			*/
			$('#saveschema').click(function () {
				var iHeader=$("#iHeader").val();
				var iValue=$("#iValue").val();
				var iCommoditySet=$("#iCommoditySet").val();
				var ispartsel = $(".htCheckboxRendererInput").is(':checked');
				if(iCommoditySet=='' || iCommoditySet== '0')
				{
					showsavmsg("red","Please select a schema.");
					return false;
				}else if(!ispartsel){
					showsavmsg("red","Please select a part.");
				}
				else{
					var iCommoditySetval = $('#iCommoditySet').find(":selected").text();
					var schemaurl = "save_schema.php?pid="+pid+"&iCommoditySet="+iCommoditySet+"&usertype="+usertype+"&iCommoditySetval="+iCommoditySet;
					$('button[name=saveschema]').attr("disabled","disabled");
					
					if(schemaurl != "" && schemaurl != null && schemaurl != undefined){
						  $.ajax({
							url: schemaurl,
							data: {"data": hot.getData()},
							type: 'POST',
							success: function (data) {
								if(data != "" && data != null && data !=undefined){
									var obj = JSON.parse(data)
									var msg = obj.msg;
									if(msg == "failure"){
										showsavmsg("red","Please select proper noun");
										 $('#saveschema').prop("disabled", false); 

									}else{
var newurl =current_url +'?pid='+pid+'&isk='+isk+'&usertype='+usertype+'&batch='+batch+'&reworkbatch='+reworkbatch+'&catid='+iCommoditySet;
									window.location.href = newurl;
									}
								}

							},
							error: function () {
								showsavmsg("red","Save schema error. Please try again.");

							}
					  });
					}
				}
		});
	 
		//Clear btn click
		$("#clear").click(function(){
			var newurl = current_url + '?pid='+pid+'&isk='+isk+'&usertype='+usertype+'&batch='+batch+'&reworkbatch='+reworkbatch+'&catid='+catid;
			window.location.href = newurl;
			
		});

	});


