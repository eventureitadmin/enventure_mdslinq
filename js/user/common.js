var tooltip, 
	hidetooltiptimer;
var confirmbtnobj = {		
						confirm: {
								label: 'Yes',
								className: 'btn-success'
							},
							cancel: {
								label: 'No',
								className: 'btn-danger'
							}
						};
	function showAlertMsg(msg){
		 bootbox.alert({
				message: msg,
				backdrop: true
		 });
	}
	function selectElementText(el){
		var range = document.createRange() // create new range object
		range.selectNodeContents(el) // set range to encompass desired element text
		var selection = window.getSelection() // get Selection object from currently user selected text
		selection.removeAllRanges() // unselect any user selected text (if any)
		selection.addRange(range) // add range to Selection object to select it
	}
	function copySelectionText(){
		var copysuccess; // var to check whether execCommand successfully executed
		try{
			copysuccess = document.execCommand("copy"); // run command to copy selected text to clipboard
		} catch(e){
			copysuccess = false;
		}
		return copysuccess;
	}
	function createtooltip(msg){ // call this function ONCE at the end of page to create tool tip object
		tooltip = document.createElement('div')
		tooltip.style.cssText = 
			'position:absolute; background:black; color:white; padding:4px;z-index:10000;'
			+ 'border-radius:2px; font-size:12px;box-shadow:3px 3px 3px rgba(0,0,0,.4);'
			+ 'opacity:0;transition:opacity 0.3s'
		tooltip.innerHTML = msg;
		document.body.appendChild(tooltip)
	}
	function getSelectionText(){
		var selectedText = "";
		if (window.getSelection()){ // all modern browsers and IE9+
			selectedText = window.getSelection().toString();
		}
		return selectedText;
	}
	function showtooltip(e){
		var evt = e || event;
		clearTimeout(hidetooltiptimer);
		tooltip.style.left = evt.pageX - 10 + 'px';
		tooltip.style.top = evt.pageY + 15 + 'px';
		tooltip.style.opacity = 1;
		hidetooltiptimer = setTimeout(function(){
			tooltip.style.opacity = 0;
		}, 500)
	}
	function getenvstatus(pid,startRow,startCol,selorderid,field,headername){
		
			$("#popupcontainer").removeClass("modal-lg");
			$("#popupcontainer").addClass("modal-sm");
			$("#unspacheader").text(headername);
			$("#unspscbody").load("getenvstatus.php", { 																														"pid":pid,"orderid":selorderid,"rows":startRow,"cols":startCol,"field":field},function(){
						$(".helpBtn").css("display","none");

					$('#unspscdialog').modal({show:true});

			});
	}
	function getEamDetails(startRow,startCol,pid,catid,partid,usertype,is_form=false,val,field){
		if(is_form){
			usertype = usertype.trim();
			var eamurl = 'geteamdetails.php?rows=&cols=&proj='+pid+'&catid='+catid+"&partid="+partid+"&usertype="+usertype+"&field="+field;
			
		$('#unspscbody').load(eamurl,function(){
				$(".helpBtn").css("display","none");
				$("#eam_data").val(val);
				checkeam(pid,startCol,startRow,"",partid,2,field);
				$('#unspscdialog').modal({show:true});
		});  
		
		
		}else{
			var eamurl = 'geteamdetails.php?rows='+startRow+'&cols='+startCol+'&proj='+pid+'&catid='+catid+"&partid="+partid+"&usertype="+usertype+"&field=";
		$('#unspscbody').load(eamurl,function(){
				$(".helpBtn").css("display","none");
				$("#eam_data").val(val);
				checkeam(pid,startCol,startRow,"",partid,2,"");
				$('#unspscdialog').modal({show:true});

		});  
		
		}
		$('#unspscdialog').on('shown.bs.modal', function () {
				$("#eam_data").focus();
				//$("#saveeambtn").hide();
			
		});
		
	}
	function attrfieldsconfirm(){
		  bootbox.confirm({
			message: "Attribute values are empty.Are you sure to submit part(s)?",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if(result){
                }
			}
		});
	}
	function saveheaders(url){
	   $.ajax({
			url:url,
			dataType: 'html',
			type: 'POST',
			success: function (data) {
				 window.location.reload();
			}  
		 });
	}
	
	function getBrandDetails(startRow,startCol,pid,catid,partid,usertype,is_form=false,val,field){
		if(is_form){
			usertype = usertype.trim();
			var brandurl = 'getbranddetails.php?rows=&cols=&proj='+pid+'&catid='+catid+"&partid="+partid+"&usertype="+usertype+"&field="+field;
			
		$('#unspscbody').load(brandurl,function(){
				$(".helpBtn").css("display","none");
				$("#brand_data").val(val);
				checkbrand(pid,startCol,startRow,"",partid,2,field);
				$('#unspscdialog').modal({show:true});
		});  
	}else{
			var brandurl = 'getbranddetails.php?rows='+startRow+'&cols='+startCol+'&proj='+pid+'&catid='+catid+"&partid="+partid+"&usertype="+usertype+"&field=";
		$('#unspscbody').load(brandurl,function(){
				$(".helpBtn").css("display","none");
				$("#brand_data").val(val);
				checkbrand(pid,startCol,startRow,"",partid,2,"");
				$('#unspscdialog').modal({show:true});
		}); 
     	}
		$('#unspscdialog').on('shown.bs.modal', function () {
				$("#brand_data").focus();
				//$("#saveeambtn").hide();
			
		});
	}
function getUnspscDetails(startRow,startCol,pid,catid,partid,usertype,is_form=false,val,field){
		if(is_form){
			usertype = usertype.trim();
		
			var nounmodindex = $("#validationtype17").val();
					var nounmodval = $("#sFld"+nounmodindex).val();
					if(nounmodval == "" || nounmodval == null || nounmodval.length == 0){
						var nonmodname = $("#sFld"+nounmodindex).attr("labelname");
						showAlertMsg("<b>"+nonmodname+"</b>  is empty.So please select "+nonmodname);
					}else{
						
$("#unspscbody").load("getunspscdetails.php", { "proj":pid,"rows":startRow,"cols":startCol,"cols":startCol,"catid":catid,"vals":val,"field":field},function(){	
	
								checkunspsc(pid,catid,startRow,startCol,val,field);
										if($("#unspsctitle").length > 0 ){
											var unspsctitle  = $("#unspsctitle").val();
										}else{
											var unspsctitle  =  "SELECT UNSPSC";
										}
										$("#unspacheader").text(unspsctitle);

									$(".helpBtn").css("display","none");

									$('#unspscdialog').modal({show:true});

							});  
					}
	
		
		}else{
			var unspscurl = 'getunspscdetails.php?rows='+startRow+'&cols='+startCol+'&proj='+pid+'&catid='+catid+"&partid="+partid+"&usertype="+usertype+"&field=";
		$('#unspscbody').load(unspscurl,function(){
				$(".helpBtn").css("display","none");
				$("#search_data").val(val);
				checkunspsc(pid,catid,startRow,startCol,val,"");
				$('#unspscdialog').modal({show:true});

		});  
		
		}
			$('#unspscdialog').on('shown.bs.modal', function () {
				$("#search_data").focus();
			});
	}
	 function getnoundetails(pid,startRow,startCol,val,catid,field="",usertype,partid){
		if(val.length == 0 || val == "" || val == null){
			$("#unspacheader").text("SELECT NOUN");
		}else{
			$("#unspacheader").text(val);
		}
		
	$('#unspscbody').load("getnoundetails.php", {'rows':startRow,'cols':startCol,'proj':pid,'catid':catid,'vals':val,"field":field,"partid":partid},function(){	
			
			if(field == 0 ){
				field = '0';
			}
			checknounmod(pid,catid,startRow,startCol,val,field,usertype,partid);
			$(".helpBtn").css("display","none");
			$('#unspscdialog').modal({show:true});
			$('#unspscdialog').on('shown.bs.modal', function () {
				$("#search_nmdata").focus();
				
		    });
			
		});
	}
	
	jQuery(function($){
		 $('[data-toggle="tooltip"]').tooltip(); 
		 $('#fimg').on('click', function(){
			 $(".tooltip").tooltip('hide');
			 var src = ($('#fbtn img').attr('src') === '../images/up.jpg') ? '../images/down.jpg' : '../images/up.jpg';
			 $('#fbtn img').attr('src', src);	
			 if(src=='../images/up.jpg'){
				  $('#fbtn').append($("#fpanel"));
				  $("#fpanel").css('display','block');
				 
			 }else{
				$("#fpanel").css('display','none');
				 
			 }
			 $(this).toggleClass('active');
		});
		 $('#himg').on('click', function(){
			  $(".tooltip").tooltip('hide');
			 var src = ($('#hbtn img').attr('src') === '../images/up.jpg') ? '../images/down.jpg' : '../images/up.jpg';
			 $('#hbtn img').attr('src', src);	
			 if(src=='../images/up.jpg'){

				  $('#hbtn').append($("#hpanel"));
				  $("#selformemory_chosen").css("width","280px");
				  $("#iCommodity_chosen").css("width","280px");
				  $("#iCommoditySet_chosen").css("width","280px");
				  $("#selforfreezcolumn_chosen").css("width","280px");
				  if($("#assign_to_chosen").length > 0){
				   $("#assign_to_chosen").css("width","280px");
				  }
				
					 if(issdldactive == 1){
					  $("#sdlddiv").css('display','none');
					 }
				
				  $("#hpanel").css('display','block');
				

			 }else{
				 $("#hpanel").css('display','none');
				  if(issdldactive == 1){
					 $("#sdlddiv").css('display','block');
				  }
				
			 }
			 $(this).toggleClass('active');
		});
	 $('.smenu').on('click', function(){
		  $(".tooltip").tooltip('hide');
				 showshortcutmenu();
		 });

	});
