			 var panelheads= []; 
			 
			 /**
			  * Hide or view panel
			  */
			 function hideviewpanel(panelid,infoid){
				
				 if($("#"+infoid).hasClass("in")){
				 	 $(panelid).val("false");
				  }else{
				 	 $("#"+panelid).val("true");
				  }
			 }
			 /**
			  * show panel
			  */
			 function showpanel(sectionid,infoid){
			 	 $("#"+sectionid).attr("aria-expanded","true");
				 $("#"+infoid).attr("aria-expanded","true");
				 $("#"+infoid).addClass("in");
				 
			 }
			
			 function updatepanelchange(){
				 var panelsarr = [];
			  	 var panelindexs = "";
				 var item ="";
				 $(".headerpanel").each(function( i ) {
					 var panelid = $(this).attr("hpanel");
					  item += ".lipanel"+panelid;
					  panelsarr.push(".lipanel"+panelid);
					
				 });
				 var panelidsdata = panelsarr.join(',');
				 $("#panelids").val(panelidsdata);
				
			 }
			 /**
			  * panel ids order
			  */
			$.fn.orderChildren =  function(order) {
				this.each(function() {
					var el = $(this);
					for(var i = order.length - 1; i >= 0; i--) {
						el.prepend(el.children(order[i]));
					}
				});
				return this;
			}
			 /**
			  * Spell check
			  */
			function spellcheck(checkword,callback){
				$.ajax({
					url: 'formviewspellcheck.php',
					dataType: 'json',
					type: 'post',
					data: {"checkword": checkword},
					success: function( data){
						
						return callback(data);
					}
				});	
			}
			 /**
			  * Check Attribute Value
			  */
			 function checkattributevalue(textvalue,datavalues){
				var datavaluesarr = datavalues.split("~");
				var envheaderid = datavaluesarr[0];
				var commodityid = datavaluesarr[1];
				$.post("checkattributevalue.php",{pagetype:"prod",textvalue:textvalue,envheaderid: envheaderid,commodityid:commodityid},function(data){	
						if(data=='AVAIL'){
							$("#word"+envheaderid).addClass("tdColor");
						}
						else{
							$("#word"+envheaderid).removeClass("tdColor");
						}
				});		
			}
			 
			 /**
			  * Get Fill Rate
			  */
			 function getFillRate(){
					var total_mandatory=0;
					var total_mandatory_filled=0;
					$(".attribute").each(function() {
						if($(this).hasClass("mandatory")){
							if($(this).val()!=''){
								total_mandatory_filled++;
							}
							total_mandatory++;
						}
					});
					var fillratevalue = Math.round(((total_mandatory_filled/total_mandatory)*100));
					
					var fillratecolor = 'red';
					if (!isNaN(fillratevalue)) {
						if (fillratevalue >= maxfillrateqc){
							fillratecolor = 'green';
						}
					}
					if(isNaN(fillratevalue)){
						fillratevalue = "0";
					}
					$("#fillrate").html(fillratevalue+"%").css('background-color', fillratecolor);
				}
				 /**
				  * Get Attribute Value
				  */
				 function getattributevalue(){
					var attr_desp = '';
					$(".attribute").each(function() {
						//remove validateattr class
						if($(this).hasClass("validateattr") && $(this).val() != ""){
						  $(this).removeClass("validateattr");
					    }
					  
						if($(this).val() !=''){
							attr_desp += $(this).val()+", ";
						}
					});	
					if(attr_desp != ''){
						$("#attr_desp").html('');
						$("#attr_desp").html(attr_desp.slice(0,-2));
					}
				}
				  /**
				  * Set panel height
				  */
				 function setpanelheight(infoid){
					  $("#"+infoid).on('shown.bs.collapse', function () {
						 $("#"+infoid).css({        
							'overflow': 'auto',
							'height': '200px'
						});
					});
				 }
				 /**
				  * Show  fillrate alert message 
				  */
				/* function showalert(fillrate,btnname){
					$("#alertfillrate").html(fillrate);
					$("#alertbtnname").html(btnname);
					if(btnname != 'save'){
						$("#overlay").show();
					 }
				}*/
 				function showalert(fillrate,btnname,validattr){
					 var msg = "";
					 if(btnname == "Submit" && !validattr){
						 if(fillrate != ""){
							 fillrate = fillrate+" % and  attribute /uom values are empty. ";
						 }
					 }else{
						 fillrate = fillrate+"% .";
					 }
					$("#alertfillrate").html(fillrate);
					$("#alertbtnname").html(btnname);
					 if(btnname != 'save'){
						$("#overlay").show();
					 }
				}
				 
				 /**
				  * Form view submission
				  */
				 function submitform(whichbtn){
					$("#overlay").hide();
					$("#frm_grid1").submit(); 
				 }
				
				 /**
				  * Get attribute List
				  */
				function getattributelist(selector,descriptor,atrributeword){
					if (!$("#"+selector).data('uiAutocomplete')) {
					var availableAttributes;

					$.post("attributelist.php",{pagetype:"prod",descriptor:descriptor,atrributeword:atrributeword},function(data){	
						var dataarr = data.split("||");
						availableAttributes="[";
						for(var i=0; i<dataarr.length;i++){
							if(i==(dataarr.length-1)){
								availableAttributes += "'"+dataarr[i]+"'";
							}
							else{
								availableAttributes += "'"+dataarr[i]+"',";
							}
						}
						availableAttributes += "]";
						availableAttributes=eval(availableAttributes);
						
					});
					}
				}
				 /**
				  * Get panel hide or show activity
				  */
				 function getpanelsactivity(){
					 hideviewpanel("ihpanel","ihinfo");
					 hideviewpanel("attrpanel","attrinfo");
					 hideviewpanel("ohpanel","ohinfo");
				 }
				 /**
				  * showshortcutmenu
				  */
					function showmenulist(){
						$("#formmenuModal").modal('show');
					}
					function formviewuploadfiles(ids)
					{
					  window.open(fomviewuploadurl+'?pid='+pid+'&ids='+ids,'UploadFiles','toolbar=0,menubar=0,scrollbars=1,width=480,fullscreen=0');
					}
				 /***
				  * Shortcut keys 
				  */
				function shortcutkeys() {
					shortcut.add("f1", function() {
						$("#save").focus();
						$("#save").click();
						});
					shortcut.add("alt+r", function() {
						$("#reject").focus();
						$("#reject").click();
						});
					shortcut.add("alt+c", function() {
						$("#close").focus();
						$("#close").click();
						});
				     shortcut.add("alt+t", function() {
						$("#submit").focus();
						$("#submit").click();
					});
					shortcut.add("ctrl+s", function() {
						$("#shortcutkeylist").focus();
						$("#shortcutkeylist").click();
					});
					
					shortcut.add("alt+p", function() {
						$("#previous").focus();
						$("#previous").click();
					});


					shortcut.add("alt+q", function() {
						$("#next").focus();
						$("#next").click();
					});
					
				}
				
			 window.onload = function () { 
				 /**
				  * Spell check
				  */
				$(".spellcheck").each(function() {
					var changedvalue = $(this).val();
					var textboxid = $(this).attr("id");
					var textboxid = textboxid.substr(4);
					if(changedvalue != ''){
						spellcheck(changedvalue, function(spellcheckstatus) {
							
							$("#spellchecklabel_"+textboxid).html(spellcheckstatus);
						});		
					}
				});
				$(".attributeclass").each(function() {
					var textvalue = $(this).val();
					var datavalues = $(this).attr("data-type");
					checkattributevalue(textvalue,datavalues);
				});
				getFillRate();
				getattributevalue();
			}
			function fileupload(ids,fname)
			{
				$("#sFld"+ids).val(fname);
				$("#lbl_"+ids).html(fname);
			}
			/**
			 * change non ascii to ascii word
			 */
			function containsAllAscii(str) {
				return  /^[\000-\177]*$/.test(str) ;
			} 			 

		/**
		 * Remove non ascii
		 */
		function removenonascii(str) {
			if ((str===null) || (str==='')){
				   return false;
			 }
			 else{
			   str = str.toString();
			 }

			  return str.replace(/[^\x20-\x7E]/g, '');
		}
		
		
		function validate(pattern,obj,msg=""){
			var isvalid = checkvaliddata(obj.val(),pattern,obj,msg);
			if(isvalid)
			{
				obj.removeClass("errormsg");
			}else{
				 obj.addClass("errormsg");
				 var currval = obj.val();labelname = obj.attr('labelname');
				if($(".errormsg").length>0 && currval.length > 0 && currval != "" && currval != null ){
				 $.toaster({ priority : 'danger',title:"Error",message :labelname+"-"+msg,'timeout': 6000, 'fade': 'slow'});
				
				}
			}
			
		}
		
function notvalid(obj,msg){
	if(obj !="" && obj != null && obj != undefined){
		obj.css("border","1px solid #FF0000");
		obj.css("border-radius","4px");
		obj.css("padding","5px");
		obj.prop("title",msg);
	}
	
}
function valid(obj){
	if(obj !="" && obj != null && obj != undefined){
		obj.css("border","1px solid #DCDCDC");
		obj.css("border-radius","4px");
		obj.css("padding","5px");
		obj.prop("title","");
	
	}
	
}
function checkvaliddata(val,expression,obj,msg){
		
	if(val !="" && val != undefined && val != null){
		var isvaliddata =  expression.test(val);
		if(!isvaliddata){
			notvalid(obj,msg);
			return false;
		}else{
			valid(obj);
			return true;
		}
	}
	
}

