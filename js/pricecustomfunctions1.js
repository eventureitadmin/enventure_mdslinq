
	/**
	 * Show filter popup
	 */
	function openModal(){
     
		$("#filtersel").val("");
		$("#search_text_div").hide();
		$("#filtersel").focus();
		var filtersel = $("#filtersel").val();
		var selection = hot.getSelected();
		if(filtersel !="" && filtersel != null && filtersel != undefined && filtersel == "text"  && filtersel == "multipleenv"){
			 $("#search_text").show();
		}else{
		  $("#search_text").hide();
		}
		if(selection !="" && selection != null && selection != undefined){
		  var selcol = selection[0][1];hot.selectCell(0,selcol);
			
				var colname = $('th.ht__highlight').find("a.colHeader").html();
				$("#filterheader").html('');
				$("#filterheader").html("Filter by value : <b>"+colname+"</b>");
				$("#selected_col").val(selcol);
				 showsearchbox();
				 hot.deselectCell();
			
		}
    }
	/**
	 * if select type as contains then hide searchbox on filter popup
	 */
	function hidesearchbox(){
		
		 showsearchbox();
		 $('.selmasterdata').find('option').remove().end();
		 $(".selmasterdata").remove().end();

	}
	function setfilterheight(){
		var solheight = $(".sol-selection").height();
		if(solheight > 0){
			var popupheight = solheight+150;
		   $("#filterbody").css("height",popupheight);
		}else{
		  $("#filterbody").css("height",'150');
		 }
	}
	/**
	 * Show searchbox on filter popup 
	 */
	function showsearchbox(){
		var selchkdata = "";
		var selcol =$("#selected_col").val();
		var uniquecolumnData = getUniqueCellData(selcol);
		 $('.selmasterdata').find('option').remove().end();
		 $(".selmasterdata").remove().end();
		var isselectexists = $(".selmasterdata").length;
		  if(isselectexists <= 0){
				$("#selectbox").append('<select id="my-select" name="smasterdata[]" class="selmasterdata" multiple="multiple"></select>');
				
		  }
		  for(var k=0;k<uniquecolumnData.length;k++){
			  if(formulatypesarr != null){
				   var  iskeyexists = selcol in formulatypesarr;

				  if(iskeyexists && Number.isInteger(uniquecolumnData[k])){
					   var  priceval =  parseFloat(uniquecolumnData[k]).toFixed(2);

				  }else{
					  var  priceval = uniquecolumnData[k];
				  }
			  }else{
			   var  priceval = uniquecolumnData[k];
			  }
			  selchkdata +="<option value='"+uniquecolumnData[k]+"'>"+priceval+"</option>";
			
		  }

			$('.selmasterdata').append(selchkdata);

			  $('.selmasterdata').searchableOptionList({
					showSelectAll: true,
				 	maxHeight: '250px',
				  	events: {
						onOpen: function(){
							setfilterheight();
						},
						onClose: function(){
							setfilterheight();
						}
					}
				});
		
		$("#myModal").modal("show");
	
	}

 
	/**
	 * Get Unique CellData
	 */
	function getUniqueCellData(column){
		var dataAtCol = hot.getSourceDataAtCol(column);
		return dataAtCol.filter((value, index, self) => self.indexOf(value) === index && value != "");
	}
	/**
	 * Get RowsFromSelectedData
	 */
	function getRowsFromSelectedData(selcol) {
		var rows = []; var selectedDataArr = [];
		var dataarr = hot.getSourceDataAtCol(selcol);
		 $('.sol-checkbox:checked').each(function (index, value) { 
			
			 selectedDataArr.push($(this).val());
		 });
		for(var i = 0, l = dataarr.length; i < l; i++) {
			if(selectedDataArr.indexOf(dataarr[i]) > -1){
				rows.push(i);

			}	
		 }
		
		return rows;
	  }
	/**
	 * Get RowsFromObjects
	 */
	function getRowsFromObjects(queryResult) {
		var rows = [];

		for (var i = 0, l = queryResult.length; i < l; i++) {
			 rows.push(queryResult[i].row);
		}

		return rows;
	  }
	/**
	 * Get EmptyDataRows
	 */
	  function getEmptyDataRows(col) {
		  var  rows = [];

	   var hotdata = hot.getData();
	   for(var i=0;i<hotdata.length;i++){

			   if((hotdata[i][col] == "" || hotdata[i][col] == null) && typeof hotdata[i][col] != undefined ){
				 rows.push(i);
			   }

	   }
		return rows;
	  }
	/**
	 * Get Not Empty Data Rows
	 */
	 function getNotEmptyDataRows(col) {
		  var  rows = [];
		  var hotdata = hot.getData();
	   for(var i=0;i<hotdata.length;i++){
			  if(hotdata[i][col] != "" && hotdata[i][col] != null && typeof hotdata[i][col] != undefined ){
				 rows.push(i);
			   }

	   }

		return rows;
	  }
	
	/**
	 * firstRowRenderer
	 */
		function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
		  Handsontable.renderers.TextRenderer.apply(this, arguments);
		  td.style.fontWeight = 'bold';
		  td.style.color = 'red';
		}
	/**
	 * showshortcutmenu
	 */
		function showshortcutmenu(){
			
			$("#menuModal").modal('show');
	
		}
	/**
	 * Scroll bar for shortcut keys menu popup
	 */
	$('#menuModal').on('shown.bs.modal', function () {
		 hot.deselectCell();
    	$('#menuModal table#menutable tbody td').first().focus();
    });	
	var currCell = $('#menuModal table#menutable td').first();
	$('#menuModal table#menutable').keydown(function (e) {
		console.log(currCell);
		var c = "";
		if (e.which == 39) {
			// Right Arrow
			c = currCell.next();
		} else if (e.which == 37) {
			// Left Arrow
			c = currCell.prev();
		} else if (e.which == 38) {
			// Up Arrow
			c = currCell.closest('tr').prev().find('td:eq(' + currCell.index() + ')');
		} else if (e.which == 40) {
			// Down Arrow
			c = currCell.closest('tr').next().find('td:eq(' + currCell.index() + ')');
		} else if ((e.which == 9 && !e.shiftKey)) {
			// Tab
			e.preventDefault();
			c = currCell.next();
		} else if ( (e.which == 9 && e.shiftKey)) {
			// Shift + Tab
			e.preventDefault();
			c = currCell.prev();
		}
		if (c.length > 0) {
			currCell = c;
			currCell.focus();
		}
	});
	/**
	 * Scroll bar for shortcut keys menu popup
	 */
$('#popupdialog').on('shown.bs.modal', function () {
		 hot.deselectCell();
    	$('#popupdialog table#envstatustable tbody td').first().focus();
    });	
	var currCell1 = $('#popupdialog table#envstatustable td').first();
	$('#popupdialog table#envstatustable').keydown(function (e) {
	
		var c = "";
		if (e.which == 39) {
			// Right Arrow
			c = currCell1.next();
		} else if (e.which == 37) {
			// Left Arrow
			c = currCell1.prev();
		} else if (e.which == 38) {
			// Up Arrow
			c = currCell1.closest('tr').prev().find('td:eq(' + currCell1.index() + ')');
		} else if (e.which == 40) {
			// Down Arrow
			c = currCell1.closest('tr').next().find('td:eq(' + currCell1.index() + ')');
		} else if ((e.which == 9 && !e.shiftKey)) {
			// Tab
			e.preventDefault();
			c = currCell1.next();
		} else if ( (e.which == 9 && e.shiftKey)) {
			// Shift + Tab
			e.preventDefault();
			c = currCell1.prev();
		}
		if (c.length > 0) {
			currCell1 = c;
			currCell1.focus();
		}
	});
function checkmandatoryval(resultdata,cnt) {
	var isvalidnounmod =true;
	for (var i = 0, ilen = cnt; i < ilen; i++) {
		var chk = resultdata[i][2];
		 if(chk && chk !=="false")  
		 {
			 if(resultdata[i][prodcommindex] == ""){
				 var errmsg = 'Please provide ';
				for(var key in mandatoryfields){
					  if(resultdata[i][key] == ""){
						 var mandataname = mandatoryfields[key];
						 isvalidnounmod = false;
						 errmsg += mandataname+",";
						
					  }

				 }
				 if(!isvalidnounmod){
					   errmsg = errmsg.slice(0, -1);
					$.toaster({ priority : 'danger',message :errmsg});
						return false;
					}
			 }
		 }
	}
	return true;
}
	/**
	 * fileuploadunspsc
	 */
		 function fileuploadunspsc(curRow,curCol,fname,fname1)
        { 
			if(hot.getSettings().columns[curCol].type == "validation13"){
				   hot.setDataAtCell(curRow,curCol,fname1);
			
				 var newcol = curCol-1;
				  if(hot.getSettings().columns[newcol].type == "validation14"){
				   hot.setDataAtCell(curRow,newcol,fname);
				 }
			 }
			
			 if(hot.getSettings().columns[curCol].type == "validation14"){
				   hot.setDataAtCell(curRow,curCol,fname);
			
			 	 var newcol = curCol+1;
				 if(hot.getSettings().columns[newcol].type == "validation13"){
				   hot.setDataAtCell(curRow,newcol,fname1);
				 }
			 } 
			hot.updateSettings({
				 data:hot.getData()
			});	
			
        }
	/**
	 * fileuploadnm
	 */
        function fileuploadnm(curRow,curCol,fname,fname1)
        {
       		$(".iCommoditySet").val(fname).trigger("chosen:updated");
	    }
    	/**
		 * change non ascii to ascii word
		 */
		function containsAllAscii(str) {
			return  /^[\000-\177]*$/.test(str) ;
		}
	
	/**
	 * Load Data
	 */
	function  loadGridData() {
		
		var result = null;
		 $.blockUI({ 
			message: 'Please wait...'
		}); 
        var totalrows = 0; 
        if(hot !="" && hot != null && hot != undefined){
		    totalrows = hot.getData().length;
         }
		var loadurl = "";
		var saveurl = "";
		loadurl = "load_pricesourcing1.php?pid="+pid+"&batch="+batch+"&cid="+cid+"&statusid="+statusid+"&sorder="+sorder;
		saveurl = "";
		var gridht  = $(window).height()-($("#headerpanel").height()+$(".panel-heading").height()+180);
		if(loadurl != "" && loadurl != null && pid >0){
			
			$.ajax({
					url: loadurl,
					dataType: 'json',
					type: 'GET',
					success: function (res) {
						result = res;
						var stdata = JSON.stringify(res.data);
						var data = JSON.parse(stdata);
						hot.loadData(res.data); 
						var resultdata =[];
						var len = hot.countRows();
						tableData = hot.getData();
						$("#totalcnt").val(res.data.length);
						
							 hot.updateSettings({
							  height: gridht
							});
						if(saveurl != "" && saveurl != null && res.data.length > 0 && usertype > 0){
								$.ajax({
								url: saveurl,
								data: {"data": JSON.stringify(tableData)},     
								type: 'POST',
								dataType:'json',
								success: function (res) { 
								
									if(statusid > 0 && statusid != ""){
										if(sorder != '' && sorder != null){
											 hot.selectColumns((Number(sorder)+Number(1)));
										}
										$("#filtercnt").html("Filter Count: <b>"+len+"</b> / "+result.totalcnt);
									}else{
										hot.selectCell(0,1); 
										$("#filtercnt").html("Total Count :<b>"+len+"</b>");
									}
									
									
									if(res !="" && res != null && res != undefined){
									  var msg = res.msg;
										if(msg !="" && msg != null && msg != undefined){
											if(msg == "success"){
												if(len > 0){
												 showsavmsg("green","Data loaded successfully.");
												}else{
												 showsavmsg("green"," Data is not available");
												}

											}else{
												 showsavmsg("red","Unable to load data.Please try again.");

											}
										}
									}

								 },
								error: function (xhr) {
									showsavmsg("red","Unable to load data.Please try again.");

								}
							});
						}else{
							if(usertype == 0){
								if(statusid > 0 && statusid != ""){
										if(sorder != '' && sorder != null){
											 hot.selectColumns((Number(sorder)+Number(1)));
										}
										$("#filtercnt").html("Filter Count: <b>"+len+"</b> / "+result.totalcnt);
									}else{
										hot.selectCell(0,1); 
										$("#filtercnt").html("Total Count :<b>"+len+"</b>");
									}
								showsavmsg("green","Data loaded successfully.");
							}else{
								showsavmsg("green","Data is not available");
							}
						}

					}, 
					 error: function (xhr) {
						showsavmsg("red","Unable to load data.Please try again.");

					 }  
				  });
			}
		}
 	/***
	 * Shortcut keys 
	 */
	function init() {
		
	
		/**
		 * Clear filter
		 */
			shortcut.add("ctrl+f5", function() {
			   hot.deselectCell();
			   $("#clear").focus();
			   $("#clear").click();
		       });
		/**
		 * Save data
		 */
			shortcut.add("f1", function() {
			   hot.deselectCell();
			  $("#save").focus();
			  $("#save").click();
			});
		
		/**
		 * Focus on grid
		 */
			shortcut.add("ctrl+g", function() {
			   hot.selectCell(0,1);
				 
			});
		/**
		 * Show short cut menu 
		 */
			shortcut.add("ctrl+s", function() {
				
				 	  showshortcutmenu();
			   
			});
		/**
		 * Mouse right click
		 */
			shortcut.add("shift+F10", function() {
				var  selobj = hot.getSelected(); 
				
				if(selobj != "" && selobj != null && selobj != undefined)
				{
					 var selhotrow = hot.getSelected()[0][0];
					 var selhotcol = hot.getSelected()[0][1];
					hot.selectCell(selhotrow,selhotcol);
				}
			   
			});
		
		
			/**
		     * Column sort by ascending
		     */
			 shortcut.add("f3",function(){
				 var selectedcell = hot.getSelected();
				if(typeof selectedcell === "object"){
					hot.deselectCell();
					 hot.updateSettings({
						
						  columnSorting: {
								column:selectedcell[0][1],
								sortOrder: 'asc'
							 }
						});
		 			 hot.selectColumns(selectedcell[0][1]);
				 }
			 });
		 /**
		  * Column sort by descending
		  */
			 shortcut.add("f4",function(){
				 var selectedcell = hot.getSelected();
				 
				 if(typeof selectedcell === "object"){
					 hot.deselectCell();
					 hot.updateSettings({
						  columnSorting: {
								column:selectedcell[0][1], 
								sortOrder: 'desc'
							 }
						});
					 hot.selectColumns(selectedcell[0][1]);
		 
				 }
			 });
			shortcut.add("escape",function(){
				 if($('#myModal').is(':visible')){
					var selcolindex = $("#selected_col").val();
					selcolindex = parseInt(selcolindex);
					hot.selectColumns(selcolindex);
					 
				}
			});
			shortcut.add("alt+h", function() {
				 $("#himg").click();
		   	 });
			shortcut.add("alt+f", function() {
				$("#fimg").click();
		 	 });
		}
		/**
		 * Popup with up and down arrow option
		 */
	  function arrow(dir) {
		  var w = $('.modal-body');
			var activeTableRow = $('.table tbody tr.active')[dir](".table tbody tr");
			if (activeTableRow.length) {
				$('.table tbody tr.active').removeClass("active");
				activeTableRow.prop("class", "active");
			} 
		  
		}

	/**
	 * Remove non ascii
	 */
	function removenonascii(str) {

	  if ((str===null) || (str==='')){
		   return false;
	  }
	 else{
	   str = str.toString();
	 }

	  return str.replace(/[^\x20-\x7E]/g, '');
	}

	 function arrow(dir) {
		var activeTableRow = $('.table tbody tr.active')[dir](".table tbody tr");
		if (activeTableRow.length) {
			$('.table tbody tr.active').removeClass("active");
			activeTableRow.addClass('active');
		} 
	 }
	  $(window).keydown(function (key) {
		if (key.keyCode == 38) {
			arrow("prev");
		}
		if (key.keyCode == 40) {
			arrow("next");
		}
	});
	$(document).ready(function(){
		init();
		
		if(usertype == 0){
			$("#save").hide();
		}
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var target = $(e.target).attr("href"); 
		  if(target == "#partslist"){
			 loadGridData();	
		  }else{
		  	
		  }
		
		});
	    $(".nav-tabs a").click(function(e){
			$(this).tab('show');
	    });
		   	
	/**
	 * Focus on filter popup
	 */
	$('#myModal').on('shown.bs.modal', function () {
    	$("#filtersel").focus();
		
    });
	
	
	/**
	 * Filter popup and on click enter search data based on filtered value
	 */
    $(document).keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if($('#myModal').is(':visible') && keycode == '13'){
				$('.sol-selection-container').hide();
			 	$("#search_btn").click();
	   	}
	});	
	function getMultipleENVRows(search_text,selcol,search){
		var rows = [];
		if(search_text.length > 0){
			var searchtxt = search_text.split(",");
			for(var k=0;k<searchtxt.length;k++){
				var queryResult1 = search.querycolumn(searchtxt[k],1);
				if(typeof queryResult1 == "object" && queryResult1.length >0){
				 rows.push(queryResult1[0].row);
				}
			}
		}
		return rows;
	}
	
	/**
	 *Search Data by filter type
	 */
	$("#search_btn").click(function(){
		  $("#myModal").modal('hide');
		  var selcol =  $("#selected_col").val();
			
		  var  tData = hot.getData();
		  var search = hot.getPlugin('search');

		  var filtersel =  $("#filtersel").val();
		  var totalcnt = $("#totalcnt").val();
		  var handsontabledata = ""; 
		  var rows = "";
		  var seldata  = $('#my.selmasterdataselect');
		  var filterpartids = "";
		
			if(filtersel != "" && filtersel != null && filtersel != undefined)
			{
				var search_text = $("#search_text").val().trim();
				if(filtersel === "text"){
					var queryResult = search.querycolumn(search_text,selcol);
						rows  = getRowsFromObjects(queryResult);
				 } else if(filtersel === "blank"){
				 	 rows  =  getEmptyDataRows(selcol);
				 }
				else if(filtersel === "notblank"){
					rows  =  getNotEmptyDataRows(selcol);
				}
				else if(filtersel === "multipleenv"){
					rows  =  getMultipleENVRows(search_text,selcol,search);
				}
				var filtered = "";
				if((filtersel === "blank" || filtersel === "notblank") || ((filtersel === "text" || filtersel === "multipleenv" )  && search_text !="" && 
			   search_text != null && search_text != undefined))
				{
					 filtered = tData.filter(function (d, ix) {
								return rows.indexOf(ix) >= 0;
							});
					
						hot.updateSettings({
								 data:filtered
							});	
					
				}
				
			
			}
		
		   if(seldata !="" && seldata != null && seldata != undefined && filtersel !== "text" && filtersel !== "multipleenv"){
				rows = getRowsFromSelectedData(selcol); 
				if(rows.length >0){
					var searchedData = tData.filter(function (d, ix) {
						
						return rows.indexOf(ix) >= 0;
					});
					
					hot.updateSettings({
						 data:searchedData
					});
				
				}
		  }
			var len = hot.countRows();
			if(len == 0){
				showsavmsg("green","No Data Found.Please clear filter....");
				
			}else{
				if(selcol !="" && selcol != undefined && selcol != null){
					hot.selectCell(0,parseInt(selcol));
					
				}
			}
			    hot.selectColumns(parseInt(selcol));
				$("#gridfilteredcnt").val(len);
				$("#filtercnt").html("Filter Count : <b>"+len+"</b>  /  "+totalcnt);
				$("#selected_col").val("");
				$("#search_text").val("");
				
		});
	
	/**
	 * Filter type change
	 */
		 $('#filtersel').on('change', function() { 
			 
				  if(this.value == 'text' || this.value == 'multipleenv' || this.value == 'blank' || this.value == 'notblank')
				  {
					  if(this.value == 'blank' || this.value == 'notblank'){
						   $("#search_text").hide();
					  }else{
						   $("#search_text").show();
					  }
					  
					   $("#search_text_div").hide();
					   hidesearchbox();
					
				  }
				  else
				  {
					$("#search_text_div").hide();
					$("#search_text").hide();
					 showsearchbox();
				  }
			 
		 });

	/**
	 * Save data
	 */
		$("#save").click(function(){
			
			var resultdata = hot.getData();
			var cnt = hot.countRows();
			if(cnt > 0){
				 $.blockUI({ 
						message: 'Please wait...'
					}); 
					
						var saveurl = "save_pricesourcing.php?pid="+pid+"&batch="+batch;
				
						if(saveurl != "" && saveurl != null ){
							$.ajax({
								url: saveurl,
								data: {"data": JSON.stringify(resultdata)},     
								type: 'POST',
								dataType:'json',
								success: function (res) {
									var completedpartcnt = 0;

									var myData =[];
									var cnt = hot.countRows(); 
									var rowscnt = hot.countRows();
									 $("#filtercnt").html("Total Count :<b>"+rowscnt+"</b>");
									
									if(res !="" && res != null && res != undefined){
									  var msg = res.msg;
										if(msg !="" && msg != null && msg != undefined){
											if(msg == "success"){
												showsavmsg("green","Data saved successfully.");

											}else{
												showsavmsg("red","Unable to save data.Please try again.");

											}
										}
									}

								},
								error: function () {
									showsavmsg("red","Unable to save data.Please try again.");
								}
							});
						}
				
			}else{
				showsavmsg("red","Data is not available");
			}
		});
	
          
	 
		//Clear btn click
		$("#clear").click(function(){
			window.location.href="home.php?batch="+batch;
			$(".nav-tabs li").removeClass();
			$('.nav-tabs a:nth-child(1)').addClass("active");
			$('.nav-tabs a:nth-child(1)').tab('show'); 
		/*
			var activeIndex = $('.nav-tabs li').index($('li.active')[0]);console.log(activeIndex+"activeIndex");
			if(activeIndex == 1){
				loadGridData();
				$("li.active a#partslist").tab('show');
				 var activeTab = $('a #partslist').find(".active");
			 }*/
		
		});

	});


