	/**
	 * Scroll bar for shortcut keys menu popup
	 */
	$('#menuModal').on('shown.bs.modal', function () {
		 hot2.deselectCell();
    	$('#menuModal table#menutable tbody td').first().focus();
    });	
	var currCell = $('#menuModal table#menutable td').first();
	$('#menuModal table#menutable').keydown(function (e) {
		
		var c = "";
		if (e.which == 39) {
			// Right Arrow
			c = currCell.next();
		} else if (e.which == 37) {
			// Left Arrow
			c = currCell.prev();
		} else if (e.which == 38) {
			// Up Arrow
			c = currCell.closest('tr').prev().find('td:eq(' + currCell.index() + ')');
		} else if (e.which == 40) {
			// Down Arrow
			c = currCell.closest('tr').next().find('td:eq(' + currCell.index() + ')');
		} else if ((e.which == 9 && !e.shiftKey)) {
			// Tab
			e.preventDefault();
			c = currCell.next();
		} else if ( (e.which == 9 && e.shiftKey)) {
			// Shift + Tab
			e.preventDefault();
			c = currCell.prev();
		}
		if (c.length > 0) {
			currCell = c;
			currCell.focus();
		}
	});
	/**
	 * Scroll bar for shortcut keys menu popup
	 */
$('#popupdialog').on('shown.bs.modal', function () {
		 hot2.deselectCell();
    	$('#popupdialog table#envstatustable tbody td').first().focus();
    });	
	var currCell1 = $('#popupdialog table#envstatustable td').first();
	$('#popupdialog table#envstatustable').keydown(function (e) {
	
		var c = "";
		if (e.which == 39) {
			// Right Arrow
			c = currCell1.next();
		} else if (e.which == 37) {
			// Left Arrow
			c = currCell1.prev();
		} else if (e.which == 38) {
			// Up Arrow
			c = currCell1.closest('tr').prev().find('td:eq(' + currCell1.index() + ')');
		} else if (e.which == 40) {
			// Down Arrow
			c = currCell1.closest('tr').next().find('td:eq(' + currCell1.index() + ')');
		} else if ((e.which == 9 && !e.shiftKey)) {
			// Tab
			e.preventDefault();
			c = currCell1.next();
		} else if ( (e.which == 9 && e.shiftKey)) {
			// Shift + Tab
			e.preventDefault();
			c = currCell1.prev();
		}
		if (c.length > 0) {
			currCell1 = c;
			currCell1.focus();
		}
	});
	/**
	 * Load Data
	 */
	function  loadGridData1() {
		
		var result = null;
		 $.blockUI({ 
			message: 'Please wait...'
		}); 
        var totalrows = 0; 
        if(hot2 !="" && hot2 != null && hot2 != undefined){
		    totalrows = hot2.getData().length;
         }
		var loadurl = "";
		var saveurl = "";
		loadurl = "load_pricesourcing.php";
		saveurl = "save_pricesourcing.php?pid="+pid+"&batch="+batch+"&statusid="+statusid+"&sorder="+sorder+"&scid="+selcid;
		var gridht  = $(window).height()-($("#headerpanel").height()+$(".panel-heading").height()+gridheight);
		if(loadurl != "" && loadurl != null && pid >0){
			
			$.ajax({
					url: loadurl,
					dataType: 'json',
					type: 'POST',
				data:{"pid":pid,"batch":batch,"statusid":statusid,"sorder":sorder,"scid":selcid,"pt":$("#part_type1").val(),"rt":$("#request_type1").val()},
	success: function (res) {
						result = res;
						var stdata = JSON.stringify(res.data);
						hot2.loadData(res.data); 
    					 $.unblockUI();
						var resultdata =[];
						var len = hot2.countRows();
                       
						tableData = hot2.getData();
						$("#totalcnt1").val(res.data.length);
						
							 hot2.updateSettings({
							  height: gridht
							});
									if(statusid > 0 && statusid != ""){
										if(sorder != '' && sorder != null){
											 hot2.selectColumns((Number(sorder)+Number(1)));
										}
										$("#filtercnt1").html("Filter Count: <b>"+len+"</b> / "+result.totalcnt);
									}else{
										hot2.selectCell(0,selgridcol); 
										$("#filtercnt1").html("Total Count :<b>"+len+"</b>");
									}
									
						
						//	showsavmsg("red","Unable to load data.Please try again.");
					if(saveurl != "" && saveurl != null && res.data.length > 0 && usertype > 0){
								$.ajax({
								url: saveurl,
								data: {"data": JSON.stringify(tableData)},     
								type: 'POST',
								dataType:'json',
								success: function (res) { 
								
									
									
									if(res !="" && res != null && res != undefined){
									  var msg = res.msg;
										if(msg !="" && msg != null && msg != undefined){
											if(msg == "success"){
												if(len > 0){
												 showsavmsg("green","Data loaded successfully.");
												}else{
												 showsavmsg("green"," Data is not available");
												}

											}else{
												 showsavmsg("red","Unable to load data.Please try again.");

											}
										}
									}

								 },
								error: function (xhr) {
									showsavmsg("red","Unable to load data.Please try again.");

								}
							});
						}else{
							if(usertype == 0){
								if(statusid > 0 && statusid != ""){
										if(sorder != '' && sorder != null){
											 hot2.selectColumns((Number(sorder)+Number(1)));
										}
										$("#filtercnt1").html("Filter Count: <b>"+len+"</b> / "+result.totalcnt);
									}else{
										hot2.selectCell(0,selgridcol); 
										$("#filtercnt1").html("Total Count :<b>"+len+"</b>");
									}
								showsavmsg("green","Data loaded successfully.");
							}else{
								showsavmsg("green","Data is not available");
							}
						}

					}, 
					 error: function (xhr) {
						showsavmsg("red","Unable to load data.Please try again.");

					 }  
				  });
			}
		}
 	/***
	 * Shortcut keys 
	 */
	function init1() {
		
	
		/**
		 * Clear filter
		 */
			shortcut.add("ctrl+f5", function() {
			   hot2.deselectCell();
			   $("#clear1").focus();
			   $("#clear1").click();
		       });
		/**
		 * Save data
		 */
			shortcut.add("f1", function() {
			   hot2.deselectCell();
			  $("#save1").focus();
			  $("#save1").click();
			});
		
		/**
		 * Focus on grid
		 */
			shortcut.add("ctrl+g", function() {
			   hot2.selectCell(0,selgridcol);
				 
			});
		/**
		 * Show short cut menu 
		 */
			shortcut.add("ctrl+s", function() {
				
				 	  showshortcutmenu();
			   
			});
		/**
		 * Mouse right click
		 */
			shortcut.add("shift+F10", function() {
				var  selobj = hot2.getSelected(); 
				
				if(selobj != "" && selobj != null && selobj != undefined)
				{
					 var selhotrow = hot2.getSelected()[0][0];
					 var selhotcol = hot2.getSelected()[0][1];
					hot2.selectCell(selhotrow,selhotcol);
				}
			   
			});
		
		
			/**
		     * Column sort by ascending
		     */
			 shortcut.add("f3",function(){
				 var selectedcell = hot2.getSelected();
				if(typeof selectedcell === "object"){
					hot2.deselectCell();
					 hot2.updateSettings({
						
						  columnSorting: {
								column:selectedcell[0][1],
								sortOrder: 'asc'
							 }
						});
		 			 hot2.selectColumns(selectedcell[0][1]);
				 }
			 });
		 /**
		  * Column sort by descending
		  */
			 shortcut.add("f4",function(){
				 var selectedcell = hot2.getSelected();
				 
				 if(typeof selectedcell === "object"){
					 hot2.deselectCell();
					 hot2.updateSettings({
						  columnSorting: {
								column:selectedcell[0][1], 
								sortOrder: 'desc'
							 }
						});
					 hot2.selectColumns(selectedcell[0][1]);
		 
				 }
			 });
			shortcut.add("escape",function(){
				 if($('#myModal').is(':visible')){
					var selcolindex = $("#selected_col").val();
					selcolindex = parseInt(selcolindex);
					hot2.selectColumns(selcolindex);
					 
				}
			});
			shortcut.add("alt+h", function() {
				 $("#himg").click();
		   	 });
			shortcut.add("alt+f", function() {
				$("#fimg").click();
		 	 });
		}
		
	  $(window).keydown(function (key) {
		if (key.keyCode == 38) {
			arrow("prev");
		}
		if (key.keyCode == 40) {
			arrow("next");
		}
	});
	$(document).ready(function(){
		 $(".chosen-select").chosen({search_contains: true,width:"200px"});
		init1();
		 $("#export1").click(function(){
			 if(hot2.countRows() >0){
				$("#req_export_form1").submit();
			 }
		 });
		$("#part_type1").change(function(){
			$("#selparttype1").val($(this).val());
			loadGridData1();
		});
		if(usertype == 0){
			$("#save1").hide();
		}
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var target = $(e.target).attr("href");
		  if(target == "#partslist"){
			 loadGridData1();	
		  }
		
		});
	   
		   	
	/**
	 * Focus on filter popup
	 */
	$('#myModal').on('shown.bs.modal', function () {
    	$(".filtersel").focus();
		
    });
	
	
	/**
	 * Filter popup and on click enter search data based on filtered value
	 */
    $(document).keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if($('#myModal').is(':visible') && keycode == '13'){
				$('.sol-selection-container').hide();
			 	$("#search_btn").click();
	   	}
	});	
	
	$("#cancel1").click(function(){
		var ischecked = $('input[type="checkbox"]').is(':checked');
			if(ischecked){
					bootbox.confirm({ 
					size: "small",
					message: deleteconfirmmsg,
					centerVertical:1,
					buttons: {
						cancel: {
							label: 'No',
							className: 'btn-danger nobtn'
						},
						confirm: {
							label: 'Yes',
							className: 'btn-success pull-left yesbtn' 
						},

					},
					callback: function(result){ 
						if(result){
							
							var resultdata = hot2.getData();
							var cnt = hot2.countRows();
							if(cnt > 0){
							$.blockUI({ 
									message: 'Please wait...'
							}); 

								var saveurl = "save_pricesourcing.php?pid="+pid+"&batch="+batch;

									$.ajax({
										url: saveurl,
										data: {"data": JSON.stringify(resultdata)},     
										type: 'POST',
										dataType:'json',
										success: function (res) {
											var completedpartcnt = 0;
											var myData =[];
											var cnt = hot2.countRows(); 
											var selparts = "";
		
												for (var i = 0, ilen = cnt; i < ilen; i++) {

													var status =$.trim(resultdata[i][statusfieldindex]);
													var chk = resultdata[i][1];
													if(!(status == initialstatusval) || (chk == "false" && (status == initialstatusval))){
															myData.push(i);
															resultdata[i][1] = false;
															 selparts +=  resultdata[i][0]+",";
													}
													
												}
										
											var filtered = resultdata.filter(function (d, ix) {
															return myData.indexOf(ix) >= 0;
														});

													hot2.updateSettings({
																data:filtered
														});
													 completedpartcnt = filtered.length;																																																							
												var rowscnt = hot2.countRows();
												var totalcnt = $("#totalcnt1").val();
												var gridfilteredcnt = $("#gridfilteredcnt").val();
												if(gridfilteredcnt > 0){
													var totalpartscnt = totalcnt-completedpartcnt;
													$("#filtercnt1").html("Filter Count : <b>"+rowscnt+"</b>  /  "+totalpartscnt);

												}else{

													$("#filtercnt1").html("Total Count :<b>"+rowscnt+"</b>");
												}

											if(res !="" && res != null && res != undefined){
											  var msg = res.msg;
												if(msg !="" && msg != null && msg != undefined){
													if(msg == "success"){
														showsavmsg("green","Data has been cancelled successfully.");

													}else{
														showsavmsg("red","Unable to cancel request(s).Please try again.");

													}
												}
											}

										},
										error: function () {
											showsavmsg("red","Unable to cancel data.Please try again.");
										}
									});
								return true;
							}else{
								showsavmsg("green","Data is not available");
										return true;
							}


					}else{
						var cnt1 = hot2.countRows();
						var resultdata = hot2.getData();
						for (var i = 0, ilen = cnt1; i < ilen; i++) {
						resultdata[i][1] = false;
							 hot2.updateSettings({
										data:resultdata
								});
						}
					}
				}
			}).find('.modal-content').css({
    			'font-size': '15px',
    			'text-align': 'center',
				'margin-top': function (){
					var w = $( window ).height();
					var b = $(".modal-dialog").height();
					// should not be (w-h)/2
					var h = (w-b)/2;
					return h+"px";
				}
			});
			}else{
				$.notify("Please click checkbox to cancel request(s)","error");
			}
		
		});
	
	/**
	 * Save data
	 */
		$("#save1").click(function(){
			var ischecked = $('input[type="checkbox"]').is(':checked');
			var resultdata = hot2.getData();
			var cnt = hot2.countRows();
			if(cnt > 0){
				 	$.blockUI({ 
							message: 'Please wait...'
					}); 
					
						var saveurl = "save_pricesourcing.php?pid="+pid+"&batch="+batch;
				
						if(saveurl != "" && saveurl != null ){
							$.ajax({
								url: saveurl,
								data: {"data": JSON.stringify(resultdata)},     
								type: 'POST',
								dataType:'json',
								success: function (res) {
									if(usertype == 1){
									var completedpartcnt = 0;
									var myData =[];
									var cnt = hot2.countRows(); 
								    var selparts = "";
									//if(ischecked){
										
										for (var i = 0, ilen = cnt; i < ilen; i++) {
											var status =$.trim(resultdata[i][statusfieldindex]);
											 if(pendingstatusarr.indexOf(status) >-1)
											 {
												myData.push(i);
												resultdata[i][1] = false;
												 selparts +=  resultdata[i][0]+",";
											 }
										}
									
										var filtered = resultdata.filter(function (d, ix) {
													return myData.indexOf(ix) >= 0;
												});
															
											hot2.updateSettings({
														data:filtered
												});
											 completedpartcnt = filtered.length;																																																							}
										var rowscnt = hot2.countRows();
										var totalcnt = $("#totalcnt1").val();
										var gridfilteredcnt = $("#gridfilteredcnt").val();
										if(gridfilteredcnt > 0){
											var totalpartscnt = totalcnt-completedpartcnt;
											$("#filtercnt1").html("Filter Count : <b>"+rowscnt+"</b>  /  "+totalpartscnt);

										}else{

											$("#filtercnt1").html("Total Count :<b>"+rowscnt+"</b>");
										}
									
									if(res !="" && res != null && res != undefined){
									  var msg = res.msg;
										if(msg !="" && msg != null && msg != undefined){
											if(msg == "success"){
												showsavmsg("green","Data saved successfully.");

											}else{
												showsavmsg("red","Unable to save data.Please try again.");

											}
										}
									}

								},
								error: function () {
									showsavmsg("red","Unable to save data.Please try again.");
								}
							});
						}
				
			}else{
				showsavmsg("green","Data is not available");
			}
		});
	
          
	 
		//Clear btn click
		$("#clear1").click(function(){
				$("#part_type1").val("").trigger('chosen:updated');
				$("#selreqtype1").val("");
				$("#request_type1").val("").trigger('chosen:updated');
				$("#selparttype1").val("");
				$("#selparttype1").val("");
			if(statusid >0){
				window.location.href="home.php?batch="+batch;
			}else{
				$('.nav-tabs a[href="#partslist"]').tab('show');
			
				loadGridData1();
			}
		});

	});


