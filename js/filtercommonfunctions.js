	/**
	 * Show filter popup
	 */
	function openModal(type=0){
		$(".filtersel").val("");
		$("#search_text_div").hide();
		$(".filtersel").focus();
		var filtersel = $(".filtersel").val();
		var selection = hotobj.getSelected();
		if(filtersel !="" && filtersel != null && filtersel != undefined && filtersel == "text"  && filtersel == "multipleenv"){
			 $("#search_text").show();
		}else{
		  $("#search_text").hide();
		}
		if(selection !="" && selection != null && selection != undefined){
		  var selcol = selection[0][1];
			hotobj.selectCell(0,selcol);
				var colname = $('th.ht__highlight').find("a.colHeader").html();
				$("#filterheader").html('');
				$("#filterheader").html("Filter by value : <b>"+colname+"</b>");
				$("#selected_col").val(selcol);
				 showsearchbox();
				 hotobj.deselectCell();
			
		}
    }
	/**
	 * if select type as contains then hide searchbox on filter popup
	 */
	function hidesearchbox(){
		
		 showsearchbox();
		 $('.selmasterdata').find('option').remove().end();
		 $(".selmasterdata").remove().end();

	}
	function setfilterheight(){
		var solheight = $(".sol-selection").height();
		if(solheight > 0){
			var popupheight = solheight+150;
		   $("#filterbody").css("height",popupheight);
		}else{
		  $("#filterbody").css("height",'150');
		 }
	}
	/**
	 * Show searchbox on filter popup 
	 */
	function showsearchbox(){
		var selchkdata = "";
		var selcol =$("#selected_col").val();
		var uniquecolumnData = getUniqueCellData(selcol);
		 $('.selmasterdata').find('option').remove().end();
		 $(".selmasterdata").remove().end();
		var isselectexists = $(".selmasterdata").length;
		  if(isselectexists <= 0){
				$("#selectbox").append('<select id="my-select" name="smasterdata[]" class="selmasterdata" multiple="multiple"></select>');
				
		  }
		  for(var k=0;k<uniquecolumnData.length;k++){
			  if(formulatypesarr != null){
				   var  iskeyexists = selcol in formulatypesarr;

				  if(iskeyexists && Number.isInteger(uniquecolumnData[k])){
					   var  priceval =  parseFloat(uniquecolumnData[k]).toFixed(2);

				  }else{
					  var  priceval = uniquecolumnData[k];
				  }
			  }else{
			   var  priceval = uniquecolumnData[k];
			  }
			  selchkdata +="<option value='"+uniquecolumnData[k]+"'>"+priceval+"</option>";
			
		  }

			$('.selmasterdata').append(selchkdata);

			  $('.selmasterdata').searchableOptionList({
					showSelectAll: true,
				 	maxHeight: '250px',
				  	events: {
						onOpen: function(){
							setfilterheight();
						},
						onClose: function(){
							setfilterheight();
						}
					}
				});
		
		$("#myModal").modal("show");
	
	}

 
	/**
	 * Get Unique CellData
	 */
	function getUniqueCellData(column){
		var dataAtCol = hotobj.getSourceDataAtCol(column);
		return dataAtCol.filter((value, index, self) => self.indexOf(value) === index && value != "");
	}
	/**
	 * Get RowsFromSelectedData
	 */
	function getRowsFromSelectedData(selcol) {
		var rows = []; var selectedDataArr = [];
		var dataarr = hotobj.getSourceDataAtCol(selcol);
		 $('.sol-checkbox:checked').each(function (index, value) { 
			
			 selectedDataArr.push($(this).val());
		 });
		for(var i = 0, l = dataarr.length; i < l; i++) {
			if(selectedDataArr.indexOf(dataarr[i]) > -1){
				rows.push(i);

			}	
		 }
		
		return rows;
	  }

	function getMultipleENVRows(search_text,selcol,search){
		var rows = [];
		if(search_text.length > 0){
			var searchtxt = search_text.split(",");
			for(var k=0;k<searchtxt.length;k++){
				var queryResult1 = search.querycolumn(searchtxt[k],1);
				if(typeof queryResult1 == "object" && queryResult1.length >0){
				 rows.push(queryResult1[0].row);
				}
			}
		}
		return rows;
	}
	/**
	 * Get RowsFromObjects
	 */
	function getRowsFromObjects(queryResult) {
		var rows = [];

		for (var i = 0, l = queryResult.length; i < l; i++) {
			 rows.push(queryResult[i].row);
		}

		return rows;
	  }
	/**
	 * Get EmptyDataRows
	 */
	  function getEmptyDataRows(col) {
		  var  rows = [];

	   var hotdata = hotobj.getData();
	   for(var i=0;i<hotdata.length;i++){

			   if((hotdata[i][col] == "" || hotdata[i][col] == null) && typeof hotdata[i][col] != undefined ){
				 rows.push(i);
			   }

	   }
		return rows;
	  }
	/**
	 * Get Not Empty Data Rows
	 */
	 function getNotEmptyDataRows(col) {
		  var  rows = [];
		  var hotdata = hotobj.getData();
	   for(var i=0;i<hotdata.length;i++){
			  if(hotdata[i][col] != "" && hotdata[i][col] != null && typeof hotdata[i][col] != undefined ){
				 rows.push(i);
			   }

	   }

		return rows;
	  }
	
	/**
	 * firstRowRenderer
	 */
		function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
		  Handsontable.renderers.TextRenderer.apply(this, arguments);
		  td.style.fontWeight = 'bold';
		  td.style.color = 'red';
		}

	function fillstatus(vals,pasastatus) {
		var selrow = $("#selrow").val();
		var selcol = $("#selcol").val();
		if(selrow !="" && selrow != null && selrow.length>0){
			selrow = parseInt(selrow);
		}
		if(selcol !="" && selcol != null && selcol.length>0){
			selcol= parseInt(selcol);
		}
		if(vals != "" && vals != null){
			hotobj.setDataAtCell(selrow,selcol,vals);
			if(pasastatus != "" && pasastatus != null){
				hotobj.setDataAtCell(selrow,(selcol+1),pasastatus);
			}
			$("#popupdialog").modal("hide");
		}
	}
function checkallofmine(chk)
	{  
		var cnt = hotobj.countRows();
		var myData = hotobj.getData();
		
		for (var i = 0, ilen = cnt; i < ilen; i++) {
			myData[i][1] =chk;
		}
		hotobj.updateSettings({
				data:myData
	    });
	   $("input[class=checker]").prop("checked",chk);		
		
	}
	function fileupload(curRow,curCol,fname)
	{
	   hotobj.setDataAtCell(curRow,curCol,fname);

	}
/**
	 * uploadfiles
	 */
	function uploadfiles(curRow,curCol)
	{
	
		var iHeader=$("#iHeader").val();
		var iValue=$("#iValue").val();
			window.open('upload.php?pid='+pid+'&curRow='+curRow+'&curCol='+curCol,
						'Upload Files','toolbar=0,menubar=0,scrollbars=1,width=480,fullscreen=0');

	}

function showenvstatus(statusvalobj){
	
			$('#popupdialog').on('shown.bs.modal', function () {
				 hotobj.deselectCell();
				$('#popupdialog table#popuptable tbody td').first().focus();
			});	
			var currCell = $('#popupdialog table#popuptable td').first();
			$('#popupdialog table#popuptable').keydown(function (e) {
				var c = "";
				if (e.which == 39) {
					// Right Arrow
					c = currCell.next();
				} else if (e.which == 37) {
					// Left Arrow
					c = currCell.prev();
				} else if (e.which == 38) {
					// Up Arrow
					c = currCell.closest('tr').prev().find('td:eq(' + currCell.index() + ')');
				} else if (e.which == 40) {
					// Down Arrow
					c = currCell.closest('tr').next().find('td:eq(' + currCell.index() + ')');
				} else if ((e.which == 9 && !e.shiftKey)) {
					// Tab
					e.preventDefault();
					c = currCell.next();
				} else if ( (e.which == 9 && e.shiftKey)) {
					// Shift + Tab
					e.preventDefault();
					c = currCell.prev();
				}
				if (c.length > 0) {
					currCell = c;
					currCell.focus();
				}
			});

		  $(document).keypress(function(event){	
			  var keycode = (event.keyCode ? event.keyCode : event.which);
			  if(keycode == '13'){
				 $(".active a.statusval").click(); 
			  }
		   });


	}
	
	/**
	 * fileuploadnm
	 */
        function fileuploadnm(curRow,curCol,fname,fname1)
        {
       		$(".iCommoditySet").val(fname).trigger("chosen:updated");
	    }
    	/**
		 * change non ascii to ascii word
		 */
		function containsAllAscii(str) {
			return  /^[\000-\177]*$/.test(str) ;
		}
	function getformulaindex(addindex,colindex){
		if(addindex != "" && colindex !=""){
		return parseInt(colindex)+addindex;
		}else{
		return "";
		}
	}
	function formulaval(formutype,row,farr,fobj,selval="",addindex){
		if(farr.length >0){
			var arr = farr.split(",");
			if(arr.length ==2 ){
				var arrindex1 = getformulaindex(addindex,arr[0]);
				var arrindex2 = getformulaindex(addindex,arr[1]);
				if(formutype == 6){
					selval = fobj.getDataAtCell(row,arrindex2);
					if(selval != "" && fobj.getDataAtCell(row,arrindex1) !=""){
						return (selval-fobj.getDataAtCell(row,arrindex1))/fobj.getDataAtCell(row,arrindex1)*100;
					}
				}else if(formutype == 7){
					selval = fobj.getDataAtCell(row,arrindex2);
					if(selval != "" && fobj.getDataAtCell(row,arrindex1) !=""){
						return fobj.getDataAtCell(row,arrindex1)-(selval);
					}
				}else if(formutype == 8){
					var reqwval = fobj.getDataAtCell(row,arrindex1);
					 if(reqwval >1000){
						  reqwval = reqwval*0.02;
					  }else if(reqwval <1000 && reqwval>0){
						  reqwval = reqwval*0.05;
					  }
					  if(selval>0 && reqwval>0){
						return selval-(Math.abs(reqwval));
					  }
				}
			}
		}
		return "";
	}
	 function getpositivenum(num){
		num =Math.abs(num);
		 var result = (num - Math.floor(num)) !== 0;
		 if(result){
			num =num.toFixed(2);
		 }
			
		return num;
	 }
/*function calculatedColumnRender(instance, td, row, col, prop, value, cellProperties) {
	td.innerHTML="";
}***/
	function calculatedColumnRender(instance, td, row, col, prop, value, cellProperties) {
		  var escaped = Handsontable.helper.stringify(value);
		var formulasize = Object.keys(formulatypesarr).length;
        	cellProperties.readOnly = true;
		
		var ftype = instance.getSettings().columns[col].type;
		var formulatype = ftype.match(/\d+/)[0];
		var res = "";
		var fval6 ="";
		var fval7 ="";
		var fval8 ="";
		var fval9 ="";
		var selval ="";
		var selval1 ="";
	
		td.innerHTML = "";
			if(formulaindex.length >0)
				for(var k=0;k<formulaindex.length;k++){
					var findex = parseInt(formulaindex[k]);
					
				if(ftype == "formulatype6"){
					fval6 =formulaval(6,row,formulatypesarr[6],instance,Math.abs(selval),frmindex);
						if(fval6 != "" && fval6 != null){
							if(getpositivenum(fval6) >0){
								td.innerHTML =getpositivenum(fval6)+"%";
							//instance.setDataAtCell(row,col,getpositivenum(fval6)+"%");
							}else{
								td.innerHTML ="";
							}
						}else{
							td.innerHTML = "";
							//instance.setDataAtCell(row,col,"");
						}
					
					}else if(ftype == "formulatype7"){
						fval7 =formulaval(7,row,formulatypesarr[7],instance,selval,frmindex);
						if(fval7 != "" && fval7 != null){
							td.innerHTML =getpositivenum(fval7);
							//instance.setDataAtCell(row,col,getpositivenum(fval7));
						}else{
							td.innerHTML = "";
						}
						
					}else if(ftype == "formulatype8"){
						fval7 =formulaval(7,row,formulatypesarr[7],instance,selval,frmindex);
						selval = getpositivenum(fval7);
						fval8 = td.innerHTML =formulaval(8,row,formulatypesarr[8],instance,selval,frmindex);;
						if(fval8 != "" && fval8 != null){
								td.innerHTML =fval8.toFixed(2);
							//instance.setDataAtCell(row,col,fval8.toFixed(2));
						}else{
							td.innerHTML = "";
							//instance.setDataAtCell(row,col,"");
						}
					}
				}

		
	 }
/*
	 * showshortcutmenu
	 */
		function showshortcutmenu(){
			
			$("#menuModal").modal('show');
	
		}
	/**
	 * Scroll bar for shortcut keys menu popup
	 */
	$('#menuModal').on('shown.bs.modal', function () {
		 hotobj.deselectCell();
    	$('#menuModal table#menutable tbody td').first().focus();
    });	
	var currCell = $('#menuModal table#menutable td').first();
	$('#menuModal table#menutable').keydown(function (e) {
		
		var c = "";
		if (e.which == 39) {
			// Right Arrow
			c = currCell.next();
		} else if (e.which == 37) {
			// Left Arrow
			c = currCell.prev();
		} else if (e.which == 38) {
			// Up Arrow
			c = currCell.closest('tr').prev().find('td:eq(' + currCell.index() + ')');
		} else if (e.which == 40) {
			// Down Arrow
			c = currCell.closest('tr').next().find('td:eq(' + currCell.index() + ')');
		} else if ((e.which == 9 && !e.shiftKey)) {
			// Tab
			e.preventDefault();
			c = currCell.next();
		} else if ( (e.which == 9 && e.shiftKey)) {
			// Shift + Tab
			e.preventDefault();
			c = currCell.prev();
		}
		if (c.length > 0) {
			currCell = c;
			currCell.focus();
		}
	});
	/**
	 * Scroll bar for shortcut keys menu popup
	 */
$('#popupdialog').on('shown.bs.modal', function () {
		 hotobj.deselectCell();
    	$('#popupdialog table#envstatustable tbody td').first().focus();
    });	
	var currCell1 = $('#popupdialog table#envstatustable td').first();
	$('#popupdialog table#envstatustable').keydown(function (e) {
	
		var c = "";
		if (e.which == 39) {
			// Right Arrow
			c = currCell1.next();
		} else if (e.which == 37) {
			// Left Arrow
			c = currCell1.prev();
		} else if (e.which == 38) {
			// Up Arrow
			c = currCell1.closest('tr').prev().find('td:eq(' + currCell1.index() + ')');
		} else if (e.which == 40) {
			// Down Arrow
			c = currCell1.closest('tr').next().find('td:eq(' + currCell1.index() + ')');
		} else if ((e.which == 9 && !e.shiftKey)) {
			// Tab
			e.preventDefault();
			c = currCell1.next();
		} else if ( (e.which == 9 && e.shiftKey)) {
			// Shift + Tab
			e.preventDefault();
			c = currCell1.prev();
		}
		if (c.length > 0) {
			currCell1 = c;
			currCell1.focus();
		}
	});
function checkmandatoryval(resultdata,cnt) {
	var isvalidnounmod =true;
	for (var i = 0, ilen = cnt; i < ilen; i++) {
		var chk = resultdata[i][2];
		 if(chk && chk !=="false")  
		 {
			 if(resultdata[i][prodcommindex] == ""){
				 var errmsg = 'Please provide ';
				for(var key in mandatoryfields){
					  if(resultdata[i][key] == ""){
						 var mandataname = mandatoryfields[key];
						 isvalidnounmod = false;
						 errmsg += mandataname+",";
						
					  }

				 }
				 if(!isvalidnounmod){
					   errmsg = errmsg.slice(0, -1);
					$.toaster({ priority : 'danger',message :errmsg});
						return false;
					}
			 }
		 }
	}
	return true;
}
	/**
	 * fileuploadunspsc
	 */
		 function fileuploadunspsc(curRow,curCol,fname,fname1)
        { 
			if(hotobj.getSettings().columns[curCol].type == "validation13"){
				   hotobj.setDataAtCell(curRow,curCol,fname1);
			
				 var newcol = curCol-1;
				  if(hotobj.getSettings().columns[newcol].type == "validation14"){
				   hotobj.setDataAtCell(curRow,newcol,fname);
				 }
			 }
			
			 if(hotobj.getSettings().columns[curCol].type == "validation14"){
				   hotobj.setDataAtCell(curRow,curCol,fname);
			
			 	 var newcol = curCol+1;
				 if(hotobj.getSettings().columns[newcol].type == "validation13"){
				   hotobj.setDataAtCell(curRow,newcol,fname1);
				 }
			 } 
			hotobj.updateSettings({
				 data:hotobj.getData()
			});	
			
        }
function arrow(dir) {
		var activeTableRow = $('.table tbody tr.active')[dir](".table tbody tr");
		if (activeTableRow.length) {
			$('.table tbody tr.active').removeClass("active");
			activeTableRow.addClass('active');
		} 
	 }
function removenonascii(str) {

	  if ((str===null) || (str==='')){
		   return false;
	  }
	 else{
	   str = str.toString();
	 }

	  return str.replace(/[^\x20-\x7E]/g, '');
	}
