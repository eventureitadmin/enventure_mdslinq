$(function() {
    $('#side-menu').metisMenu();
	
/*	$(document).ajaxStart(function () {
	  console.log('ajax request')
	  $.blockUI({ message: '<h5><img src="../images/busy.gif" /> Just a moment...</h5>',baseZ:'1045' });
	});
	$(document).ajaxStop(function () {
	  console.log('end request')
	  $.unblockUI();
	});*/
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
			//$("#sidebar .handle").hide();
			// $("#menubutton").on("click", function(){
				// $("#sidebar").slideReveal("show");
			// }); 			
        } else {
            $('div.navbar-collapse').removeClass('collapse');
			//$("#sidebar .handle").show();
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
			//$("#sidebar").slideReveal("hide");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
         var slider = $("#sidebar").slideReveal({
           // width: 100,
           push: true,
           position: "left",
           // speed: 600,
           trigger: $(".handle"),
           // autoEscape: false,
           shown: function(obj){
             obj.find(".handle").html('<span class="glyphicon glyphicon-chevron-left"></span>');
           },
           hidden: function(obj){
             obj.find(".handle").html('<span class="glyphicon glyphicon-chevron-right"></span>');
           }
         });
		if(!$(".alert").hasClass("noautoclose")){
			$(".alert").fadeIn('slow').delay(5000).fadeOut('slow');
	  	}
});


function callAlert(msg,alerttype){
	// alert type - success,info,warn,error
	$.notify(msg, alerttype);
}

	function ExportToExcel (dataSource,filename, headers) {
		var arrData;
		arrData=typeof dataSource != 'object' ? JSON.parse(dataSource) : dataSource;
		var Excel = '';    
		//This condition will generate the Label/Header
		if (headers) {
			var row = "";
			//This loop will extract the label from 1st index of on array
			for (var index in headers) {
				if(index > 1){
					 row += headers[index] + ',';
				}
				//Now convert each value to string and comma-seprated
			}
			row = row.slice(0, -1);
			//append Label row with line break
			Excel += row + '\r\n';
		}

		//1st loop is to extract each row
		for (var i = 0; i < arrData.length; i++) {
			var row = "";
			//2nd loop will extract each column and convert it in string comma-seprated
			for (var index in arrData[i]) {
				row += '"' + arrData[i][index] + '",';
			}
			row.slice(0, row.length - 1);
			Excel += row + '\r\n';
		}
		if (Excel == '') {        
			return;
		}
		//Generate a file name
		var fileName = "";
		//this will remove the blank-spaces from the title and replace it with an underscore
		fileName += filename.replace(/ /g,"_");   
		//Initialize file format you want csv or xls
		var uri = 'data:text/csv;charset=utf-8,' + escape(Excel);
		var link = document.createElement("a");    
		link.href = uri;
		link.style = "visibility:hidden";
		link.download = fileName + ".csv";
		//this part will append the anchor tag and remove it after automatic click
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
	}