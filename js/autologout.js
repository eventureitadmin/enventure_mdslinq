// Set timeout variables.
var counter = 60;
var timoutNow = (max_idle_time * 1000); // Timeout in 15 mins.
var timoutWarning = (timoutNow - 60000); // Display warning in 14 Mins.
//var timoutWarning = 60000; // Display warning in 1 Min.
//var timoutNow = 120000; // Timeout in 2 mins.
var logoutUrl = 'logout.php?t=1'; // URL to logout page.

var warningTimer;
var timeoutTimer;
var counterTimer;
var counter;
// Start timers.
function StartTimers() {
	counter = 60;
	clearTimeout(counterTimer);
    warningTimer = setTimeout("IdleWarning()", timoutWarning);
    timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
}

// Reset timers.
function ResetTimers() {
    clearTimeout(warningTimer);
    clearTimeout(timeoutTimer);
    StartTimers();
	if ($('#timeout').is(':visible')) {
    	$("#timeout").dialog('close');
	}
}

// Show idle timeout warning dialog.
function IdleWarning() {
    $("#timeout").dialog({
        modal: true,
		title: "Alert",
		height:'auto', 
		width:'auto',
      create: function(event, ui) {
          $("#timeout").parent('.ui-dialog').css('zIndex', 2)
              .nextAll('.ui-widget-overlay').css('zIndex', 1);
      },
    closeOnEscape: false,
    dialogClass: "noclose",		
    open: function(event, ui) {
        $(".ui-dialog-titlebar-close").hide();
    },		
	buttons: [
		{ 
		id:"stayloggedbtn",
		text: "Stay Logged In",
		click: function() {
			ResetTimers();
			$( this ).dialog( "close" ); 
			}
		},
		{ 
		id:"logoutbtn",
		text: "Logout", 
		click: function() {
			window.location = logoutUrl;
			$( this ).dialog( "close" ); 
			}
		},		
		]		
    });
	if ($('#timeout').is(':visible')) {
		$('#timer').html("");
		$('#timer').html("<b>1 minute</b> second");
		counterTimer = setTimeout("setcounter()", 1000);
	}	
}

function setcounter(){
    counter--;
    // Display 'counter' wherever you want to display it.
    if (counter <= 0) {
     		clearTimeout(counterTimer);
        return;
    }else{
		$('#timer').html("");
    	$('#timer').html("<b>"+counter+"</b> second");
		counterTimer = setTimeout("setcounter()", 1000);
    }	
}

// Logout the user.
function IdleTimeout() {
    window.location = logoutUrl;
}
$(function() {
    StartTimers();
	/*$(document).mousemove(function(event){
	  ResetTimers();
	});*/

});