<?php
ob_start();

session_start();

date_default_timezone_set('Asia/Calcutta');

ini_set("display_errors",1);

ini_set('memory_limit', '-1');

error_reporting(1);

include_once("/var/www/html/pasa/dbconn.php");

include_once("/var/www/html/pasa/class/db.php");

$db = new db();

$settingquery = "SELECT * FROM `env_login_config` WHERE id='1'";
$settingDet = $db->getRow($settingquery);

//records more that idle time without logout
$query = "SELECT `id`, `logintime`, `curtimestamp` FROM `env_login_audit` WHERE `is_cur_session`='1'";
$result = $db->getResults($query);
for($i=0;$i<count($result);$i++){
	$diff = abs(time() - $result[$i]['curtimestamp']);
	if($diff > $settingDet['max_idle_time']){
		$logouttime = date('Y-m-d H:i:s');
		$totalhrsary = $db->calcculatetothrs($result[$i]['logintime'],$logouttime);
		$totalhrs = $totalhrsary['hours'].":".$totalhrsary['minutes'].":".$totalhrsary['seconds'];		
		$updateqry = "UPDATE env_login_audit SET  logouttime ='".$logouttime."',totalhrs='".$totalhrs."',is_cur_session='0',logout_type='1', isactive ='0' WHERE id='".$result[$i]['id']."'";
		$db->query($updateqry);
	}
}

//temporary block user unblock automatically after blocked time over
$blockedqry = "SELECT m.id,(SELECT a.logintime FROM env_login_audit a WHERE a.isactive='1' AND a.userid=m.id AND a.faillogincnt = m.totfaillogins) AS logintime FROM (SELECT id,(SELECT c.totfaillogins FROM env_login_config c WHERE c.id='1') AS totfaillogins FROM `env_user` WHERE `failloginlock`='1') m";
$blockedresult = $db->getResults($blockedqry);
for($i=0;$i<count($blockedresult);$i++){
	$diff = abs(time() - strtotime($blockedresult[$i]['logintime']));
	if($diff > $settingDet['blockhrs']){
			$customerquery = "UPDATE env_user SET `failloginlock` = '0',`enable_recaptcha`='0' WHERE `id` ='".$blockedresult[$i]['id']."'";
			$db->query($customerquery);
			$customerquery1 = "UPDATE env_login_audit SET `isactive` = '0' WHERE `userid` ='".$blockedresult[$i]['id']."' AND curtimestamp='' AND is_cur_session='0' AND faillogincnt > 0";
			$db->query($customerquery1);			
	}
}

//failed login inactive if it is in active status
$blockedqry1 = "SELECT m.* FROM (SELECT `id`, `userid`, `logintime`,count(id) AS failcnt FROM `env_login_audit` WHERE `isactive`='1' AND `curtimestamp`='' AND `is_cur_session`='0' AND `faillogincnt` > 0 GROUP BY logdate,userid) m WHERE m.failcnt < '".$settingDet['totfaillogins']."'";
$blockedresult1 = $db->getResults($blockedqry1);
for($i=0;$i<count($blockedresult1);$i++){
	$diff = abs(time() - strtotime($blockedresult1[$i]['logintime']));
	if($diff > $settingDet['blockhrs']){
			$customerquery2 = "UPDATE env_login_audit SET `isactive` = '0' WHERE `userid` ='".$blockedresult1[$i]['userid']."' AND curtimestamp='' AND is_cur_session='0' AND faillogincnt > 0";
			$db->query($customerquery2);			
	}
}
?>