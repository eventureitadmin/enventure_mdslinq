<?php if(isset($_POST) && $_POST['proj'] > 0 ){ 
	require_once('top.php');
	if($_POST['gtype'] == "completed"){
		$type =2;
	}else if($_POST['gtype'] == "pending"){
		$type =1;
	}else{
		$type ="";
	}
	$customerid  = $dbase->getCustByProj($_POST['proj'])[0];
	$projheader = $dbase->executeQuery("SELECT status_values,COUNT(id) as cnt FROM env_projectheaders WHERE batchid =".$_POST['batch']." AND orderid =".$_POST['selorderid'],"single");
	$envstatusdetails = "";
	if($projheader['cnt'] > 0){
		$status_values  = $projheader['status_values'];
		if($type == 2){
			$envdetails = $dbase->getstatus($_POST['batch'],$type,1);
		}else{
			if($status_values != ""){
				$envdetails = $dbase->executeQuery("SELECT * FROM `env_status` WHERE `customer_id`=".$customerid." AND  id IN(".$status_values.")","multiple");
			}
		}
	}else{
			$envdetails =  $dbase->getStatusDetails($customerid,$_POST['batch']);
	}

?>
	<table class="table table-bordered" id="envstatustable">
		<input type="hidden" name="selcol" id="selcol" value="<?php echo $_POST['selcol'];?>">
		<input type="hidden" name="selrow" id="selrow" value="<?php echo $_POST['selrow'];?>">
	  <thead style="background-color:#ecf0f1;" >
		   <tr class="active">
			  <th scope="col" id="statusth">Status</th>
		  </tr>
	  </thead>
	  <tbody class="table" id="popuptable">
		  <?php if(count($envdetails) > 0){
			   $tabindex=1; 
				foreach($envdetails as $key=>$envstatus){
					if($key == 0){
						$class="active";
					}else{
						$class="";
					}
		  ?>
			<tr>
			<td align="left" tabindex="<?php echo $tabindex++; ?>" width="100%" height="40px">
				<a class="statusval" onclick="fillstatus('<?php echo $envstatus['statusval'];?>','<?php echo $envstatus['customer_status'];?>')"><?php echo $envstatus['statusval'];?></a>
			</tr>

		<?php }}?>

	  </tbody>
	</table>
<?php }?>