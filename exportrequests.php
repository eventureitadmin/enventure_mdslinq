<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
require_once('top.php');

	
if(isset($_POST['bid']) && $_POST['bid'] > 0 && isset($_POST['pid']) && $_POST['pid'] > 0){
	$reports= array();
	$rptnamearr = array();
	$startindexarr = array();
	$lastindexarr = array();
	$headerindexarr = array();
	$totalreports =0;
	$totalreportsarr =array();
		function getdetailsreport($objPHPExcel,$result,$projres,$row1,$row3,$row2,$stylearr){
			for($s=0;$s<count($result);$s++){
				if(count($projres) >0){
					$j=1;
					$start  =65;
					$cha = chr($start);
					for($i=1;$i<count($projres);$i++){
						
						if($s ==0){
							$row =2;
							$row3=3;
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$row1,"Request Type");
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row1,$projres[$i]['headername']);
							$objPHPExcel->getActiveSheet()->getColumnDimension($cha)->setWidth(20);
							$objPHPExcel->getActiveSheet()->getStyle($cha.$row1)->getFont()->setBold(true);
					        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$row2,trim($result[$s]['request_type']));
						
							if($projres[$i]['itypeid'] == 9){
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row2,trim($result[$s]['created_by']));
							}else if($projres[$i]['itypeid'] ==11){
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row2,trim($result[$s]['parttype']));
							}else{
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row2,trim($result[$s]['sFld'.($projres[$i]['orderid']-1)]));
							}
						}else{
							  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$row3,trim($result[$s]['request_type']));
						
							if($projres[$i]['itypeid'] == 9){
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row3,trim($result[$s]['created_by']));
							}else if($projres[$i]['itypeid'] ==11){
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row3,trim($result[$s]['parttype']));
							}else{
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row3,trim($result[$s]['sFld'.($projres[$i]['orderid']-1)]));
							}
						}
						++$start;
						$cha = chr($start);
						$j++;

					}

				$objPHPExcel->getActiveSheet()
					 ->getStyle('A'.$row1.':'.$cha.$row3)->applyFromArray(
						$stylearr
					);

				}
				$row3++;
			}
			//exit;
			return $objPHPExcel;
		}
	$projres = $dbase->executeQuery("SELECT projtype,progress_val,projname,is_weekly_report,customer_id 
	FROM `env_project` WHERE id =".$_POST['pid']." AND `isclosed`=0 AND `isactive`=1","single");
	
		$projtype 			=	$projres['projtype'];
		$progress_val 		=	$projres['progress_val'];
		$projectname 		=	$projres['projname'];
		$is_weekly_report 	=	$projres['is_weekly_report'];
		$customer_id 		=	$projres['customer_id'];
		$statusfield        = "";
		$statusheadres = $dbase->executeQuery("SELECT `status_headerid` FROM `env_batch` 
										WHERE `closed` =0 AND id='".$_POST['bid']."'","single");
			$stylearr = array(
					 'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
						),
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THICK ,
							'color' => array('rgb' => 'DDDDDD')
						)
					)
			);
			$statushead1 = explode(',',$statusheadres);
			if(count($statusheadres) >0){
				$charindex = 65;
				$charttext = chr($charindex);
				$rowindex = 0;
				//Current week report
				for($p=0;$p<count($statusheadres);$p++){
						if($statusheadres[$p] >0){
					 		$statusfield =($statusheadres[$p])-1;
						 	$statusquery=" AND FIND_IN_SET(s.id,(SELECT `status_values` FROM `env_projectheaders`
			 					WHERE `itypeid`=2 AND `batchid` ='".$_POST['bid']."' AND orderid='".$statusheadres[$p]."'))";
						$selqry1 =$dbase->getstatusgraphdetails($_POST['pid'],$_POST['bid'],"",$statusquery,$_POST['cid'],$statusfield);
						 if($selqry1 !=""){
							$statusfinalres = $dbase->executeQuery($selqry1,"multiple");
						 }else{
							$statusfinalres ="";
						 }
						
						if($statusfinalres != ""){
							array_push($rptnamearr," Current week status: ".date("d-M-Y"));
							$i=0;
							$reports[$i][0]= " Current week status: ".date("d-M-Y");
							$i = $i+2;
							$reports[$i][0]="Status Name";
							$reports[$i][1]= "Count";
							array_push($headerindexarr,0);
							$i++;
							array_push($startindexarr, 3);
							$lastindex = count($statusfinalres)+2;
							array_push($lastindexarr, $lastindex);
							$totalreports =0;
							for($h=0;$h<count($statusfinalres);$h++){
								$reports[$i][0]=$statusfinalres[$h]['env_status']."-".$statusfinalres[$h]['cnt'];
								$reports[$i][1]=$statusfinalres[$h]['cnt'];
								$totalreports += $statusfinalres[$h]['cnt'];
								$i++;
							}
							array_push($totalreportsarr,$totalreports);
							$rowindex = $i;
						}
					}
					}
				$is_weekly_report = $_POST['is_weekly_report'];
				//Weekreport
			/*	if($is_weekly_report == 1){
		 			$weekreportquery = "SELECT DISTINCT `reportdate` FROM `env_report` WHERE `iPrjID`='".$_POST['pid']."' AND `iBatch`='".$_POST['bid']."' ORDER BY `reportdate` DESC LIMIT 0,1";
		 			$weekreportresult = $dbase->executeQuery($weekreportquery,"multiple");
		 			if(count($weekreportresult) > 0){
						$charttext = chr(++$charindex);
						for($p=0;$p<count($statusheadres);$p++){
							if($statusheadres[$p] >0){
							$statusfield =($statusheadres[$p])-1;
							for($j=0;$j<count($weekreportresult);$j++){	
										$rptqry =$dbase->getweekstatusdetails($_POST['pid'],$_POST['bid'],$statusquery,$_POST['cid'],$statusfield,$weekreportresult[$j]['reportdate']);
										
							if($rptqry !=""){
								$weekstatusfinalres = $dbase->executeQuery($rptqry,"multiple");
							}else{
								$weekstatusfinalres ="";
							}
							if($weekstatusfinalres !="" && count($weekstatusfinalres) >0){
									$rowindex = $rowindex+2;
									$reports[$rowindex][0]= " Last week status: ".date('d-M-Y',strtotime($weekreportresult[$j]['reportdate']));
									array_push($rptnamearr,$reports[$rowindex][0]);
									$rowindex++;
									$reports[$rowindex][0]="Status Name";
									$reports[$rowindex][1]= "Count";
									$rowindex++; 
									$j=1;
									array_push($headerindexarr,$lastindex);
									$lastindex = $lastindex+2;
									array_push($startindexarr,($lastindex+1));
									$lastindex += count($weekstatusfinalres);
									array_push($lastindexarr, $lastindex);
									$totalreports =0;
									for($w=0;$w<count($weekstatusfinalres);$w++){
										$reports[$rowindex][0]=$weekstatusfinalres[$w]['env_status']."-".$weekstatusfinalres[$w]['cnt'];
										$reports[$rowindex][1]=$weekstatusfinalres[$w]['cnt'];
										$totalreports += $weekstatusfinalres[$w]['cnt'];
										$rowindex++;
										$j++;
									}
								
									array_push($totalreportsarr,$totalreports);
						}
						}
						}
						}
					}
				}*/
				
						//customer report
						$custdetails = $dbase->getcustomerdetails($_POST['pid'],$_POST['bid']);
					
						if(count($custdetails) >0){
							for($y=0;$y<count($custdetails);$y++){
								$charttext = chr(++$charindex);
								$custrptname = $custdetails[$y]['cname'];
								array_push($rptnamearr,$custrptname);
						
									$selqry2 =$dbase->getstatusgraphdetails($_POST['pid'],$_POST['bid'],$custdetails[$y]['cid'],$statusquery,$_POST['cid'],$statusfield);
								 if($selqry2 !=""){
									$statusfinalres2 = $dbase->executeQuery($selqry2,"multiple");;
								}else{
									$statusfinalres2 ="";
								}
							
							   
								if($statusfinalres2 != ""){
									$rowindex = $rowindex+2;
									$reports[$rowindex][0]= $custrptname;
									$rowindex++;
									$reports[$rowindex][0]="Status Name";
									$reports[$rowindex][1]= "Count";
									$rowindex++; 
									$k=1;
									array_push($headerindexarr,$lastindex);
									$lastindex =$lastindex+2;
									array_push($startindexarr,($lastindex+1));
									$lastindex += count($statusfinalres2);
									array_push($lastindexarr, $lastindex);
									$totalreports =0;
									for($c=0;$c<count($statusfinalres2);$c++){
										$reports[$rowindex][0]=$statusfinalres2[$c]['env_status']."-".$statusfinalres2[$c]['cnt'];
										$reports[$rowindex][1]=$statusfinalres2[$c]['cnt'];
										$totalreports +=  $statusfinalres2[$c]['cnt'];
										$rowindex++;
										$k++;
									}
									array_push($totalreportsarr,$totalreports);
								}
							}
						}
						$objPHPExcel = new PHPExcel();
						$objWorksheet = $objPHPExcel->getActiveSheet();
						$objWorksheet->fromArray(
						$reports
					);
						$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(35);
						$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(10);
						$objPHPExcel->getActiveSheet()->getStyle("A1:B1")->getFont()->setBold(true);
						$objPHPExcel->getActiveSheet()
										 ->getStyle('A1:B'.$lastindexarr[(count($lastindexarr)-1)])->applyFromArray(
											$stylearr
										);
						if(count($rptnamearr) >0){
							$chartinx = 0;
							for($s=0;$s<count($rptnamearr);$s++){
						$objPHPExcel->getActiveSheet()->getStyle("A".($headerindexarr[$s]+1))->getFont()->setBold(true);
						$objPHPExcel->getActiveSheet()->getStyle("A".($headerindexarr[$s]+2))->getFont()->setBold(true);
						$objPHPExcel->getActiveSheet()->getStyle("B".($headerindexarr[$s]+1))->getFont()->setBold(true);
						$objPHPExcel->getActiveSheet()->getStyle("B".($headerindexarr[$s]+2))->getFont()->setBold(true);
						//$objPHPExcel->getActiveSheet()->getStyle("A".($headerindexarr[$s]+1).":B".($headerindexarr[$s])+1)->getFont()->setBold(true);
								
						$dataSeriesLabels= array(
						new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$'.$startindexarr[$s].':$A$'.$lastindexarr[$s], NULL, 1),	
						);
						$xAxisTickValues = array(
						new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$'.$startindexarr[$s].':$A$'.$lastindexarr[$s], NULL, 4),	
						);
						$dataSeriesValues = array(
						new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$B$'.$startindexarr[$s].':$B$'.$lastindexarr[$s], NULL, 4),
						);
						$series = new PHPExcel_Chart_DataSeries(
							PHPExcel_Chart_DataSeries::TYPE_DONUTCHART,				// plotType

							NULL,			                                        // plotGrouping (Pie charts don't have any grouping)
							range(0, count($dataSeriesValues)-1),					// plotOrder
							$dataSeriesLabels,										// plotLabel
							$xAxisTickValues,										// plotCategory
							$dataSeriesValues										// plotValues
						);

					//	Set up a layout object for the Pie chart
					$layout = new PHPExcel_Chart_Layout();
					$layout->setShowVal(FALSE);
					$layout->setShowPercent(FALSE);
					$layout->setShowCatName(FALSE);
					$layout->setShowSerName(FALSE);

					//	Set the series in the plot area
					$plotArea = new PHPExcel_Chart_PlotArea($layout, array($series));
					//	Set the chart legend
					$legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);


					$title = new PHPExcel_Chart_Title($rptnamearr[$s]." \n Total :".$totalreportsarr[$s]);


					//	Create the chart
					$chart = new PHPExcel_Chart(
						'chart'.$s,		// name
						$title,		// title
						$legend,			// legend
						$plotArea,		// plotArea
						true,			// plotVisibleOnly
						0,				// displayBlanksAs
						NULL,			// xAxisLabel
						NULL			// yAxisLabel		- Like Pie charts, Donut charts don't have a Y-Axis
					);

					
					
					$chartinx = $chartinx+1;
					$chart->setTopLeftPosition('E'.$chartinx);
					$chartinx = $chartinx+18;
					$chart->setBottomRightPosition('L'.$chartinx);
					$objWorksheet->addChart($chart);
					
					}
							$result = $dbase->getrptrequestdetails($_POST['pid'],$_POST['bid']);
							$projres = $dbase->getheaderinfo($_POST['bid']);
							$row3 =3;
							$row1 = 2;
							$row2 = 3;
							$lastsheet=0;
							if(count($custdetails) >0){
								for($k=0;$k<count($custdetails);$k++){
									$start  =65;
									$cha = chr($start);
									// Create a new worksheet, after the default sheet
									$objPHPExcel->createSheet();

									// Add some data to the second sheet, resembling some different data types
									$objPHPExcel->setActiveSheetIndex(($k+1));
									// Rename 2nd sheet
									$objPHPExcel->getActiveSheet()->setTitle($custdetails[$k]['cust_short_name']);
									$result ="";
									$result = $dbase->getrptrequestdetails($_POST['pid'],$_POST['bid'],$custdetails[$k]['cid']);
									$lastsheet= $k+1;
								if(count($result) >0){
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,1,"Customer Name : ".$custdetails[$k]['cname']);
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,1,"Total : ".count($result));
									$objPHPExcel->getActiveSheet()->mergeCells("A1:C1");
									$objPHPExcel->getActiveSheet()->mergeCells("D1:E1");
									$objPHPExcel->getActiveSheet()->getStyle("A1:E1")->getFont()->setBold(true);

									$objPHPExcel->getActiveSheet()->getColumnDimension($cha)->setWidth(20);
									$objPHPExcel =  getdetailsreport($objPHPExcel,$result,$projres,$row1,$row3,$row2,$stylearr);
								}
						}
					}
							//Weekreport
						/*	if($is_weekly_report == 1){
								$lastweekrpt = $dbase->getrptrequestdetails($_POST['pid'],$_POST['bid'],0,1,$weekreportresult[0]['reportdate']);
								if(count($lastweekrpt) > 0){
									$objPHPExcel->createSheet();
									$objPHPExcel->setActiveSheetIndex($lastsheet+1);
									$objPHPExcel->getActiveSheet()->setTitle("Last week report");
									
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,1,"Last week status : ".date('d-M-Y',strtotime($weekreportresult[0]['reportdate'])));
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,1,"Total : ".count($lastweekrpt));
									$objPHPExcel->getActiveSheet()->mergeCells("A1:C1");
									$objPHPExcel->getActiveSheet()->mergeCells("D1:E1");
									$objPHPExcel->getActiveSheet()->getStyle("A1:E1")->getFont()->setBold(true);
									
									$objPHPExcel =  getdetailsreport($objPHPExcel,$lastweekrpt,$projres,$row1,$row3,$row2,$stylearr);
								}
							}*/
								/*if(count($result) >0){
						for($s=0;$s<count($result);$s++){
							if(count($projres) >0){
								$j=0;
								for($i=1;$i<count($projres);$i++){
									
									if($s ==0){
										$row =1;
										$cha = chr($start);
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row1,$projres[$i]['headername']);
										$objPHPExcel->getActiveSheet()->getColumnDimension($cha)->setWidth(20);
										$objPHPExcel->getActiveSheet()->getStyle($cha.$row1)->getFont()->setBold(true);
										if($projres[$i]['itypeid'] == 9){
											$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row2,trim($result[$s]['created_by']));
										}else{
										
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row2,trim($result[$s]['sFld'.($projres[$i]['orderid']-1)]));
									}
										$start++;
										
									}else{
										if($projres[$i]['itypeid'] == 9){
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row3,trim($result[$s]['created_by']));
										}else{
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row3,trim($result[$s]['sFld'.($projres[$i]['orderid']-1)]));
										}
										
									}
										$j++;
								}
								$objPHPExcel->getActiveSheet()
       							 ->getStyle('A'.$row1.':'.$cha.$row3)->applyFromArray(
									$stylearr
								);
							}
							$row3++;
						}

					// Rename 2nd sheet
					$objPHPExcel->getActiveSheet()->setTitle('Details');
					}*/
							//exit;
					// Create a new worksheet, after the default sheet
//					$objPHPExcel->createSheet();

					// Add some data to the second sheet, resembling some different data types
					/*$objPHPExcel->setActiveSheetIndex(2);
					$objPHPExcel->getActiveSheet()->setCellValue('A1', 'More data');

					// Rename 2nd sheet
					$objPHPExcel->getActiveSheet()->setTitle('Third sheet');*/
					$objPHPExcel->setActiveSheetIndex(0);
					
					//$objPHPExcel->setTitle('Summary');
					$filename = $projectname."_status_charts_".date('d_M_Y').".xlsx";
					
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
					$objWriter->setIncludeCharts(TRUE);
			
					
					ob_end_clean();
					// We'll be outputting an excel file
					header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
					// It will be called file.xls
					header('Content-Disposition: attachment; filename="'.$filename.'"');		
					$objWriter->save('php://output');
					exit();

				}
				
			}

			
}


		
