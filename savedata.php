<?php
    include_once("top.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{ 
		if($_POST){
			$save_status=0;
			//Add model
			if($_POST['type'] == 1){
				if(isset($_POST['editid'])){
					$id= $_POST['editid'];
				}else{
					$id =0;
				}
				$data['modelnum'] = strtoupper(trim($_POST['modelnum']));
				$res = $master->checkmodel($data['modelnum']);
				if($res['cnt'] == 0){
					$result = $master->savemodeldata($id,$data);//print_r($result);exit;
					if($result['value'] >0){
						echo '{"status":"success","message":"Data has been saved successfully","label":'.'"'.$result['label'].'"'.',"value":'.'"'.$result['value'].'"'.'}';
					}else{
						echo '{"status":"failure","message":"Something went wrong. Please try again!"}';
					}
				}else{
					echo '{"status":"failure","message":"Model number already exists"}';
				}
				exit;
			}
			//Add customer
			if($_POST['type'] == 2){
				if(isset($_POST['editid'])){
					$id= $_POST['editid'];
				}else{
					$id =0;
				}
				$data['cust_name'] = strtoupper(trim($_POST['cust_name']));
				$data['cust_code'] = strtoupper(trim($_POST['cust_code']));
				$res = $master->checkcustomer($data['cust_code'],$data['cust_name']);
				if($res['cnt'] == 0){
					$result = $master->savecustomerdata($id,$data);
					if($result['value'] >0){
						echo '{"status":"success","message":"Data has been saved successfully","label":'.'"'.$result['label'].'"'.',"value":'.'"'.$result['value'].'"'.'}';
					}else{
						echo '{"status":"failure","message":"Something went wrong. Please try again!"}';
					}
				}else{
					echo '{"status":"failure","message":"Customer name/code already exists"}';
				}
				exit;
			}
			//Add supplier
			if($_POST['type'] == 3){
				if(isset($_POST['editid'])){
					$id= $_POST['editid'];
				}else{
					$id =0;
				}
				$data['code'] = strtoupper(trim($_POST['code']));
				$res = $master->checksupplier($data['code']);
				if($res['cnt'] == 0){
					$result = $master->savesupplier($id,$data);
					
					if($result['value'] >0){
						echo '{"status":"success","message":"Data has been saved successfully","label":'.'"'.$data['code'].'"'.',"value":'.'"'.$result['value'].'"'.'}';}
					else{
						echo '{"status":"failure","message":"Something went wrong. Please try again!"}';
					}
				}else{
					echo '{"status":"failure","message":"Supplier code already exists"}';
				}
				exit;
		}
		}
	}?>