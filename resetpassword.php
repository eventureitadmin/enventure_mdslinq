<?php
    include_once("header.php");

	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{ ?>
	<style>
		.yesbtn{
				margin-left: 70px !important;
			}
		.nobtn{
				margin-right: 70px !important;
			}
	</style>

	<script>

	var batchid ='<?php echo $_SESSION['partlinq_user']['BATCHID'];?>';



	function validatepwd(pass_pattern){
		var isvaliddata = true;
		$(".error").css("display","none");
		$(".pwcheck").each(function(index){
			var currentval = $(".pwcheck").eq(index).val();
			if(currentval == ""){
				$(".error").eq(index).css("display","block");
				isvaliddata = false;
			}else{
				$(".error").eq(index).hide();
				var passvalid = isvalidpaassword(currentval,pass_pattern);
				 if(!passvalid){
					$(".error").eq(index).html("Password must contain at least one number, one lowercase character and one special character and one uppercase letter with min 8 characters");
					$(".error").css("display","block");

					isvaliddata = false;
				}else{
					isvaliddata =true;
				}
			}
		});
		if(isvaliddata && $("#newpassword").val() != $("#reenterpassword").val()){
			isvaliddata = false;
			$(".error").eq(1).html("Password must match");
			$(".error").eq(1).show();
		}
		return isvaliddata;
	}

		function checksequencenum(str){
			$(".error").css("display","none");
			var seqnum = false;
			var	matches = str.match(/\d+/g);
			if(matches !="" && matches !=null){
				if(matches.length >0){
					for(var k=0;k<matches.length;k++){
						if(matches[k].length >1){
							seqnum = true;
						}
					}
				}
			}
			if(seqnum){
				$(".error").html("Sequential numbers are not allowed");
				$(".error").css("display","block");
				return false;
			}else{

				$(".error").html("");
				$(".error").css("display","none");
				return true;
			}	
		}

		function isvalidpaassword(newpassword,pass_pattern){
				var isvalidpass = false;
				//var splChars = "*|\:<>[]{}`\;()@&$#%!";
				var splChars = "!`@#$%^&*()+=-[]\\\';,./{}|\":<>?~_";

				if(!/[A-Z]/.test(newpassword)) {
					isvalidpass = false;
				} else if (!/[a-z]/.test(newpassword)) {
					isvalidpass = false;
				} else if (!/[0-9]/.test(newpassword)) {
					isvalidpass = false;
				} else if (!/[\d=!\-@._*A-Za-z]/.test(newpassword)){
					isvalidpass = false;
				}else if(newpassword.length < 8){
					isvalidpass = false;
				}else{
					for (var i = 0; i < newpassword.length; i++) {
						if (splChars.indexOf(newpassword.charAt(i)) != -1){
							isvalidpass =true;
						}
					}

				}
			return isvalidpass;
		}
	</script>

			<div class="container" style="margin-top:40px">
				<div class="row"><div class="alert alert-danger col-md-4 col-md-offset-4" role="alert" id="errormsgid" style="display:none">				</div></div>
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
						<div class="login-panel panel panel-default">
							<div class="panel-heading">
								 <img style="display: block; margin-left: auto; margin-right: auto;max-width: 120px;" src="images/mdslinq.png"/>
								<p style="font-size:24px;font-weight:900;color:#024B9A;text-align:center;letter-spacing:5px;">IMDS Manager</p>
								<h4 align="center">Update Password</h4>
							</div>
							<?php if($msg!=''){?>
							<div class="alert alert-danger">
								<?php echo $msg; $msg=''; ?> 
							</div>
							<?php } ?>							
							<div class="panel-body">
								<form role="form" id="resetfrm_login" action="" method="post">
									<fieldset>
										<input type="hidden" name="is_reset_pwd" id="is_reset_pwd" value="1">
										<div class="form-group">
											<input class="form-control required pwcheck" placeholder="New password" id="newpassword" name="newpassword" type="password"/><p class="error" style="color:red;display:none">This field is required</p>
										</div>
										<div class="form-group">
											<input class="form-control required pwcheck" placeholder="Confirm password" id="reenterpassword" name="reenterpassword" type="password"/><p class="error" style="color:red;display:none">This field is required</p>
										</div>

										<!-- Change this to a button or input when using this as a form -->
										 <div class="form-group">
										 <button type="button" id="reset_btn" class="btn btn-primary btn-lg btn-block">Update</button>
											 <button type="button" id="cancel_btn" class="btn btn-danger btn-lg btn-block" onclick="window.location.href='login.php'">Cancel</button>
									
							            </div>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
				</div>

					<div class="row">
				<div class="col-md-4 col-md-offset-4" ><b>Secure password tips:</b><br/>Password Must be minimum  8 characters long.<br/>
		Must contain minimum one character from each categories below:<br/>
					<ul><li>Uppercase letter (A-Z)</li>
					<li>Lowercase letter (a-z)</li>
					<li>Digit (0-9)</li>
					<li>Special character (~`!@#$%^&*()+=_-{}[]\|:;”’?/<>,.)</li><ul>
		</div>
				</div>

			</div>
	<script>var pass_pattern = <?php echo PASSWORDREG; ?> 
	</script>
	<script>
	(function($){
			$(document).ready(function(){

			var isvaliddata = false;
			$("#reset_btn").click(function(){
//alert(validatepwd(pass_pattern)+"&&"+checksequencenum($("#newpassword").val()));
				if(validatepwd(pass_pattern) && checksequencenum($("#newpassword").val())){
					isvaliddata = true;
				}else{
					isvaliddata = false;
				}
			if(isvaliddata){
					bootbox.confirm({ 
					size: "small",
					message: " Are you sure to update password ?",
					buttons: cfmbtnobj,
					callback: function(result){ 
						if(result){
								showloadimg();
							$.ajax({
							type:"POST",
							url:"saveforgotpassword.php",
							data:$("#resetfrm_login").serialize(),
							dataType:"json",
							success: function(response){
								var obj = response;
								if(obj.status=='success'){
										$.notify(obj.message,"success");
										window.location.href="login.php";

								}
								else{
								$.notify(obj.message,"error");
								}
								  $.unblockUI();
							}
						});

						}

					}
				}).find('.modal-content').css(bootboxobj);

			}
			});

		});
	})(jQuery);	
	</script>

<?php }  ?>
