<?php
function show_success_msg($msg){
	$html = '';
	$html .= '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div>';
	return $html;
}
function show_error_msg($msg){
	$html = '';
	$html .= '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div>';
	return $html;
}
?>