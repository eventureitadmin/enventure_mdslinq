<?php 
define("SELECTNOUNMOD", "Filter Noun / Modifier:");
define("SELECTCOLUMN", "Select Memory Column:");
define("SELECTSCHEMA", "Assign Noun/Modifier:");
define("SELECTLABEL", "Please Select");
define("BTNSAVESCHEMA", "Save Noun/Modifier");
define("BTNHIDEVIEW", "Hide/View Input Headers");
define("BLANK", "Blank");
define("SHKMENU", "Shortcut Keys List(CTRL+S)");
define("IHLABEL", "Input Headers");
define("OHLABEL", "Output Headers");
define("BTNCLEAR", "Clear Filter");
define("BTNSAVE", "Save");
define("BTNREJECT", "Reject");
define("ALPHANUMMSG","allow only alphabets/digits");
define("ALPHANUMWITHMSG","allow only alphabets/digits/symbols(- and #)");
define("DECIMALMSG","allow only alphabets/digits/decimal numbers");
define("EMAILMSG","enter valid email address");
define("URLMSG","enter valid url");
define("DIGITSMSG","allow only digits");
define("EMPTYDATAMSG","No data available");
define("ALPHANUM","No data available");
define("ALPHANUMWITH","No data available");
define("RESETPWDMSG","Your default password is not updated , please change your password");
define("FREEZEMSG","Are you sure you want to continue ?<br>Freeze column will be disabled.<br>Please select less than or equal to 4 input headers to enable freeze column(s).");
define("ASSIGNSUPPLIERS", "Assign Suppliers");
define("save_success_msg", "Data has been saved successfully");
define("save_failure_msg", "Data save error");

?>