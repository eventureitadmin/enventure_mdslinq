<?php
	include_once("header.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{
	$id ="";
	if($_POST){
	$isactive = $_POST['isactive'];
	$editid = $_POST['editid'];
	$cust_name = strtoupper(trim($_POST['cust_name']));
	$cust_code = strtoupper(trim($_POST['cust_code']));
		//check it in database
		if($editid !=''){
			$idcond = " AND id != '".$editid."'";
		}
		else{
			$idcond = "";
		}			
		$check_query = "SELECT COUNT(id) as cnt FROM env_customer_details WHERE cust_code='".$cust_name."' OR cust_name='".$cust_name."'".$idcond;
		$cnt_result = $dbase->getRow($check_query);
		if($cnt_result['cnt'] > 0){
			header('Location: customerlist.php?m=3');
			exit();				
		}
		else{
			if($editid !=''){
			$custquery = "UPDATE env_customer_details SET `cust_code` = '".$cust_code."',`cust_name` = '".$cust_name."',`cust_short_name` = '".$_POST['cust_short_name']."',`isactive` = '".$isactive."' WHERE `id` ='".$editid."'";	
				$dbase->query($custquery);
			 $userid = $editid;
			}
			else{
			$custquery = "INSERT INTO `env_customer_details` (`cust_code`, `cust_name`,`cust_short_name`,`isactive`) VALUES ( '".$cust_code."', '".$cust_name."','".$_POST['cust_short_name']."','".$isactive."');";
				$dbase->query($custquery);
			 $userid = $dbase->lastId();
				
			}
			
			 if($editid !=''){
				header('Location: customerlist.php?m=2');
				exit();	
			 }
			 else{
				header('Location: customerlist.php?m=1');
				exit();				 
			 }			
		}	
}
	else{
		$label = "Add";
		$button = "Add";
	}
		if($_GET){
			$label = "Edit";
			$button = "Update";
			$id=$_GET['id'];
			if($id !=""){
			$user_result =$dbase->executeQuery("SELECT id,cust_code,cust_name,cust_short_name,isactive FROM env_customer_details WHERE id=".$id,"single");
			}
			$del = $_GET['del'];
			if($del!=''){
				if($del=='0'){
			$customerquery = "UPDATE env_customer_details SET `isactive` = '0' WHERE `id` ='".$id."'";
			$dbase->query($customerquery);					
				header('Location: customerlist.php?m=4');
				}
				if($del=='1'){
			 $customerquery = "UPDATE env_customer_details SET `isactive` = '1' WHERE `id` ='".$id."'";
			$dbase->query($customerquery);					
				header('Location: customerlist.php?m=5');
				}
			}
		}
?>
    <body>			
		<?php include("menu.php"); ?>
<link href="css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<link href="css/dataTables/dataTables.responsive.css" rel="stylesheet">		
            <div id="page-wrapper">
								<?php if($_GET['m']==1){ echo show_success_msg('Added successfully'); } ?>
								<?php if($_GET['m']==2){ echo show_success_msg('Updated successfully'); } ?>
								<?php if($_GET['m']==3){ echo show_error_msg('Customer Details Already Exits.'); } ?>
								<?php if($_GET['m']==4){ echo show_success_msg('Customer Inactive Successfully'); } ?>
								<?php if($_GET['m']==5){ echo show_success_msg('Customer Active Successfully'); } ?>
								
							
				<?php if($_SESSION['partlinq_user']['USERTYPE']=='0'){ ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header"><?php echo $label; ?> Customer</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <form role="form" id="frm_customer" name="frm_customer" action="" method="post">
                                            <div class="form-group">
                                                <label>Customer ID</label>
                                                <input type="text" name="cust_code" id="cust_code" value="<?php if($id!=''){ echo $user_result['cust_code']; } ?>" class="form-control required" >
                                            </div>
                                            <div class="form-group">
                                                <label>Customer Name</label>
                                                <input type="text" name="cust_name" id="cust_name" value="<?php if($id!=''){ echo $user_result['cust_name']; } ?>" class="form-control required alphanumericsymbol" >
                                            </div>
											   <div class="form-group">
                                                <label>Customer Short Name</label>
                                                <input type="text" name="cust_short_name" id="cust_short_name" value="<?php if($id!=''){ echo $user_result['cust_short_name']; } ?>" class="form-control required alphanumericsymbol" >
                                            </div>
                                             <div class="form-group">
                                                <label>Status</label>
                                                <label class="radio-inline" style="margin-left:10px;">
                                                    <input type="radio" name="isactive" value="1" <?php if($id!=''){  if($user_result['isactive']=='1'){ echo "checked"; } } else { echo "checked"; } ?> >Active
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="isactive" value="0" <?php if($id!=''){  if($user_result['isactive']=='0'){ echo "checked"; } } ?>>Inactive
                                                </label>
                                            </div>                                           										
											<?php if($id!=''){ ?>
												<input type="hidden" name="editid" id="editid" value="<?php echo $id; ?>" />
											<?php } ?>
                                            <button type="submit" class="btn btn-primary"><?php echo $button; ?></button>
											<?php if($id!=''){ ?>
											<button type="button" class="btn btn-primary" onclick="location.href = 'customerlist.php';" >Cancel</button>
											<?php } ?>
                                        </form>
                                    </div>
                                </div>
							
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>				
				<?php } ?>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Customer List</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover" id="customertable">
                                        <thead>
                                            <tr>
												<th>ID</th>
                                                <th>Customer ID</th>
                                                <th>Customer Name</th>
												 <th>Short Name</th>
												<th>Status</th>
                                                <th class="nosort">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
										
										$customerlist_select = "SELECT `id`, `cust_code`, `cust_name`,`cust_short_name`,isactive FROM `env_customer_details` WHERE 1=1 ORDER BY id DESC";
										$customerlist = $dbase->executeQuery($customerlist_select,"multiple");
										for($i=0;$i<count($customerlist);$i++){
											$status = "";
											$usertype ="";
											if($customerlist[$i]['isactive']=='1'){
												$status = "Active";
											}
											else{
												$status = "Inactive";
											}
											
											if (0 == $i % 2) {
												$class = 'class="even"';
											}
											else{
												$class = 'class="odd"';
											}
											$editurl = '';
											$editurl .= '<a href="customerlist.php?id='.$customerlist[$i]['id'].'">Edit</a> | ';
																			
											if($customerlist[$i]['isactive']=='1'){
												$activeurl = "<a href='customerlist.php?del=0&id=".$customerlist[$i]['id']."'>Inactive</a>  ";
											}
											else{
												$activeurl = "<a href='customerlist.php?del=1&id=".$customerlist[$i]['id']."'>Active</a>  ";												
											}
											
											echo '<tr '.$class.'>
                                                <td>'.($i+1).'</td>
                                                <td>'.$customerlist[$i]['cust_code'].'</td>
                                                <td>'.$customerlist[$i]['cust_name'].'</td>
                                                <td>'.$customerlist[$i]['cust_short_name'].'</td>
										    	<td>'.$status.'</td>
                                                <td >'.$editurl.$activeurl.'</td>
                                            </tr>';
										}
										?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->

                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>				
                </div>				
            </div>
            <!-- /#page-wrapper -->
<script src="js/dataTables/jquery.dataTables.min.js"></script>
<script src="js/dataTables/dataTables.bootstrap.min.js"></script>		
		<script type="text/javascript">
			
		$(document).ready(function() {
		
			$("#frm_customer").validate();
			jQuery.validator.addMethod("alphanumericsymbol", function(value, element) {
			  return this.optional( element ) || /^[a-z\d\._()\s]+$/i.test( value );
			}, 'Allow alphabets and numbers symbols (._) are allowed to enter');
			
                $('#customertable').DataTable({
                        responsive: true,
					   'aoColumnDefs': [{
							'bSortable': false,
							'aTargets': ['nosort']
						}]						
                });				
		});
		</script>			
	</body>

	<?php
		 include_once("footer.php");
	}	
?>
