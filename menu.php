
<style>
.marginBottom-0 {margin-bottom:0;}
.logopadding{padding: 5px 10px;}
.dropdown-submenu{position:relative;}
.dropdown-submenu>.dropdown-menu{top:0;left:100%;margin-top:-6px;margin-left:-1px;-webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;}
.dropdown-submenu>a:after{display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#ffffff;margin-top:5px;margin-right:-10px;}
.dropdown-submenu:hover>a:after{border-left-color:red;}
.dropdown-submenu.pull-left{float:none;}.dropdown-submenu.pull-left>.dropdown-menu{left:-100%;margin-left:10px;-webkit-border-radius:6px 0 6px 6px;-moz-border-radius:6px 0 6px 6px;border-radius:6px 0 6px 6px;}
/*https://www.bootply.com/nZaxpxfiXz*/
	
</style>
<nav class="navbar navbar-inverse navbar-static-top marginBottom-0" role="navigation" style="position: fixed;top:0;width: 100%;background-color:#E5E5E5;border-color=white" >
	<div class="navbar-header">
		<a class="navbar-brand logopadding" href="home.php?&batch=1"><img style="max-width: 120px;" src="images/mdslinq.png"/></a>
	</div>
	
	 <ul class="nav navbar-nav  navbar-top-links ">
		 
			<!---<li><a href="index.php" style="color:black;background-color:white">Home</a></li>-->
		 
		 <?php if($_SESSION['partlinq_user']['USERTYPE']=='0' || $_SESSION['partlinq_user']['USERTYPE']=='5'){?>
		 
			<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:black;background-color:#E5E5E5">Master <b class="caret"></b></a>
				<ul class="dropdown-menu">
					 <?php if($_SESSION['partlinq_user']['USERTYPE']=='0' || $_SESSION['partlinq_user']['USERTYPE']=='5'){?>
					<li><a href="assignparts.php?pid=<?php echo $_SESSION['partlinq_user']['BATCHID'];?>">Assign Parts</a></li>
					<li><a href="userlist.php?pid=<?php echo $_SESSION['partlinq_user']['BATCHID'];?>">User List</a></li>
					<li><a href="customerlist.php?pid=<?php echo $_SESSION['partlinq_user']['BATCHID'];?>">Customer List</a></li>
					<?php }?>
					<?php if($_SESSION['partlinq_user']['USERTYPE']=='0'){?>
					<li class="divider"></li>
					<li class="dropdown dropdown-submenu"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Import</a>
						<ul class="dropdown-menu">
							<li><a href="importheader.php">Headers</a></li>
							<li><a href="importunrptheader.php">Unreported Headers</a></li>
							<!---<li><a href="importrequests.php">Requests</a></li>-->
							
						</ul>
					</li>
					<?php } ?>
				</ul>
			</li>
		 <?php } else if($_SESSION['partlinq_user']['USERTYPE']=='2'){ ?>
			<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:black;background-color:#E5E5E5">Master <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="userlist.php?pid=<?php echo $_SESSION['partlinq_user']['BATCHID'];?>">User List</a></li>
				</ul>
			</li>		 
		 <?php } ?>
		 <?php if($_SESSION['partlinq_user']['USERTYPE'] ==0){?>
		<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:black;background-color:#E5E5E5">Report <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="weekreport.php">Week Report</a></li>
					<?php if($_SESSION['partlinq_user']['USERTYPE'] ==0){?>
					<li><a href="dailyreport.php">Daily Report</a></li>
						<?php }?>
					<?php if($_SESSION['partlinq_user']['ALLOWPROGRESS'] > 0 || $_SESSION['partlinq_user']['USERTYPE'] ==0){?>
				   <!--- <li><a href="addprogress.php">Add Progress</a></li>--->	
					<?php }?>
				</ul>
			
			</li>	
		 <?php }?>
		  <?php if($_SESSION['partlinq_user']['USERTYPE'] ==0){?>
			<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:black;background-color:#E5E5E5">Settings <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="loginconfig.php?id=1">Login Settings</a></li>
					<li><a href="iplist.php">IP Logging</a></li>
					<li><a href="ipblock.php">IP Block</a></li>
					<li><a href="userloginlog.php"> Login Audit Log</a></li>
				</ul>
		 	</li>
		 <?php }?>
	</ul> 
	<?php $pwdres = $security->checkpwdexpiry($_SESSION['partlinq_user']['ID'],1);
		if($pwdres['cnt'] == 1){
			if($pwdres['expiry_type'] == 1){
				$message ="Password expiry in".$pwdres['num']." month(s)";
			}else if($pwdres['expiry_type'] == 2){
				$message ="Password expiry in".$pwdres['num']." day(s)";
			}
		?>
		<p style="color:red"><?php echo $message;?> </p>
	<?php }?>

	 <ul class="nav navbar-right navbar-top-links">
		
		 <li class="dropdown" >
				<a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:black;background-color:#E5E5E5">
					<i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['partlinq_user']['FULLNAME']; ?> <b class="caret"></b>
				</a>
				<ul class="dropdown-menu dropdown-user">
                        <li><a href="#" data-toggle='modal' title='Change Password' onclick='showchangepwd()' id='changepwd' style="color:black;background-color:white"><i class="fa fa-user fa-fw"></i> Change Password</a>
                        </li>					
					<li class="divider"></li>
					<li><a href="logout.php" style="color:black;background-color:white"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
			</ul>
		</li>
	</ul>
	
</nav>
<style>
	.navbar-inverse .navbar-nav > .open > a, .navbar-inverse .navbar-nav > .open > a:focus, .navbar-inverse .navbar-nav > .open > a:hover {
	background-color:white
}
	.navbar-inverse{
			border-color:#E5E5E5
	}
</style>
<script>
(function($){
	$(document).ready(function(){
		$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
			event.preventDefault(); 
			event.stopPropagation(); 
			$(this).parent().siblings().removeClass('open');
			$(this).parent().toggleClass('open');
		});
	});
})(jQuery);	
</script>
<?php include('changepwdform.php');
