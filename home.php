<?php
    include_once("header.php");

	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{
		
		if(isset($_GET['batch']) && $_GET['batch'] >0){
			$cols= HIDDEN_NUM;
			$duedatefieldindex =$dbase->executeQuery("SELECT (`orderid`-1) oid FROM `env_projectheaders` 
			WHERE `batchid`=".$_GET['batch']." AND `itypeid`=10","single")[0];
			$sqadatefieldindex =$dbase->executeQuery("SELECT (`orderid`-1) oid FROM `env_projectheaders` 
			WHERE `batchid`=".$_GET['batch']." AND `hidden_type`=4","single")[0];
			$reqdateindex =$dbase->executeQuery("SELECT (`orderid`-1) oid FROM `env_projectheaders` 
			WHERE `batchid`=".$_GET['batch']." AND `hidden_type`=1","single")[0];
			
				 $ctargetdatefieldindex =$dbase->executeQuery("SELECT (`orderid`-1) oid FROM `env_projectheaders` 
			WHERE `batchid`=".$_GET['batch']." AND `hidden_type`=5","single")[0];
			 $summarylbl = "summary".$res['project_id'].$_GET['batch'];
			 $res = $dbase->executeQuery("SELECT `project_id`,(SELECT `customer_id` as cid 
				FROM `env_project` WHERE id =`project_id`)as cid  FROM `env_batch`
				WHERE id=".$_GET['batch']." AND `closed`=0","single");
			$formuladata  = $dbase->getFormulaTypes($_GET['batch']);
			$formulatypesarr = [];
			$formalasorderids = [];$formulaindex=[];
			if(count($formuladata) >0){
				  foreach($formuladata as $key=>$value){
					 $itypeid = $value['itypeid'];$formulatype =$value['formulatype'];
					 $formulatypesarr[$itypeid] = $formulatype;
					 $formalasorderids[$itypeid] = $value['orderid'];
					$formulaindex[$key]=$itypeid;
				 }
			}
			$autofieldsarr="";
			$autofieldsarr = $dbase->getfieldsorder($_GET['batch'],1);
			$checkcmpltstatus =false;
			if(isset($_GET['statusid']) && ( $_SESSION['partlinq_user']['USERTYPE'] == 1 || $_SESSION['partlinq_user']['USERTYPE'] == 2) || $_SESSION['partlinq_user']['USERTYPE'] == 0 || $_SESSION['partlinq_user']['USERTYPE'] == 6){
				if($dbase->checkcompletedpartstatus($_GET['statusid'] ,$_GET['batch'])['cnt'] >0){
					$checkcmpltstatus =true;
				}else{
					$checkcmpltstatus =false;
				}
			}
			
    ?>
	<style>
		.yesbtn{
			margin-left: 70px !important;
		}
		.yesbtn1{
			margin-left: 300px !important;
		}
		.nobtn1{
			margin-right: 300px !important;
		}
		.okbtn{
			margin-left: 50px !important;
		}
		.nobtn{
			margin-right: 70px !important;
		}
		.handsontable .htCheckboxRendererInput{
			margin-left:15px
		}
	</style>
	<script type="text/javascript" src="js/requestform.js"></script>
	<script>
		var	pid = "<?php echo $res['project_id'];?>";
		var	batch = "<?php echo $_GET['batch'];?>";
		var	usertype = "<?php echo $_SESSION['partlinq_user']['USERTYPE'];?>";
		var	statusid = "<?php echo $_GET['statusid'];?>";
	 	var	sorder = "<?php echo $_GET['sorder'];?>";
	    var	cid = "<?php echo $res['cid'];?>";
	    var	selcid = "<?php echo $_GET['scid'];?>";
	    var	tab = "<?php echo $_GET['tab'];?>";
		var	currurl ="";
		var hotobj = "";
		var hot1 = "";
		var hot2 = "";
		var hot3 = "";
		var newformulatypesarr ="";
		var frmindex =1;
		var reqformautoarr ="";
		var scid ='';
		var selgridcol = 3;
		var gridheight=230;
			var formulatypes = <?php echo json_encode($dbase->getFormulaTypes($_GET['batch']));?>;
			var formulatypesids = <?php echo json_encode($formalasorderids);?>;
			var formulatypesarr = <?php echo json_encode($formulatypesarr);?>;
			var formulaindex = <?php echo json_encode($formulaindex);?>;
			var pendingstatusarr = <?php echo json_encode(array_column($dbase->getstatus($_GET['batch'],1,1,1),"statusval"));?>;
			var completedstatusarr = <?php echo json_encode(array_column($dbase->getstatus($_GET['batch'],2,1,1),"statusval"));?>;
			var statusfieldindex = <?php echo $dbase->getfieldindex($_GET['batch'],2)[0]+$cols;?>;
			var custnumindex = <?php echo $dbase->getfieldindex($_GET['batch'],3)[0]+$cols;?>;
			var initialstatusval = "<?php echo $reqcreatorarr[0];?>";
			var duedatefieldindex = "<?php echo $duedatefieldindex+$cols;?>";
			var sqadatefieldindex = "<?php echo $sqadatefieldindex+$cols;?>";
			var ctargetdatefieldindex = "<?php echo $ctargetdatefieldindex+$cols;?>";
			var reqdateindex = "<?php echo $reqdateindex+$cols;?>";
			var reqconfirmmsg ="Request is submitted successfully.<br/>Do you want to submit  another <br/> request ? ";
			var deleteconfirmmsg =" Are you sure to cancel request? ";
    		var deletemsg =" Are you sure to delete part(s)? ";
    		var datevalidationmsg =" The Customer due date <br/> is less than two month <br/> from ComplianceXL target date. <br/> Do you want to submit the request? ";
	</script>
	<script>
		function getday(day){
			if(day <10){
				day ="0"+day;
			}
			return day;
		}
		function setcolor(td){
			td.style.background = '#FFCDCD';
		}
		function mandatoryRenderer (instance, td, row, col, prop, value, cellProperties){
			var escaped = Handsontable.helper.stringify(value);
				escaped = strip_tags(escaped, '<span>'); 
			    td.innerHTML = escaped;
			var newD1 =0;
			var D2 = 0;
			if(duedatefieldindex == col &&  value !=""){
				newD1 = gettimeforfield(value,"duedate");
				 D2 = new Date().getTime();
			}else if((sqadatefieldindex == col || ctargetdatefieldindex == col) &&  value !=""){
				 newD1 = gettimeforfield(value,"duedate");
				 D2 = gettimeforfield(instance.getDataAtCell(row,reqdateindex),"duedate");
			}
			if(newD1 < D2){
			   setcolor(td);
			}
		}
	</script>
	<?php if($autofieldsarr !=""){?>
	<script reqformautoarr= <?php echo json_encode($autofieldsarr);?>;></script>
	<?php }?>
	<?php if($_SESSION['partlinq_user']['USERTYPE'] == 3 || $_SESSION['partlinq_user']['USERTYPE'] ==2 
			 || $_SESSION['partlinq_user']['USERTYPE'] ==1  || $_SESSION['partlinq_user']['USERTYPE'] == 6){
			$is_cancel =0;
	}else{
			$is_cancel =1;
		}
	?>
	
		<script>
			var colwidthdata = [<?php $dbase->getcolwidth($_GET['batch'],0,$is_cancel);?>]; 
			var colheadersdata =[<?php $dbase->getprojheaders($_GET['batch'],$formulaarr,0,$is_cancel);
			?>];
			<?php if($_SESSION['partlinq_user']['USERTYPE'] == 6){$is_cmp =1;}else{ $is_cmp =0;}?>
			var	columnsdata = [<?php $dbase->getprojheadersprops($_GET['batch'],$is_cmp,$is_cancel);?>]; 
			var	allreqcoldata = [<?php $dbase->getprojheadersprops($_GET['batch'],1,1);?>]; 
			var allreqcolwidthdata = [<?php $dbase->getcolwidth($_GET['batch'],0,1);?>]; 
			var allreqcolheadersdata =[<?php $dbase->getprojheaders($_GET['batch'],$formulaarr,0,1)?>];
		</script>
	

 	 <body  oncontextmenu="return false;">
		 <div id="headerpanel">
		 <?php include_once("menu.php");
		$projres = $dbase->executeQuery("SELECT projtype,progress_val,projname,is_weekly_report,customer_id
				FROM env_project WHERE ID =".$res['project_id'],"single");
		$projtype 			=	$projres['projtype'];
		$progress_val 		=	$projres['progress_val'];
		$projectname 		=	$projres['projname'];
		$is_weekly_report 	=	$projres['is_weekly_report'];
		$customer_id 		=	$projres['customer_id'];
		$batchid = $dbase->getNameNew("env_batch","batchno","id='".$_GET['batch']."'");
		$headername = "Status Summary ".date('d-M-Y');
		 ?>
		</div>
	
	<form name="export_status_rpt" id="export_status_rpt" method="post" action="exportrequests.php">
		<input type="hidden" id="pid" name="pid" value="<?php echo  $dbase->executeQuery("SELECT project_id FROM env_batch WHERE id=".$_GET['batch'],"single")[0];?>"/>
		<input type="hidden" id="bid" name="bid"  value="<?php echo $_GET['batch'];?>"/>
		<input type="hidden" id="cid" name="cid"  value="<?php echo $res['cid'];?>"/>
		<input type="hidden" id="is_weekly_report" name="is_weekly_report"  value="<?php echo $is_weekly_report;?>"/>
		
	</form>
		 <br/>
		 <br/>
		 <br/>		 
		<div class="panel panel-default">
				<input type="hidden" name="is_grid_view" id="is_grid_view" value="">
				
                        <!-- /.panel-heading -->
                        <div class="panel-body" id="tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
								<li class=""><a href="#summary" data-toggle="tab">Home</a></li>
								<?php if($_SESSION['partlinq_user']['USERTYPE']=='3' || $_SESSION['partlinq_user']['USERTYPE']=='2'){ ?>
								<li class=""><a href="#request" data-toggle="tab">New Request</a></li>	
								<?php } ?>
								<li class=""><a href="#partslist" data-toggle="tab">
									<?php if($_SESSION['partlinq_user']['USERTYPE']=='1' || $_SESSION['partlinq_user']['USERTYPE']=='2' || $_SESSION['partlinq_user']['USERTYPE']=='0' || $_SESSION['partlinq_user']['USERTYPE'] == '6'){ ?>
									Pending Requests
									<?php } else if($_SESSION['partlinq_user']['USERTYPE']=='3') {?>My Requests<?php }
									?></a>
                                </li>
							
							
								<?php if($_SESSION['partlinq_user']['USERTYPE']=='1' || $_SESSION['partlinq_user']['USERTYPE']=='2' || $_SESSION['partlinq_user']['USERTYPE']=='0' || $_SESSION['partlinq_user']['USERTYPE'] == '6'){ ?>
								<li class=""><a href="#comppartslist" data-toggle="tab">Completed Requests</a>
                                </li>
								<?php } ?>
								<?php if($_SESSION['partlinq_user']['USERTYPE']=='1' ||$_SESSION['partlinq_user']['USERTYPE']=='3' || $_SESSION['partlinq_user']['USERTYPE']=='2' || $_SESSION['partlinq_user']['USERTYPE']=='0' || $_SESSION['partlinq_user']['USERTYPE'] == '6'){ ?>
								<li class=""><a href="#allreqlist" data-toggle="tab">All Requests</a>
                                </li>
								<?php } ?>
									<?php if($_SESSION['partlinq_user']['USERTYPE']=='1' || $_SESSION['partlinq_user']['USERTYPE']=='5' || $_SESSION['partlinq_user']['USERTYPE']=='2' || $_SESSION['partlinq_user']['USERTYPE']=='3' || $_SESSION['partlinq_user']['USERTYPE']=='0' || $_SESSION['partlinq_user']['USERTYPE'] == '6' ){ ?>
		
								<li class=""><a href="#unrptlist" data-toggle="tab">Unreported Parts</a>
                                </li>
								<?php } ?>
								<?php if($_SESSION['partlinq_user']['USERTYPE']=='2' || $_SESSION['partlinq_user']['USERTYPE']=='3' || $_SESSION['partlinq_user']['USERTYPE']=='0'){?>
								<li class="" ><a href="#impreq" data-toggle="tab">Import Requests</a>
                                </li>
								<?php }?>
								<!--li class="" ><a href="#newlist" data-toggle="tab">New Theme List</a></li>-->						
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                           
                                <div class="tab-pane fade statustab" id="summary" style="overflow-x:auto;overflow-y:hidden">
									<?php if($_SESSION['partlinq_user']['USERTYPE']=='0' ||$_SESSION['partlinq_user']['USERTYPE']=='3' || $_SESSION['partlinq_user']['USERTYPE']=='2' || $_SESSION['partlinq_user']['USERTYPE'] == '6'){ ?>
										<div style="float:right;margin-top:5px">
										<input type="button" class="btn btn-primary btn-xs " name="exportrpt" id="exportrpt" value="Export"/>
										</div><br/>
									<?php }?>
										<div id="summartcontent" style="display:none"></div>								
                				 </div>
								<?php if($_SESSION['partlinq_user']['USERTYPE']=='3' || $_SESSION['partlinq_user']['USERTYPE']=='2'){ ?>
								<div class="tab-pane fade statustab" id="request">
									<?php include_once('requestform.php');?>
								</div>
								
								<?php } ?>
								
                                <div class="tab-pane fade statustab" id="partslist">
									<div id="gridpage" style="display:block">
									 <?php include_once('pricesourcinggrid.php');?>
									</div>
                                </div>
						
								<?php if($_SESSION['partlinq_user']['USERTYPE']=='1' || $_SESSION['partlinq_user']['USERTYPE']=='5' || $_SESSION['partlinq_user']['USERTYPE']=='2' || $_SESSION['partlinq_user']['USERTYPE']=='0' || $_SESSION['partlinq_user']['USERTYPE'] == '6'){ ?>
                               <div class="tab-pane fade statustab" id="comppartslist">
								   <div id="completedparts" style="display:block">
                                  <?php include_once('completedpartsgrid.php');?>
									 </div>
                                </div>	
								<?php } ?>
									<?php if($_SESSION['partlinq_user']['USERTYPE']=='1' ||$_SESSION['partlinq_user']['USERTYPE']=='3' || $_SESSION['partlinq_user']['USERTYPE']=='2' || $_SESSION['partlinq_user']['USERTYPE']=='0' || $_SESSION['partlinq_user']['USERTYPE'] == '6'){ ?>
                               <div class="tab-pane fade statustab" id="allreqlist">
								   <div id="allreqlist" style="display:block">
                                  <?php include_once('allreqlistgrid.php');?>
									 </div>
                                </div>	
								<?php } ?>
										<?php if($_SESSION['partlinq_user']['USERTYPE']=='1' || $_SESSION['partlinq_user']['USERTYPE']=='5' || $_SESSION['partlinq_user']['USERTYPE']=='2' || $_SESSION['partlinq_user']['USERTYPE']=='3' || $_SESSION['partlinq_user']['USERTYPE']=='0' || $_SESSION['partlinq_user']['USERTYPE'] == '6' ){ ?>
                               <div class="tab-pane fade statustab" id="unrptlist">
								   <div id="unrptparts" style="display:block">
                                  <?php include_once('unreportedpartsgrid.php');?>
									 </div>
                                </div>	
								<?php } ?>
							<?php if($_SESSION['partlinq_user']['USERTYPE']=='2' || $_SESSION['partlinq_user']['USERTYPE']=='3' || $_SESSION['partlinq_user']['USERTYPE']=='0'){?>
								<div class="tab-pane fade statustab" id="impreq" >
									<?php include_once('importrequests.php');?>
								</div>
								<?php }?>
														
								</div>
							
                        </div>
                        <!-- /.panel-body -->
                    </div>
		<script>
			function showsummary(selbatch,selcid){
             $(".summarychart").css("display","none");
			 $("#summartcontent").css("display","none");
            $(".rptheaders").css("display","none");
			showloadimg();
                     $("#summartcontent").load("summary1.php",{batch:selbatch,cid:selcid,is_weekly_report:"<?php echo $is_weekly_report;?>"},function(data,responseText, textStatus, XMLHttpRequest){
                        if(responseText === "success"){
                       
                         $("#summartcontent").css("display","block");
                         $(".summarychart").css("display","block");
                        //  
                            setTimeout(function(){
                         $(".rptheaders").css("display","block");
                               $.unblockUI();
                    
                       }, 100);
                          }

                     });

			}
				
	function changecustduedate(val,field1,field2){
		$("#sFld"+field1).val(gettimeforfield(val,"sqadate"));
		$("#sFld"+field2).val(gettimeforfield(val,"compliancetgtdate"));
				 if(gettimeforfield($("#sFld<?php echo $reqarr['cmptdate'];?>").val(),"duedate") < gettimeforfield($("#sFld<?php echo $reqarr['rd'];?>").val(),"duedate")){
						 $(".custduedateerr").css("display","block");
				 }else{
					 $(".custduedateerr").css("display","none");
				 }
	}
			
	function saverequest(formobj,editid=0){
		$.ajax({
				type:"POST",
				url:"saverequest.php",
				data:formobj.serialize(),
				success: function(response){
					obj = JSON.parse(response);
					if(obj.status=='SUCCESS'){
					formobj.trigger("reset");
						if(editid >0){
							$("#popupdialog").modal("hide");
							loadGridData1();
						}else{
							$('.nav-tabs a[href="#partslist"]').trigger('click');
						}
				
					}
					else{
						showAlertMsg(obj.msg);
					}
				}
		});
	}
	function filterrequests(selval,index){
		var hotobj ="";
		if(selval == ""){
			$("#clear"+index).click();
		}else{
			$("#selreqtype"+index).val(selval);
			if(index ==1){
				loadGridData1();
			}else if(index ==2){
				loadGridData();
			}else if(index ==4){
				loadallreqGridData();
			}	
		}
					
	}
	function requestconfirmmsg(formobj,datevalidationmsg,batch,bootboxobj,cfmbtnobj,editid=0){
		bootbox.confirm({ 
				size: "small",
				message: datevalidationmsg,
				buttons: cfmbtnobj,
				callback: function(result){ 
					if(result){
						saverequest(formobj,editid);
				    }

				}
			}).find('.modal-content').css(bootboxobj);

	}
	function resetreqdata(is_modal_reset=0,is_suppl_reset=0){
		if(is_suppl_reset  == 1){
			$("#newsFld<?php echo $reqarr['sc'];?>").val("");
			$("#newsFld<?php echo $reqarr['sc'];?>name").val("");
		}
		
		$("#sFld<?php echo $reqarr['cpn'];?>").val("");
		if($("#newsFld<?php echo $reqarr['mn'];?>").val() >0){
			$("#sFld<?php echo $reqarr['cad'];?>").val("");
			$("#sFld<?php echo $reqarr['sqadt'];?>").val("");
			$("#sFld<?php echo $reqarr['cmptdate'];?>").val("");
		}
		$("#sFld<?php echo $reqarr['weight'];?>").val("");
		if(is_modal_reset == 1){
			$("#newsFld<?php echo $reqarr['mn'];?>name").val("");
			$("#sFld<?php echo $reqarr['mn'];?>").val("");
		}		
	}
	function setpreviousdata(selnum,selmodel){
		
		var supplier ="";
		var modelnum ="";
		if(selmodel == ""){
			modelnum =selnum;
			resetreqdata(0,1);
		}else{
			modelnum =selmodel;
			supplier =selnum;
			resetreqdata(0,0);
		}
		if(selnum >0 && $("#request_type").val() ==2){
		$.ajax({
				type:"POST",
				url:"getrequestdetails.php",
				data:{batch:"<?php echo $_GET['batch']; ?>",customer:$("#sFld<?php echo $reqarr['cn'];?>").val(),supplier:supplier,modelnum:modelnum},
				success: function(response){
					obj = JSON.parse(response);
						if(obj.length >0){
							if(selmodel == ""){
						$("#newsFld<?php echo $reqarr['sc'];?>").val(obj[0].supplcode);
						$("#newsFld<?php echo $reqarr['sc'];?>name").val(obj[0].supplcodename);
							}
						$("#sFld<?php echo $reqarr['cpn'];?>").val(obj[0].custpartnum);
						$("#sFld<?php echo $reqarr['cad'];?>").val(obj[0].custduedate);
						$("#sFld<?php echo $reqarr['sqadt'];?>").val(obj[0].sqadate);
						$("#sFld<?php echo $reqarr['cmptdate'];?>").val(obj[0].compldate);
						$("#sFld<?php echo $reqarr['weight'];?>").val(obj[0].weight);
							
						}
				
				}
		});
		}
	}
		 $(document).ready(function() {	
			 
				$("#frm_bulkrequest").validate({
					 submitHandler: function () {
						 if($("#customer_id").text() !=""){
						  showloadimg();
							var formData = new FormData($("#frm_bulkrequest")[0]);
							  formData.append('request_file', $('#request_file')[0].files[0]);
							  formData.append('batchid', $('#batchid').val());
							  formData.append('projid', $('#projid').val());

							$.ajax({
							   url:"importbulkreqsaction.php",
							   type: "POST",
							   enctype: 'multipart/form-data',
							   data:  formData,
							   contentType: false,
							   cache:false,async:false,
							   processData:false,
							   beforeSend : function()
							   {
							   },
							   success: function(data)
								{
									 $.unblockUI();
									var obj = JSON.parse(data);
									var html= "";
									var html= obj.message;
									if(obj.data.length >0){
										html +="<br/><br/><style>table,tr,td{border:1px solid black} td,th{width:200px}</style>";
										html +="<div class='container' style='width:800px;height:250px;overflow:scroll'><table>";
										html +="<tr>";
										html +="<td><b>Customer</b></td>";
										html +="<td><b>Model Number</b></td>";
										html +="<td><b>Supplier Code</b></td>";
										html +="<td><b>Customer Part Number</b></td>";
										html +="<td><b>Customer Due Date</b></td>";
										html +="<td><b>Total Measured Model Weight</b></td>";
										html +="<td><b>Request Type</b></td>";
									
										html +="</tr>";
										for(var s=0;s<obj.data.length;s++){
											html+="<tr>";
											for(var s1=0;s1<obj.data[s].length;s1++){
												if(s1== (obj.data[s].length-1) && obj.data[s][s1] == '1'){
														html+="<td width='20px'>New Request</td>";
												}else if(s1== (obj.data[s].length-1) && obj.data[s][s1] == '2'){
														 	html+="<td width='20px'>Software Change</td>";
											    }else{
														 	html+="<td width='20px'>"+obj.data[s][s1]+"</td>";
												 }
											
											}
											html+="</tr>";
										}
										html +="</table></div>";
									}
									//console.log(obj);
								
									if(obj.status ="success" && obj.notinserted =="" && obj.errtype ==""){
										
										$("#is_submit").val("0");
										$.notify(obj.message,"success");
										$("#request_file").val('')							
										$("#sFld<?php echo $reqarr['rqu'];?>").val('').trigger("chosen:updated");							
										$("#part_type").val('').trigger("chosen:updated");	
										$("#customer_id").html("");
										$("#customer_id").css("display","none");
										$('.nav-tabs a[href="#partslist"]').trigger('click');
										
									}else if(obj.notinserted !=""){
								
										showterrormsg(html);
										//$.notify(obj.message+" "+obj.notinserted+" in Excel sheet","success");
										 $.unblockUI();

									}else if((obj.errtype == 1 || obj.errtype == 2) && $("#is_submit").val() == 0){
									
										bootbox.confirm({ 
											size: "large",
											message: html,
											buttons: cfmbtnobj1,
											callback: function(result){ 
												if(result){
													$("#is_submit").val("1");
													$("#frm_bulkrequest").submit();
												}

											}
										}).find('.modal-content').css(bootboxobj);
									}else{
										showAlertMsg(obj.message);
										//$.notify(obj.message,"error");
										 $.unblockUI();

									}
								}
							});

						}else{
							$.notify("Selected requestor is not allocated to customer ","error");
						}
						
						 
					 }
				});
			
				if(statusid != "" && statusid != null){
					<?php if($checkcmpltstatus){?>
					 activaTab('comppartslist'); 
					<?php }else{?>
					activaTab('partslist'); 
					<?php }?>
				}else{

				 <?php if($_SESSION['partlinq_user']['USERTYPE']=='0' || $_SESSION['partlinq_user']['USERTYPE']=='1' || $_SESSION['partlinq_user']['USERTYPE']=='2' || $_SESSION['partlinq_user']['USERTYPE']=='3' || $_SESSION['partlinq_user']['USERTYPE']=='5' || $_SESSION['partlinq_user']['USERTYPE']=='6'){ ?>
					activaTab('summary');
					
				 <?php } ?>
				 <?php if($_SESSION['partlinq_user']['USERTYPE']=='4'){ ?>
					 activaTab('partslist');
				 <?php } ?>						
					}
			<?php if($progress_val != "" && $progress_val >0){?>
						var myProgress = $('#progress').progressbarManager({      
							 totalValue : 100,
							 initValue : <?php echo $progress_val;?> ,
							 animate : true ,
							 stripe : true
						  });
			<?php }?>
			//Switch tab
			 $(".nav-tabs li a").click(function(e){
				
			   var target = $(e.target).attr("href");
			   	$(".statustab").hide();
				 $(target).show();
				 if(target == "#partslist"){
					$("#request_type1").val("").trigger("chosen:updated");
					$("#selreqtype1").val("");
					  hotobj = hot2;
				 }else if(target == "#comppartslist"){
					 hotobj = hot1;
					$("#request_type2").val("").trigger("chosen:updated");
					$("#selreqtype2").val("");
			     }else if(target == "#summary"){
				
					 	hotobj ="";
				 }else if(target == "#unrptlist"){
					 hotobj =hot3;
				 }else if(target == "#allreqlist"){
					 $("#request_type4").val("").trigger("chosen:updated");
					$("#selreqtype4").val("");
					 hotobj =hot4;
				 }
				 });
			 $("#exportrpt").click(function(){
				  $("#export_status_rpt").submit(); 
			 });
            
			 $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
				var target = $(e.target).attr("href");
			  if(target == "#summary"){
					showsummary("<?php echo $_GET['batch'];?>","<?php echo $res['cid'];?>");
			  }
		
		});
			 //Fiter
			 
	/**
	 *Search Data by filter type
	 */
	$("#search_btn").click(function(){
		  $("#myModal").modal('hide');
		  var selcol =  $("#selected_col").val();
		  var seltabid =  $(".tab-pane.active").attr("id");
		  var  tData = hotobj.getData();
		  var search = hotobj.getPlugin('search');
		  var selparts = "";
		  var sel =0;
		if(seltabid == "partslist"){
			sel =1;
		}else if(seltabid == "comppartslist"){
			sel =2;
		}else if(seltabid == "unrptlist"){
			sel =3;
		}else if(seltabid == "allreqlist"){
			sel =4;
		}
		  var filtersel =  $(".filtersel").val();
		  var totalcnt = $("#totalcnt"+sel).val();
		  var handsontabledata = ""; 
		  var selpartsdata = "";
		  var rows = "";
		  var seldata  = $('#my.selmasterdataselect');
		  var filterpartids = "";
			if(filtersel != "" && filtersel != null && filtersel != undefined)
			{
				var search_text = $("#search_text").val().trim();
				if(filtersel === "text"){
					var queryResult = search.querycolumn(search_text,selcol);
						rows  = getRowsFromObjects(queryResult);
				 } else if(filtersel === "blank"){
				 	 rows  =  getEmptyDataRows(selcol);
				 }
				else if(filtersel === "notblank"){
					rows  =  getNotEmptyDataRows(selcol);
				}
				else if(filtersel === "multipleenv"){
					rows  =  getMultipleENVRows(search_text,selcol,search);
				}
				var filtered = "";
				if((filtersel === "blank" || filtersel === "notblank") || ((filtersel === "text" || filtersel === "multipleenv" )  && search_text !="" && 
			   search_text != null && search_text != undefined))
				{
					 filtered = tData.filter(function (d, ix) {
						  //selparts
						if(rows.indexOf(ix) >= 0){
							selpartsdata += tData[ix][0]+",";
						}
								return rows.indexOf(ix) >= 0;
							});
					
						hotobj.updateSettings({
								 data:filtered
							});	
					
				}
				
			
			}
		
		   if(seldata !="" && seldata != null && seldata != undefined && filtersel !== "text" && filtersel !== "multipleenv"){
				rows = getRowsFromSelectedData(selcol); 
				if(rows.length >0){
					var searchedData = tData.filter(function (d, ix) {
						 //selparts
						if(rows.indexOf(ix) >= 0){
							selpartsdata += tData[ix][0]+",";
						}
						return rows.indexOf(ix) >= 0;
					});
					
					hotobj.updateSettings({
						 data:searchedData
					});
				
				}
		  }
			var len = hotobj.countRows();
			if(len == 0){
				showsavmsg("green","No Data Found.Please clear filter....");
				
			}else{
				if(selcol !="" && selcol != undefined && selcol != null){
					hotobj.selectCell(0,parseInt(selcol));
					
				}
			}
	
			    hotobj.selectColumns(parseInt(selcol));
				$("#gridfilteredcnt").val(len);
				$("#filtercnt"+sel).html("Filter Count : <b>"+len+"</b>  /  "+totalcnt);
				$("#selected_col").val("");
				$("#search_text").val("");
				$("#selparts"+sel).val(selpartsdata);
				
		});
	
	/**
	 * Filter type change
	 */
		 $('#filtersel').on('change', function() { 
			 
				  if(this.value == 'text' || this.value == 'multipleenv' || this.value == 'blank' || this.value == 'notblank')
				  {
					  if(this.value == 'blank' || this.value == 'notblank'){
						   $("#search_text").hide();
					  }else{
						   $("#search_text").show();
					  }
					  
					   $("#search_text_div").hide();
					   hidesearchbox();
					
				  }
				  else
				  {
					$("#search_text_div").hide();
					$("#search_text").hide();
					 showsearchbox();
				  }
			 
		 });
						
			 //Requester login
			 if(usertype == 3 || usertype == 2){
				jQuery.validator.addMethod("alpha", function(value, element) {
						  return this.optional( element ) || /^[a-zA-Z ]+$/.test( value );
						}, 'Please enter only alphabets.');
				 jQuery.validator.addMethod("alphanumeric", function(value, element) {
					  return this.optional( element ) || /^[a-zA-Z0-9]+$/i.test( value );
					}, 'Please enter only alphabets and numbers.');	
					jQuery.validator.addMethod("alphanumericspace", function(value, element) {
					
					  return this.optional( element ) || /^[a-z\d\._\s]+$/i.test( value );
					}, 'Please enter only alphabets and numbers.');	
						$("#frm_imdsrequest").validate({
						 submitHandler: function () {
							 if($("#sFld<?php echo $custfieldindex;?>").val()>0){
								
								 if(gettimeforfield($("#sFld<?php echo $reqarr['cmptdate'];?>").val(),"duedate") < gettimeforfield($("#sFld<?php echo $reqarr['rd'];?>").val(),"duedate")){
									 $(".custduedateerr").css("display","block");
									 saverequest($('#frm_imdsrequest'));
								// requestconfirmmsg($('#frm_imdsrequest'),datevalidationmsg,batch,bootboxobj,cfmbtnobj);
								 }else{
									  $(".custduedateerr").css("display","none");
								 	saverequest($('#frm_imdsrequest'));
								 }
								
							 }else{
							 $.notify("Please fill mandatory  fields","error");
							 }
						  }
						});	
						$('#request-container .input-group.date').datepicker({
								   format: 'dd-M-yy',
									todayHighlight: true,
									autoclose: true,
									startDate: '0d',
									clearBtn:true
						});
						var datepickerdate = new Date();
						var datepickertoday = new Date(datepickerdate.getFullYear(),
						datepickerdate.getMonth(), 
						datepickerdate.getDate());	
						$('#request-container .input-group.date').datepicker( 'setDate', datepickertoday );
					
				 
			 }
		 });
			function activaTab(tab){
				if(tab == "summary"){
					showsummary("<?php echo $_GET['batch'];?>","<?php echo $res['cid'];?>");
					
				}
			  $('.nav-tabs a[href="#' + tab + '"]').tab('show');
				
			};
			
		 </script>
		 <?php  
			if(ENABLE_SECURITY && $_SESSION['partlinq_user']['USERTYPE'] > 0){
				include("autologoutscript.php");
			}
		 ?>
	<?php 
		}
		include_once("footer.php");
	}?>
	
