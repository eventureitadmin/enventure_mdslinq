<?php
	include_once("header.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{
	if($_POST){
		$pwdexpiry = $_POST['pwdexpiry'];
		$pwdexpalert = $_POST['pwdexpalert'];
		$totfaillogins = $_POST['totfaillogins'];
		$blockhrs = $_POST['blockhrs'];
		$max_idle_time = $_POST['max_idle_time'];
		$blacklistips = $_POST['blacklistips'];
		$whitelistips = $_POST['whitelistips'];
		$editid = $_POST['editid'];
				$userquery = "UPDATE env_login_config SET `pwdexpiry` = '".$pwdexpiry."',`pwdexpalert` = '".$pwdexpalert."',`totfaillogins` = '".$totfaillogins."',`blockhrs` = '".$blockhrs."',`max_idle_time` = '".$max_idle_time."',`blacklistips` = '".$blacklistips."',`whitelistips` = '".$whitelistips."' WHERE `id` ='".$editid."'";	
					$dbase->query($userquery);
					header('Location: loginconfig.php?id=1&m=1');
					exit();			
	}
	if($_GET){
		$id = $_GET['id'];
		$login_query = "SELECT * FROM env_login_config WHERE id='".trim($id)."'";
		$login_result = $dbase->getRow($login_query);
		if($id!=''){
			$label = "Edit";
			$button = "Save";
		}
	}
?>
    <body>			
		<?php include("menu.php"); ?>	
            <div id="page-wrapper">
				<?php if($_GET['m']==1){ echo show_success_msg('Updated successfully'); } ?>	
				<?php if($_SESSION['partlinq_user']['USERTYPE']=='0'){ ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header"><?php echo $label; ?> Login Settings</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <form role="form" id="frm_loginconfig" name="frm_loginconfig" action="" method="post">
                                            <div class="form-group">
                                                <label>Password Expiry Days</label>
                                                <input type="text" name="pwdexpiry" id="pwdexpiry" value="<?php if($id!=''){ echo $login_result['pwdexpiry']; } ?>" class="form-control required number" >
                                            </div>
                                            <div class="form-group">
                                                <label>Password Expiry Notification Days</label>
                                                <input type="text" name="pwdexpalert" id="pwdexpalert" value="<?php if($id!=''){ echo $login_result['pwdexpalert']; } ?>" class="form-control required number" >
                                            </div>
                                            <div class="form-group">
                                                <label>Maximum Failed Logins</label>
                                                <input type="text" name="totfaillogins" id="totfaillogins" value="<?php if($id!=''){ echo $login_result['totfaillogins']; } ?>" class="form-control required number" >
                                            </div>
                                            <div class="form-group">
                                                <label>Failed Login Lock Timings (in Sec)</label>
                                                <input type="text" name="blockhrs" id="blockhrs" value="<?php if($id!=''){ echo $login_result['blockhrs']; } ?>" class="form-control required number" >
                                            </div>
                                            <div class="form-group">
                                                <label>Looged in Maxi Idle Time (in Sec)</label>
                                                <input type="text" name="max_idle_time" id="max_idle_time" value="<?php if($id!=''){ echo $login_result['max_idle_time']; } ?>" class="form-control required number" >
                                            </div>
											<div class="form-group">
												<label>Black List IP's</label>
												<textarea name="blacklistips" id="blacklistips" class="form-control" rows="3"><?php if($id!=''){ echo $login_result['blacklistips']; } ?></textarea>
											</div>
											<div class="form-group">
												<label>White List IP's</label>
												<textarea name="whitelistips" id="whitelistips" class="form-control" rows="3"><?php if($id!=''){ echo $login_result['whitelistips']; } ?></textarea>
											</div>											
											<?php if($id!=''){ ?>
												<input type="hidden" name="editid" id="editid" value="<?php echo $id; ?>" />
											<?php } ?>
                                            <button type="submit" class="btn btn-primary"><?php echo $button; ?></button>
                                        </form>
                                    </div>
                                </div>
							
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>				
				<?php } ?>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->	
		<script type="text/javascript">
		$().ready(function() {
			$("#frm_loginconfig").validate();			
		});
		</script>
	<?php
		 include_once("footer.php");
	}	
?>
