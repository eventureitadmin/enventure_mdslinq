<?php
	include_once("header.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{
		if(isset($_GET['ip'])){
			$ip = base64_decode($_GET['ip']);
			$ipblockres=$logtrack->getloginconfig();
			if($ipblockres['blacklistips']!=''){
				if(isset($_GET['t']) && $_GET['t']=='1'){
					$blockipary = explode(",",$ipblockres['blacklistips']);
					$ipstr = '';
					foreach ($blockipary as $key => $value) {
						if ($value != $ip) {
							$ipstr .= $value.",";
						}
					}
					$ip = substr($ipstr,0,-1);
				}
				else{
					$ip = $ipblockres['blacklistips'].",".$ip;
				}
				
				$upqry="UPDATE env_login_config SET blacklistips='".$ip."' WHERE id='1'";
			}
			else{
				$upqry="UPDATE env_login_config SET blacklistips='".$ip."' WHERE id='1'";
			}
			$dbase->executeNonQuery($upqry);
			header("Location: ipblock.php?msg=1");
			exit();		
		}
?>
    <body>
		<?php include("menu.php"); ?>
<link href="css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<link href="css/dataTables/dataTables.responsive.css" rel="stylesheet">	
            <div id="page-wrapper" style="padding:40px 5px 0 5px">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="">IP List</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
				<?php if(isset($_GET['msg'])){ ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
								<?php if($_GET['msg']==1){ echo show_success_msg('IP blocked successfully'); } ?>
							    <?php if($_GET['msg']==2){ echo show_error_msg('Please select Project.'); } ?>
								 <?php if($_GET['msg']==3){ echo show_error_msg('Data save error.'); } ?>

                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>	
				<?php } ?>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover" id="weeklytable">
                                        <thead>
                                            <tr>
												<th>IP</th>
                                                <th class="nosort">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
		 	 						$ipqry = "SELECT DISTINCT `user_ip` FROM `env_login_iplog`";
		 							$ipres = $dbase->executeQuery($ipqry,"multiple");
										$ipblockres=$logtrack->getloginconfig();
										$blockipary = explode(",",$ipblockres['blacklistips']);
										for($i=0;$i<count($ipres);$i++){
											if (0 == $i % 2) {
												$class = 'class="even"';
											}
											else{
												$class = 'class="odd"';
											}
											$url = '';
											if(in_array($ipres[$i]['user_ip'],$blockipary)){
												$url = '<a href=ipblock.php?t=1&ip='.base64_encode($ipres[$i]['user_ip']).' >Unblock</a>';
											}
											else{
												$url = '<a href=ipblock.php?t=0&ip='.base64_encode($ipres[$i]['user_ip']).' >Block</a>';
											}
											echo '<tr '.$class.'>                                              
											    <td>'.$ipres[$i]['user_ip'].'</td>
                                                <td >'.$url.'</td>
                                            </tr>';
										}
										?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>				
                </div>				
            </div>
            <!-- /#page-wrapper -->
<script src="js/dataTables/jquery.dataTables.min.js"></script>
<script src="js/dataTables/dataTables.bootstrap.min.js"></script>		
		<script type="text/javascript">
		$(document).ready(function() {
                $('#weeklytable').DataTable({
                        responsive: true,
					   'aoColumnDefs': [{
							'bSortable': false,
							'aTargets': ['nosort']
						}]						
                });		
		});
		</script>
	<?php
		 include_once("footer.php");
	}	
?>
