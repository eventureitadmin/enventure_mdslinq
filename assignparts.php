<?php
    include_once("header.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{ 
		if($_GET){
			if($_GET['pid'] != ''){
				$pid = $_GET['pid'];
				$inputheaders = $admin->getInputHeadersByProjectID($pid);
				$outputheaders = $admin->getOutputHeadersByProjectID($pid);
			}   
		}
		include_once("menu.php");
	
?>
<body>
        <div id="wrapper">
            <div id="page-wrapper" style="padding:20px 5px 0 5px">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Assign Parts</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <!-- /.panel-heading -->
                           <div class="panel-body" style="position: relative;">
    						 <div class="row">
                                 <!----   <div class="col-lg-4">							   
                                           <div class="form-group" >
                                                <label>Select Project</label>
                                                <select id="project_id" name="project_id" class="form-control required" onchange="window.location='assignparts.php?pid='+this.value" >
                                                    <option value="">-Select-</option>
													<?php 
													$project_result = $admin->getprojectList();
													for($i=0;$i<count($project_result);$i++){
															$selected1 = "";
															if($pid != '' && $pid==$project_result[$i]['id']){
																$selected1 = "selected";
															}
															else{
																$selected1 = "";
															}
															echo '<option value="'.$project_result[$i]['id'].'" '.$selected1 .'>'. $project_result[$i]['projname']." - Batch ".$project_result[$i]['batchno'].'</option>';
													}
													?>
                                                </select>
                                            </div>
										</div>----->
								   <?php if($pid !=''){ ?>
								 <br>
								 <div class="col-lg-6">
									<div class="form-group" >
										<?php  if($_SESSION['partlinq_user']['USERTYPE']==0){ ?>
										<!---<button class="btn btn-primary popup" pageTitle="Assign Team Leader" mode="5" modetype="usertype" >Assign TeamLeader</button>--->
										<?php } ?>
										<?php if($_SESSION['partlinq_user']['USERTYPE']==5 || $_SESSION['partlinq_user']['USERTYPE']==0){ ?>
										<button class="btn btn-primary popup" pageTitle="Assign User" mode="2" modetype="entrytype" >Assign User</button>
										<!---<button class="btn btn-primary popup" pageTitle="Assign QC User" mode="3" modetype="entrytype" >Assign QC</button>--->
										<?php } ?>
									</div>	
								</div>
								  <?php } ?>
								 </div>
							   <?php if($pid !=''){ ?>	
									<table id="list"></table>
									<div id="pager"></div>	
							 <?php } ?>
                            </div>
						
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>				
                </div>				
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->	
<div class="modal fade" tabindex="-1" role="dialog" id="assignpartspopup">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body" id="modalbdy">
 
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->		
    </body>

	<?php// include("includejs.php"); ?>

		<?php include("includejqgridcss.php"); ?>
	<?php include("includejqgridjs.php"); ?>
<script type="text/javascript">
  //<![CDATA[
        /*global $ */
        /*jslint browser: true, nomen: true */
        $(document).ready(function () {
			<?php if($pid !=''){ ?>				
            'use strict';
            var initDateEdit = function (elem) {
                    setTimeout(function () {
                        $(elem).datepicker({
                            dateFormat: 'dd-M-yy',
                            autoSize: true,
                            //showOn: 'button', // it dosn't work in searching dialog
                            changeYear: true,
                            changeMonth: true,
                            showButtonPanel: true,
                            showWeek: true
                        });
                    }, 100);
                },
                initDateSearch = function (elem) {
                    setTimeout(function () {
                        $(elem).datepicker({
                            dateFormat: 'dd-M-yy',
                            autoSize: true,
                            changeYear: true,
                            changeMonth: true,
                            showWeek: true,
                            showButtonPanel: true
                        });
                    }, 100);
                },
                numberTemplate = {formatter: 'number', align: 'right', sorttype: 'number', editable: true,
                    searchoptions: { sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge', 'nu', 'nn', 'in', 'ni'] }
				},
				setprodcolor = function(cellvalue, options, rowObject){
					var str1 = "~0";
					var str2 = "~1";
					if(cellvalue.indexOf(str1) != -1 || cellvalue.indexOf(str2) != -1){
						var cellvalueArr = cellvalue.split("~");
						if(cellvalueArr[1]=='0'){
							return '<span style="font-weight:bold;color: red; display: block; width: 100%; height: 100%;">' + cellvalueArr[0] + '</span>';
						}
						else{
							return cellvalueArr[0];
						}						
					}
					else{
						return cellvalue;
					}
				},
				setqccolor = function(cellvalue, options, rowObject){
					var str1 = "~0";
					var str2 = "~1";
					if(cellvalue.indexOf(str1) != -1 || cellvalue.indexOf(str2) != -1){
						var cellvalueArr = cellvalue.split("~");
						if(cellvalueArr[1]=='0'){
							return '<span style="font-weight:bold;color: red; display: block; width: 100%; height: 100%;">' + cellvalueArr[0] + '</span>';
						}
						else{
							return cellvalueArr[0];
						}						
					}
					else{
						return cellvalue;
					}
				},
				statusTemplate = {
        			formatter: "checkbox", align: "center", width: 20,
        			stype: "select", searchoptions: {searchhidden: true, sopt: ["eq"], value: "'':Select;1:Completed;0:Not Completed" }
    			},				
                grid = $("#list");


            grid.jqGrid({
				url:'assignpartsdata.php?pid=<?php echo $pid;?>',
                datatype: "json",
				mtype: "POST",
                colNames: [
					'ID',				
					'partstatus',
						<?php
								  $user='';
								 echo  $user .=  "'Username',";
							 
							  ?>
				<?php
								if(count($inputheaders)>0){
									for($i=0;$i<count($inputheaders);$i++){
										if($i >0){
										echo "'".$inputheaders[$i]['headername']."',";
										}
									}
								}
								if(count($outputheaders)>0){
									for($i=0;$i<count($outputheaders);$i++){
										echo "'".$outputheaders[$i]['headername']."',";
									}
								}
							
							?>
											   
						  ],
                colModel: [
				{name:'ID',index:'ID',align:'left',hidden:true,editable:true,width:100},
				{name:'partstatus',index:'partstatus',align:'left',hidden:true,editable:true,width:100},
       						
				//{name:'TLuser',index:'TLuser',align:'left',editable:true,width:100,searchoptions: {sopt: ['eq', 'ne', 'bw', 'bn', 'ew', 'en', 'cn', 'nc']}},
				{name:'username',index:'username',align:'left',editable:true,width:100,searchoptions: {sopt: ['eq', 'ne', 'bw', 'bn', 'ew', 'en', 'cn', 'nc']}},
                <?php if(count($inputheaders)>0){
						for($i=0;$i<count($inputheaders);$i++){
							if(($inputheaders[$i]['orderid']-1)==0){
							//echo "{name:'sFld0',index:'sFld0',sFld0:'left',editable:true,width:100,searchoptions: { sopt: ['eq','in']}},";	
							}
							else{
								if(($inputheaders[$i]['is_auto_type']) == 3){
									echo "{name:'modelname',index:'modelname',align:'left',editable:true,width:100,searchoptions: { sopt: ['eq', 'ne', 'bw', 'bn', 'ew', 'en', 'cn', 'nc']}},";
								}else if(($inputheaders[$i]['is_auto_type']) == 2){
									echo "{name:'supplcode',index:'supplcode',align:'left',editable:true,width:100,searchoptions: { sopt: ['eq', 'ne', 'bw', 'bn', 'ew', 'en', 'cn', 'nc']}},";
								}else if(($inputheaders[$i]['is_auto_type']) ==1){
									echo "{name:'cname',index:'cname',align:'left',editable:true,width:100,searchoptions: { sopt: ['eq', 'ne', 'bw', 'bn', 'ew', 'en', 'cn', 'nc']}},";
								}else if(($inputheaders[$i]['is_auto_type']) ==4){
									echo "{name:'reqname',index:'reqname',align:'left',editable:true,width:100,searchoptions: { sopt: ['eq', 'ne', 'bw', 'bn', 'ew', 'en', 'cn', 'nc']}},";
								}else{
						echo "{name:'sFld".($inputheaders[$i]['orderid']-1)."',index:'sFld".($inputheaders[$i]['orderid']-1)."',align:'left',editable:true,width:100,searchoptions: { sopt: ['eq', 'ne', 'bw', 'bn', 'ew', 'en', 'cn', 'nc']}},";	
								}
							}

						}
						 if(count($outputheaders)>0){
						for($i=0;$i<count($outputheaders);$i++){
							echo "{name:'sFld".($outputheaders[$i]['orderid']-1)."',index:'sFld".($outputheaders[$i]['orderid']-1)."',align:'left',editable:true,width:100,searchoptions: { sopt: ['eq', 'ne', 'bw', 'bn', 'ew', 'en', 'cn', 'nc']}},";
						}
						 }
			
				}?>
                				
                ],
                rowNum: 1000,
              	//rowList: ['1000', '2000', '3000', '4000'],
				loadonce: false,
                pager: '#pager',
                gridview: true,
                rownumbers: true,
                sortname: 'id',
                viewrecords: true,
				showlimit : true,
				maxlimit : 1000,
                sortorder: 'asc',
                caption: 'Assign Parts',
				multiselect: true,
				autoencode: true,
                height: '400px',
				autowidth:true, 
				shrinkToFit:false,
				loadComplete: function() {
					
					var ids = grid.jqGrid('getDataIDs');
					for(var i=0; i<=ids.length; i++) {
						var rowId = ids[i];
						var rowData = grid.jqGrid('getRowData',rowId);
						
						if(rowData.partstatus == 1) {
						}else{
							grid.jqGrid('setCell',rowId,"username","",{'color':'red','font-weight': 'bold'});
						}
					}
					
				},					
				jsonReader : { repeatitems: false } ,
            }).jqGrid('navGrid', '#pager', {add: false, edit: false, del: false}, {}, {}, {},
                {
                    multipleSearch: true,
					multipleGroup:true,
					showQuery: false,
					closeOnEscape:true,
                    overlay: 0,
					beforeShowSearch: function() {
						$('#searchmodfbox_list').css('width','650');
						return true;
					},					
                    onSearch: function () {
                        var i, l, rules, rule, $grid = $('#list'),
                            postData = $grid.jqGrid('getGridParam', 'postData'),
                            filters = $.parseJSON(postData.filters);
                    }});
			<?php if($_SESSION['partlinq_user']['USERTYPE']==0){ ?>
		/*	grid.navButtonAdd('#pager', {
					caption: "Export to Excel",
					buttonicon: "ui-icon-disk",
					onClickButton: function () {
						
						ExportToExcel(grid.jqGrid('getRowData'),"assignparts",grid.jqGrid('getGridParam','colNames'));
					},
					position: "last"
				});*/
		 <?php } ?>
		
 		$(".popup").click(function(){
			var pageTitle = $(this).attr('pageTitle');
			var mode = $(this).attr('mode');
			var modetype = $(this).attr('modetype');
			$(".modal .modal-title").html(pageTitle);
			$("#modalbdy").html("Content loading please wait...");
			$('#modalbdy').load('assignusers.php?pid='+<?php echo $pid;?>+'&mode='+mode+'&modetype='+modetype,function(){
				var selectedrows = $("#list").jqGrid('getGridParam','selarrrow');
				if(selectedrows.length > 0) {
					var selectids = '';
					for(var i=0; i<selectedrows.length; i++) {
						var rowId = selectedrows[i];
						var rowData = grid.jqGrid('getRowData',rowId);
						selectids += rowData.ID+",";
					}
					$("#partids").val(selectids.toString().slice(0, -1));			
					$('#assignpartspopup').modal({show:true});
				}
				else{
					$.notify("Please select parts","error");
				}
			});			
        });				
		<?php } ?>
        });

	function assignusers(pid){
		   $.post(
			  "assignusersaction.php",
			  $("#frm_assignuser").serializeArray(),
			  function(data) {
				if(data=='success'){
					$('.modal').modal('hide');
					$("#list").setGridParam({url:'assignpartsdata.php?pid='+pid,datatype: "json",sortname:"id",sortorder:"asc",search: false, postData: { "filters": ""}}).trigger("reloadGrid"); 				
				}
				else{
					$('#assignpartspopup').modal('hide');
					$.notify("Error Occured! Please try again !!","error");
				 }
			  }
		   );			
			
	}
  
	</script>
<?php include("footer.php"); ?>
<?php } ?>
