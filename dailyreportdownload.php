<?php
include_once("top.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
else{
	
	if($_POST && $_POST['batch_id'] >0 && $_POST['from_date'] !="" && $_POST['to_date'] !=""){
		$stylearr = array(
					 'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
						),
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THICK ,
							'color' => array('rgb' => 'DDDDDD')
						)
					)
			);
		
		function getdetailsreport($objPHPExcel,$result,$projres,$row1,$row3,$row2,$stylearr){
			for($s=0;$s<count($result);$s++){
				if(count($projres) >0){
					$j=0;
					$start  =65;
					$cha = chr($start);
					for($i=1;$i<count($projres);$i++){
						if($s ==0){
							$row =2;
							$row3=3;
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row1,$projres[$i]['headername']);
							$objPHPExcel->getActiveSheet()->getColumnDimension($cha)->setWidth(20);
							$objPHPExcel->getActiveSheet()->getStyle($cha.$row1)->getFont()->setBold(true);
							if($projres[$i]['itypeid'] == 9){
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row2,trim($result[$s]['creatorname']));
							}else{
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row2,trim($result[$s]['sFld'.($projres[$i]['orderid']-1)]));
							}
						}else{
							if($projres[$i]['itypeid'] == 9){
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row3,trim($result[$s]['creatorname']));
							}else{
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$row3,trim($result[$s]['sFld'.($projres[$i]['orderid']-1)]));
							}
						}
						++$start;
						$cha = chr($start);
						$j++;

					}

				$objPHPExcel->getActiveSheet()
					 ->getStyle('A'.$row1.':'.$cha.$row3)->applyFromArray(
						$stylearr
					);

				}
				$row3++;
			}
			return $objPHPExcel;
		}
	
		$projres = $dbase->executeQuery("SELECT `project_id`,(SELECT `projname` FROM `env_project` WHERE `isactive`=1 AND `isclosed`=0)as projname FROM `env_batch` WHERE  `id`=".$_POST['batch_id']." AND `closed`=0","single");
		$pid = $projres['project_id'];
		$projname = $projres['projname'];
		$statusindex = $dbase->executeQuery("SELECT (`orderid`-1)as oid FROM `env_projectheaders` WHERE `batchid` =".$_POST['batch_id']." AND `itypeid`=2 ","single")[0];
		$reqdatefield = $dbase->executeQuery("SELECT (`orderid`-1)as oid FROM `env_projectheaders` WHERE `batchid` =".$_POST['batch_id']." AND `hidden_type`=1","single")[0];
		$reqfield = $dbase->executeQuery("SELECT (`orderid`-1)as oid FROM `env_projectheaders` WHERE `batchid` =".$_POST['batch_id']." AND `is_auto_type`=4","single")[0];
		$custfield = $dbase->executeQuery("SELECT (`orderid`-1)as oid FROM `env_projectheaders` WHERE `batchid` =".$_POST['batch_id']." AND `is_auto_type`=1","single")[0];
		if($pid > 0){
			$projres = $dbase->getheaderinfo($pid);
			$custdetails = $dbase->getcustomerdetails($pid,$_POST['batch_id']);
				if(count($custdetails) >0){
						$selfield .= $dbase->getautocompletequery($_POST['batch_id'],"u");
						$selfield .= ",(SELECT `fullname` FROM `env_user` WHERE id=u.userid)as uname";
						$createdfield = "
							(SELECT fullname FROM env_user WHERE id=u.changed_by)as updatedname,
							(SELECT u.`fullname` FROM `env_user` as u  WHERE u.`id` = u.`created_by`) as creatorname ";
					$query1 = "SELECT u.* ".$selfield.", ".$createdfield." FROM  `env_urlgrab` u,env_requestor_alloc ra WHERE u.`iPrjID`=".$pid." AND u.`iBatch`=".$_POST['batch_id']." AND u.sFld".$statusindex." IN(SELECT `statusval` FROM `env_status` WHERE `customer_id`=".$_SESSION['partlinq_user']['CUSTOMERID']." AND `isactive`=1 AND id IN(".implode(",",$_POST['status_id']).")) AND u.is_delete=0 AND ra.user_id=u.sFld".$reqfield." AND ra.customer_id =u.sFld".$custfield;
					$objPHPExcel = new PHPExcel();
					$index=0;
					for($k=0;$k<count($custdetails);$k++){
						$query = "";
						$query2 = " AND ra.customer_id =".$custdetails[$k]['cid']." AND  STR_TO_DATE(u.sFld".$reqdatefield.",'%d-%M-%y')>='".date("Y-m-d",strtotime($_POST['from_date']))."' AND STR_TO_DATE(u.sFld".$reqdatefield.",'%d-%M-%y')<='".date("Y-m-d",strtotime($_POST['to_date']))."'";
						$query = $query1.$query2;
					//	echo $query."<br/>";
						$result ="";
						$result =$dbase->executeQuery($query,"multiple");
						if(count($result) >0 && $custdetails[$k]['cust_short_name'] !=""){
							$row3 =3;
							$row1 = 2;
							$row2 = 3;
							$lastsheet=0;
							$start  =65;
							$cha = chr($start);

							// Create a new worksheet, after the default sheet
							$objPHPExcel->createSheet();
							$resultdata ="";			
							// Add some data to the second sheet, resembling some different data types
							$objPHPExcel->setActiveSheetIndex($index);
							// Rename 2nd sheet
							$objPHPExcel->getActiveSheet()->setTitle($custdetails[$k]['cust_short_name']);

							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,1,"Customer Name : ".$custdetails[$k]['cname']);
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,1,"Total : ".count($result));
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,1,"From Date : ".$_POST['from_date']);
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,1,"To Date : ".$_POST['to_date']);
							$objPHPExcel->getActiveSheet()->mergeCells("A1:C1");
							$objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
							$objPHPExcel->getActiveSheet()->getColumnDimension($cha)->setWidth(20);
							$objPHPExcel =  getdetailsreport($objPHPExcel,$result,$projres,$row1,$row3,$row2,$stylearr);
							$index++;
						}
					}
					
					$objPHPExcel->setActiveSheetIndex(0);
					$filename = $projname."_dailyreport".date('d_M_Y').".xlsx";
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
					ob_end_clean();
					// We'll be outputting an excel file
					header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
					// It will be called file.xls
					header('Content-Disposition: attachment; filename="'.$filename.'"');		
					$objWriter->save('php://output');
					exit();
					
				
				}
		}
	}

}
?>
