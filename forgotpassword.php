<?php 
session_start();
include_once("header.php");
 $_SESSION['partlinq_user']['ISFORGOTPWD']=1;
?>

<style>
.marginBottom-0 {margin-bottom:0;}
.logopadding{padding: 5px 10px;}
.dropdown-submenu{position:relative;}
.dropdown-submenu>.dropdown-menu{top:0;left:100%;margin-top:-6px;margin-left:-1px;-webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;}
.dropdown-submenu>a:after{display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;}
.dropdown-submenu:hover>a:after{border-left-color:#555;}
.dropdown-submenu.pull-left{float:none;}.dropdown-submenu.pull-left>.dropdown-menu{left:-100%;margin-left:10px;-webkit-border-radius:6px 0 6px 6px;-moz-border-radius:6px 0 6px 6px;border-radius:6px 0 6px 6px;}
/*https://www.bootply.com/nZaxpxfiXz*/
	.yesbtn{
			margin-left: 70px !important;
		}
	.nobtn{
			margin-right: 70px !important;
		}
</style>

<script>
	function seterrmessage(selid,message){
		if(message == ""){
			 $("#"+selid).html("");
			 $("#"+selid).css("display","none");
		}else{
			 $("#"+selid).html(message);
			 $("#"+selid).css("display","block");
		}
	}
	function validateEmail(value) 
	{
	 if(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value))
	 {
			var isvalid = false;
		    $("#unameerr").css("display","none");
				$.ajax({
				url:"checkemailaddress.php",
				type:"POST",
				data:{ emailid: $("#username").val()},
				dataType:"json",
				async:false,
			 	success:function(data) {
				//var obj  = JSON.parse(data);
					if(data.message != ""){
						seterrmessage("unameerr",data.message);
						isvalid= false;
					}else{
						seterrmessage("unameerr","");
						isvalid = true;
					}
				console.log( "Data Loaded: " + isvalid );
				}
			  });
		
	  }else{
		$("#unameerr").val("");
		if(value == ""){
			$("#unameerr").html("This field is required");
			 $("#unameerr").css("display","block");
		}else{
			 $("#unameerr").html("Please enter valid email");
			 $("#unameerr").css("display","block");
		}
		return isvalid;
		}
	return isvalid;
	}


</script>

        <div class="container">
			<div class="row"><div class="alert alert-danger col-md-4 col-md-offset-4" role="alert" id="errormsgid" style="display:none">
  
</div></div>
		    <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
							 <img style="display: block; margin-left: auto; margin-right: auto;max-width: 120px;" src="images/mdslinq.png"/>
                         	<p style="font-size:24px;font-weight:900;color:#024B9A;text-align:center;letter-spacing:5px;">IMDS Manager</p>
                        	<h4 align="center">Forgot Password</h4>
                        </div>
						<?php if($msg!=''){?>
						<div class="alert alert-danger">
							<?php echo $msg; $msg=''; ?> 
						</div>
						<?php } ?>							
                        <div class="panel-body">
                            <form role="form" id="resetfrm_login" action="" method="post">
                                <fieldset>
									 <div class="form-group">Enter the email address associated with the IMDS portal</div>
									  <div class="form-group">
										  <input type="hidden" name="is_forgot_pwd" id="is_forgot_pwd" value="1">
                                        <input class="form-control required email" placeholder="E-mail" id="username" name="username" type="text" onchange="validateEmail(this.value)"  autofocus />
										  <p id="unameerr" style="color:red;display:none">This field is required</p>
                                    </div>
                                    
                                    <!-- Change this to a button or input when using this as a form -->
									 <div class="form-group">
									<button type="button" id="reset_btn" class="btn btn-primary btn-lg btn-block">Reset</button>
									<button type="button" id="cancel_btn" class="btn btn-danger btn-lg btn-block" onclick="window.location.href='cancelrequest.php'">Cancel</button>
										 </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
			
        
        </div>
<script>
	var pass_pattern = <?php echo PASSWORDREG; ?> 
var batchid ='<?php echo $_SESSION['partlinq_user']['BATCHID'];?>';
</script>
<style>
	.navbar-inverse .navbar-nav > .open > a, .navbar-inverse .navbar-nav > .open > a:focus, .navbar-inverse .navbar-nav > .open > a:hover {
	background-color:white
}
	.navbar-inverse{
			border-color:#E5E5E5
	}
</style>
<script>
(function($){
		$(document).ready(function(){
			
		var isvaliddata = false;
		$("#reset_btn").click(function(){
			
			if(validateEmail($("#username").val())){
				isvaliddata = true;
			}else{
				isvaliddata = false;
			}
		if(isvaliddata){
				bootbox.confirm({ 
				size: "small",
				message: " Are you sure to reset password ?",
				buttons: cfmbtnobj,
				callback: function(result){ 
					if(result){
							showloadimg();
						$.ajax({
						url:"saveforgotpassword.php",
						type:"POST",
						data:$("#resetfrm_login").serialize(),
						dataType:"json",
						success: function(data){
							var obj = data;
							if(obj.status=='success'){
								$.notify(obj.message,"success");
								window.location.href="login.php";
							}
							else{
							$.notify(obj.message,"error");
							}
							  $.unblockUI();	
			
						}
					});
		

				    }

				}
			}).find('.modal-content').css(bootboxobj);


		}
		});
	
	});
})(jQuery);	
</script>

