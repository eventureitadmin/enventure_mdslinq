<?php
    include_once("header.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{ 
		$summarylbl = "summary".$_GET['pid'].$_GET['batch'];
		
		 if(isset($_GET['statusid'])){
		 }else{
			 $_COOKIE['sid'] = "";
		 }
    ?>
	<script type="text/javascript" src="js/apexcharts.js"></script>
	<script>
		
		var	pid = "<?php echo $_GET['pid'];?>";
		var	batch = "<?php echo $_GET['batch'];?>";
		var	usertype = "<?php echo $_SESSION['partlinq_user']['USERTYPE'];?>";
		
	    var	statusid = "<?php echo $_GET['statusid'];?>";
	 
	    var	sorder = "<?php echo $_GET['sorder'];?>";
	    var	cid = "<?php echo $_GET['cid'];?>";
	    var	tab = "<?php echo $_GET['tab'];?>";
		var	currurl ="";
	</script>

 	 <body  oncontextmenu="return false;">
		 <div id="headerpanel">
		 <?php include_once("menu.php");
			 $projtype = $dbase->getdetails("env_project","projtype"," ID = ".$_GET['pid'],"single")[0];
		
			 $progress_val = $dbase->getdetails("env_project","progress_val"," ID = ".$_GET['pid'],"single")[0];
			 $projectname = $dbase->getdetails("env_project","projname"," ID = ".$_GET['pid'],"single")[0];
			 $is_weekly_report = $dbase->getdetails("env_project","is_weekly_report"," ID = ".$_GET['pid'],"single")[0];
			 $customer_id = $dbase->getdetails("env_project","customer_id"," ID = ".$_GET['pid'],"single")[0];
			 $client = $dbase->getdetails("env_customer","customershortname"," id = ".$customer_id,"single")[0];
			 $batchid = $dbase->getNameNew("env_batch","batchno","id='".$_GET['batch']."'");
			if($_GET['batch'] == 10){ $headername= "Life Cycle Analysis";}
			else{$headername = "Status Summary ".date('d-M-Y');}
										
		 ?>
		
		 </div>
			<div class="panel panel-default">
				<input type="hidden" name="is_grid_view" id="is_grid_view" value="">
                        <!-- /.panel-heading -->
                        <div class="panel-body" id="tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
								<li class=""><a href="#summary" data-toggle="tab">Summary</a>
                                </li>
								<?php if($_SESSION['partlinq_user']['USERTYPE']=='3'){ ?>
								<li class=""><a href="#request" data-toggle="tab">New Request</a>
                                </li>	
								<?php } ?>
								<li class=""><a href="#partslist" data-toggle="tab"><?php if($_SESSION['partlinq_user']['USERTYPE']=='1'){ ?>Pending Requests<?php } else {?> Request List<?php } ?></a>
                                </li>
								<!---<li class=""><a href="#assignparts" data-toggle="tab"><?php //if($_SESSION['partlinq_user']['USERTYPE']=='0' || $_SESSION['partlinq_user']['USERTYPE']=='5'){ ?>Assign Parts<?php //} ?></a>
                                </li>--->
								<?php if($_SESSION['partlinq_user']['USERTYPE']=='1'){ ?>
								<li class=""><a href="#comppartslist" data-toggle="tab">Completed Requests</a>
                                </li>
								<?php } ?>
									<?php if($_SESSION['partlinq_user']['USERTYPE']=='1' || $_SESSION['partlinq_user']['USERTYPE']=='0'){ ?>
		
								<li class=""><a href="#unrptpartslist" data-toggle="tab">Unreported Parts</a>
                                </li>
								<?php } ?>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade" id="summary" style="overflow-x:auto;overflow-y:hidden;">
									<h4><?php //echo $headername;?></h4>
									<?php if($progress_val != "" && $progress_val >0){?>
										<strong>Project timeline</strong>
											<div id="progress">
										<span>
						<?php echo date("d-m-Y", strtotime($dbase->getNameNew("env_batch","startdate","id='".$_GET['batch']."'")));?>
										</span>
										<span class="pull-right">
						<?php echo date("d-m-Y", strtotime($dbase->getNameNew("env_batch","enddate","id='".$_GET['batch']."'")));?></span>
									</div>	
									<?php }?>
																			
									<?php 
								
		 							$statusfldres = $dbase->getNameNew("env_batch","status_headerid","id='".$_GET['batch']."'");
									$query = "SELECT  `headername`,`status_values` FROM `env_projectheaders` 
									WHERE `orderid` IN(".$statusfldres.") AND `batchid`=".$_GET['batch']." ORDER BY `orderid` ASC";
									$projres = $dbase->executeQuery($query,"multiple");
		 							 $statusfldarr =  explode(",",$statusfldres);
									 $statusdescarr = [];
										if(count($statusfldarr) == 1 && $projtype == 1){
											$width = 680;
								$colors1 = array('#5f4b8b', '#1E90FF', '#3ADF00', '#FFC0CB','#FF1493','#0090FF','#DC143C','#0000FF','#1E013C','#00B0CB','#FF0993'
																, '#3ADF00',' #f44336','#d68910');
											$reportname = "Current Week Report as on ". date('d-M-Y');
										}else{
											$width = 600;
											$reportname ="";
										}
		 							foreach($statusfldarr as $key=>$val){
										$statusfld = $val;$key1=$key;
										//if(count($statusfldarr) > 1){
										if(count($statusfldarr) > 0 && $projtype == 2){
											$reportname	= $projres[$key1]['headername'];
										}
							
							
									 $formuladata  = $dbase->getFormulaTypes($_GET['batch']);$formulatypesarr = [];$formalasorderids = [];
									 foreach($formuladata as $key=>$value){
										 $itypeid = $value['itypeid'];$formulatype =$value['formulatype'];
										 $formulatypesarr[$itypeid] = $formulatype;
										 $formalasorderids[$itypeid] = $value['orderid'];
										
									 }
		 							if($statusfld !='' && $statusfld > 0){
		 								$sfld = ($statusfld - 1);
									}
		 							if($sfld!='' && $sfld > 0){
										$sfldcol = "`sFld".$sfld."`";
										if($_SESSION['partlinq_user']['USERTYPE']=='3'){
											$usercond = " AND created_by='".$_SESSION['partlinq_user']['ID']."'";
										}
										else{
											$usercond = "";
										}
									  $statusquery = "SELECT m . * , IFNULL((SELECT COUNT( t1.ID ) AS cnt FROM `env_urlgrab` t1 WHERE t1.`iPrjID` = '".$_GET['pid']."' AND t1.`iBatch` = '".$_GET['batch']."' AND t1.".$sfldcol." = m.env_status ".$usercond." GROUP BY t1.".$sfldcol." ),0) AS cnt FROM ( SELECT s.`statusval` AS env_status,s.id as envstatusid FROM `env_status` s WHERE s.`isactive` = '1' ";
										if($projres[$key1]['status_values'] != ""){
											$statusquery .= " AND s.id IN(".$projres[$key1]['status_values'].")";
										  $estatusids = $dbase->executeQuery("SELECT s1.`id` AS env_status1 FROM `env_status` s1 WHERE s1.`isactive` = '1' AND s1.id IN(".$projres[$key1]['status_values'].") AND s1.`customer_id` = ".$_GET['cid'],"mutiple");
											//echo "<pre>";print_r($estatusids[0][0]);
										}
									
										$statusquery .= " AND s.`customer_id` = '".$_GET['cid']."') m";
		 								$statusresult = $dbase->executeQuery($statusquery,"multiple");
									
									}
									if(count($statusresult) > 0){
										$labelarray = array();
										$colorsarray = array();
										$seriesarray = array();$j=0;
										for($i=0;$i<count($statusresult);$i++){
												if(count($statusfldarr) > 0 && $projtype == 2){
									
											//if(count($statusfldarr) > 1){
												$reportname	= $projres[$key1]['headername'];
												//$colors1[$i] = $colors[$j];
												//$j++;
											}
	$envstatusres = $dbase->executeQuery("SELECT statusdesc,id,colorcode FROM `env_status` WHERE id ='".$statusresult[$i]['envstatusid']."'","single");
										//	$envstatusres['statusdesc'];
									$envstatusres['statusdesc'] =	str_replace("client",$client,$envstatusres['statusdesc']);
									$envstatusres['statusdesc'] =	str_replace("customer",$client,$envstatusres['statusdesc']);
												if($envstatusres['colorcode'] != ""){
													$labelarray[$i] = "<b>".$statusresult[$i]['env_status']." </b>: ".$envstatusres['statusdesc'];
													$colors1[$i] = $envstatusres['colorcode'];
												}else{
													//$colors1[$i] = $envstatusres['colorcode'];
													$labelarray[$i] = $statusresult[$i]['env_status'];
												}
											    $datapointsarray[$i]= $envstatusres['id'];
												$seriesarray[$i] = $statusresult[$i]['cnt'];	
											
										}
									?>
									 <?php //echo $labelarray12); exit;?>
									<div id="currentweekchart<?php echo $val;?>"></div>
									<script type="text/javascript">
									$(document).ready(function() {
											
											var options = {
												
												chart: {
													width: '<?php echo $width?>',
													height: 'auto',
													type: 'donut',
													id:'<?php echo $val; ?>',
													events: {
														dataPointSelection: function(e,chart, opts) {
														   var  lindex = opts.dataPointIndex;
														   var  statusid = opts.w.config.datapoints[lindex];
														   var statusstr = opts.w.globals.chartID;
														   var statusname1 = opts.w.globals.labels[lindex];
														window.location.href="pricesourcing.php?pid="+pid+"&batch="+batch+"&cid="+cid+"&statusid="+statusid+"&sorder="+statusstr;
														 }
													}
												},
												title: {
									text: "<?php echo ucfirst($reportname);?>  ",
													align: 'left',
													margin: 10,
													offsetX: 0,
													offsetY: 0,
													floating: false,
													style: {
													  fontSize:  '16px',
													  color:  '#263238',
													},
												},												
											 	colors: [<?php echo "'" . implode("','", $colors1) . "'";?> ],
												plotOptions: {
												  pie: {
													size: undefined,
													customScale: 1,
													offsetX: 0,
													offsetY: 0,
													expandOnClick: true,
													dataLabels: {
														offset: 0,
													}, 
													donut: {
													  size: '65%',
													  background: 'transparent',
													
													  labels: {
														show: true,
														name: {
														  show: true,
														  fontSize: '12px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: -10													
														},  
														value: {
														  show: true,
														  fontSize: '16px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: 1,
														 formatter: function (val,w) {
															  var sep_cnt = val;
															  var tot_cnt = w.globals.seriesTotals.reduce((a, b) => {return a + b}, 0)
															return ((sep_cnt / tot_cnt)* 100).toFixed(1)+"%"
														  }
														},
														total: {
														  show: true,
														  label: 'Total Parts',
														  color: '#373d3f',
														  formatter: function (w) {
															return w.globals.seriesTotals.reduce((a, b) => {
															  return a + b
															}, 0)
														  }
														}
													  }
													},      
												  },													
												},
												
												dataLabels: {
													enabled: false
												},
												
												series: [<?php echo  implode(",", $seriesarray);?>],
												labels: [<?php echo "'" . implode("','", $labelarray) . "'";?> ],
												datapoints: [<?php echo "'" . implode("','", $datapointsarray) . "'";?> ],
												fill: {
													type: 'gradient',
												},
												legend: {
													width:300,
													formatter: function(val, opts) {
														return val + " - " + opts.w.globals.series[opts.seriesIndex]
													}
												},
												responsive: [{
													breakpoint: 480,
													options: {
														chart: {
															width: 300
														},
														legend: {
															position: 'bottom'
														}
													}
												}]
											}
											var currentweekchart<?php echo $val; ?> = new ApexCharts(
												document.querySelector("#currentweekchart<?php echo $val;?>"),
												options
											);
											$("#currentweekchart<?php echo $val;?>").css("float","left");
											$("#currentweekchart<?php echo $val;?>").css("width","<?php echo $width;?>px");

											setTimeout(function(){
												currentweekchart<?php echo $val; ?>.render();
											}, 500);

											
											$('.nav-tabs a[href="#summary"]').click(function(){
												//currentweekchart<?php //echo $val; ?>.render();
											});										
									});
										
									</script>
									<?php } }?>
									
								<!-- weekwise report -->
									<?php if($is_weekly_report == 1){
		 							 $weekreportquery = "SELECT DISTINCT `reportdate` FROM `env_report` WHERE `iPrjID`='".$_GET['pid']."' AND `iBatch`='".$_GET['batch']."' ORDER BY `reportdate` DESC LIMIT 0,1";
		 							$weekreportresult = $dbase->executeQuery($weekreportquery,"multiple");
		 							if(count($weekreportresult) > 0){
		 							if($statusfld !='' && $statusfld > 0){
		 								$sfld = ($statusfld - 1);
									}
								for($j=0;$j<count($weekreportresult);$j++){										
		 							if($sfld!='' && $sfld > 0){
										$sfldcol = "`sFld".$sfld."`";
										  $statusquery1 = "SELECT m . * , IFNULL((SELECT COUNT( t1.ID ) AS cnt FROM `env_report` t1 WHERE t1.`iPrjID` = '".$_GET['pid']."' AND t1.`iBatch` = '".$_GET['batch']."' AND t1.".$sfldcol." = m.env_status AND reportdate='".$weekreportresult[$j]['reportdate']."' GROUP BY t1.".$sfldcol." ),0) AS cnt FROM ( SELECT s.`statusval` AS env_status FROM `env_status` s WHERE s.`isactive` = '1' AND s.`customer_id` = '".$_GET['cid']."') m";
		 							$statusresult1 = $dbase->executeQuery($statusquery1,"multiple");
									}
									if(count($statusresult1) > 0){
										$labelarray1 = array();
										$seriesarray1 = array();
										for($i=0;$i<count($statusresult1);$i++){
											$labelarray1[$i] = $statusresult1[$i]['env_status'];
											$seriesarray1[$i] = $statusresult1[$i]['cnt'];											
										}
									?>
									<div id="weekchart<?php echo ($j+1); ?>" ></div>
									<script type="text/javascript">
									$(document).ready(function() {
										$("#export").click(function(){
											$("#req_export_form").submit();
											
										});
										if(tab == 1){
											$(".nav-tabs li").removeClass();
											$('.nav-tabs a:nth-child(1)').addClass("active");
											$('.nav-tabs a:nth-child(1)').tab('show'); 
										}else{
											$('.nav-tabs a:first').tab('show');
										}
											var options = {
												chart: {
													width: 680,
													type: 'donut',												
												},
												title: {
													text: "Last Week Report as on <?php echo date('d-M-Y',strtotime($weekreportresult[$j]['reportdate'])); ?>",
													align: 'left',
													margin: 10,
													offsetX: 0,
													offsetY: 0,
													floating: false,
													style: {
													  fontSize:  '16px',
													  color:  '#263238',
													},
												},												
											   	colors: [<?php echo "'" . implode("','", $colors1) . "'";?> ],
												plotOptions: {
												  pie: {
													size: undefined,
													customScale: 1,
													offsetX: 0,
													offsetY: 0,
													expandOnClick: true,
													dataLabels: {
														offset: 0,
													}, 
													donut: {
													  size: '65%',
													  background: 'transparent',
													  labels: {
														show: true,
														name: {
														  show: true,
														  fontSize: '12px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: -10													
														},  
														value: {
														  show: true,
														  fontSize: '16px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: 1,
														 formatter: function (val,w) {
															  var sep_cnt = val;
															  var tot_cnt = w.globals.seriesTotals.reduce((a, b) => {return a + b}, 0)
															return ((sep_cnt / tot_cnt)* 100).toFixed(1)+"%"
														  }
														},
														total: {
														  show: true,
														  label: 'Total Parts',
														  color: '#373d3f',
														  formatter: function (w) {
															return w.globals.seriesTotals.reduce((a, b) => {
															  return a + b
															}, 0)
														  }
														}
													  }
													},      
												  },													
												},
												
												dataLabels: {
													enabled: false
												},
												series: [<?php echo  implode(",", $seriesarray1);?>],
												labels: [<?php echo "'" . implode("','", $labelarray1) . "'";?> ],
												fill: {
													type: 'gradient',
												},
												legend: {
													width:300,
													formatter: function(val, opts) {
														return val + " - " + opts.w.globals.series[opts.seriesIndex]
													}
												},
												responsive: [{
													breakpoint: 480,
													options: {
														chart: {
															width: 200
														},
														legend: {
															position: 'bottom'
														}
													}
												}]
											}
											var weekchart<?php echo ($j+1); ?> = new ApexCharts(
												document.querySelector("#weekchart<?php echo ($j+1); ?>"),
												options
											);
											weekchart<?php echo ($j+1); ?>.render();
											$("#weekchart<?php echo ($j+1); ?>").css("float","left");
											$("#weekchart<?php echo ($j+1); ?>").css("width","550px");
									});
									</script>
									<?php } } } }?>
<div id="currentweekchart123"></div>
									<script type="text/javascript">
									$(document).ready(function() {
											
											var options = {
												
												chart: {
													width: '680',
													height: 'auto',
													type: 'donut',
													id:'12',
													events: {
														dataPointSelection: function(e,chart, opts) {
														   var  lindex = opts.dataPointIndex;
														   var  statusid = opts.w.config.datapoints[lindex];
														   var statusstr = opts.w.globals.chartID;
														   var statusname1 = opts.w.globals.labels[lindex];
														window.location.href="pricesourcing.php?pid="+pid+"&batch="+batch+"&cid="+cid+"&statusid="+statusid+"&sorder="+statusstr;
														 }
													}
												},
												title: {
													text: "HONDA Report",
													align: 'left',
													margin: 10,
													offsetX: 0,
													offsetY: 0,
													floating: false,
													style: {
													  fontSize:  '16px',
													  color:  '#263238',
													},
												},												
											 	colors: ['#5f4b8b','#1E90FF','#3ADF00','#FFC0CB','#FF1493','#0090FF','#DC143C','#0000FF','#1E013C','#00B0CB','#FF0993','#3ADF00',' #f44336','#d68910' ],
												plotOptions: {
												  pie: {
													size: undefined,
													customScale: 1,
													offsetX: 0,
													offsetY: 0,
													expandOnClick: true,
													dataLabels: {
														offset: 0,
													}, 
													donut: {
													  size: '65%',
													  background: 'transparent',
													
													  labels: {
														show: true,
														name: {
														  show: true,
														  fontSize: '12px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: -10													
														},  
														value: {
														  show: true,
														  fontSize: '16px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: 1,
														 formatter: function (val,w) {
															  var sep_cnt = val;
															  var tot_cnt = w.globals.seriesTotals.reduce((a, b) => {return a + b}, 0)
															return ((sep_cnt / tot_cnt)* 100).toFixed(1)+"%"
														  }
														},
														total: {
														  show: true,
														  label: 'Total Parts',
														  color: '#373d3f',
														  formatter: function (w) {
															return w.globals.seriesTotals.reduce((a, b) => {
															  return a + b
															}, 0)
														  }
														}
													  }
													},      
												  },													
												},
												
												dataLabels: {
													enabled: false
												},
												
												series: [0,1,4,2,5,6,1,0,3],
												labels: ['Initial Pending ','Initial Completed','Completed Final Release','Completed Initial Release','Accepted Initial Release','Accepted Final Release','Rejected','Accepted by PL Read to Submit','Rejected by PL' ],
												datapoints: ['1','2','3','4','5','6','7','8','9' ],
												fill: {
													type: 'gradient',
												},
												legend: {
													width:300,
													formatter: function(val, opts) {
														return val + " - " + opts.w.globals.series[opts.seriesIndex]
													}
												},
												responsive: [{
													breakpoint: 480,
													options: {
														chart: {
															width: 300
														},
														legend: {
															position: 'bottom'
														}
													}
												}]
											}
											var currentweekchart123 = new ApexCharts(
												document.querySelector("#currentweekchart123"),
												options
											);
											$("#currentweekchart123").css("float","left");
											$("#currentweekchart123").css("width","680px");

											setTimeout(function(){
												currentweekchart123.render();
											}, 500);

											
											$('.nav-tabs a[href="#summary"]').click(function(){
												//currentweekchart.render();
											});										
									});
										
									</script>
																		
								<!-- weekwise report -->
																		<div id="weekchart12" ></div>
									<script type="text/javascript">
									$(document).ready(function() {
											var options = {
												chart: {
													width: 680,
													type: 'donut',												
												},
												title: {
													text: "TOYOTA Report",
													align: 'left',
													margin: 10,
													offsetX: 0,
													offsetY: 0,
													floating: false,
													style: {
													  fontSize:  '16px',
													  color:  '#263238',
													},
												},												
											   	colors: ['#5f4b8b','#1E90FF','#3ADF00','#FFC0CB','#FF1493','#0090FF','#DC143C','#0000FF','#1E013C','#00B0CB','#FF0993','#3ADF00',' #f44336','#d68910' ],
												plotOptions: {
												  pie: {
													size: undefined,
													customScale: 1,
													offsetX: 0,
													offsetY: 0,
													expandOnClick: true,
													dataLabels: {
														offset: 0,
													}, 
													donut: {
													  size: '65%',
													  background: 'transparent',
													  labels: {
														show: true,
														name: {
														  show: true,
														  fontSize: '12px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: -10													
														},  
														value: {
														  show: true,
														  fontSize: '16px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: 1,
														 formatter: function (val,w) {
															  var sep_cnt = val;
															  var tot_cnt = w.globals.seriesTotals.reduce((a, b) => {return a + b}, 0)
															return ((sep_cnt / tot_cnt)* 100).toFixed(1)+"%"
														  }
														},
														total: {
														  show: true,
														  label: 'Total Parts',
														  color: '#373d3f',
														  formatter: function (w) {
															return w.globals.seriesTotals.reduce((a, b) => {
															  return a + b
															}, 0)
														  }
														}
													  }
													},      
												  },													
												},
												
												dataLabels: {
													enabled: false
												},
												series: [0,2,5,1,4,5,2,0,3],
												labels: ['Initial Pending ','Initial Completed','Completed Final Release','Completed Initial Release','Accepted Initial Release','Accepted Final Release','Rejected','Accepted by PL Read to Submit','Rejected by PL' ],
												fill: {
													type: 'gradient',
												},
												legend: {
													width:300,
													formatter: function(val, opts) {
														return val + " - " + opts.w.globals.series[opts.seriesIndex]
													}
												},
												responsive: [{
													breakpoint: 480,
													options: {
														chart: {
															width: 200
														},
														legend: {
															position: 'bottom'
														}
													}
												}]
											}
											var weekchart12 = new ApexCharts(
												document.querySelector("#weekchart12"),
												options
											);
											weekchart12.render();
											$("#weekchart12").css("float","left");
											$("#weekchart12").css("width","550px");
									});
									</script>									
                                </div>
								<?php if($_SESSION['partlinq_user']['USERTYPE']=='3'){ ?>
								<div class="tab-pane fade" id="request">
									<?php include_once('requestform.php');?>
								</div>
								<?php } ?>
                                <div class="tab-pane fade" id="partslist">
                                  <?php include_once('pricesourcinggrid.php');?>
                                </div>
								<?php// if($_SESSION['partlinq_user']['USERTYPE']=='1'){ ?>
                               <!-- <div class="tab-pane fade" id="comppartslist">
                                  <?php //include_once('pricesourcinggrid1.php');?>
                                </div>	-->
								<?php// } ?>
								<?php if($_SESSION['partlinq_user']['USERTYPE']=='1' || $_SESSION['partlinq_user']['USERTYPE']=='5'){ ?>
                               <div class="tab-pane fade" id="comppartslist">
                                  <?php include_once('completedpartsgrid.php');?>
                                </div>	
								<?php } ?>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
				<script>
		 $(document).ready(function() {	
			 			<?php if($progress_val != "" && $progress_val >0){?>
									var myProgress = $('#progress').progressbarManager({      
										 totalValue : 100,
										 initValue : <?php echo $progress_val;?> ,
										 animate : true ,
										 stripe : true
									  });
										<?php }?>
							
				if(statusid != "" && statusid != null){
					$(".nav-tabs li").removeClass();
					$('.nav-tabs a:nth-child(1)').addClass("active");
					$('.nav-tabs a:nth-child(1)').tab('show'); 
				}else{
					/*if(tab == 1){
						$(".nav-tabs li").removeClass();
						$('.nav-tabs a:nth-child(1)').addClass("active");
						$('.nav-tabs a:nth-child(1)').tab('show'); 
					}else{
						$('.nav-tabs a:first').tab('show');
					}*/
			 <?php if($_SESSION['partlinq_user']['USERTYPE']=='0'){ ?>
			 	activaTab('summary');
			 <?php } ?>
			 <?php if($_SESSION['partlinq_user']['USERTYPE']=='1'){ ?>
				 activaTab('summary');
			 <?php } ?>
			 <?php if($_SESSION['partlinq_user']['USERTYPE']=='2'){ ?>
				 activaTab('summary');
			 <?php } ?>
			 <?php if($_SESSION['partlinq_user']['USERTYPE']=='3'){ ?>
				 activaTab('summary');
			 <?php } ?>	
			 <?php if($_SESSION['partlinq_user']['USERTYPE']=='4'){ ?>
				 activaTab('partslist');
			 <?php } ?>						
				}
			 
		jQuery.validator.addMethod("alpha", function(value, element) {
			  return this.optional( element ) || /^[a-zA-Z ]+$/.test( value );
			}, 'Please enter only alphabets.');
			$("#frm_imdsrequest").validate({
     		 submitHandler: function () {
					$.ajax({
						type:"POST",
						url:"saverequest.php",
						data:$("#frm_imdsrequest").serialize(),
						success: function(response){
							if(response=='SUCCESS'){
							   		bootbox.alert("Request submitted successfully.");
									$('#frm_imdsrequest').trigger("reset");
							}
							else{
									bootbox.alert("Something went wrong. Please try again!");
									$('#frm_imdsrequest').trigger("reset");
							}
						}
					});
              }
			});	
			 
			// $('.imds-chosen-select').chosen({"search_contains": true,width: "100%"});

			$('#request-container .input-group.date').datepicker({
						   format: 'dd-M-yy',
							todayHighlight: true,
							autoclose: true,
							startDate: '0d',
							clearBtn:true
			});
var datepickerdate = new Date();
var datepickertoday = new Date(datepickerdate.getFullYear(), datepickerdate.getMonth(), datepickerdate.getDate());			 
			$('#request-container .input-group.date').datepicker( 'setDate', datepickertoday );
			 if(usertype == 3){
				getautocompletedata("sFld6","modelnum","addmodelform-container",1);
			 	getautocompletedata("sFld4","cust_name","addcustomerform-container",2);
			 	getautocompletedata("sFld5","code","addsupplform-container",3);
			 }
		 });
			function activaTab(tab){
			  $('.nav-tabs a[href="#' + tab + '"]').tab('show');
			};
					
			function formsubmit(formname,divname,field){
				$("#"+formname).validate({
				 submitHandler: function () {
						$.ajax({
							type:"POST",
							url:"savedata.php",
							data:$("#"+formname).serialize(),
							success: function(response){
							
								if(response !=""){
									var obj = JSON.parse(response);
									if(obj.status=='success'){
										$.notify(obj.message,"success");
										$("#"+field+"name").val(obj.label);
										$("#"+field).val(obj.value);
										$("#"+formname).trigger("reset");
										$("#"+divname).css("display","none");
									}
									else{
										bootbox.alert(obj.message);
									}
								}else{
									bootbox.alert("Something went wrong. Please try again!");
								}
							}
						});
				  }
				});	
			}
			function formsubmit6(formname,divname){
				$("#"+formname).validate({
				 submitHandler: function () {
						$.ajax({
							type:"POST",
							url:"savedata.php",
							data:$("#"+formname).serialize(),
							success: function(response){
							
								if(response !=""){
									var obj = JSON.parse(response);
									if(obj.status=='success'){
										$.notify(obj.message,"success");
										//bootbox.alert(obj.message,"success");
										$("#"+formname).trigger("reset");
										$("#"+divname).css("display","none");
									}
									else{
										bootbox.alert(obj.message);
									}
								}else{
									bootbox.alert("Something went wrong. Please try again!");
								}
							}
						});
				  }
				});	
			}
	function hideallform(){
		$("#addmodelform-container").css("display","none");
		$("#addcustomerform-container").css("display","none");
		$("#addsupplform-container").css("display","none");
	}
	function getautocompletedata(field,newfield,divname,type){
		 $("#"+field+"name").autocomplete({
		 source: function( request, response ) {
			 hideallform();
			   // Fetch data
			   $.ajax({
				url: "getautocompletedata.php",
				type: 'post',
				dataType: "html",
				data: {
				 search: request.term,type:type
				},
				 minLength: 2,
				success: function( data ) {
					
							var parsed = JSON.parse(data);
							var newArray = new Array(parsed.length);
							var i = 0;

							parsed.forEach(function (entry) {
								var newObject = {
									label: entry.label,
									value:entry.value,
								};
								newArray[i] = newObject;
								i++;
							});
					if(parsed.length == 0)
					{ 
						$("#"+field+"name").val("");
						$("#"+field).val("");
						$("#"+newfield).val($("#"+field).val());
						if($("#"+newfield).hasClass("error")){
                          $("#"+newfield).removeClass("error");
						}
						$("#"+divname).css("display","block");
                        $("#"+newfield).focus();
					}
					else{
						$("#"+divname).val('');
						hideallform();
					}
				response(newArray);
				}
			   });
			  },
			 change: function (e, ui) {
                if (!( ui.item)){ e.target.label = "";
				 $("#"+field+"name").val("");
		         $("#"+field).val("")
								}
             },
			  select: function (event, ui) {
			   // Set selection

			  $("#"+field+"name").val(ui.item.label); // save selected id to input
			  $("#"+field).val(ui.item.value); // save selected id to input
			   return false;
			  }
			 }).data("ui-autocomplete")._renderItem = function (ul, item) {

							return $("<li></li>")
						.data("ui-autocomplete-item", item)
						.append("<a><table width='100%'><tr><td>" + item.label + "</td></tr></table></a>")
						.appendTo(ul);
						};
		
	}
		 </script>
	 </body>
	<?php }?>