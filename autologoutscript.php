<script type="text/javascript">
var max_idle_time = '<?php echo $ipblockres['max_idle_time'];?>';
</script>
<script type="text/javascript" src="js/autologout.js"></script>
<style>
#timeout {
    display: none;
}
</style>
<div id="timeout">
    <h4>Session about to expiry</h4>
	<p>
			You will be automatically logged out in <span id="timer">1 minute</span>.<br />
			<!--To remain logged in move your mouse over this window.-->
			To remain logged in click <b>Stay Logged In</b> button
	</p>
</div>