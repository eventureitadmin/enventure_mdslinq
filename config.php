<?php
ob_start();

session_start();

date_default_timezone_set('Asia/Calcutta');

ini_set("display_errors",1);

ini_set('memory_limit', '-1');

error_reporting(1);

define("FOLDER_NAME","/");

global $docroot; 

$docroot=$_SERVER['DOCUMENT_ROOT'].FOLDER_NAME;

define("PAGEURL",$docroot);

define("ROOT_DIR",$docroot);

define("DEBUG_MODE",false);

define("TEST_MODE",false);

define("SEND_MAIL",true);

define("TOOL_SHORT_NAME","MDSLinq | ComplianceXL");

define("TOOL_FULL_NAME","");

define("ENABLE_SECURITY",true);

define("ENABLE_RECAPTCHA",false);

define("RECAPTCHA_SECRETKEY","6LdIbswZAAAAANP6EgCOqa7zaCkCQIj4NY4LkkiU");

define("RECAPTCHA_SITEKEY","6LdIbswZAAAAAOjBFAlmk5YF8qa7YiZaZAlEgnC4");

if(TOOL_FULL_NAME != ''){
	define("TOOL_TITLE",TOOL_SHORT_NAME." - ".TOOL_FULL_NAME);
}
else{
	define("TOOL_TITLE",TOOL_SHORT_NAME);
}
define("TOOL_LOGIN_TITLE",TOOL_SHORT_NAME." - "." Login Page");
// project type
$project_type = array("CEG","MRO");

$usertype_array = array("0"=>"Administrator","1"=>"Enventure User","2"=>"Client Admin","3"=>"Requester","4"=>"PL Review","5"=>"Service Part PL Reviewer");
include("message.php");
define("CUSTERRMSG","The selected Customer Due Date is less than the standard two months turnaround time of ComplianceXL");
// project type
$login_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]".FOLDER_NAME."login.php";
define("SUPPORTEMAIL","support@mdslinq.com");

?>
