<?php
	include_once("header.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{
		if($_GET){
			$id=$_GET['id'];
			$auditdet = $logtrack->getauditdetbyid($id);
			$logouttime = date('Y-m-d H:i:s');
		$totalhrsary = $dbase->calcculatetothrs($auditdet['logintime'],$logouttime);
		$totalhrs = $totalhrsary['hours'].":".$totalhrsary['minutes'].":".$totalhrsary['seconds'];			
		$query = "UPDATE env_login_audit SET `logouttime`='".$logouttime."', `totalhrs`='".$totalhrs."', `curtimestamp`='".time()."',`is_cur_session`='0',`logout_type`='2',`isactive`='0' WHERE id='".$id."'";
		$dbase->query($query);
			header("Location:userloginlog.php");
			exit();
		}

?>
    <body>
		<?php include("menu.php"); ?>
<link href="css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<link href="css/dataTables/dataTables.responsive.css" rel="stylesheet">			
            <div id="page-wrapper" style="padding:40px 5px 0 5px">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class=""> Log of Logins and Logouts</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
				<?php if(isset($_GET['msg'])){ ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
								<?php if($_GET['msg']==1){ echo show_success_msg('IP blocked successfully'); } ?>
							    <?php if($_GET['msg']==2){ echo show_error_msg('Please select Project.'); } ?>
								 <?php if($_GET['msg']==3){ echo show_error_msg('Data save error.'); } ?>

                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>	
				<?php } ?>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover" id="weeklytable">
                                        <thead>
                                            <tr>
												<th>Username</th>
												<!--<th>Log Date</th>-->
												<th>Login </th>
												<th>Logout </th>
                                                <th>Total Hrs</th>
												<th>Fail Login Count</th>
												<th>Active Login</th>
												<th>Is Idle Logout</th>
												<th>Logout Type</th>
												<th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
		 	 						$ipqry = "SELECT a.id,(SELECT u.fullname FROM env_user u WHERE u.id=a.userid) AS username, DATE_FORMAT(a.`logdate`,'%d-%b-%Y') AS `logdate`, DATE_FORMAT(a.`logintime`,'%d-%b-%Y %r') AS logintime, DATE_FORMAT(a.`logouttime`,'%d-%b-%Y %r') AS logouttime, a.`totalhrs`, a.`faillogincnt`, a.`is_cur_session`, a.`is_idlelogout`,a.`logout_type` FROM `env_login_audit` a WHERE a.logdate='".date('Y-m-d')."'";
		 							$ipres = $dbase->executeQuery($ipqry,"multiple");
										for($i=0;$i<count($ipres);$i++){
											if (0 == $i % 2) {
												$class = 'class="even"';
											}
											else{
												$class = 'class="odd"';
											}
											$is_cur_session = '';
											$action = '';
											if($ipres[$i]['is_cur_session']=='1'){
												$is_cur_session = "Yes";
												$action = '<a href="userloginlog.php?id='.$ipres[$i]['id'].'">Logout</a>';
											}
											$is_idle_logout = '';
											if($ipres[$i]['is_idlelogout']=='1'){
												$is_idle_logout = "Yes";
											}
											$logouttype='';
											if($ipres[$i]['is_cur_session']=='0'){
											if($ipres[$i]['logout_type']=='0'){
												$logouttype="User Logout";
											}
											else if($ipres[$i]['logout_type']=='1'){
												$logouttype="Auto Logout";
											}
											else if($ipres[$i]['logout_type']=='2'){
												$logouttype="Admin Logout";
											}
											}
											echo '<tr '.$class.'>                                              
											    <td>'.$ipres[$i]['username'].'</td>
                                                <!--<td>'.$ipres[$i]['logdate'].'</td>-->
												<td>'.$ipres[$i]['logintime'].'</td>
												<td>'.$ipres[$i]['logouttime'].'</td>
												<td>'.$ipres[$i]['totalhrs'].'</td>
												<td>'.$ipres[$i]['faillogincnt'].'</td>
												<td>'.$is_cur_session.'</td>
												<td>'.$is_idle_logout.'</td>
												<td>'.$logouttype.'</td>
												<td>'.$action.'</td>
                                            </tr>';
										}
										?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>				
                </div>				
            </div>
            <!-- /#page-wrapper -->
<script src="js/dataTables/jquery.dataTables.min.js"></script>
<script src="js/dataTables/dataTables.bootstrap.min.js"></script>		
		<script type="text/javascript">
		$(document).ready(function() {
                $('#weeklytable').DataTable({
                        responsive: true,
					   'aoColumnDefs': [{
							'bSortable': false,
							'aTargets': ['nosort']
						}]						
                });			
		});
		</script>
	<?php
		 include_once("footer.php");
	}	
?>
