<?php include_once("top.php");?>
<script type="text/javascript" src="js/apexcharts.js"></script>
<script>
	
	function showchart(chartid,selcolors,batch,seriesarr,labelarr,datapointsarr,reportname){
		var width ="680";
			var options = {
												
												chart: {
													width: width,
													height: 'auto',
													type: 'donut',
													id:chartid,
													events: {
														dataPointSelection: function(e,chart, opts) {
														   var  lindex = opts.dataPointIndex;
														   var  statusid = opts.w.config.datapoints[lindex];
														   var statusstr = opts.w.globals.chartID;
														   var statusname1 = opts.w.globals.labels[lindex];
														window.location.href="home.php?batch="+batch+"&statusid="+statusid+"&sorder="+statusstr+"&scid=0";
														 }
													}
												},
												title: {
													text: reportname,
													align: 'left',
													margin: 10,
													offsetX: 0,
													offsetY: 0,
													floating: false,
													style: {
													  fontSize:  '16px',
													  color:  '#263238',
													},
												},												
											 	colors:selcolors,
												plotOptions: {
												  pie: {
													size: undefined,
													customScale: 1,
													offsetX: 0,
													offsetY: 0,
													expandOnClick: true,
													dataLabels: {
														offset: 0,
													}, 
													donut: {
													  size: '65%',
													  background: 'transparent',
													
													  labels: {
														show: true,
														name: {
														  show: true,
														  fontSize: '12px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: -10													
														},  
														value: {
														  show: true,
														  fontSize: '16px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: 1,
														 formatter: function (val,w) {
															  var sep_cnt = val;
															  var tot_cnt = w.globals.seriesTotals.reduce((a, b) => {return a + b}, 0)
															return ((sep_cnt / tot_cnt)* 100).toFixed(1)+"%"
														  }
														},
														total: {
														  show: true,
														  label: 'Total Parts',
														  color: '#373d3f',
														  formatter: function (w) {
															return w.globals.seriesTotals.reduce((a, b) => {
															  return a + b
															}, 0)
														  }
														}
													  }
													},      
												  },													
												},
												
												dataLabels: {
													enabled: false
												},
												
												series: seriesarr,
												labels: labelarr,
												datapoints:datapointsarr,
												fill: {
													type: 'gradient',
												},
												legend: {
													width:300,
													formatter: function(val, opts) {
														return val + " - " + opts.w.globals.series[opts.seriesIndex]
													}
												},
												responsive: [{
													breakpoint: 480,
													options: {
														chart: {
															width: 300
														},
														legend: {
															position: 'bottom'
														}
													}
												}]
											};
		return options;
										
	}
</script>
<?php
if(isset($_POST)){

$statusheadres = $dbase->executeQuery("SELECT `status_headerid`,project_id FROM `env_batch` WHERE `closed` =0 
			AND id='".$_POST['batch']."'","single");
	        $projectid = $statusheadres['project_id'];
			$statushead1 = explode(',',$statusheadres['status_headerid']);
			if(count($statushead1) >0){
				for($p=0;$p<count($statushead1);$p++){
					if($statushead1[$p] >0){
					 $statusfield =($statushead1[$p])-1;
			 $statusquery=" AND FIND_IN_SET(s.id,(SELECT `status_values` FROM `env_projectheaders` WHERE `itypeid`=2 AND `batchid` ='".$_POST['batch']."' AND orderid='".$statushead1[$p]."'))";
					$selqry1 =$dbase->getstatusgraphdetails($projectid,$_POST['batch'],"",$statusquery,$_POST['cid'],$statusfield);
								 
						if($selqry1 !=""){
							$statusfinalres = $dbase->executeQuery($selqry1,"multiple");
						}else{
							$statusfinalres ="";
						}
						$colors1 = array();
						$seriesarray = array();
						$labelarray = array();
						$datapointsarray = array();
						if($statusfinalres != ""){
							$reportname =" Current week status: ".date("d-M-Y");
							for($h=0;$h<count($statusfinalres);$h++){
								$colors1[$h] = $statusfinalres[$h]['colorcode'];
								$datapointsarray[$h] = $statusfinalres[$h]['envstatusid'];
								$labelarray[$h] = $statusfinalres[$h]['env_status'];
								$seriesarray[$h] = $statusfinalres[$h]['cnt'];
							}
									$width =680;
												  //$statusheadres[$p] ucfirst($reportname)$colors1 $seriesarray $labelarray $datapointsarray
									?>

									<div id="currentweekchart<?php echo $p;?>"></div>
	<script type="text/javascript">
									$(document).ready(function() {
											
											var options = {
												
												chart: {
													width: '<?php echo $width?>',
													height: 'auto',
													type: 'donut',
													id:'<?php echo $statusheadres[$p]; ?>',
													events: {
														dataPointSelection: function(e,chart, opts) {
														   var  lindex = opts.dataPointIndex;
														   var  statusid = opts.w.config.datapoints[lindex];
														   var statusstr = opts.w.globals.chartID;
														   var statusname1 = opts.w.globals.labels[lindex];
														window.location.href="home.php?batch="+batch+"&statusid="+statusid+"&sorder="+statusstr+"&scid=0";
														 }
													}
												},
												title: {
									text: "<?php echo ucfirst($reportname);?>  ",
													align: 'left',
													margin: 10,
													offsetX: 0,
													offsetY: 0,
													floating: false,
													style: {
													  fontSize:  '16px',
													  color:  '#263238',
													},
												},												
											 	colors: [<?php echo "'" . implode("','", $colors1) . "'";?> ],
												plotOptions: {
												  pie: {
													size: undefined,
													customScale: 1,
													offsetX: 0,
													offsetY: 0,
													expandOnClick: true,
													dataLabels: {
														offset: 0,
													}, 
													donut: {
													  size: '65%',
													  background: 'transparent',
													
													  labels: {
														show: true,
														name: {
														  show: true,
														  fontSize: '12px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: -10													
														},  
														value: {
														  show: true,
														  fontSize: '16px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: 1,
														 formatter: function (val,w) {
															  var sep_cnt = val;
															  var tot_cnt = w.globals.seriesTotals.reduce((a, b) => {return a + b}, 0)
															return ((sep_cnt / tot_cnt)* 100).toFixed(1)+"%"
														  }
														},
														total: {
														  show: true,
														  label: 'Total Parts',
														  color: '#373d3f',
														  formatter: function (w) {
															return w.globals.seriesTotals.reduce((a, b) => {
															  return a + b
															}, 0)
														  }
														}
													  }
													},      
												  },													
												},
												
												dataLabels: {
													enabled: false
												},
												
												series: [<?php echo  implode(",", $seriesarray);?>],
												labels: [<?php echo "'" . implode("','", $labelarray) . "'";?> ],
												datapoints: [<?php echo "'" . implode("','", $datapointsarray) . "'";?> ],
												fill: {
													type: 'gradient',
												},
												legend: {
													width:300,
													formatter: function(val, opts) {
														return val + " - " + opts.w.globals.series[opts.seriesIndex]
													}
												},
												responsive: [{
													breakpoint: 480,
													options: {
														chart: {
															width: 300
														},
														legend: {
															position: 'bottom'
														}
													}
												}]
											}
											var currentweekchart<?php echo $p; ?> = new ApexCharts(
												document.querySelector("#currentweekchart<?php echo $p;?>"),
												options
											);
											$("#currentweekchart<?php echo $p;?>").css("float","left");
											$("#currentweekchart<?php echo $p;?>").css("width","<?php echo $width;?>px");

											setTimeout(function(){
												currentweekchart<?php echo $p; ?>.render();
											}, 500);
											
									
									});
										
									</script>
									
								<?php $isweekreportdataexists =0;
						//Weekreport
						if($_POST['is_weekly_report'] == 1){
		 							  $weekreportquery = "SELECT DISTINCT `reportdate` FROM `env_report` WHERE `iPrjID`='".$projectid."' AND `iBatch`='".$_POST['batch']."' ORDER BY `reportdate` DESC LIMIT 0,1";
		 							$weekreportresult = $dbase->executeQuery($weekreportquery,"multiple");
		 							if(count($weekreportresult) > 0){
										$isweekreportdataexists =1;
										for($j=0;$j<count($weekreportresult);$j++){	
										
					$rptqry =$dbase->getweekstatusdetails($projectid,$_POST['batch'],$statusquery,$_POST['cid'],$statusfield,$weekreportresult[$j]['reportdate']);
										
											if($rptqry !=""){
												$weekstatusfinalres = $dbase->executeQuery($rptqry,"multiple");//echo $selqry;
											}else{
												$weekstatusfinalres ="";
											}
												$wcolors = array();
												$wseriesarray = array();
												$wlabelarray = array();
												$wdatapointsarray = array();
												
												if($weekstatusfinalres != ""){
													$reportname =" Current week Report on".date("d-M-Y");
													for($x=0;$x<count($weekstatusfinalres);$x++){
														$wcolors[$x] = $weekstatusfinalres[$x]['colorcode'];
														$wdatapointsarray[$x] = $weekstatusfinalres[$x]['envstatusid'];
														$wseriesarray[$x] = $weekstatusfinalres[$x]['cnt'];
														$wlabelarray[$x] = $weekstatusfinalres[$x]['env_status'];
													}?>
									<div id="weekchart<?php echo ($j); ?>" ></div>
									<script type="text/javascript">
									$(document).ready(function() {
									
											var options = {
												chart: {
													width: 680,
													type: 'donut',												
												},
												title: {
													text: "Last week status : <?php echo date('d-M-Y',strtotime($weekreportresult[$j]['reportdate'])); ?>",
													align: 'left',
													margin: 10,
													offsetX: 0,
													offsetY: 0,
													floating: false,
													style: {
													  fontSize:  '16px',
													  color:  '#263238',
													},
												},												
											   	colors: [<?php echo "'" . implode("','", $wcolors) . "'";?> ],
												plotOptions: {
												  pie: {
													size: undefined,
													customScale: 1,
													offsetX: 0,
													offsetY: 0,
													expandOnClick: true,
													dataLabels: {
														offset: 0,
													}, 
													donut: {
													  size: '65%',
													  background: 'transparent',
													  labels: {
														show: true,
														name: {
														  show: true,
														  fontSize: '12px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: -10													
														},  
														value: {
														  show: true,
														  fontSize: '16px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: 1,
														 formatter: function (val,w) {
															  var sep_cnt = val;
															  var tot_cnt = w.globals.seriesTotals.reduce((a, b) => {return a + b}, 0)
															return ((sep_cnt / tot_cnt)* 100).toFixed(1)+"%"
														  }
														},
														total: {
														  show: true,
														  label: 'Total Parts',
														  color: '#373d3f',
														  formatter: function (w) {
															return w.globals.seriesTotals.reduce((a, b) => {
															  return a + b
															}, 0)
														  }
														}
													  }
													},      
												  },													
												},
												
												dataLabels: {
													enabled: false
												},
												
												series: [<?php echo  implode(",", $wseriesarray);?>],
												labels: [<?php echo "'" . implode("','", $wlabelarray) . "'";?> ],
												fill: {
													type: 'gradient',
												},
												legend: {
													width:300,
													formatter: function(val, opts) {
														return val + " - " + opts.w.globals.series[opts.seriesIndex]
													}
												},
												responsive: [{
													breakpoint: 480,
													options: {
														chart: {
															width: 200
														},
														legend: {
															position: 'bottom'
														}
													}
												}]
											}
											var weekchart<?php echo ($j); ?> = new ApexCharts(
												document.querySelector("#weekchart<?php echo ($j); ?>"),
												options
											);
											weekchart<?php echo ($j); ?>.render();
											$("#weekchart<?php echo ($j); ?>").css("float","left");
											$("#weekchart<?php echo ($j); ?>").css("width","550px");
									});
									</script>
											<?php	}
										}
									}
						}
						if($isweekreportdataexists == 0){$j =0;?>
						
									<div id="weekchart<?php echo ($j); ?>" ></div>
									<script type="text/javascript">
									$(document).ready(function() {
									
											var options = {
												chart: {
													width: 680,
													type: 'donut',												
												},
												title: {
													text: "Last week status : "+"<?php echo date('d-M-Y', strtotime('last week Friday'));?>",
													align: 'left',
													margin: 10,
													offsetX: 0,
													offsetY: 0,
													floating: false,
													style: {
													  fontSize:  '16px',
													  color:  '#263238',
													},
												},												
											   	colors: [<?php echo "'" . implode("','", $colors1) . "'";?> ],
												plotOptions: {
												  pie: {
													size: undefined,
													customScale: 1,
													offsetX: 0,
													offsetY: 0,
													expandOnClick: true,
													dataLabels: {
														offset: 0,
													}, 
													donut: {
													  size: '65%',
													  background: 'transparent',
													  labels: {
														show: true,
														name: {
														  show: true,
														  fontSize: '12px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: -10													
														},  
														value: {
														  show: true,
														  fontSize: '16px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: 1,
														 formatter: function (val,w) {
															  var sep_cnt = val;
															  var tot_cnt = w.globals.seriesTotals.reduce((a, b) => {return a + b}, 0)
															return ((sep_cnt / tot_cnt)* 100).toFixed(1)+"%"
														  }
														},
														total: {
														  show: true,
														  label: 'Total Parts',
														  color: '#373d3f',
														  formatter: function (w) {
															return w.globals.seriesTotals.reduce((a, b) => {
															  return a + b
															}, 0)
														  }
														}
													  }
													},      
												  },													
												},
												
												dataLabels: {
													enabled: false
												},
												series: [0,0,0,0,0,0,0,0],
												labels: [<?php echo "'" . implode("','", $labelarray) . "'";?> ],
												fill: {
													type: 'gradient',
												},
												legend: {
													width:300,
													formatter: function(val, opts) {
														return val + " - " + opts.w.globals.series[opts.seriesIndex]
													}
												},
												responsive: [{
													breakpoint: 480,
													options: {
														chart: {
															width: 200
														},
														legend: {
															position: 'bottom'
														}
													}
												}]
											}
											var weekchart<?php echo ($j); ?> = new ApexCharts(
												document.querySelector("#weekchart<?php echo ($j); ?>"),
												options
											);
											weekchart<?php echo ($j); ?>.render();
											$("#weekchart<?php echo ($j); ?>").css("float","left");
											$("#weekchart<?php echo ($j); ?>").css("width","550px");
									});
									</script>
					<?php	}?>

					<?php
							///
								//customer report
						$custdetails = $dbase->getcustomerdetails($projectid,$_POST['batch']);
					
						if(count($custdetails) >0){
							for($y=0;$y<count($custdetails);$y++){
								$selectedcid= $custdetails[$y]['cid'];
								////
								$chartalign ="left";
								$width=680;
								
								$custrptname = $custdetails[$y]['cname'];

									$selqry2 =$dbase->getstatusgraphdetails($projectid,$_POST['batch'],$custdetails[$y]['cid'],$statusquery,$_POST['cid'],$statusfield);
								 if($selqry2 !=""){
									$statusfinalres2 = $dbase->executeQuery($selqry2,"multiple");;
								}else{
									$statusfinalres2 ="";
								}
							
							    $colors2 = array();
								$seriesarray2 = array();
								$labelarray2 = array();
								$datapointsarray2 = array();
								if($statusfinalres2 != ""){//print_r($statusfinalres);
									
									for($h=0;$h<count($statusfinalres2);$h++){
										$colors2[$h] = $statusfinalres2[$h]['colorcode'];
										$datapointsarray2[$h] = $statusfinalres2[$h]['envstatusid'];
										$labelarray2[$h] = $statusfinalres2[$h]['env_status'];
										$seriesarray2[$h] = $statusfinalres2[$h]['cnt'];
									}
								
									?>
									
									<div id="customerchart<?php echo $y;?>"></div>
									<?php if($y%2 == 0){?>
									<script type="text/javascript">
									
									$(document).ready(function() {
										
											var options = {
												
												chart: {
													width: '<?php echo $width?>',
													height: 'auto',
													type: 'donut',
													id:'<?php echo $statusheadres[$p].'_'.$selectedcid;?>',
													events: {
														dataPointSelection: function(e,chart, opts) {
														   var  lindex = opts.dataPointIndex;
														   var  statusid = opts.w.config.datapoints[lindex];
														   var statusstrarr = opts.w.globals.chartID.split("_");
															
														   var statusname1 = opts.w.globals.labels[lindex];
														window.location.href="home.php?batch="+batch+"&statusid="+statusid+"&sorder="+statusstrarr[0]+"&scid="+statusstrarr[1];
														 }
													}
												},
												title: {
									text: "<?php echo ucfirst($custrptname);?>  ",
													align: 'left',
													margin: 10,
													offsetX: 0,
													offsetY: 0,
													floating: false,
													style: {
													  fontSize:  '16px',
													  color:  '#263238',
													},
												},												
											 	colors: [<?php echo "'" . implode("','", $colors2) . "'";?> ],
												plotOptions: {
												  pie: {
													size: undefined,
													customScale: 1,
													offsetX: 0,
													offsetY: 0,
													expandOnClick: true,
													dataLabels: {
														offset: 0,
													}, 
													donut: {
													  size: '65%',
													  background: 'transparent',
													
													  labels: {
														show: true,
														name: {
														  show: true,
														  fontSize: '12px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: -10													
														},  
														value: {
														  show: true,
														  fontSize: '16px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: 1,
														 formatter: function (val,w) {
															  var sep_cnt = val;
															  var tot_cnt = w.globals.seriesTotals.reduce((a, b) => {return a + b}, 0)
															return ((sep_cnt / tot_cnt)* 100).toFixed(1)+"%"
														  }
														},
														total: {
														  show: true,
														  label: 'Total Parts',
														  color: '#373d3f',
														  formatter: function (w) {
															return w.globals.seriesTotals.reduce((a, b) => {
															  return a + b
															}, 0)
														  }
														}
													  }
													},      
												  },													
												},
												
												dataLabels: {
													enabled: false
												},
												
												series: [<?php echo  implode(",", $seriesarray2);?>],
												labels: [<?php echo "'" . implode("','", $labelarray2) . "'";?> ],
												datapoints: [<?php echo "'" . implode("','", $datapointsarray2) . "'";?> ],
												fill: {
													type: 'gradient',
												},
												legend: {
													width:300,
													formatter: function(val, opts) {
														return val + " - " + opts.w.globals.series[opts.seriesIndex]
													}
												},
												responsive: [{
													breakpoint: 480,
													options: {
														chart: {
															width: 300
														},
														legend: {
															position: 'bottom'
														}
													}
												}]
											}
											var customerchart<?php echo $y; ?> = new ApexCharts(
												document.querySelector("#customerchart<?php echo $y;?>"),
												options
											);
										
											$("#customerchart<?php echo $y;?>").css("float","<?php echo $chartalign;?>");
											$("#customerchart<?php echo $y;?>").css("width","<?php echo $width;?>px");

											//setTimeout(function(){
												customerchart<?php echo $y; ?>.render();
											//}, 500);

																				
									});
										
									</script>
									<?php }else{?>
									<script type="text/javascript">
									$(document).ready(function() {
						
											var options = {
												chart: {
													width: 680,
													type: 'donut',
												    id:'<?php echo $statusheadres[$p].'_'.$selectedcid;?>',
													events: {
														dataPointSelection: function(e,chart, opts) {
														   var  lindex = opts.dataPointIndex;
														   var  statusid = opts.w.config.datapoints[lindex];
														   var statusstrarr = opts.w.globals.chartID.split("_");
														   var statusname1 = opts.w.globals.labels[lindex];
														window.location.href="home.php?batch="+batch+"&statusid="+statusid+"&sorder="+statusstrarr[0]+"&scid="+statusstrarr[1];
														 }
													}											
												},
												title: {
													text: "<?php echo ucfirst($custrptname); ?>",
													align: 'left',
													margin: 10,
													offsetX: 0,
													offsetY: 0,
													floating: false,
													style: {
													  fontSize:  '16px',
													  color:  '#263238',
													},
												},												
											   	colors: [<?php echo "'" . implode("','", $colors2) . "'";?> ],
												plotOptions: {
												  pie: {
													size: undefined,
													customScale: 1,
													offsetX: 0,
													offsetY: 0,
													expandOnClick: true,
													dataLabels: {
														offset: 0,
													}, 
													donut: {
													  size: '65%',
													  background: 'transparent',
													  labels: {
														show: true,
														name: {
														  show: true,
														  fontSize: '12px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: -10													
														},  
														value: {
														  show: true,
														  fontSize: '16px',
														  fontFamily: 'Helvetica, Arial, sans-serif',
														  color: "#000000",
														  offsetY: 1,
														 formatter: function (val,w) {
															  var sep_cnt = val;
															  var tot_cnt = w.globals.seriesTotals.reduce((a, b) => {return a + b}, 0)
															return ((sep_cnt / tot_cnt)* 100).toFixed(1)+"%"
														  }
														},
														total: {
														  show: true,
														  label: 'Total Parts',
														  color: '#373d3f',
														  formatter: function (w) {
															return w.globals.seriesTotals.reduce((a, b) => {
															  return a + b
															}, 0)
														  }
														}
													  }
													},      
												  },													
												},
												
												dataLabels: {
													enabled: false
												},
												series: [<?php echo  implode(",", $seriesarray2);?>],
												labels: [<?php echo "'" . implode("','", $labelarray2) . "'";?> ],
												
												datapoints: [<?php echo "'" . implode("','", $datapointsarray2) . "'";?> ],
												fill: {
													type: 'gradient',
												},
												legend: {
													width:300,
													formatter: function(val, opts) {
														return val + " - " + opts.w.globals.series[opts.seriesIndex]
													}
												},
												responsive: [{
													breakpoint: 480,
													options: {
														chart: {
															width: 200
														},
														legend: {
															position: 'bottom'
														}
													}
												}]
											}
											var customerchart<?php echo ($y); ?> = new ApexCharts(
												document.querySelector("#customerchart<?php echo ($y); ?>"),
												options
											);
											customerchart<?php echo ($y); ?>.render();
											$("#customerchart<?php echo ($y); ?>").css("float","left");
											$("#customerchart<?php echo ($y); ?>").css("width","550px");
									});
									</script>
									<?php }?>
						<?php	}
								////
							}
						}//cust
							?>
						
						<?php }
	
					}
				}
			}
}
?>
		
