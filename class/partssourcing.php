<?php
	
	require_once $docroot.'/class/data.php';
	class partssourcing extends data{
		
		public function getprojectdetails($usertype,$userid=""){
			$res = "";$query = "";
			
			$query1 = "SELECT b.id as batchid,b.project_id,b.batchno,b.totalparts,b.startdate,b.enddate  FROM `env_batch` as b";
			$query2=" WHERE b.`project_id` IN ";
			$query3="(SELECT ID FROM env_project WHERE
							isactive = 1 AND isclosed  = 0)";
			if($usertype == 1){
				$query .= $query1.$query2.$query3." AND b.`project_id` IN(SELECT u.`iPrjID` FROM `env_urlgrab` as u 
				WHERE  u.iBatch = b.id AND u.`userid` = ".$userid.")";
			}else if($usertype == 2){
				$query .= $query1.$query2."(SELECT ID FROM env_project WHERE customer_id = ".$_SESSION['partlinq_user']['CUSTOMERID']." AND isactive = 1 AND isclosed  = 0)";
			}else if($usertype == 0){
				$query .= $query1.$query2.$query3;
			}
			else if($usertype == 3){
				$query .= $query1.$query2."(SELECT ID FROM env_project WHERE customer_id = ".$_SESSION['partlinq_user']['CUSTOMERID']." AND isactive = 1 AND isclosed  = 0)";
			}else if($usertype == 5){
				$query ="SELECT DISTINCT b.id as batchid,b.project_id,b.batchno,b.totalparts,b.startdate,b.enddate FROM `env_userallocTL`tl ,env_batch b,env_project p WHERE tl.`iUserid`='".$_SESSION['partlinq_user']['ID']."' AND b.id= tl.iPrjId AND b.closed =0 AND  p.customer_id='".$_SESSION['partlinq_user']['CUSTOMERID']."'";
			}
			 $query .= " AND b.closed=0 ORDER BY b.`project_id`, b.`batchno` ASC";

			$res = $this->executeQuery($query,"multiple");
			return $res;
		
		}
		public function getdetails($tablename,$columns,$where,$datatype){
			$res = "";$query = "";
			$query =" SELECT ".$columns." FROM ".$tablename." WHERE ".$where;
			$res = $this->executeQuery($query,$datatype);
			return $res;
		}
		public function getprojectheaders($batchid,$headertype=""){
		
			$res = "";$query = "";
			if($batchid > 0){
			$query =" SELECT `id`,`headername`,`headertype`,`orderid`,`is_mandatory`,`is_readonly`,`is_user_edit`,`is_client_edit`,`is_requestor_edit`,`itypeid`,`formula`,`formulatype`,`is_cmplparts_edit`,`hidden_type` 
						FROM `env_projectheaders` WHERE itypeid <> 4 AND `batchid`=".$batchid;
			if(!empty($headertype)){
				if($headertype == "input"){ 
					$query .= " AND headertype = 0";
				}else if($headertype == "output"){
					$query .= " AND headertype = 1";
					
				}
			}
			/*if($_SESSION['partlinq_user']['USERTYPE'] != 0 ){
				$query .= " AND itypeid <> 4";
			}*/
			$query .= " ORDER BY `orderid` ASC";
			$res = $this->executeQuery($query,"multiple");
			}
			return $res;
		}
		public function getunrptheader($batchid){
			$result ="";
			if($batchid >0){
				$result =$this->executeQuery("SELECT * FROM `env_unrptheaders` WHERE `batchid` ='".$batchid."' ORDER BY `orderid` ASC","multiple");
			}
			return $result;
		}
		public function getunrptprojheaders($unrpres)
		 {
			 $props='"",'; 
			if($_SESSION['partlinq_user']['USERTYPE']=='2' || $_SESSION['partlinq_user']['USERTYPE']=='3' || $_SESSION['partlinq_user']['USERTYPE']=='6'){
				$props.='"",'; 
			}else{
				$props .='"Delete<br/><input name=\'iHeaderSubmit\' id=\'iHeaderSubmit\' type=\'checkbox\' class=\'checker\' '.$checkedval.'?checked=\'checked\':checked=\'\' onclick=\'checkallofmine(this.checked)\'>",';
			}
			 
			 $col=0;
			 
			 if(count($unrpres) > 0){
				 //$props .='"''",';
				 foreach($unrpres as $key=>$headerinfo){
					 $props .='"';
					 if($headerinfo['headertype'] == 0){
					 $props .="<a href='#' data-toggle='modal' onclick='openModal()' id='filter'><img src='images/filter.png'  height=10 width=10></a>"."<br/><div class='inputcls'><a  onclick='sortdata()' class='colHeader columnSorting'><b>".$headerinfo['headername']."</b></a></div>";
						 
					 }else if($headerinfo['headertype'] == 1){
						 $orderid     = $headerinfo['orderid'];
					    $props .="<a href='#' data-toggle='modal' onclick='openModal()' id='filter'><img src='images/filter.png'  height=10 width=10></a><br/>"."<div class='outputcls'><a class='colHeader columnSorting'><b>".$headerinfo['headername']."</b></a></div>";
					 
					 }
						   $props .='",';
						   $col++;
				 }
			 }
			  echo rtrim($props,",");   
		 }
		
		public  function getunrptheadersprops($unrpres)
		{
			$props	=	'{readOnly:true},';
				if($_SESSION['partlinq_user']['USERTYPE']=='2' || $_SESSION['partlinq_user']['USERTYPE']=='3' || $_SESSION['partlinq_user']['USERTYPE']=='6'){
					$props	.=	'{readOnly:true},';
				}else{
					$props	.=	'{type: "checkbox"},';
				}
			
			if(count($unrpres) > 0){
				 foreach($unrpres as $key=>$headerinfo){
			
					   $props .="{";
					   if($headerinfo['headertype']==0){
						    $props .="readOnly: true,";$props .="type: 'searchforinput',";
					    }else if($headerinfo['headertype']==1){
						   
						     if($_SESSION['partlinq_user']['USERTYPE'] == 1 || $_SESSION['partlinq_user']['USERTYPE'] == 0){
								$is_editable = $headerinfo['is_user_edit'];
							 }else{
								$is_editable = 0;
							 }
						   if($headerinfo['itypeid'] == 2){
							  $orderid =  $headerinfo['orderid'];
							   $props .="type:'envstatus',orderid:'".$orderid."',editor:false,headername:'".$headerinfo['headername']."'";
						   }
						   else{
							   if($is_editable == 1){
									$props .="type:'text',";

							   }else{
								   if($headerinfo['headername'] == "File Name" || $headerinfo['itypeid'] == 6){
									   $props .="readOnly: true,";$props .="type: 'fileupload',";
								   }else{
										$props .="readOnly: true,";$props .="type: 'searchforinput',";
								   }
							   }
						   	}
					   }
					  
					  $props .="},";
				 }
			}
			echo rtrim($props,",");   
		}
		public function getunrptcolwidth($unrpres)
		{
			$props='1,';
				if($_SESSION['partlinq_user']['USERTYPE']=='2' || $_SESSION['partlinq_user']['USERTYPE']=='3' || $_SESSION['partlinq_user']['USERTYPE']=='6'){
					$props.='1,';
				}else{
						$props.='60,';
				}
			if(count($unrpres) > 0){
				 $prorjheaderinfo = $this->getprojectheaders($pid);
				 foreach($unrpres as $key=>$headerinfo){
					    $props .='150,';
				 }
			}
			echo rtrim($props,",");     
	    }
		 public function getprojheaders($pid,$formulaarr,$is_comp=0,$is_chk=0)
		 {
			 
			 $props='"",'; 
				 if((($_SESSION['partlinq_user']['USERTYPE'] == 2 || $_SESSION['partlinq_user']['USERTYPE'] == 3) && $is_chk ==1) || $_SESSION['partlinq_user']['USERTYPE'] == 0 || $_SESSION['partlinq_user']['USERTYPE'] == 1 || $_SESSION['partlinq_user']['USERTYPE'] == 6){
				 $props.='"",';
			 }else{
			 $props .='"Cancel<br/><input name=\'iHeaderSubmit\' id=\'iHeaderSubmit\' type=\'checkbox\' class=\'checker\' '.$checkedval.'?checked=\'checked\':checked=\'\' onclick=\'checkallofmine(this.checked)\'>",';
			 }
			 $col=0;
			 
			 if($pid > 0){
				 $prorjheaderinfo = $this->getprojectheaders($pid);
				 foreach($prorjheaderinfo as $key=>$headerinfo){
					 if($key == 0){
						 $props.='"",'; 
					 }else{
					 $props .='"';
					 if($headerinfo['headertype'] == 0 ){
			
					 $props .="<a href='#' data-toggle='modal' onclick='openModal()' id='filter'><img src='images/filter.png'  height=10 width=10></a>"."<br/><div class='inputcls'><a  onclick='sortdata()' class='colHeader columnSorting'><b>".$headerinfo['headername']."</b></a></div>";
						 
					 }else if($headerinfo['headertype'] == 1){
						 $orderid     = $headerinfo['orderid'];
					     $formulaname = "<font size='1' color='blue'>".$headerinfo['formula']."</font>";
						
						 if(!empty($formulaname)){
						
						  $props .="<a href='#' data-toggle='modal' onclick='openModal()' id='filter'><img src='images/filter.png'  height=10 width=10></a>"."<div class='outputcls'><a class='colHeader columnSorting'><b>".$headerinfo['headername']."</b></a></div>".$formulaname."<br/>";
						 }else{
					
							  $props .="<a href='#' data-toggle='modal' onclick='openModal()' id='filter'><img src='images/filter.png'  height=10 width=10></a><br/>"."<div class='outputcls'><a class='colHeader columnSorting'><b>".$headerinfo['headername']."</b></a></div>";
							
						 
						 }
					 
					 }
						   $props .='",';
						   $col++;
				 }
			 
				 }
				 }
			  echo rtrim($props,",");   
		 }
		public  function getprojheadersprops($pid,$is_comp=0,$is_chk=0)
		{  
			$props	=	'{readOnly:true},';
			$is_custduedate_valid = 0;
			 if((($_SESSION['partlinq_user']['USERTYPE'] == 2 || $_SESSION['partlinq_user']['USERTYPE'] == 3) && $is_chk ==1) || $_SESSION['partlinq_user']['USERTYPE'] == 0 || $_SESSION['partlinq_user']['USERTYPE'] == 1 || $is_chk ==1 || $_SESSION['partlinq_user']['USERTYPE'] == 6){
				$props	.=	'{readOnly:true},';
				 
				 if($_SESSION['partlinq_user']['USERTYPE'] == 1 || $_SESSION['partlinq_user']['USERTYPE'] == 0 && $is_comp == 0){
					 $is_custduedate_valid = 1;
			 	 }else{
					 $is_custduedate_valid =0;
				 }
			 }else{
				 $props	.=	'{type: "checkbox"},';
				 $is_custduedate_valid=1;
			 }
			
			if($pid > 0){
				 $prorjheaderinfo = $this->getprojectheaders($pid);
				 $formulatypesarr = $this->getFormulaTypes($_GET['batch']);
				 $formulatypes =  array_column($formulatypesarr,'itypeid');
				 foreach($prorjheaderinfo as $key=>$headerinfo){
					if($key == 0){
						 $props	.=	'{readOnly:true},';
					 }else{
					   $props .="{";
					   if($headerinfo['headertype']==0){
						   if($headerinfo['itypeid'] == 1){
							    $props .= "type: 'numeric',readOnly: true,numericFormat:{pattern: '$0,0.00',culture: 'en-US'},";
						   }else{
						    $props .="readOnly: true,";$props .="type: 'searchforinput',";
						   }
						   
						  
					   }else if($headerinfo['headertype']==1){
						   
						     if($_SESSION['partlinq_user']['USERTYPE'] == 1){
								$is_editable = $headerinfo['is_user_edit'];
								 
							 }else if($_SESSION['partlinq_user']['USERTYPE'] == 2){
								 $is_editable = $headerinfo['is_client_edit'];
							 }else if($_SESSION['partlinq_user']['USERTYPE'] == 3){
								 $is_editable = $headerinfo['is_requestor_edit'];
							 }else{
								$is_editable = 0;
							 }
							   if($is_comp == 1){
								   $is_editable = $headerinfo['is_cmplparts_edit'];
							   }
						   if($headerinfo['itypeid'] == 2){
							  $orderid =  $headerinfo['orderid'];
							   $props .="type:'envstatus',orderid:'".$orderid."',editor:false,headername:'".$headerinfo['headername']."'";
						   }
						   else if($headerinfo['formulatype'] != ""){
							   $formulatypeid = "'"."formulatype".$headerinfo['itypeid']."'";
							   $props .= "type: ".$formulatypeid.",renderer:calculatedColumnRender,";
							
						   }else if(($headerinfo['itypeid'] == 10 && $is_custduedate_valid ==1) || $headerinfo['hidden_type'] == 5 || $headerinfo['hidden_type'] == 4){
							  	 $props .= "type:'text',renderer:mandatoryRenderer,";
						  }else{
							   if($is_editable == 1){

								$props .="type:'text',";

							   }else{
								   if($headerinfo['headername'] == "File Name" || $headerinfo['itypeid'] == 6){
									   $props .="readOnly: true,";$props .="type: 'fileupload',";
								   }else if($headerinfo['hidden_type'] == "3"){
									  $props .=" type: 'searchforinput',editor: false,";
								   }else{
										$props .="readOnly: true,";$props .="type: 'searchforinput',";
								   }
							   }
						   	}
					   }
					  
					  $props .="},";
				 }
			
				 }
				 }
			echo rtrim($props,",");   
		}
		public function getcolwidth($pid,$is_comp=0,$is_chk)
		{
			$props='1,';
			 if((($_SESSION['partlinq_user']['USERTYPE'] == 2 || $_SESSION['partlinq_user']['USERTYPE'] == 3) && $is_chk ==1) || $_SESSION['partlinq_user']['USERTYPE'] == 0 || $_SESSION['partlinq_user']['USERTYPE'] == 1 || $_SESSION['partlinq_user']['USERTYPE'] == 6){
				 	$props.='1,';
			 }else{
				 	$props.='60,';
			 }
			//$props.='60,';
			if($pid > 0){
				 $prorjheaderinfo = $this->getprojectheaders($pid);
				 foreach($prorjheaderinfo as $key=>$headerinfo){
				
					 if($key == 0){
						 $props.='1,';
						 	 
					 }else{
					    $props .='150,';
					 }
				 }
			}
			echo rtrim($props,",");     
	    }
		
		public function getcomppartsdata($pid,$batch,$userid,$usertype,$cid,$statusid=""){
	
			if($statusid > 0){
			$sorder = $_GET['sorder']-1;
				$returndata['statusorderid'] = $_GET['sorder'];
			  	 $qry= "SELECT statusval FROM  env_status WHERE customer_id=".$cid." AND isactive = 1 AND id=".$_POST['statusid'];
				$envstatus = $this->executeQuery($qry,"single");
				$statusname = $envstatus[0];
				}
			$res= "";$query ="";
			if($usertype == 0 ){
				$query1 = "SELECT `env_urlgrab`.*,(SELECT u.`fullname` FROM `env_user` as u  WHERE u.`id` = env_urlgrab.`changed_by`) as custname FROM `env_urlgrab` WHERE `iPrjID`= ".$pid." AND `iBatch`= ".$batch;
			} else if( $usertype == 2 ){
				$query1 = "SELECT `env_urlgrab`.*,(SELECT u.`fullname` FROM `env_user` as u  WHERE u.`id` = env_urlgrab.`changed_by`) as custname FROM `env_urlgrab` WHERE `iPrjID`= ".$pid." AND `iBatch`= ".$batch." AND partstatus='1'";
			}else if($usertype == 1 || $usertype == 3 ){
				$query1 = "SELECT `env_urlgrab`.*,(SELECT u.`fullname` FROM `env_user` as u  WHERE u.`id` = env_urlgrab.`changed_by`) as custname FROM `env_urlgrab` WHERE `iPrjID`= ".$pid." AND `iBatch`= ".$batch." AND partstatus='1'";
			}else{
				$query1 = "SELECT * FROM `env_urlgrab` WHERE `iPrjID`= ".$pid." AND `iBatch`= ".$batch;
			}
			if($usertype == 1){
				$query .= $query1." AND `userid`=".$userid;
			}
		    else if($usertype == 2){
				$query .= $query1." AND `iPrjID` IN(SELECT ID FROM env_project WHERE customer_id= ".$_SESSION['partlinq_user']['CUSTOMERID'].")";
			}
			else if($usertype == 3){
				$query .= $query1." AND `created_by`=".$userid;
			}else{
				$query .= $query1;
			}
			
			$rescnt = count($this->executeQuery($query,"multiple"));
			if($statusid > 0){
			
				$query .= " AND sFld".$sorder."='".trim($statusname)."'";
			}
		
			$query .= " ORDER BY ID ASC ";
			$res = $this->executeQuery($query,"multiple");
			$returndata['data'] = $res;
			$returndata['totalcnt'] = $rescnt;
			
			
			return $returndata;
	
		}	
		
		public function getweekstatusdetails($pid,$batch,$statusqry,$cid,$statusfield,$rptdate){
			$query ="";
			$reqfieldindex = $this->getreqfieldindex($batch);
			if($pid >0 && $batch >0){
				$condn ="";
				$usercondn ="";
				$custfieldindex = $this->getcustfieldindex($batch);
				if($_SESSION['partlinq_user']['USERTYPE']=='3' || $_SESSION['partlinq_user']['USERTYPE']=='6'){
					$usercondn = " AND ra.user_id =".$_SESSION['partlinq_user']['ID'];
				}else{
					$usercondn = " AND ra.user_id =sFld".$reqfieldindex;
				}
				if($_SESSION['partlinq_user']['USERTYPE']=='1'){
					$condn .= " AND t1.userid ='".$_SESSION['partlinq_user']['ID']."'";
				}
				if($_SESSION['partlinq_user']['USERTYPE']=='6'){
					$custpartnumfieldindex = $this->getcustpartnumfieldindex($batch);
					$supplcodefieldindex = $this->getsuppliercodefieldindex($batch);
					$modelnumfieldindex = $this->getmodelnumfieldindex($batch);
					
				$parttypecond = " AND u1.iPrjID = '".$pid."' AND u1.iBatch = '".$batch."' AND u1.sFld".$reqfieldindex."=t1.sFld".$reqfieldindex." AND u1.sFld".$custfieldindex."=t1.sFld".$custfieldindex." AND u1.sFld".$modelnumfieldindex."=t1.sFld".$modelnumfieldindex." AND u1.sFld".$custpartnumfieldindex."=t1.sFld".$custpartnumfieldindex." AND u1.sFld".$supplcodefieldindex."=t1.sFld".$supplcodefieldindex." AND u1.sFld4=t1.sFld4 AND u1.part_type=2";
					$subtbl =" ,env_urlgrab u1 ";
				}else{
					$parttypecond = "";
				}
				
				
					$condn .= " AND t1.sFld".$custfieldindex." IN(SELECT ra.customer_id FROM env_requestor_alloc ra WHERE ra.customer_id= t1.sFld".$custfieldindex.$usercondn." ) ";
					
			$query .= "SELECT m . * , IFNULL((SELECT COUNT( t1.ID ) AS cnt FROM `env_report` t1 ".$subtbl." WHERE t1.`iPrjID` = '".$pid."' AND t1.`iBatch` = '".$batch."' AND t1.sFld".$statusfield." = m.env_status AND  t1.reportdate='".$rptdate."'".$parttypecond.$condn." GROUP BY t1.sFld".$statusfield." ),0) AS cnt FROM ( SELECT s.colorcode,s.`statusval` AS env_status,s.id as envstatusid,s.sorder FROM `env_status` s WHERE s.`isactive` = '1' AND s.`customer_id` = '".$cid."'".$statusqry.") m ORDER BY m.sorder ASC";
			//echo $query;exit;
				return $query ;
			}
		}
		public function getstatus($batch,$type,$is_arr =0,$status_label=""){
			$status ="";
			$res1 ="";
			if($type == 1){
			$fieldname ="pending_statusids";
			}else if($type==2){
				$fieldname ="completed_statusids";
			}else{
				$type ="";
			}
			if($status_label == 1){
				$fields = "s.statusval";
			}else{
				$fields = " s.id,s.statusval,s.customer_status";
			}
				$res1 = $this->executeQuery("SELECT ".$fields." FROM `env_status` s 
				WHERE s.isactive=1 AND s.customer_id=(SELECT (SELECT customer_id FROM env_project WHERE id=project_id) 
				FROM `env_batch` WHERE id=".$batch." AND closed=0) AND 
				FIND_IN_SET(id,(SELECT ".$fieldname." FROM env_batch WHERE id=".$batch." AND closed=0))","multiple");
			if($is_arr ==1){
				return $res1;
			}else{
				if($res1 != ""){
					for($i=0;$i<count($res1);$i++){
						 $status .= "'".$res1[$i]['statusval']."',";
					 }
					$status = rtrim($status,",");
				}
				return $status  ;
			}
			
		}
		public function getstatusfields($pid,$batch){
			$res1= "";
			$res1 = $this->executeQuery("SELECT `orderid` FROM `env_projectheaders` WHERE `itypeid`=2 AND  `batchid`=".$batch,"multiple");
			return $res1;
		}
		public function getautocompletequery($batch,$field){
			$query ="";
			if($batch >0){
				$fieldsarr = $this->getfieldsorder($batch,1);
				if(count($fieldsarr) >0){
					for($j=0;$j<count($fieldsarr);$j++){
						if($fieldsarr[$j]['is_auto_type'] == 1){
							$condn = $field.".sFld".$fieldsarr[$j]['oid'];
							
						  $query .=", (SELECT CONCAT(`cust_name`,' ( ',`cust_code`,' )')  FROM `env_customer_details` WHERE id =".$condn." AND `isactive`=1)as sFld".$fieldsarr[$j]['oid'];
						}else if($fieldsarr[$j]['is_auto_type'] == 3){
							$query .=" ,(SELECT `modelnum` FROM `env_model_details` WHERE id =".$field.".sFld".$fieldsarr[$j]['oid']." AND `isactive`=1) as sFld".$fieldsarr[$j]['oid'];
						}else if($fieldsarr[$j]['is_auto_type'] == 2){
							$query .=", (SELECT `code` FROM `env_supplier` WHERE id =".$field.".sFld".$fieldsarr[$j]['oid']." AND `isactive`=1)as  sFld".$fieldsarr[$j]['oid'];
						}else if($fieldsarr[$j]['is_auto_type'] == 4){
							$query .=", (SELECT `username` FROM `env_user` WHERE id =".$field.".sFld".$fieldsarr[$j]['oid']." AND `isactive`=1)as  sFld".$fieldsarr[$j]['oid'];
						}
						
					}
				}
			}
			return $query;
		}
		public function getreqfieldindex($batch){
			$reqfieldindex = $this->executeQuery("SELECT (orderid -1)as oid FROM `env_projectheaders` WHERE `is_auto_type`=4 AND batchid=".$batch,"single")[0];
			return $reqfieldindex;
		}
		public function getcustfieldindex($batch){
			$reqfieldindex = $this->executeQuery("SELECT (orderid -1)as oid FROM `env_projectheaders` WHERE `is_auto_type`=1 AND batchid=".$batch,"single")[0];
			return $reqfieldindex;
		}
		public function getmodelnumfieldindex($batch){
			$modelnumfieldindex = $this->executeQuery("SELECT (orderid -1)as oid FROM `env_projectheaders` WHERE `is_auto_type`=3 AND batchid=".$batch,"single")[0];
			return $modelnumfieldindex;
		}
		public function getsuppliercodefieldindex($batch){
			$reqfieldindex = $this->executeQuery("SELECT (orderid -1)as oid FROM `env_projectheaders` WHERE `is_auto_type`=2 AND batchid=".$batch,"single")[0];
			return $reqfieldindex;
		}
		public function getcustpartnumfieldindex($batch){
			$reqfieldindex = $this->executeQuery("SELECT (orderid -1)as oid FROM `env_projectheaders` WHERE `itypeid`=3 AND batchid=".$batch,"single")[0];
			return $reqfieldindex;
		}
		public function getallreqlist($pid,$batch,$cid,$statusid="",$rpttype="",$selcid=""){
			$condn ="";
			$res ="";
			$returndata['data'] = "";
			$returndata['totalcnt'] = 0;						   
			$reqfieldindex = $this->getreqfieldindex($batch);
			$custfieldindex= $this->getcustfieldindex($batch);
			$requesttypecondn ="";
			if($_POST['rt'] !=""){
				$requesttypecondn .= " AND env_urlgrab.request_type=".$_POST['rt'];
			}
			if($_POST['pt'] !=""){
				$requesttypecondn .= " AND env_urlgrab.part_type=".$_POST['pt'];
			}
			$fieldalias ="n";
			//$condn  .= " AND env_urlgrab.sFld".$custfieldindex." IN (SELECT customer_id FROM  env_requestor_alloc ra WHERE env_urlgrab.sFld".$custfieldindex." =ra.customer_id AND ra.user_id=env_urlgrab.sFld".$reqfieldindex.") ";
			if($statusid >0){
				
				$statusfield = $this->executeQuery("SELECT (`orderid`-1)as oid,COUNT(id)as cnt 
				FROM `env_projectheaders` 
				WHERE `batchid`=".$batch." AND `itypeid`=2","single")[0];
				if($statusfield != ""){
					$condn .= " AND env_urlgrab.`sFld".$statusfield."`=(SELECT statusval FROM env_status WHERE id=".$statusid." 
					AND isactive=1 AND customer_id=".$cid.")";
				}
				if($selcid >0){
					$customerfield = $this->executeQuery("SELECT (`orderid`-1)as oid,COUNT(id)as cnt 
					FROM `env_projectheaders` 
					WHERE `batchid`=".$batch." AND `is_auto_type`=1","single")[0];
						$condn .= " AND env_urlgrab.`sFld".$customerfield."`= ".$selcid;
				}
			
			}
			if($_SESSION['partlinq_user']['USERTYPE']== 6){
					$condn.=" AND env_urlgrab.part_type=2 ";
		    }
			if($_POST['pt'] >0){
				$partcondn = " AND env_urlgrab.part_type=".$_POST['pt'];
			}
			
			$selfield .= $this->getautocompletequery($batch,$fieldalias);
					$query1 ="SELECT (SELECT `name` FROM `env_parttype` WHERE id=".$fieldalias.".part_type AND `is_active`=1)as parttype, ".$fieldalias.".* ".$selfield." ,(SELECT fullname FROM env_user WHERE id=".$fieldalias.".created_by)as creatornname,(SELECT fullname FROM env_user WHERE id=".$fieldalias.".changed_by)as updatedname ";
			$query = $query1." FROM ( SELECT env_urlgrab.* FROM `env_urlgrab`,env_requestor_alloc ra WHERE
		 env_urlgrab.is_delete=0  AND  env_urlgrab.`iPrjID`=".$pid." AND env_urlgrab.sFld".$custfieldindex." =ra.customer_id AND ra.user_id=env_urlgrab.sFld".$reqfieldindex." AND env_urlgrab.`iBatch`=".$batch.$condn.$partcondn.$requesttypecondn."  ORDER BY  env_urlgrab.ID DESC ) n";
					$res= $this->executeQuery($query,"multiple");
				
			$returndata['data'] = $res;
			$returndata['totalcnt'] = count($res);						   
				return $returndata;
		}
		public function getpartsdata($pid,$batch,$userid,$usertype,$cid,$statusid="",$rpttype="",$selcid=""){
			
			$res= "";
			$query ="";
			$condn = "";
			$selfield="";
			$rescnt=0;
			$custcond="";
			$groupby ="";
			$partcondn ="";
			$fieldalias ="n";
			if($_POST['pt'] >0){
				
				$partcondn = " AND env_urlgrab.part_type=".$_POST['pt'];
			}else if($usertype == 6){
				$partcondn = " AND env_urlgrab.part_type=2";
			}
			if($pid >0 && $batch >0){
				$reqfieldindex = $this->getreqfieldindex($batch);
				if($statusid > 0){
					$custfield =$this->getfieldsorder($batch,1,1)[0]['oid'];
					$sorder = $_POST['sorder']-1;
					if($selcid >0){
						$custcond = " AND env_urlgrab.sFld".$custfield."=".$selcid;
					}
					$returndata['statusorderid'] = $_POST['sorder'];
					 $qry= "SELECT statusval FROM  env_status WHERE customer_id=".$cid." AND isactive = 1 AND id=".$_POST['statusid'];
					$envstatus = $this->executeQuery($qry,"single");
					$statusname = $envstatus[0];
				}
				$reqtypecondn ="";
				if($_POST['rt'] >0){
					$reqtypecondn = " AND env_urlgrab.request_type =".$_POST['rt'];
				}
				$selfield .= $this->getautocompletequery($batch,$fieldalias,$selcid);
				$createdfield = "
					(SELECT u.`fullname` FROM `env_user` as u  WHERE u.`id` = ".$fieldalias.".`created_by`) as creatorname ,";
					$custfieldindex= $this->getcustfieldindex($batch);
					$query2 = "SELECT (SELECT `name` FROM `env_parttype` WHERE id=".$fieldalias.".part_type AND `is_active`=1)as parttype, `".$fieldalias."`.*,".$createdfield."(SELECT u.`fullname` FROM `env_user` as u  
								WHERE u.`id` = ".$fieldalias.".`changed_by`) as custname ".$selfield;
				$query1 = $query2." FROM ( SELECT `env_urlgrab`.* 
								FROM `env_urlgrab`,env_requestor_alloc as ra WHERE env_urlgrab.`iPrjID`= ".$pid." 
								AND env_urlgrab.`iBatch`= ".$batch." AND env_urlgrab.is_delete=0 ".$reqtypecondn." AND env_urlgrab.sFld".$custfieldindex." = ra.customer_id 
								".$custcond.$partcondn." ";

				if($usertype == 3 || $usertype == 6){
					$query1 .= " AND ra.user_id = ".$_SESSION['partlinq_user']['ID'];
				}else{
					$query1 .=" AND ra.user_id = env_urlgrab.sFld".$reqfieldindex;
				}
			
				if($usertype == 1){
					$query .= $query1." AND `userid`=".$userid;
				}
				
				if($usertype == 2 || $usertype == 3 || $usertype == 0 || $usertype == 6){
					$query .= $query1;
				}
						
				if($rpttype >0 && ($usertype == 1 || $usertype == 6  || $usertype == 2 || $usertype ==0)){
					$statusfieldres = $this->getstatusfields($pid,$batch);
					if(count($statusfieldres) == 1){
						if($rpttype ==1){
							$statusfield = "b.pending_statusids";
					
						}else if($rpttype ==2){
							$statusfield = "b.completed_statusids";
						}
						$statusres = $this->executeQuery("SELECT s.statusval FROM env_status s WHERE FIND_IN_SET(s.id,(SELECT ".$statusfield." FROM env_batch b WHERE id='1' AND b.closed =0))","multiple");
						$statusqry ="";
						if(count($statusres) >0){
							$statusqry .=" AND (";
							for($s=0;$s<count($statusres);$s++){
								$statusqry .=" sFld".($statusfieldres[0]['orderid']-1)." ='".$statusres[$s]['statusval']."' OR ";
							}
							$statusqry = substr($statusqry,0,-3);
							$statusqry .=" ) ";
						}
						$query .=$statusqry.") n";
					}
				}else{
				
				$query .= ") n";
				}
						//echo $query;exit;
				$rescnt = count($this->executeQuery($query,"multiple"));
				
				if($statusid > 0){
					$query .= " WHERE  ".$fieldalias.".sFld".$sorder."='".trim($statusname)."'";
				}
				$query .= " GROUP BY  ".$fieldalias.".ID ORDER BY ".$fieldalias.".ID DESC ";

				$res = $this->executeQuery($query,"multiple");
			}
	
			$returndata['data'] = $res;
			$returndata['totalcnt'] = $rescnt;
			
			
			return $returndata;
	
		}
		public function escapeJsonString($value) {
		 
			$escapers =     array("\\",     "/",   "\"",  "\n",  "\r",  "\t", "\x08", "\x0c");
			$replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t",  "\\f",  "\\b");
			$result = str_replace($escapers, $replacements, $value);
				return $result;
		}
		public function updatedata($pid,$batchid,$userid,$partid,$buildqry,$cust_comment,$custcmtorder){
			$updateqry =" UPDATE env_urlgrab SET ";
			if($_SESSION['partlinq_user']['USERTYPE'] == 2 ){
				
				  if(!empty($cust_comment)){
					  $cmtres = $this->getcomment($partid,$pid,$batch,$cust_comment,$custcmtorder);
					  if($cmtres['comment_status']){
						  $updateqry .= " changed_by=".$userid.",";
						 $this->insertcusthistrorydata($pid,$batchid,$partid,$cmtres['changed_by'],$cmtres['comment']);
						 
					  }
				   }
					$updateqry .= rtrim($buildqry,",")." WHERE ID=".$partid;  
				
			}else{
				$updateqry .= rtrim($buildqry,",")." WHERE ID=".$partid;  
			}
			
     		$res = $this->executeNonQuery($updateqry);
			return $res;
		}
		public function getcomment($partid,$pid,$batch,$cmt,$custcmtorder){
			 $qry = "SELECT sFld".$custcmtorder." , changed_by  FROM `env_urlgrab` WHERE ID =".$partid;
			 $res = $this->executeQuery($qry,'single');
			 $cmtres = [];
			 if(count($res) > 0){
				if($res[0] != $cmt){
					 $cmtres['comment'] = $res[0];
					 $cmtres['changed_by'] = $res['changed_by'] ;
					 $cmtres['comment_status'] = true;
				
				 }else{
					 $cmtres['comment'] = "";
					 $cmtres['changed_by'] = "" ;
					 $cmtres['comment_status'] = false;
				 }
			 }
			return $cmtres;
		}
		public function insertdata($pid,$batch,$userid,$buildqry,$cust_comment,$custcmtorder){
			  $inssql=" INSERT INTO env_urlgrab SET iPrjID=".$_GET['pid'];
			  if($_SESSION['partlinq_user']['USERTYPE'] == 2){
				     if(!empty($cust_comment)){
							 $inssql .= ",changed_by=".$userid;
						 	 $inssql .= ",iBatch=".$batch."',userid=".$userid.",".rtrim($buildqry,",");
				   			 $res = $this->executeNonQuery($inssql);
				     }
			   }else{
				   $inssql .= ",iBatch=".$batch."',userid=".$userid.",".rtrim($buildqry,",");
				   $res = $this->executeNonQuery($inssql);
			   }
			 
			 
			return $res;
		}
		public function checkcusthistrory($pid,$batch,$partid,$userid,$cust_comment){
			$selres = "";
			if($pid > 0 && $batch > 0 && $partid >0 && $userid > 0){
			$selqry = " SELECT COUNT(id) as cnt FROM env_custcmts_histrory WHERE proj_id=".$pid." AND batch_id =".$batch.
				  		" AND part_id =".$partid." AND customer_id =".$userid." AND cust_comment = "."'".trim($cust_comment)."'";
			    $selres = $this->executeQuery($selqry,"single");
			
			}
			return $selres;
			
		}
		public function insertcusthistrorydata($pid,$batch,$partid,$userid,$cust_comment){
		   $inssql=" INSERT INTO env_custcmts_histrory SET proj_id=".$pid.",batch_id=".$batch.",part_id=".$partid.",customer_id=".$userid.
				 		  ",changed_date="."'".date('Y-m-d h:i:s')."'".",cust_comment="."'".$cust_comment."'";
			  $res = $this->executeNonQuery($inssql);
			
			return $res;
		}
		
		public function getStatusDetails($customerid,$batch=0){
			$res ="";
			if($customerid > 0){
				$query = "SELECT `id`,`statusval` FROM `env_status` WHERE `customer_id`=".$customerid." AND `isactive` =1";
				
				$res = $this->executeQuery($query,"multiple");
			}
			return $res;
		}
		public function getCustByProj($pid){
			$res ="";
			$query = "SELECT `customer_id` FROM `env_project` WHERE `ID` =".$pid." AND isactive=1 AND isclosed=0";
			$res = $this->executeQuery($query,"single");
			return $res;
		}
			public function getFormulaTypes($batchid){
			$res ="";
			 $query = "SELECT `orderid`,`itypeid`,`formulatype`,`formula` FROM `env_projectheaders` WHERE `formulatype`!='' AND batchid=".$batchid." ORDER BY orderid ASC";
			$res = $this->executeQuery($query,"multiple");
			return $res;
		}
		public function getminorderid($pid,$userid,$condn,$type="min"){
			if($type == "min"){
				$q1 = " ORDER BY orderid ASC LIMIT 1";
			}else if($type == "max"){
				$q1 = " ORDER BY orderid DESC LIMIT 1";
			}
			if($cond == "input"){
				$htype= "0";
			}else if($cond == "output"){
				$htype= "1";
			}
			$query = "SELECT orderid  FROM `env_projectheaders` WHERE `headertype` = ".$htype." AND batchid=".$pid;
			$res = $this->executeQuery($query,"single");
			return $res;
		
		}
		public function getheaderbytype($batchid,$itypeid){
			$res ="";
			if($batchid > 0 && $itypeid >0 ){
				$query = "SELECT `orderid` FROM `env_projectheaders` WHERE batchid = ".$batchid." AND `itypeid` = ".$itypeid;
				$res = $this->executeQuery($query,"single");
			}
			return $res;
			
		}
		public function getunrptheaderbytype($batchid,$itypeid){
			$res ="";
			if($batchid > 0 && $itypeid >0 ){
				$query = "SELECT `orderid` FROM `env_projectheaders` WHERE batchid = ".$batchid." AND `itypeid` = ".$itypeid;
				$res = $this->executeQuery($query,"single");
			}
			return $res;
			
		}
		public function getBatchId($pid,$batch){
			$res ="";
			if($pid > 0 && $batch >0 ){
				 $query = "SELECT `id` FROM `env_batch` WHERE project_id = ".$pid." AND `batchno` = ".$batch;
				$res = $this->executeQuery($query,"single");
			}
			return $res;
		}
			public function chmodr($path, $filemode) {
				if (!is_dir($path))
					echo $path;
					return chmod($path, $filemode);

				$dh = opendir($path);
				while (($file = readdir($dh)) !== false) {
					if($file != '.' && $file != '..') {
						$fullpath = $path.'/'.$file;
						if(is_link($fullpath))
							return FALSE;
						elseif(!is_dir($fullpath) && !chmod($fullpath, $filemode))
								return FALSE;
						elseif(!$this->chmodr($fullpath, $filemode))
							return FALSE;
					}
				}

				closedir($dh);

				if(chmod($path, $filemode))
					return TRUE;
				else
					return FALSE;
		}
	public function getfieldarr($is_field =0){
		if($is_field == 1){
				$fieldsarr["customer"] = 'sFld4';
				$fieldsarr["supplier"] = 'sFld5';
				$fieldsarr["modelnum"] = 'sFld6';
		}else{
			$fieldsarr["customer"] = '4';
			$fieldsarr["supplier"] = '5';
			$fieldsarr["modelnum"] = '6';
		}
		return $fieldsarr;
	}
	public function getcustomerdetails($pid,$batchid,$requestor_id=0){
		$res  ="";
		if($pid >0 && $batchid >0){
			$fieldindex = $this->getfieldsorder($batchid,1,1)[0]['oid'];
			$condn ="";
			if($_SESSION['partlinq_user']['USERTYPE']=='3'){
				//$reqfieldindex = $this->getreqfieldindex($batchid);
					//$condn .=" AND u. sFld".$reqfieldindex."=ra.user_id";

			}else if($_SESSION['partlinq_user']['USERTYPE']=='1'){
				$condn = " AND u.userid ='".$_SESSION['partlinq_user']['ID']."'";
			}
				$query ="SELECT u.`sFld".$fieldindex."`as cid,
				cd.id as custid,
					CONCAT(cd.`cust_name`,' ( ',cd.`cust_code`,' ) ' ) as cname,cd.cust_short_name ";
					$query .= " FROM `env_urlgrab` as u,`env_customer_details` as cd WHERE u.`iPrjID`=".$pid."  
					AND u.is_delete = 0 AND u.`iBatch`=".$batchid.$condn." AND  u.`sFld".$fieldindex."`=cd.id  AND cd.id IN(SELECT customer_id FROM env_requestor_alloc ra WHERE ra.customer_id = cd.id ";
			
				if($_SESSION['partlinq_user']['USERTYPE']=='3'){
					$query .="  AND ra.user_id=".$_SESSION['partlinq_user']['ID'];
				}
				if($_SESSION['partlinq_user']['USERTYPE']=='2' && $requestor_id >0){
					$query .="  AND ra.user_id=".$requestor_id;
				}
				$query .=") AND cd.isactive=1 AND u.is_delete=0  GROUP BY cname ORDER BY cname ASC ";

			$res =$this->executeQuery($query,"multiple");
		}
		return $res;
	}
		public function getstatusgraphdetails($pid,$batch,$selid,$statusqry,$cid,$statusfield){
			$query ="";
			if($pid >0 && $batch >0){
				$condn ="";
				$reqfieldindex = $this->getreqfieldindex($batch);
				$fieldindex = $this->getfieldsorder($batch,1,1)[0]['oid'];
				if($_SESSION['partlinq_user']['USERTYPE']=='3'){
					
					//$condn .=" AND t1. sFld".$reqfieldindex."=".$_SESSION['partlinq_user']['ID'];



				}else if($_SESSION['partlinq_user']['USERTYPE']=='1'){
					$condn = " AND t1.userid ='".$_SESSION['partlinq_user']['ID']."'";
				}
				if($selid >0){
					$fieldarrr = $this->getfieldarr();
					 $condn .= " AND sFld".$fieldindex."='".$selid."'";
				}
				$custfieldindex= $this->getcustfieldindex($batch);
				
				if($_SESSION['partlinq_user']['USERTYPE']=='3' || $_SESSION['partlinq_user']['USERTYPE']=='6'){
					
					$condn1  = " AND t1.sFld".$custfieldindex." IN (SELECT customer_id FROM  env_requestor_alloc ra WHERE t1.sFld".$custfieldindex."= ra.customer_id
					AND ra.user_id =".$_SESSION['partlinq_user']['ID'].")";
					
		
				}else{
					$condn1  = " AND t1.sFld".$custfieldindex." IN (SELECT customer_id FROM  env_requestor_alloc ra WHERE t1.sFld".$custfieldindex." =ra.customer_id AND ra.user_id=t1.sFld".$reqfieldindex.") ";
				}
				if($_SESSION['partlinq_user']['USERTYPE']=='6'){
					$condn1  .= " AND t1.part_type=2";
				}
			$query .=" SELECT m . * , IFNULL((SELECT COUNT( t1.ID ) AS cnt FROM `env_urlgrab` t1 
				  WHERE t1.`iPrjID` = '".$pid."' AND t1.`iBatch` = '".$batch."'  AND t1.is_delete = 0 ".$condn1."
				  AND t1.`sFld".$statusfield."` = m.env_status   ".$condn." GROUP BY t1.`sFld".$statusfield."` ),0) AS cnt
				  FROM ( SELECT s.colorcode,s.`statusval` AS env_status,s.id as envstatusid,s.sorder FROM `env_status` s 
				  WHERE s.`isactive` = '1'  ";
		
				$query .=$statusqry;
				$query .=" AND s.`customer_id` = '".$cid."') m ORDER BY m.sorder ASC";

				return $query ;
			}
		}
		public function saveunreportedparts1($pid,$batch,$data){
			$sttaus = false;
			$fields ="";
			if($pid >0 && $batch>0){
		/*$res =$this->executeQuery("SELECT COUNT(id)as cnt ,id FROM `env_unrpt_parts` WHERE `project_id`='".$pid."' AND `batchid`=".$batch." AND `sFld0`='".$data['sFld0']."' AND `sFld1`='".$data['sFld1']."' AND `sFld2`= '".$data['sFld2']."' AND is_delete=0","single");
				 $cnt =$res['cnt'];*/
			$cnt = 0;
				for($k=0;$k<9;$k++){
					$fields .="sFld".$k."='".$data['sFld'.($k)]."',";
				}
				$fields = substr($fields,0,-1);
				if($cnt == 0 && $fields !=""){
					$sttaus = $this->executeNonQuery("INSERT  env_unrpt_parts SET project_id='".$pid."', batchid='".$batch."',".$fields.",created_by='".$_SESSION['partlinq_user']['ID']."',created_date='".date("Y-m-d")."'");
				}else if($cnt == 1 && $fields !=""){
					$sttaus = $this->executeNonQuery("UPDATE  env_unrpt_parts SET ".$fields.",updated_by='".$_SESSION['partlinq_user']['ID']."'WHERE id='".$res['id']."'");
					
				}
			}
			return $sttaus;
		}
		
		public function getunrptparts($project_id,$batch,$is_cnt=0){
			$result ="";
			if($project_id >0 && $batch >0){
				if($is_cnt ==1){
					$fields =" COUNT(id) as cnt";
				}else{
					$fields ="*";
				}
				 $query ="SELECT ".$fields." FROM `env_unrpt_parts` WHERE `is_delete`=0 AND `project_id`='".$project_id."' AND `batchid`='".$batch."' AND is_delete=0 ";
				if($_SESSION['partlinq_user']['USERTYPE'] ==1){
					//$query .=" AND created_by='".$_SESSION['partlinq_user']['ID']."'";
				}
				$result = $this->executeQuery($query,"multiple");
			}
			return $result;
		}
		
		public function getfieldsorder($batch,$is_auto=0,$auto_type =0){
			$res="";
			if($batch >0){
				$query = "SELECT id,(`orderid`-1) oid,`req_form_order`,`req_field_label`,`is_auto_type`,is_auto,fieldtype,itypeid,hidden_type
				FROM `env_projectheaders` WHERE batchid=".$batch."  AND `req_form_order` >0 ";
				if($is_auto == 1){
					$query .= " AND is_auto =1 ";
				}
				if($auto_type >0){
					$query .= " AND is_auto_type = ".$auto_type;
				}
				$query .= " ORDER BY `req_form_order` ASC";
			$res = $this->executeQuery($query,"multiple");
			}
			
			return $res;
		}
		public function checkcompletedpartstatus($selid ,$batch){
			$res ="";
		  if($batch >0){
		  $res = $this->executeQuery("SELECT COUNT(id)as cnt FROM `env_batch` WHERE  FIND_IN_SET('".$selid."',
		  (SELECT `completed_statusids` FROM env_batch WHERE id=".$batch." AND closed =0))","single");
		  }
			return $res;
		}
			public function getfieldindex($batch,$type){
				$res  ="";
				if($batch >0){
					$res =$this->executeQuery("SELECT (`orderid`-1)as oid FROM `env_projectheaders` WHERE `batchid`=".$batch." AND `itypeid`=".$type,"single");
				}
				return $res;
			}
		public function getautocmplqry($autotype,$fieldindex,$field){
			$query="";
						if($autotype == 1){
							$condn = $field.".sFld".$fieldindex;
							
						  $query =" (SELECT CONCAT(`cust_name`,' ( ',`cust_code`,' )')  FROM `env_customer_details` WHERE id =".$condn." AND `isactive`=1)as sFld".$fieldindex;
						}else if($autotype== 3){
							$query =" (SELECT `modelnum` FROM `env_model_details` WHERE id =".$field.".sFld".$fieldindex." AND `isactive`=1) as sFld".$fieldindex;
						}else if($autotype == 2){
							$query =" (SELECT `code` FROM `env_supplier` WHERE id =".$field.".sFld".$fieldindex." AND `isactive`=1)as  sFld".$fieldindex;
							
						}else if($autotype == 4){
							$query =" (SELECT `username` FROM `env_user` WHERE id =".$field.".sFld".$fieldindex." AND `isactive`=1)as  sFld".$fieldindex;
							
						}
			return $query;
		}
			public function getrequestdetails($batch,$pid,$editid){
				$res="";
				if($editid >0 && $batch >0){
					$fields ="";
					//$fields .= $this->getautocompletequery($batch,"env_urlgrab");
								
					$fileldarr = $this->getfieldsorder($batch);
					if(count($fileldarr) >0){
						for($j=0;$j<count($fileldarr);$j++){
							if($fileldarr[$j]['is_auto'] == 1){
								$fields .=$this->getautocmplqry($fileldarr[$j]['is_auto_type'],$fileldarr[$j]['oid'],"env_urlgrab").",";
								$fields .="sFld".$fileldarr[$j]['oid']." as sFldid".$fileldarr[$j]['oid'].",";
							}else{
								$fields .="sFld".$fileldarr[$j]['oid'].",";
							}
						}
					}
						$fields = substr($fields,0,-1);
					$query ="SELECT iPrjID, ".$fields.",part_type,request_type FROM env_urlgrab WHERE id='".$editid."' AND `iPrjID`='".$pid."' AND `iBatch`='".$batch."'";
//echo $query;exit;
					$res= $this->executeQuery($query,"single");
				}
				return $res;
			}
		
		public function getcustdetailsbyrequestor($requestor_id){
			$result ="";
			if($requestor_id >0){
				$query =" SELECT n.* FROM (SELECT ra.customer_id as custid ,(SELECT CONCAT(cd.`cust_name`,' ( ',cd.`cust_code`,' ) ') FROM env_customer_details as cd WHERE cd.id=ra.`customer_id` AND cd.isactive=1 )as cname FROM `env_requestor_alloc` ra ";
				if($requestor_id != $_SESSION['partlinq_user']['ID'] && $_SESSION['partlinq_user']['USERTYPE'] == 3){
				$query .=" WHERE ra.`user_id` =".$_SESSION['partlinq_user']['ID']." AND ra.`customer_id` IN(SELECT ra2.`customer_id` FROM `env_requestor_alloc` ra2 WHERE ra2.`user_id` =".$requestor_id.")";
				}else{
					$query .=" WHERE ra.`user_id` =".$requestor_id;
				}
				$query .= 	" )n WHERE n.cname !='' ORDER BY n.cname ASC";
		$result =  $this->executeQuery($query,"multiple");
			}
			return $result;
		}
		
		public function getheaderinfo($batchid){
			$header ="";
			$header = $this->executeQuery("SELECT `headername`,orderid,itypeid ,is_auto_type FROM `env_projectheaders`
			WHERE `batchid`=".$batchid."  AND itypeid <>  4 ORDER BY orderid ASC","multiple");
			return  $header;
		}
	public function getrptrequestdetails($pid,$batchid,$customer_id=0,$is_rpt=0,$rptdate=""){
		if($is_rpt == 0){
			$tablename="env_urlgrab";
			$reqcondn ="";
			if($_POST['request_type'] >0){
				$reqcondn =  " AND u1.request_type=".$_POST['request_type'];
			}
			$deletecondn = " AND u1.is_delete=0 ".$reqcondn;
		}else{
			$tablename="env_report";
			$deletecondn =" AND u1.reportdate='".$rptdate."'";
		}
		$condn ="";
		$query1 ="";
		$custfield ="";
		$reqfield ="";
		$result ="";
		$condn = "";
		$autocmpldata = $this->executeQuery("SELECT (`orderid`-1) oid,`is_auto_type` FROM `env_projectheaders` WHERE `batchid`=".$batchid." 
		AND `is_auto_type`>0 ORDER BY orderid ASC");
		if(count($autocmpldata) >0){
			for($j=0;$j<count($autocmpldata);$j++){
				if($autocmpldata[$j]['is_auto_type'] == 1){
					$custfield = $autocmpldata[$j]['oid'];
				}else if($autocmpldata[$j]['is_auto_type'] == 4){
					$reqfield = $autocmpldata[$j]['oid'];
				}
				$query1 .=$this->getautocmplqry($autocmpldata[$j]['is_auto_type'],$autocmpldata[$j]['oid'],'n').",";
			}
		}
		$query1 = substr($query1,0,-1);
		if($_SESSION['partlinq_user']['USERTYPE'] == 3 || $_SESSION['partlinq_user']['USERTYPE'] == 6){
			 $condn = " AND ra.user_id=".$_SESSION['partlinq_user']['ID'];
		}else{
			 $condn = " AND u1.sFld".$reqfield."= ra.user_id";
		}
		if($_SESSION['partlinq_user']['USERTYPE'] == 6){
			$condn.= " AND u1.part_type=2";
		}
		if($customer_id >0){
			$condn .= " AND ra.customer_id = ".$customer_id;
		}
			$query =" SELECT n.*,(SELECT `req_name` FROM `env_requesttype` WHERE `id`= request_type AND `is_active`=1)as request_type,(SELECT fullname FROM env_user WHERE id=created_by)as created_by ,(SELECT `name` FROM `env_parttype` WHERE id=part_type AND `is_active`=1)as parttype, ".$query1."
FROM (SELECT u1.* FROM `".$tablename."` as u1,`env_requestor_alloc` ra WHERE u1.`iPrjID` =".$pid." AND u1.`iBatch`=".$batchid.$deletecondn."   AND u1.sFld".$custfield."= ra.customer_id ".$condn." )n WHERE 1=1 ORDER BY n.ID DESC";
		
				$result= $this->executeQuery($query,"multiple");
		return $result;
	}
		public function getparttypedetails(){
			return $this->executeQuery("SELECT id,name FROM `env_parttype` WHERE is_active=1","multiple");
		}
		public function getreqtypedetails(){
			return $this->executeQuery("SELECT id,req_name FROM `env_requesttype` WHERE is_active=1","multiple");
		}
		
		}
?>
