<?php
	
	require_once $docroot.'/class/data.php';
	class security extends data{
		//$months = 6;
	
		public function checkresetpassword($userid,$is_expiry=0){
				$result['cnt']=0;
				if($is_expiry ==1){
					$result =$this->checkpwdexpiry($userid);
				}else{
			$result =$this->executeQuery("SELECT COUNT(id)as cnt FROM `env_user` WHERE `is_reset_pwd` =1 AND id=".$userid,"single");
				}
			return $result;
		}
		public function checkpwdexpiry($userid,$notify=0){
			$expiryres['cnt'] =0;
			if($userid >0){
			$result=  $this->executeQuery("SELECT COUNT(id) as cnt,(SELECT `pwdexpiry` FROM `env_login_config` WHERE id=1) as pwdexpiry,(SELECT `pwdexpalert` FROM `env_login_config` WHERE id=1) as pwdexpalert ,`changepwddt` FROM `env_user` WHERE id =".$userid,"single");//print_r($result);exit;
					if($result['cnt'] == 1 && $result['pwdexpiry'] >0){
						$enddatetime =date("Y-m-d H:i:s");
						$days = $this->getnumberofdays($result['changepwddt'],$enddatetime);
						if($days > $result['pwdexpiry'] && $days >0){
							if($notify == 1){
									//show notification of  expiry date
								if(strtotime(date('Y-m-d')) == strtotime(date('Y-m-d', strtotime('-'.$result['pwdexpalert'].' days')))){
									$expiryres['cnt'] =1;
									$expiryres['pwdexpiry'] = $result['pwdexpiry'];
								}else{
									$expiryres['cnt'] =0;
									$expiryres['pwdexpiry'] = $result['pwdexpiry'];
								}
							}else{
								//current date is greater than expiry date
									if(strtotime(date('Y-m-d')) >= strtotime(date('Y-m-d', strtotime($days.' days')))){
										$expiryres['cnt'] =1;
										$expiryres['pwdexpiry'] = $result['pwdexpiry'];
									}else{
											$expiryres['cnt'] =0;
									}
							}
								
						}
					}else{
						$expiryres['cnt'] =0;
					}
			}
			
			return  $expiryres;
		}
		public function getnumberofdays($startdatetime,$enddatetime,$months=""){
			
			if($startdatetime !="" && $enddatetime !=""){
				$result =$this->timediff($startdatetime,$enddatetime);
				if(abs($result['day']) >0){
					return abs($result['day']);
				}else{
					return 0;
				}
			}else{
				return 0;
			}
			
		}
		public function resetpwddetails(){
			$result = "";
			$result =$this->executeQuery("SELECT `pwdexpiry`,`expiry_type`,`pwdexpalert` FROM `env_security` WHERE id=1","single");
			return $result;
		}
	}
	?>
