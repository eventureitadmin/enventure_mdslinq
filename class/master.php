<?php
	
	require_once $docroot.'/class/data.php';
	class master extends data{
	public function checkmodel($modelnum){
		$res = "";
		if($modelnum != ""){
		$res = $this->executeQuery("SELECT COUNT(id) as cnt,id,modelnum FROM env_model_details 
				WHERE modelnum='".$modelnum."'","single");
		}
		return $res;
	}
	public function checkcustomer($cust_code,$cust_name){
		$res = "";
		if($cust_code != "" && $cust_name != ""){
			
		$res = $this->executeQuery("SELECT COUNT(id) as cnt,id,cust_code FROM env_customer_details WHERE cust_code='".$cust_code."' AND cust_name="."'".$cust_name."'" ,"single");
		}
		return $res;
	}
	public function checksupplier($code){
		$res = "";
		if($code != ""){
		$res = $this->executeQuery("SELECT COUNT(id) as cnt,id,code FROM env_supplier WHERE code='".$code."'","single");
		}
		return $res;
	}
	public function savemodeldata($id,$data){
			$query = "";
			$result['label']=$data['modelnum'];
			$result['value']=0;
		
			if($data['modelnum'] !=""){
				
				if($id >0){
					$query="UPDATE env_model_details SET modelnum='".$data['modelnum']."' WHERE id='".$id."'";
					 $this->executeNonQuery($query);
					$result['value'] = $id;
				}else{
						 $query="INSERT INTO env_model_details(modelnum,created_date,created_by) VALUES('".$data['modelnum']."','".date("Y-m-d")."',".$_SESSION['partlinq_user']['ID'].")";
						$this->executeNonQuery($query);
					    $result['value'] = $this->lastId();
					
				}
			}
			return $result;
		}
	
		public function savecustomerdata($id,$data){
			$query = "";
			$result['label']=$data['cust_name']."(".$data['cust_code'].")";
			$result['value']=0;
			if($data['cust_code'] !="" && $data['cust_name'] !=""){
				if($id >0){
					$query="UPDATE env_customer_details SET cust_code='".$data['cust_code']."',cust_name='".$data['cust_name']."' WHERE id='".$id."'";
					$result = $this->executeNonQuery($query);
					$result['value'] = $id;
				}else{
						$query="INSERT INTO env_customer_details(cust_code,cust_name,created_date,created_by) VALUES('".$data['cust_code']."','".$data['cust_name']."','".date("Y-m-d")."',".$_SESSION['partlinq_user']['ID'].")";
					$this->executeNonQuery($query);
					$result['value'] = $this->lastId();
				}
			}
			return $result;
		}
		
	public function savesupplier($id,$data){
			$query = "";
			$result = "";
			$result['label']=$data['code'];
			$result['value']=0;
			if($data['code'] !=""){
				if($id >0){
					$query="UPDATE env_supplier SET code='".strtoupper($data['code'])."' WHERE id='".$id."'";
					 $this->executeNonQuery($query);
					$result['value'] = $id;
				}else{
						    $query="INSERT INTO env_supplier(code,created_date,isactive,created_by) VALUES('".$data['code']."','".date("Y-m-d")."','1',".$_SESSION['partlinq_user']['ID'].")";
					 $this->executeNonQuery($query);
				$result['value']= $this->lastId();
				}
			}
			return $result;
		}
		public function getmodeldetails($search){
			$res = "";
			$response = array();
			if($search != ""){
			    $query ="SELECT * FROM env_model_details  WHERE modelnum LIKE '%".$search."%' AND isactive=1";
				$res = $this->executeQuery($query,"multiple");
				if(count($res) >0){
					for($i=0;$i<count($res);$i++){
						$response[] = array("value"=>$res[$i]['id'],"label"=>$res[$i]['modelnum']);
					}
				}
			}
			return $response;
		}
			public function getrequestdetails($search){
			$res = "";
			$response = array();
			if($search != ""){
				 $query ="SELECT id,username FROM env_user  WHERE username LIKE '%".$search."%' AND usertype = 3 AND isactive=1";
				$res = $this->executeQuery($query,"multiple");
				if(count($res) >0){
					for($i=0;$i<count($res);$i++){
						$response[] = array("value"=>$res[$i]['id'],"label"=>$res[$i]['username']);
					}
				}
			}
			return $response;
		}
		public function getsupplierdetails($search){
			$res = "";
			$response = array();
			if($search != ""){
				 $query ="SELECT * FROM env_supplier  WHERE code LIKE '%".$search."%' AND isactive=1";
				$res = $this->executeQuery($query,"multiple");
				if(count($res) >0){
					for($i=0;$i<count($res);$i++){
						$response[] = array("value"=>$res[$i]['id'],"label"=>$res[$i]['code']);
					}
				}
			}
			return $response;
		}
		public function getcustomerdetails($search){
			$res = "";
			$response = array();
			if($search != ""){
				if($_SESSION['partlinq_user']['USERTYPE'] == 3){
					$query = " SELECT cd.`cust_code`,cd.`cust_name`,cd.`id` FROM `env_customer_details` cd,`env_requestor_alloc` as ra 
								WHERE cd.`id`= ra.customer_id  AND ra.`user_id`=".$_SESSION['partlinq_user']['ID']." 
								AND isactive=1 AND (cd.cust_code LIKE '%".$search."%'OR cd.cust_name LIKE '%".$search."%')";
				}else{
					$query =" SELECT * FROM env_customer_details  WHERE  isactive=1 AND (cust_code LIKE '%".$search."%'OR cust_name 
					LIKE '%".$search."%')";
				}
				$res = $this->executeQuery($query,"multiple");
				if(count($res) >0){
					for($i=0;$i<count($res);$i++){
						$response[] = array("value"=>$res[$i]['id'],"label"=>$res[$i]['cust_name']."( ".$res[$i]['cust_code']." )");
					}
				}
			}
			return $response;
		}
		public function getcustomerids($pid,$batchid){
			$res= "";
			if($pid > 0 && $batchid >0){
			$query ="SELECT u.sFld6,c.cust_name FROM `env_urlgrab` u,`env_customer_details` c 
			WHERE u.`iPrjID`=".$pid." AND u.`iBatch`=".$batchid." AND u.sFld6=c.id AND c.`isactive`=1
			GROUP BY c.`cust_name` ORDER BY c.cust_name ASC";
				$res =$this->executeQuery($query,"multiple");
			}
			return $res;
		}
		
	}
	?>
