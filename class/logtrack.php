<?php
	require_once $docroot.'/class/data.php';

	class logtrack extends data{
		
		function getUserBrowser() { 
		  $u_agent = $_SERVER['HTTP_USER_AGENT'];
		  $bname = 'Unknown';
		  $platform = 'Unknown';
		  $version= "";

		  //First get the platform?
		  if (preg_match('/linux/i', $u_agent)) {
			$platform = 'linux';
		  }elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
			$platform = 'mac';
		  }elseif (preg_match('/windows|win32/i', $u_agent)) {
			$platform = 'windows';
		  }

		  // Next get the name of the useragent yes seperately and for good reason
		  if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)){
			$bname = 'Internet Explorer';
			$ub = "MSIE";
		  }elseif(preg_match('/Firefox/i',$u_agent)){
			$bname = 'Mozilla Firefox';
			$ub = "Firefox";
		  }elseif(preg_match('/OPR/i',$u_agent)){
			$bname = 'Opera';
			$ub = "Opera";
		  }elseif(preg_match('/Chrome/i',$u_agent) && !preg_match('/Edge/i',$u_agent)){
			$bname = 'Google Chrome';
			$ub = "Chrome";
		  }elseif(preg_match('/Safari/i',$u_agent) && !preg_match('/Edge/i',$u_agent)){
			$bname = 'Apple Safari';
			$ub = "Safari";
		  }elseif(preg_match('/Netscape/i',$u_agent)){
			$bname = 'Netscape';
			$ub = "Netscape";
		  }elseif(preg_match('/Edge/i',$u_agent)){
			$bname = 'Edge';
			$ub = "Edge";
		  }elseif(preg_match('/Trident/i',$u_agent)){
			$bname = 'Internet Explorer';
			$ub = "MSIE";
		  }

		  // finally get the correct version number
		  $known = array('Version', $ub, 'other');
		  $pattern = '#(?<browser>' . join('|', $known) .
		')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
		  if (!preg_match_all($pattern, $u_agent, $matches)) {
			// we have no matching number just continue
		  }
		  // see how many we have
		  $i = count($matches['browser']);
		  if ($i != 1) {
			//we will have two since we are not using 'other' argument yet
			//see if version is before or after the name
			if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
				$version= $matches['version'][0];
			}else {
				$version= $matches['version'][1];
			}
		  }else {
			$version= $matches['version'][0];
		  }

		  // check if we have a number
		  if ($version==null || $version=="") {$version="?";}

		 /* return array(
			'userAgent' => $u_agent,
			'name'      => $bname,
			'version'   => $version,
			'platform'  => $platform,
			'pattern'    => $pattern
		  );*/
			return  $bname. " " . $version . " on " .$platform;
		}
		
    public function getIP() {
        $result = null;

        //for proxy servers
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $result = end(array_filter(array_map('trim', explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']))));
        }
        else {
            $result = $_SERVER['REMOTE_ADDR'];
        }

        return $result;
    }

    public function getReverseDNS() {
        return gethostbyaddr($this->getIP());
    }

    public function getCurrentURL() {
        return 'http'. (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 's': '') 
                . '://' . $_SERVER["SERVER_NAME"] 
                . ($_SERVER['SERVER_PORT'] != '80' ? $_SERVER['SERVER_PORT'] : '')
                . $_SERVER["REQUEST_URI"];
    }

    public function getRefererURL() {
        return (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
    }
		
	public function checkiplog($ip){
		$query = "SELECT id FROM env_login_iplog WHERE user_logdate='".date('Y-m-d')."' AND user_ip='".$ip."'";
		$logcnt = $this->numRows($query);
		if($logcnt==0){
			return true;
		}
		else{
			return false;
		}
	}
		
	public function insertiplog($data){
		$query = "INSERT INTO `env_login_iplog` (`id`, `user_ip`, `user_browser`, `user_logdate`, `user_logtime`, `user_refferer`, `user_page`) VALUES (NULL, '".$data['user_ip']."', '".$data['user_browser']."', '".$data['user_logdate']."', '".$data['user_logtime']."', '".$data['user_refferer']."', '".$data['user_page']."')";
		$this->executeNonQuery($query);
	}
		
	public function getloginconfig(){
		$query ="SELECT * FROM `env_login_config` WHERE `id`='1'";
		$result = $this->getRow($query);
		return $result;
	}
		
	public function insertloginlog($userid){
		$query="INSERT INTO `env_login_audit` (`id`, `userid`, `logdate`, `logintime`) VALUES (NULL, '".$userid."', '".date('Y-m-d')."', '".date('Y-m-d H:i:s')."')";
		$this->query($query);
		return $this->lastId();
	}

	public function updateloginlog($id){
		$query = "UPDATE env_login_audit SET `curtimestamp`='".time()."', `is_cur_session`='1' WHERE id='".$id."'";
		$this->query($query);
	}

	public function updatelogoutlog($logoutcond){
		$logouttime = date('Y-m-d H:i:s');
		$auditdet = $this->getauditdetbyid($_SESSION['partlinq_user']['AUDIT_ID']);
		$totalhrsary = $this->calcculatetothrs($auditdet['logintime'],$logouttime);
		$totalhrs = $totalhrsary['hours'].":".$totalhrsary['minutes'].":".$totalhrsary['seconds'];
		$query = "UPDATE env_login_audit SET `logouttime`='".$logouttime."', `totalhrs`='".$totalhrs."', `curtimestamp`='".time()."', `is_cur_session`='0',`isactive`='0'".$logoutcond." WHERE id='".$_SESSION['partlinq_user']['AUDIT_ID']."'";
		$this->query($query);
	}
		
	public function getuseralreadyloginornot($userid,$returncnt=false){
		if($returncnt){
			$query ="SELECT `id` FROM `env_login_audit` WHERE is_cur_session='1' AND `userid`='".$userid."'";
			$resultcnt = $this->numRows($query);
			return $resultcnt;
		}
		else{
			$query ="SELECT `id` FROM `env_login_audit` WHERE is_cur_session='1' AND `userid`='".$userid."' ORDER BY id DESC LIMIT 0,1";
			$result = $this->getRow($query);
			return $result;			
		}
	}
		
	public function getauditdetbyid($id){
		$query="SELECT * FROM env_login_audit WHERE id='".$id."'";
		$result = $this->getRow($query);
		return $result;	
	}
	
	public function updatefaillogin($id,$userid){
		$auditres = $this->getuserfaillogincnt($userid);
		if($auditres['cnt'] != ''){
			$faillogincnt = $auditres['cnt'] +1;
		}
		$query = "UPDATE env_login_audit SET `faillogincnt`='".$faillogincnt."' WHERE id='".$id."'";
		$this->query($query);		
	}
	
	public function getbalancefaillogins($userid){
		$login_config = $this->getloginconfig();
		$auditres = $this->getuserfaillogincnt($userid);
		return ($login_config['totfaillogins'] - $auditres['cnt']);
	}
	
	public function getuserfaillogincnt($userid){
		$query = "SELECT COUNT(id) AS cnt FROM env_login_audit WHERE isactive='1' AND userid='".$userid."' AND curtimestamp='' AND is_cur_session='0' AND faillogincnt > 0";
		$result = $this->getRow($query);
		return $result;
	}
		
	public function isuserlogoutbyadmin($id){
		$query ="SELECT `logout_type` FROM `env_login_audit` WHERE `logout_type`='2' AND `id`='".$id."'";
		$resultcnt = $this->numRows($query);
		if($resultcnt=='1'){
			return true;
		}
		else{
			return false;
		}	
	}
		
	public function isloginrecordnotfound($userid){
		$query ="SELECT id FROM `env_login_audit` WHERE `userid`='".$userid."' AND `is_cur_session`='1'";
		$resultcnt = $this->numRows($query);
		if($resultcnt=='1'){
			return true;
		}
		else{
			return false;
		}	
	}		
	
	public function updatecurrenttime($id){
		$query = "UPDATE env_login_audit SET `curtimestamp`='".time()."' WHERE id='".$id."'";
		$this->query($query);		
	}
}
?>
