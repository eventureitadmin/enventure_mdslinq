<?php

class db{
    // DB Connection Settings
    private $db_name    = DB_DATABASE;
    private $db_user    = DB_USERNAME;
    private $db_pass    = DB_PASSWORD;
    private $db_host    = DB_SERVER;
    private $db_charset = 'utf8';

    // Debug Settings
    public $debug          = true;
    public $display_errors = true;
    public $transactions   = false;
	public $log_queries    = false;

    // Class Settings
    private $link          = null;
    public $filter;
    public static $inst    = null;
	public $dateformat = "d-M-y";
    public $queries        = array();


    public function __construct(){
		
        $args = func_get_args();

        if (sizeof($args) > 0) {
            $this->link = new mysqli($args[0], $args[1], $args[2], $args[3]);
        } else {
            $this->link = new mysqli($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
        }
        if ($this->link->connect_errno) {
            $this->logDbErrors("Connect failed", $this->link->connect_error);
            exit("connect failed");
        }

        $this->link->set_charset($this->db_charset);
    }

    public function __destruct(){
        if ($this->link) {
            $this->disconnect();
        }
    }

    public function logQueries($query){
        $this->queries[] = $query;
        if ($this->debug === true) {
            $string = "[" . date("Y-m-d h:i:s A T") . "]" . "\t$query\n";
            // appends to file, creates if doesn't exist
            file_put_contents(ROOT_DIR."/logs/queries_".date('ymd').".log", $string, FILE_APPEND | LOCK_EX);
        }
    }
	
    public function logContent($content){
        if ($this->debug === true) {
            $string = "[" . date("Y-m-d h:i:s A T") . "]" . "\t$content\n";
            // appends to file, creates if doesn't exist
            file_put_contents(ROOT_DIR."/logs/content_".date('ymd').".log", $string, FILE_APPEND | LOCK_EX);
        }
    }	

    public function showProcedure($procedure, $object = false){
        if (empty($procedure)) {
            return false;
        }

        //Overwrite the $row var to null
        $row = null;

        $results = $this->link->query("SHOW CREATE PROCEDURE {$procedure}");

        if ($this->link->error) {
            $this->logDbErrors($this->link->error, $procedure);
            return false;
        } else {
            $row = array();
            while ($r = (!$object) ? $results->fetch_assoc() : $results->fetch_object()) {
                $row[] = $r;
            }
            return $row;
        }
    }

    public function callProcedure($procedure, $params = array(), $responses = array()){
        $sql = "CALL {$procedure}( ";

        $param_sql = array();

        foreach ($params as $field => $value) {
            if ($value === null) {
                $param_sql[] = "@{$field} := NULL";
            } else {
                if (is_numeric($value)) {
                    $param_sql[] = "@{$field} := {$value}";
                } else {
                    $param_sql[] = "@{$field} := '{$value}'";
                }
            }
        }

        foreach ($responses as $field) {
            $param_sql[] = "@{$field}";
        }

        $sql .= implode(', ', $param_sql);
        $sql .= ' )';

        $query = $this->query($sql);

        return $query;
    }

    public function showFunction($function, $object = false){
        if (empty($function)) {
            return false;
        }

        $results = $this->query("SHOW CREATE FUNCTION {$function}");

        $row = array();
        while ($r = (!$object) ? $results->fetch_assoc() : $results->fetch_object()) {
            $row[] = $r;
        }
        return $row;
    }

    public function logDbErrors($error, $query){
        if ($this->debug == true) {
            if ($this->display_errors) {
                $message = "<p>Error at ". date("Y-m-d H:i:s").":</p>";
                $message .= "<p>Query: ". htmlentities($query)."<br />";
                $message .= "Error: {$error}<br />";
                $message .= "Page: " . $_SERVER["REQUEST_URI"] ."<br />";
                $message .= "</p>";
				echo $message;
				exit();
            }
        }
    }

    public function filter($data){
        if (!is_array($data)) {
            $data = $this->link->real_escape_string($data);
            $data = trim(htmlentities($data, ENT_QUOTES, 'UTF-8', false));
        } else {
            //Self call function to sanitize array data
            $data = array_map(array( $this, 'filter' ), $data);
        }
        return $data;
    }

    public function escape($data){
        if (!is_array($data)) {
            $data = $this->link->real_escape_string($data);
        } else {
            //Self call function to sanitize array data
            $data = array_map(array( $this, 'escape' ), $data);
        }
        return $data;
    }
	
    public function clean($data){
        $data = stripslashes($data);
        $data = html_entity_decode($data, ENT_QUOTES, $this->db_charset);
        $data = nl2br($data);
        $data = urldecode($data);
        return $data;
    }

    public function dbCommon($value = ''){
        if (is_array($value)) {
            foreach ($value as $v) {
                if (preg_match('/AES_DECRYPT/i', $v) || preg_match('/AES_ENCRYPT/i', $v) || preg_match('/now()/i', $v) || preg_match('/NOW()/i', $v)) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            if (preg_match('/AES_DECRYPT/i', $value) || preg_match('/AES_ENCRYPT/i', $value) || preg_match('/now()/i', $value) || preg_match('/NOW()/i', $value)) {
                return true;
            }
        }
    }

    public function query($query){
		

		if($this->log_queries == true){
			$this->logQueries($query);
		}
        
        if ($this->transactions == true) {
            $this->link->query('START TRANSACTION;');
        }

        $full_query = $this->link->query($query);

        if ($this->transactions == true) {
            $this->link->query('COMMIT;');
        }

        if ($this->link->error) {
            $this->logDbErrors($this->link->error, $query);

            if ($this->transactions == true) {
                $this->link->query('ROLLBACK;');
            }

            return false;
        } else {
            return $full_query;
        }
    }

    public function tableExists($table){
        $check = $this->query("SELECT * FROM information_schema.tables WHERE table_schema = '{$this->db_name}' AND table_name = '{$table}' LIMIT 1;");

        if ($check !== false) {
            if ($check->num_rows > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function numRows($query){
        $query = $this->query($query);
        return $query->num_rows;
    }

    public function exists($table = '', $check_val = '', $params = array()){
        if (empty($table) || empty($check_val) || empty($params)) {
            return false;
        }

        $check = array();

        foreach ($params as $field => $value) {
            if (!empty($field) && !empty($value)) {
                //Check for frequently used mysql commands and prevent encapsulation of them
                if ($this->dbCommon($value)) {
                    $check[] = "`{$field}` = {$value}";
                } else {
                    $check[] = "`{$field}` = '{$value}'";
                }
            }
        }

        $check = implode(' AND ', $check);

        $rs_check = "SELECT {$check_val} FROM `{$table}` WHERE {$check}";
        $number = $this->numRows($rs_check);

        if ($number === 0) {
            return false;
        } else {
            return true;
        }
    }

    public function getArray($query, $type = MYSQLI_ASSOC){
        $row = $this->query($query);

        while ($q = $row->fetch_array($type)) {
            $r[] = $q;
        }

        return $r;
    }

    public function getRow($query, $object = false){
        $row = $this->query($query);
        $r = (!$object) ? $row->fetch_assoc() : $row->fetch_object();
        return $r;
    }

    public function getResultwithPosition($query, $pos = 0){
        $results = $this->query($query);
        $result = $results->fetch_array();
        return $result[$pos];
    }
	
    public function getResult($query){
        $results = $this->query($query);
        $result = $results->fetch_array();
		return $result;
    }	

    public function getResults($query, $object = false){
        $results = $this->query($query);		
        $row = array();
        while ($r = (!$object) ? $results->fetch_assoc() : $results->fetch_object()) {
            $row[] = $r;
        }

        return $row;
    }

    public function insert($table, $variables = array()){
        //Make sure the array isn't empty
        if (empty($variables)) {
            return false;
        }

        $variables = $this->filter($variables);

        $sql = "INSERT INTO {$table}";

        $fields = array();
        $values = array();

        foreach ($variables as $field => $value) {
            $fields[] = $field;
            if ($value === null) {
                $values[] = "NULL";
            } else {
                $values[] = "'{$value}'";
            }
        }

        $fields = " (`" . implode("`, `", $fields) . "`)";
        $values = "(". implode(", ", $values) .")";

        $sql .= $fields ." VALUES {$values};";

        $query = $this->query($sql);

        return $query;
    }

    public function insertMulti($table, $columns = array(), $records = array()){
        //Make sure the arrays aren't empty
        if (empty($columns) || empty($records)) {
            return false;
        }

        //Count the number of fields to ensure insertion statements do not exceed the same num
        $number_columns = count($columns);

        //Start a counter for the rows
        $added = 0;

        //Start the query
        $sql = "INSERT INTO {$table}";

        $fields = array();

        //Loop through the columns for insertion preparation
        foreach ($columns as $field) {
            $fields[] = "`{$field}`";
        }
        $fields = ' (`' . implode('`, `', $fields) . '`)';

        //Loop through the records to insert
        $values = array();

        $records = $this->filter($records);

        foreach ($records as $record) {
            //Only add a record if the values match the number of columns
            if (count($record) == $number_columns) {
                $values[] = "('" . implode("', '", array_values($record)) ."')";
                $added++;
            }
        }
        $values = implode(', ', $values);

        $sql .= $fields . " VALUES {$values}";

        $query = $this->query($sql);

        return $query;
    }

    public function search($table, $where = array(), $limit = null){
        if (empty($where)) {
            return false;
        }

        $where = $this->filter($where);

        $sql = "SELECT * FROM `{$table}`";

        //Add the $where clauses as needed
        if (!empty($where)) {
            foreach ($where as $field => $value) {
                $clause[] = "`{$field}` = '{$value}'";
            }
            $sql .= ' WHERE '. implode(' AND ', $clause);
        }

        if ($limit !== null) {
            $sql .= " LIMIT {$limit}";
        }

        return $this->getResults($sql);
    }
	
    public function update($table, $variables = array(), $where = array(), $limit = null){
        if (empty($variables)) {
            return false;
        }

        $variables = $this->filter($variables);

        $sql = "UPDATE {$table} SET ";
        foreach ($variables as $field => $value) {
            if ($value === null) {
                $updates[] = "`{$field}` = NULL";
            } else {
                $updates[] = "`{$field}` = '{$value}'";
            }
        }
        $sql .= implode(', ', $updates);

        //Add the $where clauses as needed
        if (!empty($where)) {
            foreach ($where as $field => $value) {
                $value = $value;

                $clause[] = "`{$field}` = '{$value}'";
            }
            $sql .= ' WHERE '. implode(' AND ', $clause);
        }

        if ($limit !== null) {
            $sql .= " LIMIT {$limit}";
        }

        $query = $this->query($sql);

        return $query;
    }

    public function upsert($table, $data = array(), $where = array()){
        //Make sure the args aren't empty
        if (empty($table) || empty($data) || empty($where)) {
            return false;
        }

        // Find if the row exists
        $find = $this->search($table, $where);

        // if the row exists, update, if not, insert
        if (empty($find)) {
            return $this->insert($table, $data);
        } else {
            return $this->update($table, $data, $where);
        }
    }

    public function delete($table, $where = array(), $limit = null){
        //Delete clauses require a where param, otherwise use "truncate"
        if (empty($where)) {
            return false;
        }

        $sql = "DELETE FROM `{$table}`";

        foreach ($where as $field => $value) {
            $value = $value;
            $clause[] = "`{$field}` = '{$value}'";
        }

        $sql .= " WHERE ". implode(' AND ', $clause);

        if ($limit !== null) {
            $sql .= " LIMIT {$limit}";
        }

        $query = $this->query($sql);

        return $query;
    }

    public function lastId(){
        return $this->link->insert_id;
    }

    public function affected(){
        return $this->link->affected_rows;
    }

    public function numFields($query){
        $query = $this->query($query);
        $fields = $query->field_count;
        return $fields;
    }

    public function showColumns($table){
        $query = $this->getResults("SHOW COLUMNS FROM `{$table}`;");
        return $query;
    }

    public function truncate($tables = array()){
        if (!empty($tables)) {
            $truncated = 0;
            foreach ($tables as $table) {
                $table = trim($table);
                $truncate = "TRUNCATE TABLE `{$table}`";
                $this->query($truncate);
                $truncated++;
            }
            return $truncated;
        }
    }

    public function optimize($tables = array()){
        if (!empty($tables)) {
            $optimized = 0;
            foreach ($tables as $table) {
                $table = trim($table);
                $optimize = "OPTIMIZE TABLE `{$table}`";
                $this->query($optimize);
                $optimized++;
            }
            return $optimized;
        }
    }

    public function lastQuery(){
        $last_query = array_values(array_slice($this->queries, -1))[0];
        return $last_query;
    }

    public static function getInstance(){
        if (self::$inst == null) {
            self::$inst = new DB();
        }
        return self::$inst;
    }

    public function disconnect(){
        $this->link->close();
    }
	
	public function to_bool($val){
	    return !!$val;
	}
	
	public function to_date($val){
	    return date('Y-m-d', $val);
	}
	
	public function to_time($val){
	    return date('H:i:s', $val);
	}
	
	public function to_datetime($val){
	    return date('Y-m-d H:i:s', strtotime($val));
	}

	public function dateconvert($date,$format){
	    return date($format, strtotime($date));
	}	
	
	public function current_datetime(){
	    return date('Y-m-d H:i:s');
	}

	public function getRandomString($length = 8) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$string = '';
		for ($i = 0; $i < $length; $i++) {
			$string .= $characters[mt_rand(0, strlen($characters) - 1)];
		}
		return $string;
	}
	
	public function cleanstring($string) {
 		 return preg_replace('/[^A-Za-z0-9]/', '', $string);
	}
	
	public function encode_arr($data) {
		return base64_encode(serialize($data));
	}

	public function decode_arr($data) {
		return unserialize(base64_decode($data));
	}
	
	public function add3dots($string, $repl, $limit) {
		return strlen($string) > $limit ? substr($string,0,$limit).$repl : $string;
	}
	public function encrypt_decrypt($action, $string) {
		$output = false;
		$encrypt_method = "AES128";
		$secret_key = 'pasaimds';

		$key = hash('sha256', $secret_key);
		if ( $action == 'encrypt' ) {
			$output = openssl_encrypt($string, $encrypt_method, $key);
			$output = base64_encode($output);
		} else if( $action == 'decrypt' ) {
			$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key);
		}
		return $output;
	}
	
	public function calcculatetothrs($logintime,$logouttime){
		$date1 = strtotime($logintime);
		$date2 = strtotime($logouttime);
		$diff = abs($date2 - $date1);
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));  
		$days = floor(($diff - $years * 365*60*60*24 -  $months*30*60*60*24)/ (60*60*24));
		$hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
		$minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);  
		$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));
		$data['years'] = $years;
		$data['months'] = $months;
		$data['days'] = $days;
		$data['hours'] = $hours;
		$data['minutes'] = $minutes;
		$data['seconds'] = $seconds;
		return $data;
	}
	public function getfuturedate($today,$num,$type){
		$date = date($this->dateformat,strtotime(date($this->dateformat, strtotime($today)) . " +".$num.$type));
		return $date;
	}
	public function getpreviousdate($today,$num,$type){
		$date = date($this->dateformat,strtotime(date($this->dateformat, strtotime($today)) . " -".$num.$type));
		return $date;
	}
	
    public function post_captcha($user_response) {
        $fields_string = '';
        $fields = array(
            'secret' => RECAPTCHA_SECRETKEY,
            'response' => $user_response
        );
        foreach($fields as $key=>$value)
        $fields_string .= $key . '=' . $value . '&';
        $fields_string = rtrim($fields_string, '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }
	
}

?>
