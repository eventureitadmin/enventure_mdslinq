<?php
	require_once $docroot.'/class/data.php';
	class admin extends data{
		public function getInputHeadersByProjectID($pid){
		 $query = "SELECT orderid,headername,is_auto_type FROM env_projectheaders WHERE batchid=".$pid." AND headertype=0 ORDER BY orderid";
		$result = $this->executeQuery($query,"multiple");
		return $result;
	}
	
	public function getUserList($value,$type){
		 if($_SESSION['partlinq_user']['USERTYPE']=='0'){
			 $query = "SELECT id,fullname FROM env_user WHERE FIND_IN_SET($value,$type) and isactive=1 ORDER BY fullname";
		}
		else{
			$query = "SELECT id,fullname FROM env_user WHERE  FIND_IN_SET($value,$type) and isactive=1 ORDER BY fullname";
		}
		$result = $this->executeQuery($query,"multiple");
		return $result;
	}
	public function getOutputHeadersByProjectID($pid){
		$query = "SELECT orderid,headername FROM env_projectheaders WHERE batchid=".$pid." AND headertype=1
		AND orderid <15 ORDER BY orderid";
		$result = $this->executeQuery($query,"multiple");
		return $result;
	}
		
	public function getprojectList(){
	 	if($_SESSION['partlinq_user']['USERTYPE']==5){
			$project_query = "SELECT *,(SELECT `projname` FROM `env_project` WHERE id=b.id) as projname FROM env_batch b WHERE b.project_id IN ( SELECT DISTINCT(iPrjID) 
			FROM env_urlgrab)
			 AND b.ID IN (SELECT DISTINCT(batchid)  FROM  env_projectheaders) AND b.ID IN ( SELECT DISTINCT(iPrjId) FROM env_userallocTL WHERE iUserid=".$_SESSION['partlinq_user']['ID'].") AND b.closed=0  ORDER BY b.ID";
		}
		else{
			$project_query = "SELECT b.id,b.project_id,b.batchno,(SELECT `projname` FROM `env_project` WHERE id=b.id) as projname FROM env_batch b WHERE b.project_id IN ( SELECT DISTINCT(iPrjID) FROM env_urlgrab )  AND ID IN (SELECT DISTINCT(batchid) FROM env_projectheaders ) AND b.closed=0 ORDER BY b.ID";  
		}	
		$project_result = $this->executeQuery($project_query,"multiple");
		return $project_result;
	}
	
	public function getsFldFields($pid,$fa='m'){
		$fields1 = '';
		$fields2 = '';
		$fields = array();
		$inputheaders = $this->getInputHeadersByProjectID($pid);
		$lastindex =0;
      	if(count($inputheaders)>0){
			for($k=0;$k<sizeof($inputheaders);$k++){
				$fields1 .= 'e.`sFld'.$k.'`,';
				$fields2 .= $fa.'.`sFld'.$k.'`,';
				$lastindex = $k;
			}
		}
		$outputheaders = $this->getOutputHeadersByProjectID($pid);
		if(count($outputheaders)>0){
			for($k1=($lastindex+1);$k1<(sizeof($outputheaders)+$lastindex);$k1++){
				 $fields1 .= 'e.`sFld'.$k1.'`,';
				$fields2 .= $fa.'.`sFld'.$k1.'`,';
			}
		}
		$fields1 .= 'e.`partstatus`,';
		$fields[0] = $fields1;
		$fields[1] = $fields2;	
		return $fields;
	}
		
	public function getTeamleaderFields($pid){
		$fields1 = '';
		$fields2 = '';
		$fields = array();		
		$fields1 = 'IFNULL((SELECT t1.iUserid FROM env_userallocTL t1 WHERE t1.sEntryType=5 AND t1.iPartID=e.ID),\'\') as TLuserid,';
		$fields2 = 'IFNULL((SELECT t2.fullname FROM env_user t2 WHERE t2.id=m.TLuserid),\'\') as TLuser,';
		$fields[0] = $fields1;
		$fields[1] = $fields2;
		return $fields;
	}
	public function getPartDetailsByID($id){
		$query = "SELECT * FROM env_urlgrab WHERE ID='".$id."'";
		$result = $this->executeQuery($query,"single");
		return $result;
	}
		
	public function getUsernameFields(){
		$fields1 = '';
		$fields2 = '';
		$fields = array();
		//$fields1 .= 'IFNULL((SELECT t1.iUserid FROM env_useralloc t1 WHERE t1.sEntryType=2  AND t1.iPartID=e.ID),\'\') as userid,';
		$fields2 .= 'IFNULL((SELECT t2.fullname FROM env_user t2 WHERE t2.id=m.userid),\'\') as username,';
		$fields[0] = $fields1;
		$fields[1] = $fields2;	
		
		return $fields;
	}
	public function getPartsAllocationQuery($data,$count=false){
		$query = '';
		$custfieldindex= $this->getcustfieldindex($_SESSION['partlinq_user']['BATCHID']);
		$reqfieldindex= $this->getreqfieldindex($_SESSION['partlinq_user']['BATCHID']);
		$selquery =$this->getautocompletequery($data['pid'],'m').",";
	    $sFldfields = $this->getsFldFields($data['pid']);
		$usernamefields = $this->getUsernameFields($data['pid']);
		$query .= 'SELECT  n.* FROM (';
		$query .= 'SELECT  m.`ID`,m.partstatus';
		$query .=$selquery;
		
		$query .= $usernamefields[1];
		$query .= substr($sFldfields[1],0,-1);
		$query .= ' FROM (SELECT e.`ID`,e.userid,';
		
		
		$query .= $usernamefields[0];
		$query .= substr($sFldfields[0],0,-1);
		$query .= '	FROM env_urlgrab e 
					WHERE 
					 e.iBatch=\''.$data['pid'].'\' AND
				sFld'.$custfieldindex.' IN (SELECT DISTINCT(customer_id) FROM `env_requestor_alloc` WHERE user_id=e.sFld'.$reqfieldindex.')';
	
		$query .= ' AND  e.is_delete =0 ORDER BY e.ID DESC) m ORDER BY m.ID DESC';
		$query .= ") n  ";
		$query .= "WHERE 1=1 ORDER BY n.ID DESC";
		$query .= $data['cond']." ";
		if(!$count){	
		$query .= ' LIMIT '.$data['start'].','.$data['limit'];
		}//echo $query;exit;
		return $query;
	}
		public function getautocompletequery($batch,$field){
			$query ="";
			if($batch >0){
				$fieldsarr = $this->getfieldsorder($batch,1);
				if(count($fieldsarr) >0){
					for($j=0;$j<count($fieldsarr);$j++){
						if($fieldsarr[$j]['is_auto_type'] == 1){
						 	$query .=", (SELECT CONCAT(`cust_name`,' ( ',`cust_code`,' )')  FROM `env_customer_details` WHERE id =".$field.".sFld".$fieldsarr[$j]['oid']." AND `isactive`=1)as cname";
						}else if($fieldsarr[$j]['is_auto_type'] == 3){
							$query .=" ,(SELECT `modelnum` FROM `env_model_details` WHERE id =".$field.".sFld".$fieldsarr[$j]['oid']." AND `isactive`=1) as modelname";
						}else if($fieldsarr[$j]['is_auto_type'] == 2){
							$query .=", (SELECT `code` FROM `env_supplier` WHERE id =".$field.".sFld".$fieldsarr[$j]['oid']." AND `isactive`=1)as  supplcode";
						}else if($fieldsarr[$j]['is_auto_type'] == 4){
							$query .=", (SELECT `username` FROM `env_user` WHERE id =".$field.".sFld".$fieldsarr[$j]['oid']." AND `isactive`=1)as  reqname";
						}
						
					}
				}
			}
			return $query;
		}
		public function getfieldsorder($batch,$is_auto=0,$auto_type =0){
			$res="";
			if($batch >0){
				$query = "SELECT id,(`orderid`-1) oid,`req_form_order`,`req_field_label`,`is_auto_type` FROM `env_projectheaders` WHERE batchid=".$batch."  AND `req_form_order` >0 ";
				if($is_auto == 1){
					$query .= " AND is_auto =1 ";
				}
				if($auto_type >0){
					$query .= " AND is_auto_type = ".$auto_type;
				}
				$query .= " ORDER BY `req_form_order` ASC";
			$res = $this->executeQuery($query,"multiple");
			}
			
			return $res;
		}
		public function getcustfieldindex($batch){
			$reqfieldindex = $this->executeQuery("SELECT (orderid -1)as oid FROM `env_projectheaders` WHERE `is_auto_type`=1 AND batchid=".$batch,"single")[0];
			return $reqfieldindex;
		}
			public function getreqfieldindex($batch){
			$reqfieldindex = $this->executeQuery("SELECT (orderid -1)as oid FROM `env_projectheaders` WHERE `is_auto_type`=4 AND batchid=".$batch,"single")[0];
			return $reqfieldindex;
		}
	}
	?>
