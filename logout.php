<?php
include_once("top.php");
if($_SESSION['partlinq_user']['ID']!=''){
	$logoutcond ='';
	if($_GET['t']=='1'){
		$logoutcond = ",`is_idlelogout`='1',`logout_type`='1'";
	}	
	if(ENABLE_SECURITY){
		if($_SESSION['partlinq_user']['AUDIT_ID'] != '' && $_SESSION['partlinq_user']['AUDIT_ID'] > 0){
			$logtrack->updatelogoutlog($logoutcond);
		}
	}
		$_SESSION['partlinq_user']['ID']='';
		unset($_SESSION['partlinq_user']);
		session_destroy();
		header("Location:login.php");
		exit();	
}
else{
		header("Location:login.php");
		exit();		
}
?>