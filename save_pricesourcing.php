<?php 
    include_once("top.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{
		
        if(isset($_POST['data'])){
			//print_r($_POST['data']);exit;
			$statusindex = $dbase->getfieldindex($_GET['batch'],2)[0]+2;
			$pendingreqarr = $dbase->getstatus($_GET['batch'],1,1,1);
	  	    $new_brand_eam_exists = false;
            $json_string = $_POST['data'];
            $my_array_data = json_decode($json_string, TRUE);
	    	$is_data_saved_error  = false;
            $error_message = "";
			$projheaders = $dbase->getprojectheaders($_GET['batch']);
			$headers = array_column($projheaders,"orderid");
			
			$ihfield = $dbase->executeQuery("SELECT (MAX(`orderid`)-1)field FROM `env_projectheaders` WHERE `batchid`=".$_GET['batch']." AND `headertype`=0","single")['field'];
			 for($k=0;$k<count($my_array_data);$k++)
             {
                $buildqry="";
				$p = 0;
				$partid = $my_array_data[$k][0];

				for($m=2;$m<sizeof($my_array_data[$k]);$m++)
                {
					if(array_key_exists($p,$headers) && $p>$ihfield && $p != ($dbase->getheaderbytype($_GET['batch'],9)['orderid']-1) && ($projheaders[$p]['itypeid'] != 11 || $projheaders[$p]['itypeid'] != 9)) { 
						$patterns = array("/\s+/", "/\s([?.!])/");
						$replacer = array(" ","$1");
					 	 $str = preg_replace( $patterns, $replacer, trim($my_array_data[$k][$m],", ;") );
						
						//customer comment
						$custcmtorder = $dbase->getheaderbytype($_GET['batch'],5);
						$custcmtorder = $custcmtorder['orderid']-1;
						if($p == $custcmtorder){
							 $cust_comment = addslashes($str);
						 }
						 $buildqry .='sFld'.($p).'="'.addslashes($str).'",';
					
					}
					$p++;
				}
				
				if(in_array($my_array_data[$k][$statusindex],$pendingreqarr)){
					 $buildqry .='partstatus="0",';
				}else{
					$buildqry .='partstatus="1",';
				}
				 $buildqry .='updated_by="'.$_SESSION['partlinq_user']['ID'].'",';
				if($my_array_data[$k][1] == "true" && ($my_array_data[$k][$statusindex] == $reqcreatorarr[0] || $my_array_data[$k][$statusindex] == "")){
					$buildqry .='is_delete="1",';
				}
				 $whr = " `ID`=".$partid." AND `iPrjID` =".$_GET['pid']." AND `iBatch` =".$_GET['batch'];
				 $partsres = $dbase->getdetails("env_urlgrab"," COUNT(ID) as cnt",$whr,"single");
				 if($partsres['cnt'] > 0){
				$resultstatus = $dbase->updatedata($_GET['pid'],$_GET['batch'],$_SESSION['partlinq_user']['ID'],$partid,$buildqry,$cust_comment,$custcmtorder);
				 }else{
				 	$resultstatus =  $dbase->insertdata($_GET['pid'],$_GET['batch'],$_SESSION['partlinq_user']['ID'],$buildqry,$cust_comment,$custcmtorder);
				 }
			
				 if(!$resultstatus){
					 $error_message =" Add/update  data error ";
					 $is_data_saved_error = true;
				}
				
			 }//echo $buildqry;
            if(empty($error_message) && (!$is_data_saved_error )){
				echo  $jsondata = '{ "msg":"success","error_message":""}';
			}else{
	            echo $jsondata = '{ "msg":"failure","error_message":'.$error_message.'}' ;
	        }

        }//post
    }
   
	
exit;

?>

