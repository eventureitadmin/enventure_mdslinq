<?php
include_once("top.php");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo TOOL_TITLE; ?></title>
		<?php $checkedval = false;?>
		<script>
		var isChecked = '<?php echo $checkedval ?>';
		var customerid = '<?php echo $_SESSION['partlinq_user']['CUSTOMERID'] ?>';
		</script>
		<!-- jQuery -->
		<script type="text/javascript" src="js/jquery.js"></script>
		<!-- jQueryUI -->
		<script type="text/javascript" src="js/jqueryui.js"></script>
		<!-- Bootstrap Core JavaScript -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.datepicker.min.js"></script>

		<script type="text/javascript" src="js/user/jquery.toaster.js"></script>
		<script type="text/javascript" src="js/jquery.blockUI.js"></script>
		<script type="text/javascript" src="js/jquery_validate.js"></script>
		<script type="text/javascript" src="js/user/chosen.jquery.min.js"></script>
		<script type="text/javascript" src="js/user/bootbox.min.js"></script>
		<script type="text/javascript" src="js/user/shortcut.js"></script>
		<script type="text/javascript" src="js/common.js"></script>
		<script type="text/javascript" src="js/sol.js"></script>
		<script type="text/javascript" src="js/notify.js"></script>
		<script type="text/javascript" src="js/bootstrap-progressbar-manager.js"></script>
		
		<script type="text/javascript" src="js/user/checksession.js"></script>
		<script type="text/javascript" src="js/handsontable.full.js"></script>
		<script type="text/javascript" src="js/filtercommonfunctions.js"></script>
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/jqueryui.css" rel="stylesheet" type="text/css">
		<link href="css/bootstrap.datepicker.min.css" rel="stylesheet" type="text/css">
		<link href="css/startmin.css" rel="stylesheet" type="text/css">
		<link href="css/chosen.css" rel="stylesheet" type="text/css">
		<link href="css/custom.css" rel="stylesheet" type="text/css">
		<link href="css/handsontable.full.css" rel="stylesheet" type="text/css">
		<link href="css/sol.css" rel="stylesheet" type="text/css">
		<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<?php require_once("Label.php");?>
		<?php require_once("popuppage.php"); ?>
		<?php require_once("popupdata.php"); ?>
		<?php require_once("changepwdjs.php"); ?>
		

