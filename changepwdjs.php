<script>
	function showchangepwd(){
		$("#changepwdModal").modal("show");
	}
	function changepwdsubmit(){
		var oldpassword = $("#oldpassword").val();
		var newpassword = $("#newpassword").val();
		var isvaliddata = false;
		if(oldpassword == "" || oldpassword == null){
			$("#oldpwderr").html("<span style='color:red'>This field is required</span>");
		}else{
			$("#oldpwderr").html("");
			
			if(newpassword == "" || newpassword == null){
				$("#newpwderr").html("<span style='color:red'>This field is required</span>");
			}else{
				$("#newpwderr").html("");
				isvaliddata = validatePassword(newpassword,pass_pattern);
			}
		}
		
		
  
		if(oldpassword != "" && oldpassword != null && newpassword != "" && newpassword != null && isvaliddata){
				$.ajax({
							url: "changepassword.php",
							data: $("#changepswform").serialize(),     
							type: 'POST',
							dataType:'json',
							success: function (res) {
								if(res.msg == "success"){
									 showsavmsg("green",res.message);
									 $("#changepwdModal").modal("hide");
									document.getElementById("changepswform").reset();
								}else{
								 	showsavmsg("red",res.message);
									
									
								}
							},
							error: function (res) {
							 showsavmsg("red","Unable to change password.Please try again.");
							}
				 });			
		}
	}
	function showsavmsg(color,msg){
		$.unblockUI();
		if(color == "green"){
			$.toaster({priority : 'success',title : 'Success',message : msg});
		}else{
			$.toaster({priority : 'danger',title : 'Error',message :msg});
		}
		
		
	}
	function validatePassword(newpassword,pass_pattern){
		var oldpassword = $("#oldpassword").val();var isvalidpass = false;
		if(oldpassword !== newpassword){
				    if(!/[A-Z]/.test(newpassword)) {
						isvalidpass = false;
					} else if (!/[a-z]/.test(newpassword)) {
						isvalidpass = false;
					} else if (!/[0-9]/.test(newpassword)) {
						isvalidpass = false;
					} else if (!/[\d=!\-@._*]/.test(newpassword)){
						isvalidpass = false;
					}else if(newpassword.length < 8){
						isvalidpass = false;
					}else{
						isvalidpass = true;
					}
					if(!isvalidpass){
				  		 showsavmsg("red","Password contains at least one number, one lowercase character and one special character and one uppercase letter with min 8 characters");
				    }else{
						if(newpassword.length < 8){
							 showsavmsg("red","Please enter atleast 8 characters");
						}else{
							return true;
						}
					}
		}else{
			 showsavmsg("red","New and old password must not same");
		 }
	}
	$(document).ready(function(){
		$('#changepwdModal').on('shown.bs.modal', function () {
			$("#oldpassword").focus();
		});
		$('#changepwdModal').on('hidden.bs.modal', function () {
			document.getElementById("changepswform").reset();
			$("#newpwderr").html("");
			$("#oldpwderr").html("");
		});
	});

</script>
