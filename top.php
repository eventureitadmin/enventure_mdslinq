<?php
include_once("config.php");
include_once("dbconn.php");
include_once($docroot."/class/partssourcing.php");
include_once($docroot."/class/master.php");
include_once($docroot."/class/admin.php");
include_once($docroot."/class/security.php");
include_once($docroot."/class/logtrack.php");
$dbase = new partssourcing();
$master = new master();
$admin = new admin();
$security = new security();
$logtrack = new logtrack();
if(ENABLE_SECURITY){
	require_once 'usertracking.php';
}
require_once 'Classes/PHPExcel.php';
require_once 'Classes/xlsxwriter.class.php';
require_once 'Classes/PHPExcel/IOFactory.php';
$excel_type_arr = array('application/excel','application/x-msexcel','application/vnd.ms-excel','application/vnd.msexcel',
						'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/kset','application/wps-office.xlsx',
						'application/wps-office.xls');

//Project import header array
$proj_header = array("header","input/output","orderid","mandatory","readonly","user_edit","client_edit","requestor_edit","completed_req_edit","typeid","formulatype","formula","status_values","request_form_order","request_field_label");
$proj_header1 = array("header","input/output","orderid","user_edit","typeid");
//Project import header array
$unrpt_header = array("customer","panasonic_model","part_number","description","manufacturer_name","manufacturer_part_number","contact_person","email_id","notes");
$unrpt_headerarr = array("Customer","Panasonic Model","Part Number","Description","Manufacturer Name","Manufacturer Name","Manufacturer Part Number","Contact Person","Email Id","Updates / Notes");
$password_pattern = "/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/";
define("PASSWORDREG",$password_pattern);
$fieldarr= array("1","2","3","4","5","6","7","11","12","14");
//Request form field index
$reqarr['rd'] =6;
$reqarr['s'] =12;
$reqarr['ps'] =13;
$reqarr['rqu'] =1;
$reqarr['mn'] =5;
$reqarr['cn'] =2;
$reqarr['sc'] =3;
$reqarr['cpn'] =4;
$reqarr['cad'] =7;
$reqarr['sqadt'] =8;
$reqarr['cmptdate'] =9;
$reqarr['weight'] =14;
$reqcreatorarr =array("WIP","Open");
$usertype_array = array("0"=>"Administrator","1"=>"Compliance User","2"=>"Client Admin","3"=>"Requestor","4"=>"PL Reviewer","5"=>"Compliance TLUser","6"=>"Service Part PL Reviewer");

define("REQUESTED_DATE", 1);
define("STATUS", 2);
define("PASA_STATUS",3);
define("SQA_TARGET_DATE", 4);
define("COMPLIANCE_DATE", 5);
define("CUST_DUE_DATE", 6);
define("HIDDEN_NUM", 2);

define("CUSTOMER",1);
define("SUPPLIER_CODE", 2);
define("MODEL_NUMBER", 3);
define("REQUESTOR", 4);
define("SQA_TARGET_DATE_WEEK", 2);
define("COMPLIANCE_DATE_WEEK", 2);
define("CUST_DUE_DATE_TYPE", 10);
define("CUST_PARTNUM_TYPE", 3);
define("WEIGHT", 12);
?>
