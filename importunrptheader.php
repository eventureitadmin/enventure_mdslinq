<?php
	include_once("header.php");
	if($_SESSION['partlinq_user']['ID']==''){
		header("Location:login.php");
	}
	else{
if($_FILES){
		if($_POST){
			$batchid = $_POST['batchid'];
		}	
		if($batchid == ''){
			header("Location: importunrptheader.php?msg=5");
			exit();		
		}	
		if(isset($_FILES['proj_file'])){		
		 // $filepath = $docroot."/upload/partsourcing/header/";
		  $filepath = $docroot."/upload/unreportedparts/";
		  $errors= array();
		  $file_name = $_FILES['proj_file']['name'];
		  $file_size =$_FILES['proj_file']['size'];
		  $file_tmp =$_FILES['proj_file']['tmp_name'];
		  $file_type=$_FILES['proj_file']['type'];
		  $file_ext=strtolower(end(explode('.',$_FILES['proj_file']['name'])));
		
		  $extensions= array("xls","xlsx");
		  
		  if(in_array($file_ext,$extensions)=== false){
			 $errors[]="extension not allowed, please choose a xls or xlsx file.";
		  }
		  
		  if(empty($errors)==true){		  
			 move_uploaded_file($file_tmp,$filepath.$file_name);
			try {
				$inputFileName = $filepath.$file_name;
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME). '": '.$e->getMessage());
			}
			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objPHPExcel->getActiveSheet()->setShowGridlines(true);
			$timestamps=time();
			$highestRow = $objWorksheet->getHighestRow();
			$highestColumn = $objWorksheet->getHighestColumn(); 
			$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); 
			$i=0;
			$j=0;
			$fileheader = array();
			for ($col = 0; $col < $highestColumnIndex; ++$col) {
				if(trim($objWorksheet->getCellByColumnAndRow($col, 1)->getValue()) !=""){
				$fileheader[] = strtolower(str_replace(' ', '_',trim($objWorksheet->getCellByColumnAndRow($col, 1)->getValue())));
				}
			}
			//echo"<pre>";print_r($proj_header); echo"<pre>";print_r($fileheader);exit;
		if($proj_header1 !== $fileheader){
				header("Location: importunrptheader.php?msg=4");
				exit();				
		  }	
			for ($row = 2; $row <= $highestRow; ++$row) {
				if($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()!=''){
				$col0=$dbase->escape(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue())));
				$col1=$dbase->escape(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue())));
				$col2=$dbase->escape(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue())));
				$col3=$dbase->escape(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue())));
				$col4=$dbase->escape(str_replace('"',"'",trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue())));
				$i++;
						//insert Abbr
						$abbr_insert="INSERT INTO env_unrptheaders SET `batchid`='".$batchid."', `headername`='".$col0."', `headertype`='".$col1."',`orderid`='".$col2."', `is_user_edit`='".$col3."', `itypeid`='".$col4."', `created_date`='".date("Y-m-d")."'";
						if(!$dbase->executeNonQuery($abbr_insert)){
							echo $i."-";
							echo $abbr_insert;
							echo "<br/>";
							$j++;
							exit;
						}        
				}
			}	
		
			header("Location: importunrptheader.php?msg=1&i=".$i."&j=".$j);
			exit();
		  }
		  else{
			 //print_r($errors);
				header("Location: importunrptheader.php?msg=2");
				exit();			 
		  }
	   }
	   else{
		header("Location: importunrptheader.php?msg=3");
		exit();	   
	   }   
}		
	?>
    <body>
		<?php include("menu.php"); ?>
		<link href="../css/jquery_confirm.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="../js/jquery_validate.js"></script>
		<script type="text/javascript" src="../js/jquery_confirm.js"></script>
            <div id="page-wrapper" style="padding:40px 5px 0 5px">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="">Import Unreported Headers</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
								<?php include_once("message.php"); ?>
								<?php if($_GET['msg']==1){ echo show_success_msg('Header file imported successfully'); } ?>
								<?php if($_GET['msg']==2){ echo show_error_msg('File upload error'); } ?>
								<?php if($_GET['msg']==3){ echo show_error_msg('No file is added for import. Please upload a file'); } ?>
								<?php if($_GET['msg']==4){ echo show_error_msg('Import file header mismatching. Please change it and upload again.'); } ?>
								<?php if($_GET['msg']==5){ echo show_error_msg('Please select Batch.'); } ?>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <form role="form" id="frm_header" name="frm_header" action="" method="post" enctype="multipart/form-data">
                                           <div class="form-group" >
											    <label>Project Name: </label>
                                                <input type="hidden" id="batchid" name="batchid" class="form-control" value="<?php echo $_SESSION['partlinq_user']['BATCHID']; ?>">
                                                	<?php
														$batch_query = "SELECT b.`id`, (SELECT p.projname FROM env_project p WHERE p.ID=b.`project_id`) as projname,CONCAT('Batch ','',b.`batchno`) AS batchno FROM `env_batch` b WHERE `closed`='0' AND id=".$_SESSION['partlinq_user']['BATCHID'];
														$batch_result = $dbase->executeQuery($batch_query,"single");
														echo $batch_result['projname'];
													?>
                                               <!---- <label>Select Batch</label>
                                                <select id="batchid" name="batchid" class="form-control required">
                                                    <option value="">-Select-</option>--->
													<?php
														/*$batch_query = "SELECT b.`id`, (SELECT p.projname FROM env_project p WHERE p.ID=b.`project_id`) as projname,CONCAT('Batch ','',b.`batchno`) AS batchno FROM `env_batch` b WHERE `closed`='0'";
														$batch_result = $dbase->executeQuery($batch_query,"multiple");
														for($i=0;$i<count($batch_result);$i++){
															if($batch_result[$i]['id']==$batchid){
																$select = 'selected="selected"';
															}
															else{
																$select = '';
															}
															echo '<option value="'.$batch_result[$i]['id'].'" '.$select.'>'.$batch_result[$i]['projname'].' - '.$batch_result[$i]['batchno'].'</option>';
														}*/
													?>
                                               <!------------ </select>--->	
                                            </div>								
                                             <div class="form-group">
                                                <label>Upload Header File</label>
                                                <input type="file" id="proj_file" name="proj_file" class="required">
                                            </div>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </form>
                                    </div>
									<div class="col-lg-8">
								<div class="row">
									<p class="form-control-static" style="margin-left: 20px;">Note : Please check the header of the import file in the following format. If it is mismatch it wont allow to insert / update.</p>
								<?php
								$html = '';
								$html = '<style>
							table#projheadertable {
								border-collapse: collapse;
								margin-left: 20px;
							}

							table#projheadertable td, table#projheadertable th {
								border: 1px solid black;
								 padding: 10px; 
								 font-size:12px;
							}
							</style>';
								$html .= '<table id="projheadertable"><tr>';
								foreach($proj_header as $key =>$value){
									if($key == 10){
										$html .="</tr><tr>";
									}
									$html .='<td><b>'.ucfirst(str_replace('_', ' ',trim($value))).'</b></td>';
								}
								$html .= '</tr></table>';
								echo $html;
								?>				
								</div>										
									</div>
                                </div>
							
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>	
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->
		<script type="text/javascript">
		$().ready(function() {
			$("#frm_header").validate();
			$("#proj_file").change(function () {
				var fileExtension = ["xls", "xlsx"];
				if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
				$.alert({
					title: 'Alert',
					content: 'Only formats are allowed : '+fileExtension.join(', '),
					//icon: 'fa fa-rocket',
					animation: 'scale',
					closeAnimation: 'scale',
					buttons: {
						okay: {
							text: 'Ok',
							btnClass: 'btn-blue'
						}
					}					
				});							
					$("#proj_file").val("");
				}
			});			
		});
		</script>			
	</body>

	<?php
		 include_once("footer.php");
	}	
?>
