<script>var pass_pattern = <?php echo PASSWORDREG; ?> </script>
<div class="modal fade" id="changepwdModal" tabindex="-1" role="dialog" aria-labelledby="changepwdModalLabel" aria-hidden="true">
	 <div class="modal-dialog">
		<div class="modal-content">
			  <div class="modal-header" style="background-color:#286090;color:#fff;padding:6px;">
				<button type="button" class="close" style="color:white; opacity:1;font-size:25px;" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Change Password</h4>
			  </div>
			  <div class="modal-body">
				  <form class="form-horizontal" id='changepswform' method='post'>
					  <div class="form-group">
							<label class="control-label col-sm-3" for="pwd">Old Password<span style="color:red"> * </span></label>
							<div class="col-sm-5">
							  <input type="password" name="oldpassword" id="oldpassword" class="form-control" autocomplete="off" />
							</div>
							<label class="col-sm-4" id="oldpwderr"></label>
					  </div>
					   <div class="form-group">
							<label class="control-label col-sm-3" for="pwd">New Password <span style="color:red"> * </span></label>
							<div class="col-sm-5">
								<input type="password" name="newpassword" id="newpassword" value=""  class="form-control"  autocomplete="off"/>
							</div>
							<label class="col-sm-4" id="newpwderr"></label>
					  </div>
				  </form>
			 </div>
			  <div class="modal-footer">
				  <input type="button" name="changepwd" id="changepwd" class="btn btn-primary" onclick="changepwdsubmit()" value="Save"/>
				  <button type="button"  class="btn btn-danger" data-dismiss="modal">Close</button>
			 </div>
		 </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
		